package urlpath_test

import (
	"testing"

	"bitbucket.org/ide8io/ide8/backend/urlpath"
	"github.com/stretchr/testify/assert"
)

func TestSplit2Empty(t *testing.T) {
	a, b := urlpath.Split2NoError("")
	assert.Equal(t, "", a)
	assert.Equal(t, "", b)
}

func TestSplit2OnlySlash(t *testing.T) {
	a, b := urlpath.Split2NoError("/")
	assert.Equal(t, "", a)
	assert.Equal(t, "", b)
}

func TestSplit2EndingSlash(t *testing.T) {
	a, b := urlpath.Split2NoError("bla/")
	assert.Equal(t, "bla", a)
	assert.Equal(t, "", b)
}

func TestSplit2EndingDualSlash(t *testing.T) {
	a, b := urlpath.Split2NoError("bla//")
	assert.Equal(t, "bla", a)
	assert.Equal(t, "/", b)
}
