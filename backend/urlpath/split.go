package urlpath

import "strings"

func Split2NoError(in string) (string, string) {
	arr := strings.SplitN(in, "/", 2)
	second := ""
	if len(arr) >= 2 {
		second = arr[1]
	}
	return arr[0], second
}
