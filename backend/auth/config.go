package auth

import (
	"bitbucket.org/ide8io/ide8/backend/util"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sync"
)

type config struct {
	CookieKey     []byte `json:"cookiekey"`
	PWHashPrefix  string `json:"pwhashprefix"`
	PWHashPostfix string `json:"pwhashpostfix"`
	GitHub        struct {
		ClientId     string `json:"clientid"`
		ClientSecret string `json:"clientsecret"`
	} `json:"github"`
	BitBucket struct {
		ClientId     string `json:"clientid"`
		ClientSecret string `json:"clientsecret"`
	} `json:"bitbucket"`
	DiscourseSSO struct {
		Site   string `json:"site"`
		Secret string `json:"secret"`
	} `json:"discourse_sso"`
}

var confmu = sync.Mutex{}

var configfile string

func bootstrapAuth(conf *config) bool {
	touched := false
	if len(conf.CookieKey) == 0 {
		var err error
		conf.CookieKey, err = util.GenerateRandom(32)
		if err != nil {
			log.Fatal("Failed to generate initial cookie key!")
		}
		touched = true
	}
	if len(conf.PWHashPrefix) == 0 {
		var err error
		conf.PWHashPrefix, err = util.GenerateRandomString(4)
		if err != nil {
			log.Fatal("Failed to generate password has prefix!")
		}
		touched = true
	}
	if len(conf.PWHashPostfix) == 0 {
		var err error
		conf.PWHashPostfix, err = util.GenerateRandomString(4)
		if err != nil {
			log.Fatal("Failed to generate password has postfix!")
		}
		touched = true
	}
	if touched {
		writeConfig(*conf)
		return true
	}
	return false
}

func Init(datadir string) bool {
	configfile = filepath.Join(datadir, "ide8_auth_config.json")
	confmu.Lock()
	defer confmu.Unlock()
	conf, err := readConfig()
	if err != nil {
		log.Fatalf("Failed to read config! " + err.Error())
	}
	bootstrapped := bootstrapAuth(&conf)
	initKeys(conf.CookieKey, conf.PWHashPrefix, conf.PWHashPostfix)
	githubInit(conf.GitHub.ClientId, conf.GitHub.ClientSecret)
	bitbucketInit(conf.BitBucket.ClientId, conf.BitBucket.ClientSecret)
	discourseSSOInit(conf.DiscourseSSO.Site, conf.DiscourseSSO.Secret)
	return bootstrapped
}

func readConfig() (config, error) {
	if _, err := os.Stat(configfile); os.IsNotExist(err) {
		return config{}, nil
	}

	data, err := ioutil.ReadFile(configfile)
	if err != nil {
		return config{}, err
	}
	conf := config{}
	err = json.Unmarshal(data, &conf)
	if err != nil {
		return config{}, err
	}

	return conf, nil
}

func writeConfig(conf config) error {
	data, err := json.MarshalIndent(conf, "", "  ")
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(configfile, data, 0600)
	if err != nil {
		return err
	}
	return nil
}
