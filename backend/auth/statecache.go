package auth

import (
	"bitbucket.org/ide8io/ide8/backend/util"
	"golang.org/x/oauth2"
	"sync"
	"time"
)

type UserAuthInfo struct {
	Source      string
	ID          int64
	UserName    string
	FullName    string
	Email       string
	AvatarURL   string
	AccessToken *oauth2.Token
}

type StateHandler interface {
	GetURL(replyURL string, state string) string
	GetUserInfo(code string) (UserAuthInfo, error)
}

type stateItem struct {
	expires time.Time
	handler StateHandler
}

var stateCache = map[string]stateItem{}
var stateCacheMu sync.Mutex

// Create a new state and add it to cache with one hour lifetime.
// Also purges any outdated entries.
func CreateState(handler StateHandler, replyURL string, now time.Time) (string, error) {
	state, err := util.GenerateRandomString(32)
	if err != nil {
		return "", err
	}
	stateCacheMu.Lock()
	defer stateCacheMu.Unlock()
	purgeStates := []string{}
	for k := range stateCache {
		if stateCache[k].expires.After(now) {
			purgeStates = append(purgeStates, k)
		}
	}
	for _, k := range purgeStates {
		delete(stateCache, k)
	}
	stateCache[state] = stateItem{now.Add(time.Hour), handler}
	return handler.GetURL(replyURL, state), nil
}

// Validate if state is in cache and remove it if found
func PopState(state string) StateHandler {
	stateCacheMu.Lock()
	defer stateCacheMu.Unlock()
	item, ok := stateCache[state]
	if !ok {
		return nil
	}
	delete(stateCache, state)
	if time.Now().After(item.expires) {
		return nil
	}
	return item.handler
}
