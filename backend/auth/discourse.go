package auth

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"net/url"
	"strconv"
)

var discourseSSOSite string
var discourseSSOSecret string

func discourseSSOInit(site string, secret string) {
	discourseSSOSite = site
	discourseSSOSecret = secret
}

func SetDiscourseSSOConfig(site string, secret string) error {
	discourseSSOInit(site, secret)
	confmu.Lock()
	defer confmu.Unlock()
	conf, err := readConfig()
	if err != nil {
		return err
	}
	conf.DiscourseSSO.Site = site
	conf.DiscourseSSO.Secret = secret
	return writeConfig(conf)

}

func GetDiscourseSite() string {
	return discourseSSOSite
}

func ValidateDiscourseSSO(sso string, sig string) (string, error) {
	h := hmac.New(sha256.New, []byte(discourseSSOSecret))
	_, err := h.Write([]byte(sso))
	if err != nil {
		return "", err
	}
	sig2 := hex.EncodeToString(h.Sum(nil))
	if sig != sig2 {
		return "", fmt.Errorf("invalid sso")
	}

	qs, err := base64.StdEncoding.DecodeString(sso)
	if err != nil {
		return "", err
	}
	v, err := url.ParseQuery(string(qs))
	if err != nil {
		return "", err
	}

	return v.Get("nonce"), nil
}

func GetDiscourseSSOReplyURL(nonce string, id int, username string, email string, fullname string, avatarURL string) (string, error) {
	v := make(url.Values)
	v.Set("nonce", string(nonce))
	v.Set("email", email)
	v.Set("external_id", strconv.Itoa(id))
	v.Set("username", username)
	v.Set("name", fullname)
	v.Set("avatar_url", avatarURL)

	p := base64.StdEncoding.EncodeToString([]byte(v.Encode()))

	h := hmac.New(sha256.New, []byte(discourseSSOSecret))
	_, err := h.Write([]byte(p))
	if err != nil {
		return "", err
	}
	sig := hex.EncodeToString(h.Sum(nil))

	return discourseSSOSite + "/session/sso_login?sso=" + p + "&sig=" + sig, nil
}
