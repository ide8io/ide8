package auth

import (
	"fmt"
	"sync"
	"time"
)

type userEntry struct {
	ui     UserAuthInfo
	expire time.Time
}

var userCache = map[string]userEntry{}
var userCacheMu sync.Mutex

// Add oauth user info to cache with one hour lifetime.
// Also purges any outdated entries.
func AddUserToCache(ui UserAuthInfo, now time.Time) {
	userCacheMu.Lock()
	defer userCacheMu.Unlock()
	purgeTokens := []string{}
	for k, entry := range userCache {
		if entry.expire.After(now) {
			purgeTokens = append(purgeTokens, k)
		}
	}
	for _, k := range purgeTokens {
		delete(userCache, k)
	}
	userCache[ui.UserName] = userEntry{ui, now.Add(time.Hour)}
}

// Get oauth user info given by key
func GetUserFromCache(key string) (UserAuthInfo, error) {
	userCacheMu.Lock()
	defer userCacheMu.Unlock()
	entry, ok := userCache[key]
	if !ok {
		return UserAuthInfo{}, fmt.Errorf("no user info found")
	}
	if time.Now().After(entry.expire) {
		delete(userCache, key)
		return UserAuthInfo{}, fmt.Errorf("user info expired")
	}
	return entry.ui, nil
}

// Delete oauth user info given by key
func DeleteUserInCache(key string) {
	userCacheMu.Lock()
	defer userCacheMu.Unlock()
	delete(userCache, key)
}
