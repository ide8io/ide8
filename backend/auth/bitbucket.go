package auth

import (
	"github.com/tpiha/go-bitbucket/bitbucket"
	"golang.org/x/oauth2"
	bitbucketoauth "golang.org/x/oauth2/bitbucket"
	"log"
	"sync"
)

var bitbucketOAuthConf = oauth2.Config{
	Endpoint: bitbucketoauth.Endpoint,
	Scopes:   []string{"account", "email"},
}

var bitbucketOAuthConfMu = sync.Mutex{}

func bitbucketInit(id string, secret string) {
	bitbucketOAuthConfMu.Lock()
	bitbucketOAuthConf.ClientID = id
	bitbucketOAuthConf.ClientSecret = secret
	bitbucketOAuthConfMu.Unlock()
}

func SetBitbucketClient(id string, secret string) error {
	bitbucketInit(id, secret)
	confmu.Lock()
	defer confmu.Unlock()
	conf, err := readConfig()
	if err != nil {
		return err
	}
	conf.BitBucket.ClientId = id
	conf.BitBucket.ClientSecret = secret
	return writeConfig(conf)
}

type bitbucketHandler struct {
}

func (h bitbucketHandler) GetURL(replyURL string, state string) string {
	bitbucketOAuthConfMu.Lock()
	defer bitbucketOAuthConfMu.Unlock()
	bitbucketOAuthConf.RedirectURL = replyURL
	return bitbucketOAuthConf.AuthCodeURL(state, oauth2.AccessTypeOnline)
}

func (h bitbucketHandler) GetUserInfo(code string) (UserAuthInfo, error) {
	bitbucketOAuthConfMu.Lock()
	defer bitbucketOAuthConfMu.Unlock()
	token, err := bitbucketOAuthConf.Exchange(oauth2.NoContext, code)
	if err != nil {
		return UserAuthInfo{}, err
	}

	oauthClient := bitbucketOAuthConf.Client(oauth2.NoContext, token)
	client := bitbucket.NewClient(oauthClient)
	user, _, err := client.Users.Current()
	if err != nil {
		return UserAuthInfo{}, err
	}
	log.Println("bitbucket user data:", user)
	emails, _, err := client.Users.ListEmails(nil)
	if err != nil {
		return UserAuthInfo{}, err
	}
	email := ""
	if len(emails.Emails) > 0 {
		// TODO: check for primary email?
		email = emails.Emails[0].Email
	}
	return UserAuthInfo{Source: "bitbucket", ID: -1, UserName: user.Username, FullName: user.DisplayName, Email: email, AvatarURL: user.Avatar, AccessToken: token}, nil
}

func CreateBitBucketHandler() StateHandler {
	return bitbucketHandler{}
}
