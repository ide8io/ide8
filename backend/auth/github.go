package auth

import (
	"github.com/google/go-github/github"
	"golang.org/x/oauth2"
	githuboauth "golang.org/x/oauth2/github"
	"log"
	"sync"
)

var githubOAuthConf = oauth2.Config{
	Endpoint: githuboauth.Endpoint,
	Scopes:   []string{"user:email", "repo"},
}

var githubOAuthConfMu = sync.Mutex{}

func githubInit(id string, secret string) {
	githubOAuthConfMu.Lock()
	githubOAuthConf.ClientID = id
	githubOAuthConf.ClientSecret = secret
	githubOAuthConfMu.Unlock()
}

func SetGitHubClient(id string, secret string) error {
	githubInit(id, secret)
	confmu.Lock()
	defer confmu.Unlock()
	conf, err := readConfig()
	if err != nil {
		return err
	}
	conf.GitHub.ClientId = id
	conf.GitHub.ClientSecret = secret
	return writeConfig(conf)
}

type githubHandler struct {
}

func (h githubHandler) GetURL(replyURL string, state string) string {
	githubOAuthConfMu.Lock()
	defer githubOAuthConfMu.Unlock()
	githubOAuthConf.RedirectURL = replyURL
	return githubOAuthConf.AuthCodeURL(state, oauth2.AccessTypeOnline)
}

func (h githubHandler) GetUserInfo(code string) (UserAuthInfo, error) {
	githubOAuthConfMu.Lock()
	defer githubOAuthConfMu.Unlock()
	token, err := githubOAuthConf.Exchange(oauth2.NoContext, code)
	if err != nil {
		return UserAuthInfo{}, err
	}

	oauthClient := githubOAuthConf.Client(oauth2.NoContext, token)
	client := github.NewClient(oauthClient)
	user, _, err := client.Users.Get(oauth2.NoContext, "")
	if err != nil {
		return UserAuthInfo{}, err
	}
	log.Println("github user data:", *user)
	ui := UserAuthInfo{Source: "github", ID: -1, AccessToken: token}
	if user.ID != nil {
		ui.ID = *user.ID
	}
	if user.Login != nil {
		ui.UserName = *user.Login
	}
	if user.Email != nil {
		ui.Email = *user.Email
	}
	if user.Name != nil {
		ui.FullName = *user.Name
	}
	if user.AvatarURL != nil {
		ui.AvatarURL = *user.AvatarURL
	}
	return ui, nil
}

func CreateGitHubHandler() StateHandler {
	return githubHandler{}
}
