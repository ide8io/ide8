package auth

import (
	"crypto/sha256"
	"encoding/base64"
	"io"
)

var cookiekey []byte
var pwhashprefix string
var pwhashpostfix string

func initKeys(ck []byte, pwpre string, pwpost string) {
	cookiekey = ck
	pwhashprefix = pwpre
	pwhashpostfix = pwpost
}

func GetCookieKey() []byte {
	return cookiekey
}

func HashPW(username string, password string) string {
	h := sha256.New()
	io.WriteString(h, pwhashprefix)
	io.WriteString(h, username)
	io.WriteString(h, pwhashpostfix)
	io.WriteString(h, password)
	return base64.URLEncoding.EncodeToString(h.Sum(nil))
}
