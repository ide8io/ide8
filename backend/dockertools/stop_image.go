package dockertools

import (
	"os/exec"
	"strings"
)

func StopImage(name string) error {
	out, err := exec.Command("docker", "ps", "-a", "-q", "--filter", "ancestor="+name).Output()
	if err != nil {
		return err
	}
	if len(out) == 0 {
		return nil
	}

	args := []string{"kill"}
	args = append(args, strings.Split(string(out), "\n")...)
	err = exec.Command("docker", args...).Run()
	if err != nil {
		return err
	}
	return nil
}
