package connect

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"time"

	"bitbucket.org/ide8io/ide8/backend/gizmo"
	"bitbucket.org/ide8io/ide8/backend/user"
	"bitbucket.org/ide8io/ide8/backend/util"
	"github.com/boltdb/bolt"
	"github.com/gorilla/websocket"
)

type message struct {
	Type    string      `json:"type"`
	Id      string      `json:"id"`
	Payload interface{} `json:"payload"`
}

type connectPayload struct {
	Name     string             `json:"name"`
	UserName string             `json:"username"`
	Gizmos   []gizmo.NamedGizmo `json:"gizmos"`
}

type tunnelPayload struct {
	Gizmo  string `json:"gizmo"`
	Target string `json:"target"`
	Plugin string `json:"plugin"`
}

type sessionItem struct {
	Name    string    `json:"name"`
	Secret  string    `json:"secret"`
	Created time.Time `json:"created"`
}

type Session struct {
	ws     *websocket.Conn
	mu     sync.Mutex
	gizmos map[string]gizmo.Gizmo
	client gizmo.AgentClient
}

var gizmostore gizmo.Store

func Init(datadir string, gstore gizmo.Store) {
	// TODO: Remove this when deployed in production
	dbfile := filepath.Join(datadir, "ide8_connect_sessions.db")
	if _, err := os.Stat(dbfile); !os.IsNotExist(err) {
		db, err := bolt.Open(dbfile, 0600, &bolt.Options{Timeout: 1 * time.Second})
		if err != nil {
			log.Fatal("connect: Init:", err)
		}
		sesss := []sessionItem{}
		err = db.View(func(tx *bolt.Tx) error {
			b := tx.Bucket([]byte("sessions"))
			b.ForEach(func(k, v []byte) error {
				var item sessionItem
				err := json.Unmarshal(v, &item)
				if err != nil {
					return err
				}
				sesss = append(sesss, item)
				return nil
			})
			return nil
		})
		if err != nil {
			log.Fatal("connect: Init:", err)
		}
		for _, item := range sesss {
			err := gstore.StoreOldSession(item.Name, item.Secret, item.Created)
			if err != nil {
				log.Println("connect.Init:", err)
			}
		}
		db.Close()
		os.Remove(dbfile)
	}

	gizmostore = gstore
}

func createSession(name string, secret string, username string, ver string, os string, raddr string) (*Session, error) {
	s := &Session{gizmos: map[string]gizmo.Gizmo{}}
	var err error
	s.client, err = gizmostore.AgentClient(name, gizmo.Agent{
		Secret:     secret,
		Owner:      username,
		Version:    ver,
		OS:         os,
		RemoteAddr: raddr,
	})
	if err != nil {
		return nil, err
	}
	go s.listen()
	return s, nil
}

func (s *Session) SetWS(ws *websocket.Conn) {
	s.ws = ws
}

func (s *Session) writeMessage(data []byte) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.ws == nil {
		return fmt.Errorf("no websocket assigned to session %s", s.client.Session().ID)
	}
	s.ws.SetWriteDeadline(time.Now().Add(time.Second * 10))
	return s.ws.WriteMessage(websocket.TextMessage, data)
}

func (s *Session) Destroy() {
	if s.client != nil {
		s.client.Close()
		s.client = nil
	}
}

func (s *Session) tunnel(gizmo string, plugin string, target string) {
	t := ""
	if target != "" {
		log.Println("connect: Initiating tunnel for gizmo", gizmo, "to", target)
		t = "/_tunnel/" + target + "/" + s.client.Session().ID + "/" + gizmo
	} else {
		log.Println("connect: Stopping any tunnel for", gizmo)
	}
	msg := message{Type: "tunnel", Payload: tunnelPayload{Gizmo: gizmo, Target: t}}
	b, err := json.Marshal(msg)
	if err != nil {
		log.Println("connect: tunnel:", err)
		return
	}
	err = s.writeMessage(b)
	if err != nil {
		log.Println("connect: tunnel WriteMessage:", err)
	}
}

func (s *Session) handleConnect(payload json.RawMessage) error {
	var con connectPayload
	err := json.Unmarshal(payload, &con)
	if err != nil {
		return err
	}

	gizmos := map[string]gizmo.Gizmo{}
	for _, g := range con.Gizmos {
		gizmos[g.ID] = g.Gizmo
	}
	s.gizmos = gizmos
	return s.client.Update(gizmos)
}

func (s *Session) handleAdd(payload json.RawMessage) error {
	var g gizmo.NamedGizmo
	err := json.Unmarshal(payload, &g)
	if err != nil {
		return err
	}

	s.gizmos[g.ID] = g.Gizmo
	return s.client.Update(s.gizmos)
}

func (s *Session) handleRemove(payload json.RawMessage) error {
	var g gizmo.NamedGizmo
	err := json.Unmarshal(payload, &g)
	if err != nil {
		return err
	}

	delete(s.gizmos, g.ID)
	return s.client.Update(s.gizmos)
}

func (s *Session) listen() {
	updateCh := s.client.UpdateCh()
	for {
		u, ok := <-updateCh
		if !ok {
			return
		}
		log.Println("connect tunnel:", u)
		for id, res := range u {
			s.tunnel(id, res.Plugin, res.Target)
		}
	}
}

func List() ([]gizmo.AgentInfo, error) {
	return gizmostore.ListAgents("", 50)
}

func DeleteAgent(agentid string) error {
	return gizmostore.DeleteAgent(agentid)
}

var upgrader = websocket.Upgrader{}

func ServeConnect(w http.ResponseWriter, r *http.Request) {
	ver := r.Header.Get("ide8agent-version")
	os := r.Header.Get("ide8agent-os")
	name := r.Header.Get("ide8agent-name")
	secret := r.Header.Get("ide8agent-secret")
	username := r.Header.Get("ide8agent-username")

	log.Println("connect: new connection from", util.GetRemoteAddr(r), "with ide8agent version", ver, "on", os)

	if ver != "" {
		gv, err := util.GitVerParse(ver)
		if err != nil {
			log.Println("connect: Error parsing gitver from agent:", err)
		} else if gv.IsOlderThan(util.GitVer{Major: 0, Minor: 5, Patch: 7}) {
			http.Error(w, "Upgrade!", http.StatusUpgradeRequired)
			return
		}
	}

	if username != "" && !user.IsValid(username) {
		log.Println("connect: username not valid:", username)
		http.Error(w, "Invalid user: "+username, http.StatusUnauthorized)
		return
	}

	sess, err := createSession(name, secret, username, ver, os, util.GetRemoteAddr(r))
	if err != nil {
		log.Println("connect: getOrCreateSession:", err)
		http.Error(w, "session error", http.StatusInternalServerError)
		return
	}
	respHdr := http.Header{}
	if secret == "" {
		respHdr.Set("ide8agent-name", sess.client.Session().ID)
		respHdr.Set("ide8agent-secret", sess.client.Session().Secret)
	} else if sess.client.Session().Secret != secret {
		log.Println("connect: session secret mismatch:", name, ver, os, util.GetRemoteAddr(r))
		http.Error(w, "unauthorized", http.StatusUnauthorized)
		return
	}

	ws, err := upgrader.Upgrade(w, r, respHdr)
	if err != nil {
		http.Error(w, "websocket upgrade failure: "+err.Error(), http.StatusInternalServerError)
		return
	}
	defer ws.Close()
	sess.SetWS(ws)

	// Client pings every minute, so set read deadline for every ping in order
	// to close connection on missing network. Uses client ping instead of sending
	// ping in both directions.
	orgph := ws.PingHandler()
	ws.SetPingHandler(func(appData string) error {
		ws.SetReadDeadline(time.Now().Add(time.Minute + time.Second*10))
		return orgph(appData)
	})

	ws.SetReadDeadline(time.Now().Add(time.Minute + time.Second*10))
	for {
		_, b, err := ws.ReadMessage()
		if err != nil {
			log.Println("connect: ReadMessage:", err)
			break
		}
		log.Println("connect:", string(b))
		var payload json.RawMessage
		msg := message{Payload: &payload}
		err = json.Unmarshal(b, &msg)
		if err != nil {
			log.Println("connect: Unmarshal:", err)
			continue
		}

		if msg.Type == "connect" {
			err = sess.handleConnect(payload)
		} else if msg.Type == "add" {
			err = sess.handleAdd(payload)
		} else if msg.Type == "remove" {
			err = sess.handleRemove(payload)
		} else {
			log.Println("connect: Unknown message type", msg.Type)
		}
		if err != nil {
			log.Println("connect: handle", msg.Type, err)
		}
	}

	log.Println("connect: closed connection from", util.GetRemoteAddr(r))
	sess.Destroy()
}
