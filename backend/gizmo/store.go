package gizmo

import (
	"time"

	"github.com/oklog/ulid"
)

type Gizmo struct {
	Class  string `json:"class"`
	If     string `json:"if,omitempty"`
	Dev    string `json:"dev,omitempty"`
	Serial string `json:"serial,omitempty"`
	VID    string `json:"vid,omitempty"`
	PID    string `json:"pid,omitempty"`
}

type NamedGizmo struct {
	ID string `json:"id"`
	Gizmo
}

type GizmoInfo struct {
	NamedGizmo
	Assignment
	Status string `json:"status"`
}

type Agent struct {
	Secret     string `json:"secret"`
	Owner      string `json:"owner"`
	Version    string `json:"version"`
	OS         string `json:"os"`
	RemoteAddr string `json:"raddr"`
}

type AgentInfo struct {
	ID         string    `json:"id"`
	Online     bool      `json:"online"`
	Owner      string    `json:"owner"`
	Version    string    `json:"version"`
	OS         string    `json:"os"`
	RemoteAddr string    `json:"raddr"`
	Created    time.Time `json:"created"`
	Last       time.Time `json:"last"`
}

type Session struct {
	ID     string    // Unique Agent ID. Can change from original to create uniqueness.
	Secret string    // Agent secret to create matched ID/Secret pair for authentication.
	Key    ulid.ULID // Session key to make sure only latest agent session is allowed.
}

type AgentClient interface {
	Close()
	Session() Session
	UpdateCh() <-chan map[string]Assignment
	Update(gizmos map[string]Gizmo) error
	//Add(gizmos []*Gizmo)
	//Remove(gizmoids []string)
}

type ResourcesClient interface {
	Close()
	UpdateCh() <-chan map[string]Resource
	Target(target string) error
}

type Assignment struct {
	Plugin string `json:"plugin"`
	Target string `json:"target"`
}

const (
	// StatusUnknown Unknown resource status
	StatusUnknown = "unknown"
	// StatusNotFound Resource not found
	StatusNotFound = "not found"
	// StatusDisconnected Resource disconnected from agent (unplugged)
	StatusDisconnected = "disconnected"
	// StatusOffline Resource agent offline (agent not running/network disconnect)
	StatusOffline = "offline"
	// StatusError Agent failed to set up resource tunnel
	StatusError = "error"
	// StatusOnline Resource connected
	StatusOnline = "online"
)

type Resource struct {
	Class        string                       `json:"class"`
	Name         string                       `json:"name"`
	Status       string                       `json:"status"`
	Capabilities map[string]map[string]string `json:"capabilities"`
}

type Store interface {
	ListAgents(previd string, count int) ([]AgentInfo, error)
	ListAgentGizmos(agentid string, previd string, count int) ([]GizmoInfo, error)
	AgentClient(agentid string, agent Agent) (AgentClient, error)
	DeleteAgent(agentid string) error
	ResourcesClient(resources []string) (ResourcesClient, error)
	Destroy()

	// TODO: Remove once deployed to production
	StoreOldSession(agentid string, secret string, created time.Time) error
}
