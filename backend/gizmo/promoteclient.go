package gizmo

type Promotion struct {
	Class        string                       `json:"class,omitempty"`
	Name         string                       `json:"name,omitempty"`
	Capabilities map[string]map[string]string `json:"capabilities,omitempty"`
	Plugin       string                       `json:"plugin,omitempty"`
}

type PromoteUpdate struct {
	AgentID string
	Owner   string
	Gizmos  map[string]Gizmo
}

type PromoteClient interface {
	Close()
	UpdateCh() <-chan PromoteUpdate
	Promote(agentid string, proms map[string]Promotion) error
}

type PromoteStore interface {
	PromoteClient() (PromoteClient, error)
	Destroy()
}
