package impl_test

import (
	"testing"
	"time"

	"bitbucket.org/ide8io/ide8/backend/gizmo"
	"bitbucket.org/ide8io/ide8/backend/gizmo/impl"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPromoteBadly(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
		pc, err := store.PromoteClient()
		require.Nil(t, err)
		err = pc.Promote("agentid", map[string]gizmo.Promotion{})
		assert.NotNil(t, err)
		pc.Close()
		select {
		case _, ok := <-pc.UpdateCh():
			assert.False(t, ok)
		case <-time.After(time.Second):
			require.Fail(t, "promote update on added gizmo missing")
		}
	})
}

func TestPromoteGizmo(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
		ac, err := store.AgentClient("agentid", gizmo.Agent{})
		require.Nil(t, err)
		pc, err := store.PromoteClient()
		require.Nil(t, err)
		err = ac.Update(map[string]gizmo.Gizmo{
			"gizmo1": gizmo.Gizmo{},
		})
		assert.Nil(t, err)
		select {
		case u := <-pc.UpdateCh():
			assert.Equal(t, "agentid", u.AgentID)
			assert.Equal(t, 1, len(u.Gizmos))
			_, ok := u.Gizmos["gizmo1"]
			require.True(t, ok)
		case <-time.After(time.Second):
			require.Fail(t, "promote update on added gizmo missing")
		}
		err = pc.Promote("agentid", map[string]gizmo.Promotion{
			"gizmo1": gizmo.Promotion{
				Class: "promclass",
				Capabilities: map[string]map[string]string{
					"newcap": nil,
				},
				Plugin: "plugin",
			},
		})
		assert.Nil(t, err)
		select {
		case u := <-ac.UpdateCh():
			assert.Equal(t, 1, len(u))
			g, ok := u["gizmo1"]
			require.True(t, ok)
			assert.Equal(t, "plugin", g.Plugin)
		case <-time.After(time.Second):
			require.Fail(t, "agent update on promotion missing")
		}
		pc.Close()
	})
}
