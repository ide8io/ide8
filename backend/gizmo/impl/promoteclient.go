package impl

import (
	"encoding/json"
	"fmt"
	"log"

	"bitbucket.org/ide8io/ide8/backend/gizmo"
	"github.com/boltdb/bolt"
)

type promoteClient struct {
	store    *GizmoStore
	updateCh chan gizmo.PromoteUpdate
}

func newPromoteClient(s *GizmoStore) *promoteClient {
	p := &promoteClient{
		store:    s,
		updateCh: make(chan gizmo.PromoteUpdate),
	}
	go p.refresh()
	return p
}

func (c *promoteClient) refresh() {
	var puu []gizmo.PromoteUpdate
	err := c.store.db.View(func(tx *bolt.Tx) error {
		aa := tx.Bucket(agentsBucketName)
		if aa == nil {
			return nil
		}
		return aa.ForEach(func(ak, _ []byte) error {
			a := aa.Bucket(ak)
			if a == nil {
				return nil
			}
			m := a.Get(metaKey)
			if m == nil {
				return nil
			}
			var meta agentMeta
			err := json.Unmarshal(m, &meta)
			if err != nil {
				return err
			}
			rr := a.Bucket(resourcesBucketName)
			if rr == nil {
				return nil
			}
			gg := map[string]gizmo.Gizmo{}
			err = rr.ForEach(func(k, v []byte) error {
				var data resourceData
				err := json.Unmarshal(v, &data)
				if err != nil {
					return err
				}
				gg[string(k)] = data.Gizmo
				return nil
			})
			if err != nil {
				return nil
			}
			if len(gg) > 0 {
				puu = append(puu, gizmo.PromoteUpdate{
					AgentID: string(ak),
					Owner:   meta.Owner,
					Gizmos:  gg,
				})
			}
			return nil
		})
	})
	if err != nil {
		log.Println("promoteclient:", err)
	}
	for _, pu := range puu {
		c.updateCh <- pu
	}
}

func (c *promoteClient) Close() {
	c.store.promosMu.Lock()
	defer c.store.promosMu.Unlock()
	for i, p := range c.store.promos {
		if p == c {
			c.store.promos = append(c.store.promos[:i], c.store.promos[i+1:]...)
			c.destroy()
			return
		}
	}
}

func (c *promoteClient) destroy() {
	close(c.updateCh)
}

func (c *promoteClient) UpdateCh() <-chan gizmo.PromoteUpdate {
	return c.updateCh
}

func (c *promoteClient) Promote(agentid string, proms map[string]gizmo.Promotion) error {
	update := map[string]gizmo.Assignment{}
	err := c.store.updateAgentResources(agentid, func(rr *bolt.Bucket) error {
		for id, p := range proms {
			bid := []byte(id)
			b := rr.Get(bid)
			if b == nil {
				return fmt.Errorf("no such resource %v", id)
			}
			var data resourceData
			err := json.Unmarshal(b, &data)
			if err != nil {
				return err
			}
			data.Promotion = p
			b, err = json.Marshal(data)
			if err != nil {
				return err
			}
			err = rr.Put(bid, b)
			if err != nil {
				return err
			}
			update[id] = gizmo.Assignment{
				Plugin: data.Promotion.Plugin,
				Target: data.Target,
			}
		}
		return nil
	})
	go func() {
		c.store.agentsMu.Lock()
		defer c.store.agentsMu.Unlock()
		for _, a := range c.store.agents {
			if a.session.ID == agentid {
				a.updateCh <- update
				return
			}
		}
	}()
	return err
}
