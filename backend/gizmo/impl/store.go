package impl

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	"bitbucket.org/ide8io/ide8/backend/boltpaging"
	"bitbucket.org/ide8io/ide8/backend/event"
	"bitbucket.org/ide8io/ide8/backend/event/message"
	"bitbucket.org/ide8io/ide8/backend/gizmo"
	"bitbucket.org/ide8io/ide8/backend/util"
	"github.com/boltdb/bolt"
	"github.com/oklog/ulid"
)

type GizmoStore struct {
	db        *bolt.DB
	publisher event.Publisher
	agents    map[[16]byte]*agentClient
	agentsMu  sync.Mutex
	clients   map[string][]*resourcesClient
	clientsMu sync.Mutex
	promos    []*promoteClient
	promosMu  sync.Mutex
}

func NewGizmoStore(dbfilename string, publisher event.Publisher) *GizmoStore {
	s := &GizmoStore{
		publisher: publisher,
		agents:    make(map[[16]byte]*agentClient),
		clients:   make(map[string][]*resourcesClient),
	}
	var err error
	s.db, err = bolt.Open(dbfilename, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Panicln("NewGizmoStore:", err)
	}
	err = s.db.Update(func(tx *bolt.Tx) error {
		a, err := tx.CreateBucketIfNotExists(agentsBucketName)
		if err != nil {
			return err
		}
		// Enforce all to be offline on init
		err = a.ForEach(func(k, v []byte) error {
			b := a.Bucket(k)
			err := modifyAgentMeta(string(k), b, func(meta *agentMeta) error {
				meta.Online = false
				return nil
			})
			if err != nil {
				log.Println("NewGizmoStore:", err)
			}
			// Don't fail completely due to meta failure
			return nil
		})
		return err
	})
	if err != nil {
		log.Panicln("NewGizmoStore:", err)
	}
	return s
}

func (s *GizmoStore) ListAgents(previd string, count int) ([]gizmo.AgentInfo, error) {
	agents := []gizmo.AgentInfo{}
	err := s.db.View(func(tx *bolt.Tx) error {
		a := tx.Bucket(agentsBucketName)
		if a == nil {
			return fmt.Errorf("db is missing agents bucket")
		}
		err := boltpaging.IterPage(a, previd, count, func(k, v []byte) error {
			var meta agentMeta
			b := a.Bucket(k)
			m := b.Get([]byte(metaKey))
			if m != nil {
				json.Unmarshal(m, &meta)
			}
			agents = append(agents, gizmo.AgentInfo{
				ID:         string(k),
				Online:     meta.Online,
				Owner:      meta.Owner,
				Version:    meta.Version,
				OS:         meta.OS,
				RemoteAddr: meta.RemoteAddr,
				Created:    meta.Created,
				Last:       meta.Last,
			})
			return nil
		})
		return err
	})
	return agents, err
}

func (s *GizmoStore) ListAgentGizmos(agentid string, previd string, count int) ([]gizmo.GizmoInfo, error) {
	gis := []gizmo.GizmoInfo{}
	err := s.db.View(func(tx *bolt.Tx) error {
		a := tx.Bucket(agentsBucketName)
		if a == nil {
			return fmt.Errorf("db is missing agents bucket")
		}
		b := a.Bucket([]byte(agentid))
		if b == nil {
			return fmt.Errorf("no db entry for agent: %v", agentid)
		}
		rr := b.Bucket(resourcesBucketName)
		if rr == nil {
			// No bucket; empty gizmo list
			return nil
		}
		err := boltpaging.IterPage(rr, previd, count, func(k, v []byte) error {
			gi := gizmo.GizmoInfo{
				NamedGizmo: gizmo.NamedGizmo{ID: string(k)},
				Status:     "invalid",
			}
			var data resourceData
			err := json.Unmarshal(v, &data)
			// Don't fail on error; just give empty data
			if err == nil {
				gi.Status = data.Status
				gi.Gizmo = data.Gizmo
				gi.Target = data.Target
				gi.Plugin = data.Promotion.Plugin
			}
			gis = append(gis, gi)
			return nil
		})
		return err
	})
	return gis, err
}

func (s *GizmoStore) AgentClient(agentid string, agent gizmo.Agent) (gizmo.AgentClient, error) {
	t := time.Now()
	key, err := ulid.New(ulid.Timestamp(t), rand.Reader)
	if err != nil {
		return nil, err
	}
	session := gizmo.Session{
		ID:     agentid,
		Secret: agent.Secret,
		Key:    key,
	}
	if session.Secret == "" {
		session.Secret, err = util.GenerateRandomString(16)
		if err != nil {
			return nil, err
		}
	}
	err = s.updateAgents(func(a *bolt.Bucket) error {
		var b *bolt.Bucket
		now := time.Now()
		meta := agentMeta{
			Created: now,
		}
		// Retry a few times to get different random extension
		for i := 0; i < 10; i++ {
			b = a.Bucket([]byte(session.ID))
			if b == nil {
				b, err = a.CreateBucket([]byte(session.ID))
				if err != nil {
					return err
				}
				break
			}
			m := b.Get(metaKey)
			if m == nil {
				// If no meta; then use this bucket
				break
			} else if agent.Secret != "" {
				err := json.Unmarshal(m, &meta)
				if err != nil {
					return err
				}
				if meta.Secret != session.Secret {
					return fmt.Errorf("invalid secret for agent: %v", agentid)
				}
				break
			}
			ext, err := util.GenerateRandomString(4)
			if err != nil {
				return err
			}
			session.ID = agentid + "-" + ext
		}
		if b == nil {
			return fmt.Errorf("failed to create unique id for agent: %v", agentid)
		}
		err := setKey(b, key)
		if err != nil {
			return fmt.Errorf("failed to set agent session key in db: %v", err)
		}
		meta.Last = now
		meta.Online = true
		meta.Agent = agent
		meta.Secret = session.Secret
		m, err := json.Marshal(meta)
		if err != nil {
			return err
		}
		err = b.Put(metaKey, m)
		return err
	})
	if err != nil {
		return nil, err
	}
	c := &agentClient{
		store:    s,
		session:  session,
		updateCh: make(chan map[string]gizmo.Assignment),
	}
	s.agentsMu.Lock()
	defer s.agentsMu.Unlock()
	s.agents[key] = c
	return c, nil
}

func (s *GizmoStore) DeleteAgent(agentid string) error {
	return s.updateAgents(func(a *bolt.Bucket) error {
		return a.DeleteBucket([]byte(agentid))
	})
}

// TODO: Remove once deployed to production
func (s *GizmoStore) StoreOldSession(agentid string, secret string, created time.Time) error {
	err := s.updateAgents(func(a *bolt.Bucket) error {
		meta := agentMeta{
			Created: created,
			Agent:   gizmo.Agent{Secret: secret},
		}
		b, err := a.CreateBucket([]byte(agentid))
		if err != nil {
			return err
		}
		m, err := json.Marshal(meta)
		if err != nil {
			return err
		}
		return b.Put(metaKey, m)
	})
	return err
}

func (s *GizmoStore) ResourcesClient(resources []string) (gizmo.ResourcesClient, error) {
	update := map[string]gizmo.Resource{}
	err := s.db.View(func(tx *bolt.Tx) error {
		aa := tx.Bucket(agentsBucketName)
		if aa == nil {
			return fmt.Errorf("db is missing agents bucket")
		}
		for _, res := range resources {
			resarr := strings.SplitN(res, "/", 2)
			if len(resarr) != 2 {
				return fmt.Errorf("invalid resource id %v", res)
			}
			agentid, gizmoid := resarr[0], resarr[1]
			status := "not found"
			var class string
			var name string
			var caps map[string]map[string]string
			a := aa.Bucket([]byte(agentid))
			if a != nil {
				rr := a.Bucket(resourcesBucketName)
				if rr != nil {
					gb := rr.Get([]byte(gizmoid))
					if gb != nil {
						var data resourceData
						err := json.Unmarshal(gb, &data)
						if err == nil {
							class = data.Class()
							name = data.Promotion.Name
							status = data.Status
							caps = data.Promotion.Capabilities
						}
					}
				}
			}
			update[res] = gizmo.Resource{
				Class:        class,
				Name:         name,
				Capabilities: caps,
				Status:       status,
			}
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	c := &resourcesClient{
		resources: resources,
		store:     s,
		updateCh:  make(chan map[string]gizmo.Resource),
	}
	s.clientsMu.Lock()
	defer s.clientsMu.Unlock()
	for _, res := range resources {
		_, ok := s.clients[res]
		if !ok {
			s.clients[res] = []*resourcesClient{c}
		} else {
			s.clients[res] = append(s.clients[res], c)
		}
	}
	go func() {
		s.publishResourceStatus(update)
	}()
	return c, nil
}

func (s *GizmoStore) publishResourceStatus(update map[string]gizmo.Resource) {
	pmsg := &event_resource.StatusUpdate{
		Resources: make(map[string]*event_resource.StatusUpdate_Status),
	}
	for resourceid, resource := range update {
		pmsg.Resources[resourceid] = &event_resource.StatusUpdate_Status{Status: resource.Status}
	}
	s.publisher.Publish("resource.update", pmsg)
	s.clientsMu.Lock()
	defer s.clientsMu.Unlock()
	clientUpdate := map[*resourcesClient]map[string]gizmo.Resource{}
	for resourceid, resource := range update {
		clients, ok := s.clients[resourceid]
		if !ok {
			continue
		}
		for _, c := range clients {
			_, ok := clientUpdate[c]
			if ok {
				clientUpdate[c][resourceid] = resource
			} else {
				clientUpdate[c] = map[string]gizmo.Resource{resourceid: resource}
			}
		}
	}
	for c, update := range clientUpdate {
		c.updateCh <- update
	}
}

func (s *GizmoStore) publishNewGizmos(newgs gizmo.PromoteUpdate) {
	s.promosMu.Lock()
	defer s.promosMu.Unlock()
	for _, pc := range s.promos {
		pc.updateCh <- newgs
	}
}

func (s *GizmoStore) PromoteClient() (gizmo.PromoteClient, error) {
	s.promosMu.Lock()
	defer s.promosMu.Unlock()
	pc := newPromoteClient(s)
	s.promos = append(s.promos, pc)
	return pc, nil
}

func (s *GizmoStore) Destroy() {
	s.agentsMu.Lock()
	for _, a := range s.agents {
		a.destroy()
	}
	s.agents = map[[16]byte]*agentClient{}
	s.agentsMu.Unlock()

	s.clientsMu.Lock()
	for _, cc := range s.clients {
		for _, c := range cc {
			c.destroy()
		}
	}
	s.clients = map[string][]*resourcesClient{}
	s.clientsMu.Unlock()

	s.promosMu.Lock()
	for _, pc := range s.promos {
		pc.destroy()
	}
	s.promos = []*promoteClient{}
	s.promosMu.Unlock()

	if s.db != nil {
		s.db.Close()
		s.db = nil
	}
}
