package impl

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/ide8io/ide8/backend/gizmo"
	"github.com/boltdb/bolt"
	"github.com/oklog/ulid"
)

type agentMeta struct {
	Created time.Time `json:"created"`
	Last    time.Time `json:"last"`
	Online  bool      `json:"online"`
	gizmo.Agent
}

type resourceData struct {
	Status    string          `json:"status"`
	Created   time.Time       `json:"created"`
	Last      time.Time       `json:"last"`
	Target    string          `json:"target,omitempty"`
	Gizmo     gizmo.Gizmo     `json:"gizmo,omitempty"`
	Promotion gizmo.Promotion `json:"promo,omitempty"`
}

var agentsBucketName = []byte("agents")
var metaKey = []byte("meta")
var resourcesBucketName = []byte("resources")
var keyKey = []byte("key")

func setKey(b *bolt.Bucket, key ulid.ULID) error {
	kb, err := key.MarshalBinary()
	if err != nil {
		return err
	}
	return b.Put([]byte(keyKey), kb)
}

func isValidKey(b *bolt.Bucket, key ulid.ULID) bool {
	kb := b.Get(keyKey)
	if kb != nil {
		var skey ulid.ULID
		err := skey.UnmarshalBinary(kb)
		if err == nil && skey == key {
			return true
		}
	}
	return false
}

func (s *GizmoStore) updateAgents(cb func(a *bolt.Bucket) error) error {
	return s.db.Update(func(tx *bolt.Tx) error {
		a := tx.Bucket(agentsBucketName)
		if a == nil {
			return fmt.Errorf("db is missing agents bucket")
		}
		return cb(a)
	})
}

func (s *GizmoStore) updateAgent(session gizmo.Session, cb func(a *bolt.Bucket) error) error {
	return s.updateAgents(func(a *bolt.Bucket) error {
		b := a.Bucket([]byte(session.ID))
		if b == nil {
			return fmt.Errorf("no bucket found for agent id: %v", session.ID)
		}
		if !isValidKey(b, session.Key) {
			return fmt.Errorf("invalid session key for agent id: %v", session.ID)
		}
		return cb(b)
	})
}

func modifyAgentMeta(agentid string, b *bolt.Bucket, cb func(meta *agentMeta) error) error {
	m := b.Get(metaKey)
	if m == nil {
		return fmt.Errorf("no meta for agent id: %v", agentid)
	}
	var meta agentMeta
	err := json.Unmarshal(m, &meta)
	if err != nil {
		return err
	}
	err = cb(&meta)
	if err != nil {
		return err
	}
	m, err = json.Marshal(meta)
	if err != nil {
		return err
	}
	return b.Put(metaKey, m)
}

func (s *GizmoStore) updateAgentMeta(session gizmo.Session, cb func(meta *agentMeta) error) error {
	err := s.updateAgents(func(a *bolt.Bucket) error {
		b := a.Bucket([]byte(session.ID))
		if b == nil {
			return fmt.Errorf("no bucket found for agent id: %v", session.ID)
		}
		if !isValidKey(b, session.Key) {
			return fmt.Errorf("invalid session key for agent id: %v", session.ID)
		}
		return modifyAgentMeta(session.ID, b, func(meta *agentMeta) error {
			return cb(meta)
		})
	})
	return err
}

func (s *GizmoStore) updateAgentResources(agentid string, cb func(*bolt.Bucket) error) error {
	err := s.updateAgents(func(a *bolt.Bucket) error {
		b := a.Bucket([]byte(agentid))
		if b == nil {
			return fmt.Errorf("no bucket found for agent id: %v", agentid)
		}
		r, err := b.CreateBucketIfNotExists(resourcesBucketName)
		if err != nil {
			return err
		}
		return cb(r)
	})
	return err
}

func iterResources(rr *bolt.Bucket, cb func(id string, data *resourceData) error) error {
	return rr.ForEach(func(k, v []byte) error {
		id := string(k)
		var data resourceData
		err := json.Unmarshal(v, &data)
		if err != nil {
			return err
		}
		err = cb(id, &data)
		if err != nil {
			return err
		}
		b, err := json.Marshal(data)
		if err != nil {
			return err
		}
		rr.Put(k, b)
		return nil
	})
}

func (d resourceData) Class() string {
	if d.Promotion.Class != "" {
		return d.Promotion.Class
	}
	return d.Gizmo.Class
}
