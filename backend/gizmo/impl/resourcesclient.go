package impl

import (
	"encoding/json"
	"fmt"
	"strings"

	"bitbucket.org/ide8io/ide8/backend/gizmo"
	"github.com/boltdb/bolt"
)

type resourcesClient struct {
	resources []string
	store     *GizmoStore
	updateCh  chan map[string]gizmo.Resource
}

func (c *resourcesClient) destroy() {
	close(c.updateCh)
}

func (c *resourcesClient) Close() {
	c.store.clientsMu.Lock()
	defer c.store.clientsMu.Unlock()
	for _, rid := range c.resources {
		delete(c.store.clients, rid)
	}
	c.destroy()
}

func (c *resourcesClient) UpdateCh() <-chan map[string]gizmo.Resource {
	return c.updateCh
}

func (c *resourcesClient) Target(target string) error {
	targetUpdate := map[string]map[string]gizmo.Assignment{}
	statusUpdate := map[string]gizmo.Resource{}
	err := c.store.db.Update(func(tx *bolt.Tx) error {
		aa := tx.Bucket(agentsBucketName)
		if aa == nil {
			return fmt.Errorf("db is missing agents bucket")
		}
		for _, res := range c.resources {
			resarr := strings.SplitN(res, "/", 2)
			if len(resarr) != 2 {
				return fmt.Errorf("invalid resource id: %v", res)
			}
			agentid, gizmoid := resarr[0], resarr[1]
			bgid := []byte(gizmoid)
			a := aa.Bucket([]byte(agentid))
			if a == nil {
				return fmt.Errorf("no bucket for agent: %v", agentid)
			}
			rr := a.Bucket(resourcesBucketName)
			if rr == nil {
				return fmt.Errorf("no resources bucket for agent: %v", agentid)
			}
			gb := rr.Get(bgid)
			if gb == nil {
				return fmt.Errorf("no such resource %v", gizmoid)
			}
			var data resourceData
			err := json.Unmarshal(gb, &data)
			if err != nil {
				return err
			}
			data.Target = target
			gb, err = json.Marshal(data)
			if err != nil {
				return err
			}
			err = rr.Put(bgid, gb)
			if err != nil {
				return err
			}
			_, ok := targetUpdate[agentid]
			if ok {
				targetUpdate[agentid][gizmoid] = gizmo.Assignment{
					Plugin: data.Promotion.Plugin,
					Target: target,
				}
			} else {
				targetUpdate[agentid] = map[string]gizmo.Assignment{gizmoid: {
					Plugin: data.Promotion.Plugin,
					Target: target,
				}}
			}
			statusUpdate[res] = mkResourceUpdate(&data)
		}
		return nil
	})
	if err != nil {
		return err
	}
	go func() {
		c.store.agentsMu.Lock()
		defer c.store.agentsMu.Unlock()
		for agentid, ass := range targetUpdate {
			// TODO: Possible to optimize lookup with agentid map
			for _, a := range c.store.agents {
				if a.session.ID == agentid {
					a.updateCh <- ass
					return
				}
			}
		}
	}()
	go func() {
		c.store.publishResourceStatus(statusUpdate)
	}()
	return err
}
