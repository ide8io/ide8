package impl

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"bitbucket.org/ide8io/ide8/backend/gizmo"
	"github.com/boltdb/bolt"
)

type agentClient struct {
	store    *GizmoStore
	session  gizmo.Session
	updateCh chan map[string]gizmo.Assignment
}

func (c *agentClient) destroy() {
	close(c.updateCh)
}

func mkResourceUpdate(data *resourceData) gizmo.Resource {
	return gizmo.Resource{
		Class:        data.Class(),
		Name:         data.Promotion.Name,
		Status:       data.Status,
		Capabilities: data.Promotion.Capabilities,
	}
}

func (c *agentClient) Close() {
	update := map[string]gizmo.Resource{}
	err := c.store.updateAgent(c.session, func(a *bolt.Bucket) error {
		err := modifyAgentMeta(c.session.ID, a, func(meta *agentMeta) error {
			meta.Online = false
			return nil
		})
		if err != nil {
			return err
		}
		rr := a.Bucket(resourcesBucketName)
		if rr == nil {
			return nil
		}
		err = iterResources(rr, func(id string, data *resourceData) error {
			data.Status = gizmo.StatusOffline
			update[c.session.ID+"/"+id] = mkResourceUpdate(data)
			return nil
		})
		return err
	})
	if err != nil {
		log.Println("gizmo.Store:", err)
	}
	go func() {
		c.store.publishResourceStatus(update)
	}()
	c.store.agentsMu.Lock()
	defer c.store.agentsMu.Unlock()
	delete(c.store.agents, c.session.Key)
	c.destroy()
}

func (c *agentClient) Session() gizmo.Session {
	return c.session
}

func (c *agentClient) UpdateCh() <-chan map[string]gizmo.Assignment {
	return c.updateCh
}

func (c *agentClient) Update(gizmos map[string]gizmo.Gizmo) error {
	newGizmos := map[string]struct{}{}
	for id := range gizmos {
		newGizmos[id] = struct{}{}
	}
	newgs := gizmo.PromoteUpdate{
		AgentID: c.session.ID,
		Gizmos:  make(map[string]gizmo.Gizmo),
	}
	update := map[string]gizmo.Resource{}
	assignments := map[string]gizmo.Assignment{}
	err := c.store.updateAgents(func(a *bolt.Bucket) error {
		b := a.Bucket([]byte(c.session.ID))
		if b == nil {
			return fmt.Errorf("no bucket found for agent id: %v", c.session.ID)
		}
		m := b.Get(metaKey)
		if m == nil {
			return fmt.Errorf("no meta for agent id: %v", c.session.ID)
		}
		var meta agentMeta
		err := json.Unmarshal(m, &meta)
		if err != nil {
			return err
		}
		newgs.Owner = meta.Owner
		rr, err := b.CreateBucketIfNotExists(resourcesBucketName)
		if err != nil {
			return err
		}
		err = iterResources(rr, func(id string, data *resourceData) error {
			g, ok := gizmos[id]
			if ok {
				data.Status = gizmo.StatusOnline
				data.Last = time.Now()
				data.Gizmo = g
				assignments[id] = gizmo.Assignment{
					Plugin: data.Promotion.Plugin,
					Target: data.Target,
				}
			} else {
				data.Status = gizmo.StatusDisconnected
			}
			delete(newGizmos, id)
			update[c.session.ID+"/"+id] = mkResourceUpdate(data)
			return nil
		})
		for id := range newGizmos {
			now := time.Now()
			data := resourceData{
				Status:  gizmo.StatusOnline,
				Created: now,
				Last:    now,
				Gizmo:   gizmos[id],
			}
			b, err := json.Marshal(data)
			if err != nil {
				return err
			}
			err = rr.Put([]byte(id), b)
			if err != nil {
				return err
			}
			update[c.session.ID+"/"+id] = mkResourceUpdate(&data)
			newgs.Gizmos[id] = gizmos[id]
		}
		return err
	})
	if err != nil {
		return err
	}
	go func() {
		c.store.publishResourceStatus(update)
		c.store.publishNewGizmos(newgs)
		if len(assignments) > 0 {
			c.updateCh <- assignments
		}
	}()
	return nil
}
