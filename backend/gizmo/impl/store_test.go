package impl_test

import (
	"runtime"
	"testing"
	"time"

	"bitbucket.org/ide8io/ide8/backend/filename"
	"bitbucket.org/ide8io/ide8/backend/gizmo"
	"bitbucket.org/ide8io/ide8/backend/gizmo/impl"
	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type pubMock struct {
}

func (p *pubMock) Publish(topic string, msg proto.Message) {
}

func setupMyGizmoStore(test func(store *impl.GizmoStore)) {
	tmp := filename.NewTemp()
	defer tmp.Remove()
	store := impl.NewGizmoStore(tmp.Filename(), &pubMock{})
	test(store)
}

func TestNewGizmoStore(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
	})
}

func TestNewGizmoStoreBadFilename(t *testing.T) {
	assert.Panics(t, func() { impl.NewGizmoStore("/", &pubMock{}) })
}

func TestGoingOfflineOnRestart(t *testing.T) {
	tmp := filename.NewTemp()
	defer tmp.Remove()
	fname := tmp.Filename()
	store := impl.NewGizmoStore(fname, &pubMock{})
	_, err := store.AgentClient("agentid", gizmo.Agent{})
	require.Nil(t, err)
	l, err := store.ListAgents("", 10)
	require.Nil(t, err)
	require.Equal(t, 1, len(l))
	assert.True(t, l[0].Online)
	store.Destroy()
	store = impl.NewGizmoStore(fname, &pubMock{})
	l, err = store.ListAgents("", 10)
	require.Nil(t, err)
	require.Equal(t, 1, len(l))
	assert.False(t, l[0].Online)
}

func TestBoardClient(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
		c, err := store.ResourcesClient([]string{"agentid1/gizmo1"})
		require.Nil(t, err)
		select {
		case u := <-c.UpdateCh():
			assert.Equal(t, 1, len(u))
			for k, v := range u {
				assert.Equal(t, "agentid1/gizmo1", k)
				assert.Equal(t, "not found", v.Status)
			}
		case <-time.After(time.Second):
			assert.Fail(t, "board update on creation missing")
		}
		c.Close()
		select {
		case _, ok := <-c.UpdateCh():
			assert.False(t, ok)
		default:
			require.Fail(t, "board update chan not closed")
		}
	})
}

func TestBoardClientReopen(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
		c1, err := store.ResourcesClient([]string{"agentid1/gizmo1"})
		require.Nil(t, err)
		c1.Close()
		_, err = store.ResourcesClient([]string{"agentid1/gizmo1"})
		require.Nil(t, err)
	})
}

func TestBoardClientUnknownTarget(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
		c, err := store.ResourcesClient([]string{"agentid1/gizmo1"})
		require.Nil(t, err)
		err = c.Target("target")
		require.NotNil(t, err)
	})
}

func TestBoardClientTarget(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
		ac, err := store.AgentClient("agentid1", gizmo.Agent{})
		require.Nil(t, err)
		err = ac.Update(map[string]gizmo.Gizmo{
			"gizmo1": gizmo.Gizmo{},
		})

		// Make sure update broadcast is performed
		runtime.Gosched()

		assert.Nil(t, err)
		bc, err := store.ResourcesClient([]string{"agentid1/gizmo1"})
		require.Nil(t, err)
		select {
		case u := <-bc.UpdateCh():
			require.Equal(t, 1, len(u))
			status, ok := u["agentid1/gizmo1"]
			require.True(t, ok)
			assert.Equal(t, "online", status.Status)
		case <-time.After(time.Second):
			require.Fail(t, "board update on creation missing")
		}

		err = bc.Target("target")
		require.Nil(t, err)
		select {
		case update := <-ac.UpdateCh():
			require.Equal(t, 1, len(update))
			a, ok := update["gizmo1"]
			require.True(t, ok)
			assert.Equal(t, "target", a.Target)
		case <-time.After(time.Second):
			require.Fail(t, "agent update event timeout")
		}
		select {
		case u := <-bc.UpdateCh():
			require.Equal(t, 1, len(u))
			status, ok := u["agentid1/gizmo1"]
			require.True(t, ok)
			assert.Equal(t, gizmo.StatusOnline, status.Status)
		case <-time.After(time.Second):
			require.Fail(t, "agent update event timeout")
		}

		err = ac.Update(map[string]gizmo.Gizmo{})
		assert.Nil(t, err)
		select {
		case u := <-bc.UpdateCh():
			require.Equal(t, 1, len(u))
			status, ok := u["agentid1/gizmo1"]
			require.True(t, ok)
			assert.Equal(t, gizmo.StatusDisconnected, status.Status)
		case <-time.After(time.Second):
			require.Fail(t, "board update on creation missing")
		}
	})
}
