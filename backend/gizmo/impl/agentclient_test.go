package impl_test

import (
	"testing"

	"bitbucket.org/ide8io/ide8/backend/gizmo"
	"bitbucket.org/ide8io/ide8/backend/gizmo/impl"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestAgentClient(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
		c, err := store.AgentClient("agentid", gizmo.Agent{})
		require.Nil(t, err)
		assert.Equal(t, "agentid", c.Session().ID)
		assert.NotEqual(t, "", c.Session().Secret)
		assert.NotEqual(t, "", c.Session().Key)
		c.Close()
		select {
		case _, ok := <-c.UpdateCh():
			assert.False(t, ok)
		default:
			require.Fail(t, "agent update chan not closed")
		}
	})
}

func TestAgentClientWithValidSecret(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
		c1, err := store.AgentClient("agentid", gizmo.Agent{})
		require.Nil(t, err)
		assert.Equal(t, "agentid", c1.Session().ID)
		c2, err := store.AgentClient("agentid", gizmo.Agent{Secret: c1.Session().Secret})
		require.Nil(t, err)
		assert.Equal(t, "agentid", c2.Session().ID)
		assert.Equal(t, c1.Session().Secret, c2.Session().Secret)
	})
}

func TestAgentClientWithBadSecret(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
		c1, err := store.AgentClient("agentid", gizmo.Agent{})
		require.Nil(t, err)
		assert.Equal(t, "agentid", c1.Session().ID)
		_, err = store.AgentClient("agentid", gizmo.Agent{Secret: "bad" + c1.Session().Secret})
		assert.NotNil(t, err)
	})
}

func TestTwoAgentClientsWithSameName(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
		c1, err := store.AgentClient("agentid", gizmo.Agent{})
		require.Nil(t, err)
		assert.Equal(t, "agentid", c1.Session().ID)
		c2, err := store.AgentClient("agentid", gizmo.Agent{})
		require.Nil(t, err)
		assert.NotEqual(t, "agentid", c2.Session().ID)
	})
}

func TestListAgents(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
		_, err := store.AgentClient("agentid1", gizmo.Agent{})
		require.Nil(t, err)
		_, err = store.AgentClient("agentid2", gizmo.Agent{})
		require.Nil(t, err)
		l, err := store.ListAgents("", 10)
		require.Nil(t, err)
		assert.Equal(t, 2, len(l))
		l, err = store.ListAgents("agentid1", 10)
		require.Nil(t, err)
		assert.Equal(t, 1, len(l))
	})
}

func TestAgentsOnline(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
		_, err := store.AgentClient("agentid1", gizmo.Agent{})
		require.Nil(t, err)
		c2, err := store.AgentClient("agentid2", gizmo.Agent{})
		require.Nil(t, err)
		c2.Close()
		l, err := store.ListAgents("", 10)
		require.Nil(t, err)
		require.Equal(t, 2, len(l))
		assert.True(t, l[0].Online)
		assert.False(t, l[1].Online)
	})
}

func TestUpdateGizmos(t *testing.T) {
	setupMyGizmoStore(func(store *impl.GizmoStore) {
		c, err := store.AgentClient("agentid1", gizmo.Agent{})
		require.Nil(t, err)
		err = c.Update(map[string]gizmo.Gizmo{
			"gizmo1": gizmo.Gizmo{},
		})
		require.Nil(t, err)
		l, err := store.ListAgentGizmos("agentid1", "", 10)
		require.Nil(t, err)
		require.Equal(t, 1, len(l))
		assert.Equal(t, "gizmo1", l[0].ID)
		assert.Equal(t, gizmo.StatusOnline, l[0].Status)
		err = c.Update(map[string]gizmo.Gizmo{
			"gizmo1": gizmo.Gizmo{},
			"gizmo2": gizmo.Gizmo{},
		})
		require.Nil(t, err)
		l, err = store.ListAgentGizmos("agentid1", "", 10)
		require.Nil(t, err)
		require.Equal(t, 2, len(l))
		assert.Equal(t, "gizmo1", l[0].ID)
		assert.Equal(t, "gizmo2", l[1].ID)
		assert.Equal(t, gizmo.StatusOnline, l[0].Status)
		assert.Equal(t, gizmo.StatusOnline, l[1].Status)
		err = c.Update(map[string]gizmo.Gizmo{
			"gizmo2": gizmo.Gizmo{},
		})
		require.Nil(t, err)
		l, err = store.ListAgentGizmos("agentid1", "", 10)
		require.Nil(t, err)
		require.Equal(t, 2, len(l))
		assert.Equal(t, "gizmo1", l[0].ID)
		assert.Equal(t, "gizmo2", l[1].ID)
		assert.Equal(t, gizmo.StatusDisconnected, l[0].Status)
		assert.Equal(t, gizmo.StatusOnline, l[1].Status)
	})
}
