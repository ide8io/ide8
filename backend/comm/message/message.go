package message

type Message struct {
	Type    string      `json:"type"`
	Id      string      `json:"id"`
	Payload interface{} `json:"payload"`
}
