package util

import "strings"

func SplitMsgType(msgtype string) (string, string) {
	types := strings.SplitN(msgtype, ":", 2)
	if len(types) > 1 {
		return types[0], types[1]
	} else {
		return types[0], ""
	}
}
