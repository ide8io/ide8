package util

import "log"

type EventLog interface {
	Println(v ...interface{})
}

type EventLogImpl struct {
	Prefix string
}

func (el *EventLogImpl) Println(v ...interface{}) {
	log.Println(append([]interface{}{el.Prefix}, v...)...)
}
