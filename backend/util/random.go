package util

import (
	"crypto/rand"
	"encoding/base64"
	"strings"
)

func GenerateRandom(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	return b, err
}

func GenerateRandomString(n int) (string, error) {
	b, err := GenerateRandom(n)
	return strings.TrimRight(base64.URLEncoding.EncodeToString(b), "="), err
}
