package util

import (
	"fmt"
	"regexp"
	"strconv"
)

type GitVer struct {
	Major  int
	Minor  int
	Patch  int
	Commit int
	Hash   string
}

var gitverre = regexp.MustCompile(`^([0-9]+)[.]([0-9]+)(([.])([0-9]+))?((-)([0-9]+)-([0-9a-z]{8}))?$`)

func GitVerParse(ver string) (GitVer, error) {
	var gv GitVer
	result := gitverre.FindStringSubmatch(ver)
	if len(result) != 10 {
		return gv, fmt.Errorf("error parsing gitver %s", ver)
	}
	var err error
	gv.Major, err = strconv.Atoi(result[1])
	if err != nil {
		return gv, err
	}
	gv.Minor, err = strconv.Atoi(result[2])
	if err != nil {
		return gv, err
	}
	if result[4] == "." {
		gv.Patch, err = strconv.Atoi(result[5])
		if err != nil {
			return gv, err
		}
	}
	if result[7] == "-" {
		gv.Commit, err = strconv.Atoi(result[8])
		if err != nil {
			return gv, err
		}
	}
	gv.Hash = result[9]
	return gv, nil
}

func (gv *GitVer) IsOlderThan(ogv GitVer) bool {
	if gv.Major < ogv.Major {
		return true
	} else if gv.Major == ogv.Major {
		if gv.Minor < ogv.Minor {
			return true
		} else if gv.Minor == ogv.Minor {
			if gv.Patch < ogv.Patch {
				return true
			} else if gv.Patch == ogv.Patch {
				if gv.Commit < ogv.Commit {
					return true
				}
			}
		}
	}
	return false
}
