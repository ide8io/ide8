package util

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseReleaseWithoutPatch(t *testing.T) {
	gv, err := GitVerParse("0.5")
	assert.Nil(t, err)
	assert.Equal(t, GitVer{0, 5, 0, 0, ""}, gv)
}

func TestParseBetweenReleaseWithoutPatch(t *testing.T) {
	gv, err := GitVerParse("0.5-11-g7fd7c8d")
	assert.Nil(t, err)
	assert.Equal(t, GitVer{0, 5, 0, 11, "g7fd7c8d"}, gv)
}

func TestParseReleaseWithPatch(t *testing.T) {
	gv, err := GitVerParse("0.5.7")
	assert.Nil(t, err)
	assert.Equal(t, GitVer{0, 5, 7, 0, ""}, gv)
}

func TestParseBetweenReleaseWithPatch(t *testing.T) {
	gv, err := GitVerParse("0.5.3-11-g7fd7c8d")
	assert.Nil(t, err)
	assert.Equal(t, GitVer{0, 5, 3, 11, "g7fd7c8d"}, gv)
}

func TestParseEmpty(t *testing.T) {
	_, err := GitVerParse("")
	assert.NotNil(t, err)
}

func TestParseMajorOnly(t *testing.T) {
	_, err := GitVerParse("5-g7fd7c8d")
	assert.NotNil(t, err)
}

func TestParseTooShortHash(t *testing.T) {
	_, err := GitVerParse("0.5.3-11-7fd7c8d")
	assert.NotNil(t, err)
}

func TestParseBadCharsInCommit(t *testing.T) {
	_, err := GitVerParse("0.5.3-fd-7fd7c8d")
	assert.NotNil(t, err)
}

func TestMajorIsOlder(t *testing.T) {
	assert.False(t, (&GitVer{Major: 3}).IsOlderThan(GitVer{Major: 2}))
	assert.False(t, (&GitVer{Major: 2}).IsOlderThan(GitVer{Major: 2}))
	assert.True(t, (&GitVer{Major: 1}).IsOlderThan(GitVer{Major: 2}))
}

func TestMinorIsOlder(t *testing.T) {
	assert.False(t, (&GitVer{Major: 3, Minor: 2}).IsOlderThan(GitVer{Major: 2, Minor: 2}))
	assert.False(t, (&GitVer{Minor: 3}).IsOlderThan(GitVer{Minor: 2}))
	assert.False(t, (&GitVer{Minor: 2}).IsOlderThan(GitVer{Minor: 2}))
	assert.True(t, (&GitVer{Minor: 1}).IsOlderThan(GitVer{Minor: 2}))
}

func TestPatchIsOlder(t *testing.T) {
	assert.False(t, (&GitVer{Minor: 3, Patch: 2}).IsOlderThan(GitVer{Minor: 2, Patch: 2}))
	assert.False(t, (&GitVer{Patch: 3}).IsOlderThan(GitVer{Patch: 2}))
	assert.False(t, (&GitVer{Patch: 2}).IsOlderThan(GitVer{Patch: 2}))
	assert.True(t, (&GitVer{Patch: 1}).IsOlderThan(GitVer{Patch: 2}))
}

func TestCommitIsOlder(t *testing.T) {
	assert.False(t, (&GitVer{Patch: 3, Commit: 2}).IsOlderThan(GitVer{Patch: 2, Commit: 2}))
	assert.False(t, (&GitVer{Commit: 3}).IsOlderThan(GitVer{Commit: 2}))
	assert.False(t, (&GitVer{Commit: 2}).IsOlderThan(GitVer{Commit: 2}))
	assert.True(t, (&GitVer{Commit: 1}).IsOlderThan(GitVer{Commit: 2}))
}
