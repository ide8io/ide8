package util

import "net/http"

func GetRemoteAddr(r *http.Request) string {
	remote := r.Header.Get("X-Real-IP") // Forwared from proxy
	if remote == "" {
		return r.RemoteAddr
	}
	return remote
}
