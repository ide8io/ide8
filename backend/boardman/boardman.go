package boardman

import (
	"fmt"
	"log"
	"sync"
	"time"
)

// Board structure used for user view
type UserBoard struct {
	Bridge        string `json:"bridge"`
	BridgeVersion string `json:"bridgeversion"`
	Class         string `json:"class"`
	Type          string `json:"type"`
	Id            string `json:"id"`
	Addr          string `json:"addr"`
	// Possible values: free, mine, other
	Status string `json:"status"`
	// Depends on Status: mine: workspace, other: username
	Owner    string    `json:"owner"`
	LastSeen time.Time `json:"lastseen"`
}

type UserBoards []UserBoard

type Board struct {
	Bridge            string    `json:"bridge"`
	BridgeVersion     string    `json:"bridgeversion"`
	BridgeOs          string    `json:"bridgeos"`
	Class             string    `json:"class"`
	Type              string    `json:"type"`
	Id                string    `json:"id"`
	Addr              string    `json:"addr"`
	Status            string    `json:"status"`
	URL               string    `json:"url"`
	Online            bool      `json:"online"`
	LastSeen          time.Time `json:"lastseen"`
	Owner             string    `json:"owner"`
	AssignedUser      string    `json:"assingeduser"`
	AssignedWorkspace string    `json:"assingedworkspace"`
}

type Boards []Board

type ListenData struct {
	Class     string
	Type      string
	Id        string
	Username  string
	Workspace string
	Target    string
	Online    bool
}

var boards Boards
var boardsMu sync.Mutex
var listenchans = []chan ListenData{}

func ListUserBoards(username string, btype string, status string) (UserBoards, error) {
	list := UserBoards{}
	boardsMu.Lock()
	defer boardsMu.Unlock()
	for _, board := range boards {
		if ((btype == "") || (btype == board.Type)) && board.Online && (board.Owner == "" || board.Owner == username) {
			userboard := UserBoard{Bridge: board.Bridge, BridgeVersion: board.BridgeVersion, Class: board.Class, Type: board.Type, Id: board.Id, Addr: board.Addr, Status: "free", LastSeen: board.LastSeen}
			if board.AssignedUser == username {
				userboard.Status = "mine"
				userboard.Owner = board.AssignedWorkspace
			} else if board.AssignedUser != "" {
				userboard.Status = "other"
				userboard.Owner = board.AssignedUser
			}
			if (status == "") || (userboard.Status == status) {
				list = append(list, userboard)
			}
		}
	}
	return list, nil
}

func ListBoards() (Boards, error) {
	boardsMu.Lock()
	defer boardsMu.Unlock()
	list := make(Boards, len(boards))
	copy(list, boards)
	return list, nil
}

func Online(id string, class string, board string, bridge string, owner string, raddr string, ver string, os string) string {
	boardsMu.Lock()
	defer boardsMu.Unlock()
	for i := range boards {
		if boards[i].Id == id {
			boards[i].Online = true
			boards[i].BridgeVersion = ver
			boards[i].BridgeOs = os
			boards[i].Addr = raddr
			boards[i].Owner = owner
			boards[i].LastSeen = time.Now()
			log.Println("boardman: online:", id)
			if owner != "" && owner != boards[i].AssignedUser {
				boards[i].URL = ""
				broadcast(boards[i])
				boards[i].AssignedUser = ""
				boards[i].AssignedWorkspace = ""
			} else {
				broadcast(boards[i])
			}
			return boards[i].URL
		}
	}
	log.Println("boardman: new:", id)
	boards = append(boards, Board{Id: id, Class: class, Type: board, Bridge: bridge, BridgeVersion: ver, BridgeOs: os, Addr: raddr, Online: true, Owner: owner, LastSeen: time.Now()})
	return ""
}

func Offline(id string) {
	boardsMu.Lock()
	defer boardsMu.Unlock()
	for i := range boards {
		if boards[i].Id == id {
			boards[i].Online = false
			boards[i].LastSeen = time.Now()
			log.Println("boardman: offline:", id)
			broadcast(boards[i])
			return
		}
	}
}

func Claim(id string, username string, workspace string, target string) error {
	boardsMu.Lock()
	defer boardsMu.Unlock()
	// Release any other board claimed to workspace
	for i := range boards {
		if (boards[i].AssignedUser == username) && (boards[i].AssignedWorkspace == workspace) {
			boards[i].URL = ""
			log.Println("boardman:", boards[i].AssignedUser+"/"+boards[i].AssignedWorkspace, "released:", id)
			broadcast(boards[i])
			boards[i].AssignedUser = ""
			boards[i].AssignedWorkspace = ""
		}
	}
	for i := range boards {
		if boards[i].Id == id {
			if (boards[i].AssignedUser == "") && (boards[i].AssignedWorkspace == "") {
				boards[i].AssignedUser = username
				boards[i].AssignedWorkspace = workspace
				boards[i].URL = target
				log.Println("boardman:", username+"/"+workspace, "claimed:", id)
				broadcast(boards[i])
			} else {
				return fmt.Errorf("Can't claim previously claimed board: %s", id)
			}
			return nil
		}
	}
	return fmt.Errorf("Board not found: %s", id)
}

func Release(id string, username string) error {
	boardsMu.Lock()
	defer boardsMu.Unlock()
	for i := range boards {
		if boards[i].Id == id {
			if boards[i].AssignedUser != username {
				return fmt.Errorf("Can't release board owned by other user: %s", id)
			}
			boards[i].URL = ""
			log.Println("boardman:", boards[i].AssignedUser+"/"+boards[i].AssignedWorkspace, "released:", id)
			broadcast(boards[i])
			boards[i].AssignedUser = ""
			boards[i].AssignedWorkspace = ""
			return nil
		}
	}
	return fmt.Errorf("Board not found: %s", id)
}

func ForcedRelease(id string) error {
	boardsMu.Lock()
	defer boardsMu.Unlock()
	for i := range boards {
		if boards[i].Id == id {
			boards[i].URL = ""
			log.Println("boardman:", boards[i].AssignedUser+"/"+boards[i].AssignedWorkspace, "released by force:", id)
			broadcast(boards[i])
			boards[i].AssignedUser = ""
			boards[i].AssignedWorkspace = ""
			return nil
		}
	}
	return fmt.Errorf("Board not found: %s", id)
}

func ReleaseByWorkspace(username string, workspace string) {
	boardsMu.Lock()
	defer boardsMu.Unlock()
	for i := range boards {
		if boards[i].AssignedUser == username && boards[i].AssignedWorkspace == workspace {
			boards[i].URL = ""
			log.Println("boardman:", boards[i].AssignedUser+"/"+boards[i].AssignedWorkspace, "released for workspace:")
			broadcast(boards[i])
			boards[i].AssignedUser = ""
			boards[i].AssignedWorkspace = ""
			return
		}
	}
}

func broadcast(board Board) {
	for j := range listenchans {
		listenchans[j] <- ListenData{board.Class, board.Type, board.Id, board.AssignedUser, board.AssignedWorkspace, board.URL, board.Online}
	}
}

func Listen() chan ListenData {
	listenchan := make(chan ListenData)
	listenchans = append(listenchans, listenchan)
	return listenchan
}

func GetBoard(username string, wsname string) (ListenData, error) {
	for i := range boards {
		if boards[i].AssignedUser == username && boards[i].AssignedWorkspace == wsname {
			board := boards[i]
			return ListenData{board.Class, board.Type, board.Id, board.AssignedUser, board.AssignedWorkspace, board.URL, board.Online}, nil
		}
	}
	return ListenData{}, fmt.Errorf("no board assigned to %v/%v", username, wsname)
}
