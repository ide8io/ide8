package filename_test

import (
	"io/ioutil"
	"os"
	"testing"

	"bitbucket.org/ide8io/ide8/backend/filename"
	"github.com/stretchr/testify/assert"
)

func TestNewTemp(t *testing.T) {
	tmp := filename.NewTemp()
	fname := tmp.Filename()
	_, err := os.Stat(fname)
	assert.True(t, os.IsNotExist(err))
	err = ioutil.WriteFile(fname, []byte{}, 0600)
	assert.Nil(t, err)
	tmp.Remove()
	_, err = os.Stat(fname)
	assert.True(t, os.IsNotExist(err))
}
