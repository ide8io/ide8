package filename

import (
	"os"
	"path/filepath"
	"strconv"
	"time"
)

// Temp Temporary filename.
type Temp struct {
	Pub string
}

// NewTemp New temporary filename filename.
func NewTemp() Temp {
	return Temp{filepath.Join(os.TempDir(), "temp_"+strconv.Itoa(int(time.Now().UnixNano())))}
}

// Filename Name of temporary file.
func (t Temp) Filename() string {
	return t.String()
}

// String Filename as string.
func (t Temp) String() string {
	return t.Pub
}

// Remove Remove temporary file if present.
func (t Temp) Remove() {
	os.Remove(t.Filename())
}
