package boltpaging_test

import (
	"fmt"
	"testing"

	"bitbucket.org/ide8io/ide8/backend/boltpaging"
	"bitbucket.org/ide8io/ide8/backend/filename"
	"github.com/boltdb/bolt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewGizmoStore(t *testing.T) {
	tmp := filename.NewTemp()
	defer tmp.Remove()
	db, err := bolt.Open(tmp.Filename(), 0600, nil)
	require.Nil(t, err)
	err = db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucket([]byte("bucket"))
		require.Nil(t, err)
		err = b.Put([]byte("a"), nil)
		require.Nil(t, err)
		err = b.Put([]byte("b"), nil)
		require.Nil(t, err)
		err = b.Put([]byte("c"), nil)
		require.Nil(t, err)
		count := 0
		err = boltpaging.IterPage(b, "", 2, func(k, v []byte) error {
			if count == 0 {
				assert.Equal(t, "a", string(k))
			} else if count == 1 {
				assert.Equal(t, "b", string(k))
			}
			count++
			return nil
		})
		assert.Nil(t, err)
		assert.Equal(t, 2, count)
		if err != nil {
			return err
		}
		count = 0
		err = boltpaging.IterPage(b, "b", 2, func(k, v []byte) error {
			if count == 0 {
				assert.Equal(t, "c", string(k))
			}
			count++
			return nil
		})
		assert.Nil(t, err)
		assert.Equal(t, 1, count)
		if err != nil {
			return err
		}
		err = boltpaging.IterPage(b, "b", 2, func(k, v []byte) error {
			return fmt.Errorf("some error")
		})
		assert.NotNil(t, err)
		return nil
	})
	assert.Nil(t, err)
}
