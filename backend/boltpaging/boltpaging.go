package boltpaging

import (
	"bytes"

	"github.com/boltdb/bolt"
)

func PageCursor(b *bolt.Bucket, previd string) ([]byte, []byte, *bolt.Cursor) {
	c := b.Cursor()
	prevb := []byte(previd)
	k, v := c.Seek(prevb)
	if k != nil && bytes.Compare(k, prevb) == 0 {
		// Spool to next if previous is found
		k, v = c.Next()
	}
	return k, v, c
}

func IterPage(b *bolt.Bucket, previd string, count int, cb func(k, v []byte) error) error {
	k, v, c := PageCursor(b, previd)
	for i := 0; k != nil && i < count; k, v = c.Next() {
		err := cb(k, v)
		if err != nil {
			return err
		}
		i++
	}
	return nil
}
