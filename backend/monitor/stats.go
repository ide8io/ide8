package monitor

import (
	"bitbucket.org/ide8io/ide8/backend/boardman"
	"encoding/json"
	"log"
	"os"
	"runtime"
	"sync"
	"time"
	"io"
	"path/filepath"
)

type UsersStats struct {
	Total  int `json:"total"`
	Active int `json:"active"`
}

type WorkspacesStats struct {
	Total          int   `json:"total"`
	Active         int   `json:"active"`
	TotalDiskUsage int64 `json:"total_disk_usage"`
}

type BoardsStats struct {
	Total     int `json:"total"`
	Connected int `json:"connected"`
	Claimed   int `json:"claimed"`
}

type MemStats struct {
	Alloc       uint64 `json:"alloc"`
	HeapObjects uint64 `json:"heap_objects"`
}

type StackStats struct {
	Total int `json:"total"`
}

type Stats struct {
	Memory     MemStats        `json:"memory"`
	Users      UsersStats      `json:"users"`
	Workspaces WorkspacesStats `json:"workspaces"`
	Boards     BoardsStats     `json:"boards"`
}

type UserStats struct {
	NumLogins     int `json:"num_logins"`
	NumWorkspaces int `json:"num_workspaces"`
	DiskUsage     int `json:"disk_usage"`
	NumBoards     int `json:"num_boards"`
	ClaimCount    int
}

type WorkspaceStats struct {
	RunTime      time.Duration `json:"run_time"`
	DiskUsage    int           `json:"disk_usage"`
	BuildCount   int
	ProgramCount int
	ClaimCount   int
	NumChannels  int
}

type UsersLogEntry struct {
	Time  time.Time  `json:"time"`
	Users UsersStats `json:"users"`
}

var log_users_filename string
var statsMu sync.Mutex
var stats Stats
var usersLog []UsersLogEntry


func Init(datadir string) {
	log_users_filename = filepath.Join(datadir, "ide8_stats_users.jsonlog")
	statsMu.Lock()
	defer statsMu.Unlock()
	readUsersStats()
	if len(usersLog) > 0 {
		stats.Users = usersLog[len(usersLog) - 1].Users
	}
}

func GetStats() Stats {
	statsMu.Lock()
	defer statsMu.Unlock()
	var memstats runtime.MemStats
	runtime.ReadMemStats(&memstats)
	stats.Memory.Alloc = memstats.Alloc
	stats.Memory.HeapObjects = memstats.HeapObjects

	brds, _ := boardman.ListBoards()
	stats.Boards.Total = len(brds)
	connected := 0
	claimed := 0
	for i := range brds {
		if brds[i].Online {
			connected++
		}
		if brds[i].AssignedUser != "" {
			claimed++
		}
	}
	stats.Boards.Connected = connected
	stats.Boards.Claimed = claimed
	return stats
}

func GetUsersStatsLog() []UsersLogEntry {
	statsMu.Lock()
	defer statsMu.Unlock()
	return usersLog
}

func readUsersStats() {
	if _, err := os.Stat(log_users_filename); os.IsNotExist(err) {
		return
	}

	f, err := os.OpenFile(log_users_filename, os.O_RDONLY, 0600)
	if err != nil {
		log.Println("monitor: readUsersStats:", err)
		return
	}
	defer f.Close()
	d := json.NewDecoder(f)
	for {
		var le UsersLogEntry
		err := d.Decode(&le)
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Println("monitor: readUsersStats:", err)
			break
		}
		usersLog = append(usersLog, le)
	}
}

func logUsersStats() {
	le := UsersLogEntry{time.Now(), stats.Users}
	usersLog = append(usersLog, le)
	b, err := json.Marshal(le)
	if err != nil {
		log.Println("monitor: logUsersStats:", err)
		return
	}
	f, err := os.OpenFile(log_users_filename, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		log.Println("monitor: logUsersStats:", err)
		return
	}
	defer f.Close()
	_, err = f.Write(b)
	if err != nil {
		log.Println("monitor: logUsersStats:", err)
	}
}

func UpdateUsersTotal(total int) {
	statsMu.Lock()
	defer statsMu.Unlock()
	if stats.Users.Total != total {
		stats.Users.Total = total
		logUsersStats()
	}
}

func UpdateUsersActive(active int) {
	statsMu.Lock()
	defer statsMu.Unlock()
	if stats.Users.Active != active {
		stats.Users.Active = active
		logUsersStats()
	}
}

func UpdateWorkspacesTotal(total int) {
	statsMu.Lock()
	defer statsMu.Unlock()
	stats.Workspaces.Total = total
}

func UpdateWorkspacesActive(active int) {
	statsMu.Lock()
	defer statsMu.Unlock()
	stats.Workspaces.Active = active
}

func UpdateWorkspacesDiskUsage(usage int64) {
	statsMu.Lock()
	defer statsMu.Unlock()
	stats.Workspaces.TotalDiskUsage = usage
}
