package session

import (
	"strings"
	"sync"
	"time"

	"bitbucket.org/ide8io/ide8/backend/workspace/channel"
)

// Session for a running workspace. Can have several clients active.
type Session struct {
	Start time.Time
	css   map[string]*channel.Session
	mu    sync.RWMutex
	env   []string
	touch time.Time
}

func CreateUserSession() *Session {
	now := time.Now()
	return &Session{Start: now, css: map[string]*channel.Session{}, touch: now}
}

func (us *Session) Touch() {
	us.mu.Lock()
	defer us.mu.Unlock()
	us.touch = time.Now()
}

func (us *Session) GetChannelSession(id string) *channel.Session {
	us.mu.RLock()
	defer us.mu.RUnlock()
	return us.css[id]
}

func (us *Session) Detach(output channel.SessionOutputFunc, done channel.SessionDoneFunc) {
	us.mu.RLock()
	defer us.mu.RUnlock()
	for _, cs := range us.css {
		cs.Detach(output, done)
	}
}

func (us *Session) HasActiveChannels() bool {
	us.mu.RLock()
	defer us.mu.RUnlock()
	for _, cs := range us.css {
		if cs.IsActive() {
			return true
		}
	}
	return false
}

func (us *Session) HasChannel(id string) bool {
	us.mu.RLock()
	defer us.mu.RUnlock()
	_, ok := us.css[id]
	return ok
}

func (us *Session) SetChannelSession(id string, cs *channel.Session) {
	us.mu.Lock()
	defer us.mu.Unlock()
	us.css[id] = cs
}

func (us *Session) DeleteChannelSession(id string) {
	us.mu.Lock()
	defer us.mu.Unlock()
	delete(us.css, id)
}

func (us *Session) SetEnv(key string, value string) {
	us.mu.Lock()
	defer us.mu.Unlock()
	for i := range us.env {
		items := strings.Split(us.env[i], "=")
		if items[0] == key {
			us.env[i] = key + "=" + value
			return
		}
	}
	us.env = append(us.env, key+"="+value)
}

func (us *Session) GetEnvs() []string {
	us.mu.RLock()
	defer us.mu.RUnlock()
	return us.env
}

func (us *Session) ListChannels() []channel.ChannelInfo {
	cis := []channel.ChannelInfo{}
	us.mu.RLock()
	defer us.mu.RUnlock()
	for _, cs := range us.css {
		cis = append(cis, cs.Info())
	}
	return cis
}

func (us *Session) PurgeChannels(limit time.Duration) []string {
	us.mu.RLock()
	defer us.mu.RUnlock()
	ids := []string{}
	for id, cs := range us.css {
		if cs.IsDead(limit) {
			cs.Stop()
			ids = append(ids, id)
		}
	}
	for _, id := range ids {
		delete(us.css, id)
	}
	return ids
}

func (us *Session) Destroy() {
	us.mu.RLock()
	defer us.mu.RUnlock()
	for _, cs := range us.css {
		cs.Stop()
	}
}
