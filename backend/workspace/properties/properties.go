package properties

type Properties interface {
	Get(key string, props interface{}) error
	Store(key string, props interface{}) error
}
