package impl

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"sync"
)

type JSONFileProperties struct {
	filename string
	mu       sync.Mutex
}

func New(filename string) *JSONFileProperties {
	p := &JSONFileProperties{filename: filename}
	return p
}

func (p *JSONFileProperties) read() (map[string]json.RawMessage, error) {
	if _, err := os.Stat(p.filename); os.IsNotExist(err) {
		return make(map[string]json.RawMessage), nil
	}
	b, err := ioutil.ReadFile(p.filename)
	if err != nil {
		return nil, err
	}
	var keyvals map[string]json.RawMessage
	err = json.Unmarshal(b, &keyvals)
	if err != nil {
		return nil, err
	}
	return keyvals, nil
}

func (p *JSONFileProperties) Get(key string, props interface{}) error {
	p.mu.Lock()
	defer p.mu.Unlock()
	keyvals, err := p.read()
	if err != nil {
		return err
	}
	val, ok := keyvals[key]
	if ok {
		err = json.Unmarshal(val, props)
		if err != nil {
			return err
		}
	}
	return nil
}

func (p *JSONFileProperties) Store(key string, props interface{}) error {
	p.mu.Lock()
	defer p.mu.Unlock()
	b, err := json.Marshal(props)
	if err != nil {
		return err
	}
	keyvals, err := p.read()
	if err != nil {
		return err
	}
	keyvals[key] = b
	b, err = json.Marshal(keyvals)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(p.filename, b, 0600)
}
