package impl

import (
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestOpenNoFile(t *testing.T) {
	p := New("/tmp/not_a_file")
	var data struct {
		Val string
	}
	err := p.Get("hmm", &data)
	assert.Nil(t, err)
	assert.Equal(t, "", data.Val)
}

func TestWriteRead(t *testing.T) {
	fname := filepath.Join("/tmp", "jsonfile_test_"+string(time.Now().UnixNano()))
	defer os.Remove(fname)
	p := New(fname)
	var data = struct {
		Val string
	}{"some_value"}
	err := p.Store("hmm", &data)
	assert.Nil(t, err)
	var dataReadback struct {
		Val string
	}
	err = p.Get("hmm", &dataReadback)
	assert.Nil(t, err)
	assert.Equal(t, data.Val, dataReadback.Val)
}

func TestWriteReadModify(t *testing.T) {
	fname := filepath.Join("/tmp", "jsonfile_test_"+string(time.Now().UnixNano()))
	defer os.Remove(fname)
	p := New(fname)
	var data = struct {
		Val string
	}{"some_value"}
	err := p.Store("hmm", &data)
	assert.Nil(t, err)
	var dataReadback struct {
		Val string
	}
	err = p.Get("hmm", &dataReadback)
	assert.Nil(t, err)
	assert.Equal(t, data.Val, dataReadback.Val)
	data.Val = "some_other_value"
	err = p.Store("hmm", &data)
	assert.Nil(t, err)
	err = p.Get("hmm", &dataReadback)
	assert.Nil(t, err)
	assert.Equal(t, data.Val, dataReadback.Val)
}
