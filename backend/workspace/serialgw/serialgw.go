package serialgw

import (
	"bitbucket.org/ide8io/ide8/backend/util"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"
)

type SerialGWChanData struct {
	Action string
	Data   []byte
}
type SerialGWChan chan SerialGWChanData

type serialGW struct {
	port          int
	tunnelread    SerialGWChan
	tunnelwrite   SerialGWChan
	tunnelstop    chan struct{}
	frontendread  SerialGWChan
	frontendwrite SerialGWChan
	frontendstop  chan struct{}
	listener      net.Listener
	conn          net.Conn
	elog          util.EventLog
}

var sgws = map[string]*serialGW{}
var sgwsmu = sync.RWMutex{}

func route(read SerialGWChan, write SerialGWChan, park chan bool, stop chan struct{}, elog util.EventLog) {
loop:
	for {
		var data SerialGWChanData
		select {
		case data = <-write:
		case <-park:
			<-park
			continue
		case <-stop:
			break loop
		}
		for {
			select {
			case read <- data:
				continue loop
			case moredata, ok := <-write:
				if !ok {
					return
				}
				fifo := []SerialGWChanData{data, moredata}
				overflow := false
				for {
					select {
					case read <- fifo[0]:
						fifo = fifo[1:]
						if len(fifo) == 0 {
							continue loop
						}
					case data, ok = <-write:
						if !ok {
							return
						}
						if len(fifo) > 1000 {
							if !overflow {
								elog.Println("fifo overflow")
								overflow = true
							}
						} else {
							fifo = append(fifo, data)
						}
					case <-park:
						<-park
					case <-stop:
						break loop
					}
				}
			case <-park:
				<-park
			case <-stop:
				break loop
			}
		}
	}
	close(read)
	return
}

func trimQuotedString(str string) string {
	return strings.TrimSuffix(strings.TrimPrefix(str, "\""), "\"")
}

func (gw *serialGW) localListen(l net.Listener, parkfrontend chan bool, parktunnel chan bool) {
	for {
		conn, err := l.Accept()
		if err != nil {
			gw.elog.Println("listen:", err)
			return
		}
		gw.conn = conn

		gw.elog.Println("Accepted connection from", conn.RemoteAddr())

		parkfrontend <- true
		parktunnel <- true

		kill := make(chan struct{})

		go func() {
			ready := false

			for {
				select {
				case data := <-gw.tunnelwrite:
					if data.Action == "output" {
						if !ready {
							continue
						}
						var payload string
						err = json.Unmarshal(data.Data, &payload)
						if err != nil {
							gw.elog.Println("localListen Unmarshal:", err)
							continue
						}
						// Uses another quoting since golang's json.Marshal have issues with extended ascii chars
						payload, err = strconv.Unquote("\"" + payload + "\"")
						if err != nil {
							gw.elog.Println("localListen Unquote:", err)
							continue
						}
						//log.Println("->tcp:", strconv.Quote(payload))
						_, err := conn.Write([]byte(payload))
						if (err != nil) && (err != io.EOF) {
							gw.elog.Println("localListen Write:", err)
						}
					} else if data.Action == "status" {
						ready = true
					}
				case <-kill:
					return
				}
			}
		}()

		terminate := func() {
			kill <- struct{}{}
			parkfrontend <- false
			parktunnel <- false
			gw.elog.Println("Closed connection from", conn.RemoteAddr())
			conn.Close()
			gw.conn = nil
		}

		select {
		case gw.tunnelread <- SerialGWChanData{"baud", []byte("\"115200\"")}:
		case <-time.After(time.Second):
			terminate()
			continue
		}

		select {
		case gw.tunnelread <- SerialGWChanData{"arduino-bootloader", []byte("\"enable\"")}:
		case <-time.After(time.Second):
			terminate()
			continue
		}

		data := make([]byte, 1024)
	readloop:
		for {
			n, err := conn.Read(data)
			if err != nil {
				if err != io.EOF {
					gw.elog.Println("localListen Read:", err)
				}
				break
			}
			//log.Println("<-tcp:", strconv.Quote(string(data[:n])))
			// Uses another quoting since golang's json.Marshal have issues with extended ascii chars
			b, err := json.Marshal(trimQuotedString(strconv.Quote(string(data[:n]))))
			if err != nil {
				gw.elog.Println("localListen Marshal:", err)
				continue
			}
			select {
			case gw.tunnelread <- SerialGWChanData{"input", b}:
			case <-time.After(time.Second):
				break readloop
			}
		}
		select {
		case gw.tunnelread <- SerialGWChanData{"baud", []byte("\"9600\"")}:
		case <-time.After(time.Second):
		}
		terminate()
	}
}

func (gw *serialGW) stop() {
	gw.listener.Close()
	conn := gw.conn
	if conn != nil {
		conn.Close()
	}
	gw.tunnelstop <- struct{}{}
	gw.frontendstop <- struct{}{}
}

func newSerialGW(elog util.EventLog) (*serialGW, error) {
	gw := &serialGW{
		tunnelread:    make(SerialGWChan),
		tunnelwrite:   make(SerialGWChan),
		tunnelstop:    make(chan struct{}),
		frontendread:  make(SerialGWChan),
		frontendwrite: make(SerialGWChan),
		frontendstop:  make(chan struct{}),
		elog:          elog,
	}

	parktunnel := make(chan bool)
	parkfrontend := make(chan bool)

	go route(gw.tunnelread, gw.frontendwrite, parkfrontend, gw.tunnelstop, gw.elog)
	go route(gw.frontendread, gw.tunnelwrite, parktunnel, gw.frontendstop, gw.elog)

	l, err := net.Listen("tcp", "localhost:0")
	if err != nil {
		return nil, err
	}
	gw.listener = l
	gw.port = l.Addr().(*net.TCPAddr).Port
	elog.Println("Listening to", gw.port)
	go gw.localListen(l, parkfrontend, parktunnel)

	return gw, nil
}

func ConnectTunnel(tunnelid string) (SerialGWChan, SerialGWChan, error) {
	sgwsmu.Lock()
	defer sgwsmu.Unlock()
	gw, found := sgws[tunnelid]
	if found {
		return gw.tunnelread, gw.tunnelwrite, nil
	} else {
		return nil, nil, fmt.Errorf("Unable to find tunnel %s", tunnelid)
	}
}

func ConnectFrontend(tunnelid string) (SerialGWChan, SerialGWChan, error) {
	sgwsmu.Lock()
	defer sgwsmu.Unlock()
	gw, found := sgws[tunnelid]
	if found {
		return gw.frontendread, gw.frontendwrite, nil
	} else {
		return nil, nil, fmt.Errorf("Unable to find tunnel %s", tunnelid)
	}
}

func CreateSerialGW(tunnelid string) (int, error) {
	sgwsmu.Lock()
	defer sgwsmu.Unlock()
	gw, found := sgws[tunnelid]
	if found {
		return 0, fmt.Errorf("Unable to create already exisiting tunnel %s", tunnelid)
	} else {
		var err error
		gw, err = newSerialGW(&util.EventLogImpl{Prefix: "serialgw: " + tunnelid})
		if err != nil {
			return 0, err
		}
		sgws[tunnelid] = gw
	}
	return gw.port, nil
}

func DestroySerialGW(tunnelid string) {
	sgwsmu.Lock()
	defer sgwsmu.Unlock()
	gw, found := sgws[tunnelid]
	if !found {
		return
	}
	delete(sgws, tunnelid)
	gw.stop()
}
