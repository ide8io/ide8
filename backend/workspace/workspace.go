package workspace

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/ide8io/ide8/backend/stackdef"
	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/properties/impl"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
	"bitbucket.org/ide8io/ide8/backend/workspace/session"
	"bitbucket.org/ide8io/ide8/backend/workspace/storage"
	"bitbucket.org/ide8io/ide8/backend/wsintapigw/pb"
	"github.com/gorilla/websocket"
)

type Workspace struct {
	wsdir     string
	eventlog  util.EventLog
	sd        *stackdef.StackDef
	stack     *wsintapi.Stack
	sessions  *session.Session
	storage   *storage.Storage
	resources *Resources
}

func New(wsdir string, sd *stackdef.StackDef, stack *wsintapi.Stack) (*Workspace, error) {
	elog := util.EventLogImpl{"ws:"}
	props := impl.New(filepath.Join(filepath.Dir(wsdir), ".properties.json"))
	stor := storage.NewStorage(sd, wsdir)
	ress, err := setupResources(wsdir, stack, stor, props, &elog)
	if err != nil {
		return nil, err
	}
	ws := &Workspace{
		wsdir:     wsdir,
		eventlog:  &elog,
		sd:        sd,
		stack:     stack,
		sessions:  session.CreateUserSession(),
		storage:   stor,
		resources: ress,
	}

	go func() {
		for {
			<-time.After(time.Hour * 24)
			ids := ws.sessions.PurgeChannels(time.Hour * 72)
			for _, id := range ids {
				elog.Println("ch", id, "purged")
			}
		}
	}()

	return ws, nil
}

func (ws *Workspace) NewSession(ctx context.Context, sendCB func(msgType string, id string, payload json.RawMessage)) *Session {
	s := &Session{
		ws:     ws,
		ctx:    ctx,
		sendCB: sendCB,
	}
	s.send = s.Send
	s.done = s.Done
	go func() {
		<-ctx.Done()
		// Remove response functions from active channels
		ws.sessions.Detach(&s.send, &s.done)
	}()
	return s
}

func (ws *Workspace) SetBoards(board *wsintapi.BoardResponse) {
	ws.resources.SetBoards(board, ws.sessions)
}

func (ws *Workspace) Stop() {
	ws.sessions.Destroy()
}

func refreshStats() {

}

func checkOrigin(r *http.Request) bool {
	return true
}

func (ws *Workspace) ServeFiles(w http.ResponseWriter, r *http.Request, path []string) {
	fullpath := filepath.Join(ws.wsdir, filepath.Join(path...))
	s, err := os.Stat(fullpath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	if s.IsDir() {
		nodes, err := ws.sd.GetNodes(fullpath)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}
		b, err := json.Marshal(nodes)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(b)
		return
	}

	if r.Method == "GET" {
		file, err := os.Open(fullpath)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer file.Close()
		io.Copy(w, file)
	}
}

func (ws *Workspace) ServeStackShares(w http.ResponseWriter, r *http.Request, path []string) {
	p := string(os.PathSeparator) + filepath.Join(path...)
	for _, s := range ws.sd.Shares {
		if strings.HasPrefix(p, s) {
			http.ServeFile(w, r, p)
			return
		}
	}
	http.Error(w, "Invalid stack path: "+p, http.StatusNotFound)
}

func (ws *Workspace) ServeExamples(w http.ResponseWriter, r *http.Request, path []string) {
	l, err := ws.sd.LookupExample(path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	nodes, err := l.GetNodes()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if nodes != nil {
		b, err := json.Marshal(nodes)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(b)
		return
	}

	fpath := l.GetPath()
	if fpath != "" {
		f, err := os.Open(fpath)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer f.Close()
		io.Copy(w, f)
		return
	}

	http.Error(w, "invalid node", http.StatusInternalServerError)
}

var upgrader = websocket.Upgrader{CheckOrigin: checkOrigin}

func (ws *Workspace) ServeUserShare(w http.ResponseWriter, r *http.Request, path []string) {
	res, err := ws.resources.GetWWW()
	if err != nil {
		http.Error(w, "no web server associated for workspace", http.StatusNotFound)
		return
	}

	res.Serve(w, r, path, true, upgrader)
}

func (ws *Workspace) ConnectTunnel(ctx context.Context, tunnelid string) (*resource.TunnelClient, error) {
	return ws.resources.ConnectTunnel(ctx, tunnelid)
}

func (ws *Workspace) ServeProjects(w http.ResponseWriter, r *http.Request, path []string) {
	projs := ws.storage.ListProjects()
	b, err := json.Marshal(projs)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(b)
}
