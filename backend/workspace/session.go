package workspace

import (
	"context"
	"encoding/json"

	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel/cmd"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel/gdb"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel/pty"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel/remote"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel/resources"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel/serial"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel/ttn"
)

type Session struct {
	ws     *Workspace
	ctx    context.Context
	sendCB func(msgType string, id string, payload json.RawMessage)
	send   func(id string, action string, payload json.RawMessage, elog util.EventLog) bool
	done   func(id string, errmsg string, elog util.EventLog)
}

func (s *Session) Input(msgType string, id string, payload json.RawMessage) {
	action, variant := util.SplitMsgType(msgType)
	if action == "start" {
		if s.ws.sessions.HasChannel(id) {
			s.Done(id, "Channel id already in use", s.ws.eventlog)
			return
		}
		cs := channel.CreateSession(id, variant, &s.send, &s.done, s.ws.eventlog)
		cs.ELog.Println("start")
		switch variant {
		case "cmd":
			envs := s.ws.sessions.GetEnvs()
			ch, err := cmd.StartCmd(payload, cs.Output, cs.Done, s.ws.storage, cs.ELog, envs)
			if err != nil {
				s.Done(id, err.Error(), cs.ELog)
				return
			}
			cs.Channel = ch
		case "pty":
			envs := s.ws.sessions.GetEnvs()
			ch := pty.StartBashPty(payload, cs.Output, cs.Done, cs.ELog, s.ws.wsdir, envs)
			if ch == nil {
				s.Done(id, "Failed to start pty", cs.ELog)
				return
			}
			cs.Channel = ch
		case "ser":
			ch := serial.StartSer(payload, cs.Output, cs.Done, cs.ELog)
			if ch == nil {
				s.Done(id, "Failed to start ser", cs.ELog)
				return
			}
			cs.Channel = ch
		case "remote":
			ch := remote.StartRemote(payload, cs.Output, cs.Done, cs.ELog, s.ws.resources)
			if ch == nil {
				s.Done(id, "Failed to start remote", cs.ELog)
				return
			}
			cs.Channel = ch
		case "resources":
			ch := resources.StartResourcesListen(payload, cs.Output, cs.Done, s.ws.resources)
			if ch == nil {
				s.Done(id, "Failed to start resources listen", cs.ELog)
				return
			}
			cs.Channel = ch
		case "ttn":
			tr, err := s.ws.resources.GetTTN()
			if err != nil {
				s.Done(id, err.Error(), cs.ELog)
				return
			}
			ch := ttn.StartTTN(payload, cs.Output, cs.Done, cs.ELog, tr)
			if ch == nil {
				s.Done(id, "Failed to start ttn", cs.ELog)
				return
			}
			cs.Channel = ch
		case "gdb":
			ch, err := gdb.StartGDB(payload, cs.Output, cs.Done, cs.ELog, s.ws.resources)
			if err != nil {
				s.Done(id, "Failed to start ttn: " + err.Error(), cs.ELog)
				return
			}
			cs.Channel = ch
		default:
			s.Done(id, "Unknown message type variant: "+variant, cs.ELog)
			return
		}
		s.ws.sessions.SetChannelSession(id, cs)
		refreshStats()
		return
	}

	cs := s.ws.sessions.GetChannelSession(id)
	if cs == nil {
		s.Done(id, "Channel not found: "+id, s.ws.eventlog)
		return
	}

	// Re-connect any previously running channel
	if action == "sync" {
		cs.Attach(&s.send, &s.done)
		refreshStats()
		return
	}

	if action == "stop" {
		cs.ELog.Println("stop")
		cs.Stop()
		s.ws.sessions.DeleteChannelSession(id)
		return
	}

	// Variant is the action for data messages
	cs.Input(variant, payload)
}

func (s *Session) Send(id string, action string, payload json.RawMessage, elog util.EventLog) bool {
	s.sendCB(action, id, payload)
	return true
}

func (s *Session) Done(id string, errmsg string, elog util.EventLog) {
	if errmsg == "" {
		elog.Println("done")
	} else {
		elog.Println("done w/error:", errmsg)
	}
	b := []byte{}
	if len(errmsg) > 0 {
		b, _ = json.Marshal(map[string]string{"error": errmsg})
	}
	s.sendCB("done", id, b)
	s.ws.sessions.DeleteChannelSession(id)
}
