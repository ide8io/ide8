package storage

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"

	"bitbucket.org/ide8io/ide8/backend/stackdef"
)

type Project struct {
	path  string
	files map[string]string
	cmds  []stackdef.Command
}

func (p *Project) GetDir() string {
	return p.path
}

func (p *Project) GetFile(id string) (string, error) {
	file, ok := p.files[id]
	if !ok {
		return "", fmt.Errorf("no project file for id: %v", id)
	}
	return filepath.Join(p.path, file), nil
}

func (p *Project) GetCommand(name string) (string, error) {
	for _, cmd := range p.cmds {
		if cmd.Name == name {
			cmdline := os.Expand(cmd.CmdLine, func(env string) string {
				file, ok := p.files[env]
				if !ok {
					return ""
				}
				files, err := ioutil.ReadDir(p.path)
				if err != nil {
					return ""
				}
				for _, f := range files {
					matched, err := regexp.MatchString(file, f.Name())
					if err != nil {
						break
					}
					if matched {
						return f.Name()
					}
				}
				return file
			})
			return cmdline, nil
		}
	}
	return "", fmt.Errorf("no command named: %v", name)
}
