package storage

import (
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"regexp"
	"strings"
	"sync"

	"bitbucket.org/ide8io/ide8/backend/stackdef"
)

type Storage struct {
	sd       *stackdef.StackDef
	wsdir    string
	mu       sync.Mutex
	projects map[string]*Project
}

func NewStorage(sd *stackdef.StackDef, wsdir string) *Storage {
	stor := &Storage{
		sd:       sd,
		wsdir:    wsdir,
		projects: make(map[string]*Project),
	}
	stor.Refresh()
	return stor
}

func (s *Storage) Refresh() error {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.projects = make(map[string]*Project)
	var find func(string) error
	find = func(path string) error {
		files, err := ioutil.ReadDir(filepath.Join(s.wsdir, path))
		if err != nil {
			return err
		}
		for _, entry := range files {
			if !entry.IsDir() {
				pd := s.detectProject(entry.Name())
				if pd != nil {
					s.projects[path] = &Project{path, pd.FileMap, pd.Commands}
					break
				}
			}
		}
		for _, entry := range files {
			if entry.IsDir() {
				err := find(filepath.Join(path, entry.Name()))
				if err != nil {
					log.Println(err)
				}
			}
		}
		return nil
	}
	return find("")
}

func (s *Storage) detectProject(name string) *stackdef.Project {
	for _, projdef := range s.sd.Projects {
		matched, err := regexp.MatchString(projdef.FileMatch, name)
		if err != nil {
			log.Println(err)
			continue
		}
		if !matched {
			continue
		}
		return &projdef
	}
	return nil
}

func (s *Storage) FindProject(name string) (*Project, error) {
	s.Refresh()
	s.mu.Lock()
	defer s.mu.Unlock()
	proj, ok := s.projects[strings.TrimRight(name, "/")]
	if !ok {
		return nil, fmt.Errorf("no project named: %v", name)
	}
	return proj, nil
}

func (s *Storage) FindExample(name string) (*Project, error) {
	path := strings.Split(strings.TrimRight(name, "/"), "/")
	fspath := []string{}
	var findEx func([]stackdef.ExampleGroup, []stackdef.Example, []string) (*Project, error)
	findEx = func(groups []stackdef.ExampleGroup, examples []stackdef.Example, path []string) (*Project, error) {
		if len(path) >= 2 {
			for _, g := range groups {
				if path[0] == g.Name {
					fspath = append(fspath, g.Path)
					return findEx(g.Groups, g.Examples, path[1:])
				}
			}
		}
		for _, ex := range examples {
			if path[0] == ex.Name {
				fspath = append(fspath, ex.Path)
				return &Project{
					path: filepath.Join(fspath...),
					cmds: ex.Commands,
				}, nil
			}
		}
		return nil, fmt.Errorf("invalid example path: %v", name)
	}
	return findEx(s.sd.Examples.Groups, []stackdef.Example{}, path)
}

// Resolve a full path into an exported repo/path
func (s *Storage) ResolveFullname(fullname string) (string, string, error) {
	if strings.HasPrefix(fullname, s.wsdir) {
		path := strings.TrimLeft(strings.TrimPrefix(fullname, s.wsdir), "/")
		return "workspace", path, nil
	}
	for _, share := range s.sd.Shares {
		if strings.HasPrefix(fullname, share) {
			return "stack", fullname, nil
		}
	}
	return "", "", nil
}

func (s *Storage) ListProjects() []string {
	projs := []string{}
	s.mu.Lock()
	defer s.mu.Unlock()
	for p := range s.projects {
		projs = append(projs, p)
	}
	return projs
}
