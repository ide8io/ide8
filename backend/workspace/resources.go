package workspace

import (
	"context"
	"fmt"
	"log"
	"path/filepath"
	"strings"
	"sync"

	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/properties"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource/gdb"
	kvdbres "bitbucket.org/ide8io/ide8/backend/workspace/resource/kvdb"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource/pty"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource/remote"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource/serial"
	ttnres "bitbucket.org/ide8io/ide8/backend/workspace/resource/ttn"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource/www"
	"bitbucket.org/ide8io/ide8/backend/workspace/storage"
	"bitbucket.org/ide8io/ide8/backend/wsintapigw/pb"
)

type Resources struct {
	resource.Resources
	mu       sync.Mutex
	wsResMap map[string]resource.Resource
	boardMap map[string]*boardResources
	subs     []chan []resource.Descriptor
}

type boardResources struct {
	resMap map[string]resource.Resource
}

func setupResources(wsdir string, stack *wsintapi.Stack, storage *storage.Storage, props properties.Properties, elog util.EventLog) (*Resources, error) {
	ress := &Resources{
		wsResMap: make(map[string]resource.Resource),
		boardMap: make(map[string]*boardResources),
	}
	var setupRes func(resName string) (resource.Resource, error)
	setupRes = func(resName string) (resource.Resource, error) {
		sres, ok := stack.Resources[resName]
		if !ok {
			return nil, fmt.Errorf("resource %v not enabled in stack", resName)
		}
		r, ok := ress.wsResMap[resName]
		if ok {
			return r, nil
		}
		var res resource.Resource
		var err error
		switch resName {
		case "pty":
			res = &pty.PtyResource{Desc: resource.BaseDescriptor{Class: resName, Name: "BASH Terminal"}}
		case "ttn":
			dbres, err := setupRes("kvdb")
			if err != nil {
				return nil, err
			}
			db, ok := dbres.(*kvdbres.KVDBResource)
			if !ok {
				return nil, fmt.Errorf("can't find kvdb resorce for www")
			}
			res, err = ttnres.Create(db, props, elog)
		case "kvdb":
			filename := filepath.Join(filepath.Dir(wsdir), ".kvdb.db")
			res, err = kvdbres.Create(filename, elog)
		case "www":
			dbres, err := setupRes("kvdb")
			if err != nil {
				return nil, err
			}
			db, ok := dbres.(*kvdbres.KVDBResource)
			if !ok {
				return nil, fmt.Errorf("can't find kvdb resorce for www")
			}
			url := "/usershare/"
			res, err = www.Create(url, wsdir, db, elog)
		case "gdb":
			log.Println("stack resoruce:", sres)
			res, err = gdb.New(ress, storage, elog)
		default:
			return nil, fmt.Errorf("unknown resource: %v", resName)
		}
		if res != nil {
			ress.wsResMap[resName] = res
		}
		return res, err
	}

	for res := range stack.Resources {
		_, err := setupRes(res)
		if err != nil {
			return nil, err
		}
	}
	return ress, nil
}

func (r *Resources) getResListUnsafe() []resource.Descriptor {
	descs := []resource.Descriptor{}
	for _, v := range r.wsResMap {
		descs = append(descs, v.Descriptor())
	}
	for _, b := range r.boardMap {
		for _, v := range b.resMap {
			descs = append(descs, v.Descriptor())
		}
	}
	return descs
}

func (r *Resources) Subscribe() chan []resource.Descriptor {
	sub := make(chan []resource.Descriptor)
	r.mu.Lock()
	defer r.mu.Unlock()
	r.subs = append(r.subs, sub)
	desclist := r.getResListUnsafe()
	go func() {
		// Push a current state off context to avoid deadlock
		sub <- desclist
	}()
	return sub
}

func (r *Resources) UnSubscribe(sub chan []resource.Descriptor) {
	r.mu.Lock()
	defer r.mu.Unlock()
	for i := range r.subs {
		if r.subs[i] == sub {
			r.subs = append(r.subs[:i], r.subs[i+1:]...)
			close(sub)
			return
		}
	}
}

func (r *Resources) setupBoardResource(
	boardname string,
	resid string,
	res *wsintapi.BoardResponse_Board_Resource,
	envs resource.Envs,
) (resource.Resource, error) {
	switch res.Class {
	case "remote":
		return remote.New(boardname, res.Name, resid, resid)
	case "serial":
		return serial.New(boardname, res, resid, envs)
	}
	return nil, fmt.Errorf("invalid resource class: %v", res.Class)
}

func (r *Resources) SetBoards(boardresp *wsintapi.BoardResponse, envs resource.Envs) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	updateBoardsMap := map[string]*boardResources{}
	for id, board := range boardresp.Boards {
		br, found := r.boardMap[id]
		if found {
			delete(r.boardMap, id)
		} else {
			br = &boardResources{map[string]resource.Resource{}}
		}
		updateResMap := map[string]resource.Resource{}
		for rid, res := range board.Resources {
			foundRes := br.resMap[rid]
			if foundRes != nil {
				// TODO: handle update
				foundRes.UpdateStatus(res)
				delete(br.resMap, rid)
				updateResMap[rid] = foundRes
			} else {
				log.Println("creating resource:", rid, res)
				newres, err := r.setupBoardResource(id, rid, res, envs)
				if err != nil {
					log.Print("Failed to create resource:", err)
					continue
				}
				updateResMap[rid] = newres
			}
		}
		for rid, res := range br.resMap {
			log.Println("destroying resource:", rid, res)
			res.Destroy()
		}
		br.resMap = updateResMap
		updateBoardsMap[id] = br
	}
	for bid, br := range r.boardMap {
		log.Println("destroying board:", bid, br)
		for rid, res := range br.resMap {
			log.Println("destroying resource:", rid, res)
			res.Destroy()
		}
	}
	r.boardMap = updateBoardsMap
	desclist := r.getResListUnsafe()
	for i := range r.subs {
		r.subs[i] <- desclist
	}
	return nil
}

func (r *Resources) Get(name string) (resource.Resource, error) {
	res, ok := r.wsResMap[name]
	if ok {
		return res, nil
	}
	arr := strings.SplitN(name, "/", 3)
	if len(arr) == 3 {
		bid := arr[0] + "/" + arr[1]
		rid := arr[2]
		b, ok := r.boardMap[bid]
		if ok {
			res, ok := b.resMap[rid]
			if ok {
				return res, nil
			}
		}
	}
	return nil, fmt.Errorf("no resource named %v", name)
}

func (r *Resources) GetByBoardTunnel(board string, tunnelid string) (resource.Resource, error) {
	b, ok := r.boardMap[board]
	if !ok {
		return nil, fmt.Errorf("no board named %v", board)
	}
	res, ok := b.resMap[tunnelid]
	if !ok {
		return nil, fmt.Errorf("no board resource named %v", tunnelid)
	}
	return res, nil
}

func (r *Resources) GetByBoardName(board string, name string) (resource.Resource, error) {
	b, ok := r.boardMap[board]
	if !ok {
		return nil, fmt.Errorf("no board named %v", board)
	}
	for _, res := range b.resMap {
		if res.Descriptor().GetName() == name {
			return res, nil
		}
	}
	return nil, fmt.Errorf("no board resource named %v", name)
}

func (r *Resources) GetTTN() (*ttnres.TTNResource, error) {
	res, err := r.Get("ttn")
	if err != nil {
		return nil, err
	}
	tr, ok := res.(*ttnres.TTNResource)
	if !ok {
		return nil, fmt.Errorf("TTN resource casting failed")
	}
	return tr, nil
}

func (r *Resources) GetWWW() (*www.WWWResource, error) {
	res, err := r.Get("www")
	if err != nil {
		return nil, err
	}
	tr, ok := res.(*www.WWWResource)
	if !ok {
		return nil, fmt.Errorf("WWW resource casting failed")
	}
	return tr, nil
}

func (r *Resources) ConnectTunnel(ctx context.Context, tunnelid string) (*resource.TunnelClient, error) {
	for _, res := range r.wsResMap {
		tc, err := res.TunnelConnect(ctx, tunnelid)
		if err != nil {
			return nil, err
		}
		if tc == nil {
			continue
		}
		return tc, nil
	}
	for _, b := range r.boardMap {
		for _, res := range b.resMap {
			tc, err := res.TunnelConnect(ctx, tunnelid)
			if err != nil {
				return nil, err
			}
			if tc == nil {
				continue
			}
			return tc, nil
		}

	}
	return nil, fmt.Errorf("no such tunnel: %v", tunnelid)
}
