package resource

import (
	"context"

	"bitbucket.org/ide8io/ide8/backend/wsintapigw/pb"
)

type Descriptor interface {
	GetName() string
}

type BaseDescriptor struct {
	Class        string                 `json:"class"`
	Name         string                 `json:"name"`
	Detail       string                 `json:"detail"`
	Capabilities map[string]interface{} `json:"capabilities"`
}

func (r BaseDescriptor) GetName() string {
	return r.Name
}

type Resource interface {
	UpdateStatus(resource *wsintapi.BoardResponse_Board_Resource)
	Descriptor() Descriptor
	TunnelConnect(ctx context.Context, tunnelid string) (*TunnelClient, error)
	Destroy()
}

type BaseResource struct {
}

func (r *BaseResource) UpdateStatus(resource *wsintapi.BoardResponse_Board_Resource) {
}

func (r *BaseResource) TunnelConnect(ctx context.Context, tunnelid string) (*TunnelClient, error) {
	// Will not match any tunnel
	return nil, nil
}

func (r *BaseResource) Destroy() {
}

type Resources interface {
	Get(name string) (Resource, error)
	GetByBoardTunnel(boardid string, tunnelid string) (Resource, error)
	GetByBoardName(boardid string, name string) (Resource, error)
	Subscribe() chan []Descriptor
	UnSubscribe(chan []Descriptor)
}

type Envs interface {
	SetEnv(key string, value string)
}
