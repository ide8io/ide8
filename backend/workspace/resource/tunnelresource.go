package resource

import (
	"context"
	"fmt"
	"log"
	"sync"

	"github.com/golang/protobuf/ptypes/any"
)

type TunnelResource struct {
	ctx         context.Context
	cancel      context.CancelFunc
	mu          sync.Mutex
	connected   bool
	tunnelId    string
	TunnelInCh  chan *any.Any
	tunnelOutCh chan *any.Any
	ConnectCh   chan bool
}

func NewTunnelResource(ctx context.Context, tunnelid string) TunnelResource {
	r := TunnelResource{
		tunnelId:    tunnelid,
		TunnelInCh:  make(chan *any.Any),
		tunnelOutCh: make(chan *any.Any),
		ConnectCh:   make(chan bool),
	}
	r.ctx, r.cancel = context.WithCancel(ctx)
	return r
}

func (r *TunnelResource) GetContext() context.Context {
	return r.ctx
}

func (r *TunnelResource) GetTunnelID() string {
	return r.tunnelId
}

func (r *TunnelResource) TunnelConnect(ctx context.Context, tunnelid string) (*TunnelClient, error) {
	if tunnelid != r.tunnelId {
		return nil, nil
	}

	r.mu.Lock()
	defer r.mu.Unlock()
	if r.connected {
		return nil, fmt.Errorf("tunnel busy: %v", tunnelid)
	}
	r.connected = true

	select {
	case <-r.ctx.Done():
		return nil, fmt.Errorf("resource closed")
	case r.ConnectCh <- true:
	}

	go func() {
		select {
		case <-r.ctx.Done():
			return
		case <-ctx.Done():
		}
		r.mu.Lock()
		defer r.mu.Unlock()
		r.connected = false
		select {
		case <-r.ctx.Done():
		case r.ConnectCh <- false:
		}
	}()

	done := make(chan struct{})
	go func() {
		select {
		case <-r.ctx.Done():
		case <-ctx.Done():
		}
		log.Println("closing tunnel client")
		close(done)
	}()
	return &TunnelClient{
		done:  done,
		inCh:  r.TunnelInCh,
		outCh: r.tunnelOutCh,
	}, nil
}

func (r *TunnelResource) Send(ctx context.Context, pb *any.Any) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-r.ctx.Done():
		return r.ctx.Err()
	case r.tunnelOutCh <- pb:
		return nil
	}
}

func (r *TunnelResource) Destroy() {
	r.cancel()
}
