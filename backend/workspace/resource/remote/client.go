package remote

import "context"

type Status struct {
	Online  bool   `json:"online"`
	Error   string `json:"error"`
	Running bool   `json:"running"`
}

type Client struct {
	res      *RemoteResource
	ctx      context.Context
	StatusCh <-chan Status
	DataCh   <-chan []byte
}

func (c *Client) Start() {
	c.res.Start(c.ctx)
}

func (c *Client) Stop() {
	c.res.Stop(c.ctx)
}

func (c *Client) Close() {
	go func() {
		for range c.StatusCh {
			// Flush until closed
		}
	}()
	go func() {
		for range c.DataCh {
			// Flush until closed
		}
	}()
	c.res.FrontendDisconnect(c)
}
