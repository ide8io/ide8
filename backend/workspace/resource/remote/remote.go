package remote

import (
	"context"
	"errors"
	"fmt"
	"log"
	"sync"
	"time"

	"bitbucket.org/ide8io/ide8/backend/api/tunnel/remote"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
	"bitbucket.org/ide8io/ide8/backend/wsintapigw/pb"
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes/any"
)

type RemoteResource struct {
	resource.TunnelResource
	boardid         string
	name            string
	detail          string
	status          Status
	statusMu        sync.Mutex
	fclients        []*intClient
	fclientsMu      sync.Mutex
	fwdMap          map[uint16]*Forwarder
	portClientMap   map[uint16]*intPortClient
	portClientMapMu sync.Mutex
}

type remoteDescriptor struct {
	resource.BaseDescriptor
	BoardID  string `json:"boardid"`
	TunnelID string `json:"tunnelid"`
}

func New(boardid string, name string, detail string, tunnelid string) (*RemoteResource, error) {
	r := &RemoteResource{
		TunnelResource: resource.NewTunnelResource(context.Background(), tunnelid),
		boardid:        boardid,
		name:           name,
		detail:         detail,
		status:         Status{Error: "No remote connection"},
		fwdMap:         make(map[uint16]*Forwarder),
		portClientMap:  make(map[uint16]*intPortClient),
	}
	ctx := r.GetContext()
	var err error
	r.fwdMap[3333], err = NewForwarder(ctx, r, 3333)
	if err != nil {
		return nil, err
	}
	r.fwdMap[4444], err = NewForwarder(ctx, r, 4444)
	if err != nil {
		return nil, err
	}
	go r.handler()

	return r, nil
}

func (r *RemoteResource) UpdateStatus(resource *wsintapi.BoardResponse_Board_Resource) {

}

func (r *RemoteResource) Descriptor() resource.Descriptor {
	caps := map[string]interface{}{}
	caps["remoteexec"] = nil
	caps["gdbserver"] = struct {
		Port uint16 `json:"port"`
	}{
		Port: 3333,
	}
	caps["telnet"] = struct {
		Port uint16 `json:"port"`
	}{
		Port: 4444,
	}
	return &remoteDescriptor{
		resource.BaseDescriptor{
			Class:        "remoteexec",
			Name:         r.boardid + "/" + r.name,
			Detail:       r.detail,
			Capabilities: caps,
		},
		r.boardid,
		r.GetTunnelID(),
	}
}

func (r *RemoteResource) broadcastStatus(status Status) {
	r.fclientsMu.Lock()
	defer r.fclientsMu.Unlock()
	for _, ic := range r.fclients {
		select {
		case ic.statusCh <- status:
		default:
		}
	}
}

func (r *RemoteResource) broadcastData(data []byte) {
	r.fclientsMu.Lock()
	defer r.fclientsMu.Unlock()
	for _, ic := range r.fclients {
		select {
		case ic.dataCh <- data:
		default:
		}
	}
}

func (r *RemoteResource) handler() {
	for {
		select {
		case <-r.GetContext().Done():
			return
		case connected := <-r.ConnectCh:
			r.statusMu.Lock()
			r.status.Online = connected
			if connected {
				r.status.Error = ""
			} else {
				r.status.Error = "Remote disconnected"
			}
			s := r.status
			r.statusMu.Unlock()
			r.broadcastStatus(s)
		case msg := <-r.TunnelInCh:
			switch msg.TypeUrl {
			case proto.MessageName(&api_tunnel_remote.Status{}):
				var status api_tunnel_remote.Status
				err := proto.Unmarshal(msg.Value, &status)
				if err != nil {
					log.Println("remote error:", err)
				} else {
					s := Status{Online: true, Running: status.Running, Error: status.Error}
					r.statusMu.Lock()
					r.status = s
					r.statusMu.Unlock()
					r.broadcastStatus(s)
				}
			case proto.MessageName(&api_tunnel_remote.Data{}):
				var data api_tunnel_remote.Data
				err := proto.Unmarshal(msg.Value, &data)
				if err != nil {
					log.Println("remote error:", err)
				} else {
					r.broadcastData(data.Payload)
				}
			case proto.MessageName(&api_tunnel_remote.ForwardStatus{}):
				var status api_tunnel_remote.ForwardStatus
				err := proto.Unmarshal(msg.Value, &status)
				if err != nil {
					log.Println("remote error:", err)
					continue
				}
				log.Println("remote fwd status:", status)
				r.portClientMapMu.Lock()
				for _, fwd := range status.ForwardList {
					client, ok := r.portClientMap[uint16(fwd.Port)]
					if ok {
						client.remoteStatusCh <- fwd.Open
					}
				}
				r.portClientMapMu.Unlock()
			case proto.MessageName(&api_tunnel_remote.ForwardData{}):
				var data api_tunnel_remote.ForwardData
				err := proto.Unmarshal(msg.Value, &data)
				if err != nil {
					log.Println("remote error:", err)
					continue
				}
				r.portClientMapMu.Lock()
				client, ok := r.portClientMap[uint16(data.Port)]
				if ok {
					client.remoteInCh <- data.Data
				}
				r.portClientMapMu.Unlock()
			default:
				log.Println("unknown message type:", msg.TypeUrl)
			}
		}
	}
}

type intClient struct {
	statusCh chan Status
	dataCh   chan []byte
}

func (r *RemoteResource) FrontendConnect() *Client {
	log.Println("remote: connecting frontend")
	ic := &intClient{
		statusCh: make(chan Status, 1),
		dataCh:   make(chan []byte, 1),
	}
	c := &Client{
		res:      r,
		ctx:      context.Background(),
		StatusCh: ic.statusCh,
		DataCh:   ic.dataCh,
	}
	go func() {
		r.statusMu.Lock()
		s := r.status
		r.statusMu.Unlock()
		ic.statusCh <- s
	}()
	r.fclientsMu.Lock()
	r.fclients = append(r.fclients, ic)
	r.fclientsMu.Unlock()
	return c
}

func (r *RemoteResource) FrontendDisconnect(client *Client) {
	log.Println("remote: disconnecting frontend")
	r.fclientsMu.Lock()
	defer r.fclientsMu.Unlock()
	for i, ic := range r.fclients {
		if ic.statusCh == client.StatusCh {
			r.fclients = append(r.fclients[:i], r.fclients[i+1:]...)
			close(ic.statusCh)
			close(ic.dataCh)
			return
		}
	}
}

var RemoteNotConnected = errors.New("remote resource not connected")
var RemotePortBusy = errors.New("remote resource port is busy")

type intPortClient struct {
	remoteStatusCh chan bool
	remoteInCh     chan []byte
}

func (r *RemoteResource) PortConnect(ctx context.Context, port uint16) (*PortClient, error) {
	r.statusMu.Lock()
	online := r.status.Online
	r.statusMu.Unlock()
	if !online {
		return nil, RemoteNotConnected
	}
	r.portClientMapMu.Lock()
	_, ok := r.portClientMap[port]
	if ok {
		r.portClientMapMu.Unlock()
		return nil, RemotePortBusy
	}
	ipc := &intPortClient{
		make(chan bool),
		make(chan []byte),
	}
	r.portClientMap[port] = ipc
	r.portClientMapMu.Unlock()

	removeClient := func() {
		go func() {
			for range ipc.remoteStatusCh {
				// Flush any incoming status
			}
		}()
		go func() {
			for range ipc.remoteInCh {
				// Flush any incoming data
			}
		}()
		r.portClientMapMu.Lock()
		close(ipc.remoteStatusCh)
		close(ipc.remoteInCh)
		delete(r.portClientMap, port)
		r.portClientMapMu.Unlock()
	}

	err := r.send(ctx, &api_tunnel_remote.ForwardOpen{Port: int32(port)})
	if err != nil {
		removeClient()
		return nil, err
	}
loop:
	for {
		select {
		case <-ctx.Done():
			err = ctx.Err()
			break loop
		case ok := <-ipc.remoteStatusCh:
			if ok {
				err = nil
			} else {
				err = fmt.Errorf("failed to connect remote port")
			}
			break loop
		case <-ipc.remoteInCh: // Flush out any pre-connect data
		}
	}
	if err != nil {
		removeClient()
		return nil, err
	}
	pc := &PortClient{
		remres:     r,
		port:       port,
		statusCh:   ipc.remoteStatusCh,
		remoteInCh: ipc.remoteInCh,
	}
	return pc, nil
}

func (r *RemoteResource) PortDisconnect(c *PortClient) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*1)
	defer cancel()
	r.send(ctx, &api_tunnel_remote.ForwardClose{Port: int32(c.port)})
	go func() {
		for range c.statusCh {
			// Flush any incoming status
		}
	}()
	go func() {
		for range c.remoteInCh {
			// Flush any incoming data
		}
	}()
	r.portClientMapMu.Lock()
	defer r.portClientMapMu.Unlock()
	for port, client := range r.portClientMap {
		if client.remoteStatusCh == c.statusCh {
			close(client.remoteStatusCh)
			close(client.remoteInCh)
			delete(r.portClientMap, port)
			return
		}
	}
}

func (r *RemoteResource) send(ctx context.Context, pb proto.Message) error {
	b, err := proto.Marshal(pb)
	if err != nil {
		return err
	}
	a := &any.Any{
		TypeUrl: proto.MessageName(pb),
		Value:   b,
	}
	c, cancel := context.WithTimeout(ctx, time.Second*60)
	defer cancel()
	return r.Send(c, a)
}

func (r *RemoteResource) Start(ctx context.Context) error {
	return r.send(ctx, &api_tunnel_remote.Start{
		Arguments: "-c \"interface jlink\" -c \"transport select swd\" -f target/nrf52.cfg",
	})
}

func (r *RemoteResource) Stop(ctx context.Context) error {
	return r.send(ctx, &api_tunnel_remote.Stop{})
}

func (r *RemoteResource) SendPort(ctx context.Context, port uint16, data []byte) error {
	return r.send(ctx, &api_tunnel_remote.ForwardData{
		Port: int32(port),
		Data: data,
	})
}
