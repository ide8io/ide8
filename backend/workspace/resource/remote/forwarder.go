package remote

import (
	"context"
	"io"
	"log"
	"net"
	"strconv"
	"time"

	"bitbucket.org/ide8io/ide8/backend/util"
)

type Forwarder struct {
	remres   *RemoteResource
	port     uint16
	listener net.Listener
	ctx      context.Context
}

func NewForwarder(ctx context.Context, remres *RemoteResource, port uint16) (*Forwarder, error) {
	l, err := net.Listen("tcp", "localhost:"+strconv.Itoa(int(port)))
	if err != nil {
		return nil, err
	}
	f := &Forwarder{
		remres:   remres,
		port:     port,
		listener: l,
		ctx:      ctx,
	}
	go func() {
		<-f.ctx.Done()
		l.Close()
	}()
	go f.handler()
	return f, nil
}

func (f *Forwarder) handler() {
	var startTime time.Time
	for {
		conn, err := f.listener.Accept()
		if err != nil {
			log.Println("listen:", err)
			break
		}
		log.Println("Accepted connection from", conn.RemoteAddr())
		util.RelaxReconnect(f.ctx, startTime, time.Second*10)
		connctx, cancel := context.WithTimeout(f.ctx, time.Second*10)
		defer cancel()
		client, err := f.remres.PortConnect(connctx, f.port)
		if err != nil {
			log.Println("forwarder.handler: PortConnect:", err)
			conn.Close()
			continue
		}

		go func() {
			for {
				data, err := client.Recv(f.ctx)
				if err != nil {
					log.Println("forwarder.handler: client.Recv:", err)
					break
				}
				_, err = conn.Write(data)
				if (err != nil) && (err != io.EOF) {
					log.Println("forwarder.handler: conn.Write:", err)
				}
			}
			conn.Close()
		}()

		data := make([]byte, 1024)
		for {
			n, err := conn.Read(data)
			if err != nil {
				if err != io.EOF {
					log.Println("forwarder.handler: conn.Read:", err)
				}
				break
			}
			err = client.Send(f.ctx, data[:n])
			if err != nil {
				log.Println("forwarder.handler: client.Send", err)
			}
		}
		client.Close()
	}
}
