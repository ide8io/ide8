package remote

import (
	"context"
	"fmt"
)

type PortClient struct {
	remres     *RemoteResource
	port       uint16
	statusCh   <-chan bool
	remoteInCh <-chan []byte
}

func (c *PortClient) Send(ctx context.Context, data []byte) error {
	return c.remres.SendPort(ctx, c.port, data)
}

func (c *PortClient) Recv(ctx context.Context) ([]byte, error) {
	for {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		case status, ok := <-c.statusCh:
			if !ok {
				return nil, fmt.Errorf("port client closed")
			}
			if status {
				continue
			}
			return nil, fmt.Errorf("port client closed")
		case data, ok := <-c.remoteInCh:
			if !ok {
				return nil, fmt.Errorf("port client closed")
			}
			return data, nil
		}
	}
}

func (c *PortClient) Close() {
	c.remres.PortDisconnect(c)
}
