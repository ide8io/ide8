package ttn

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/properties"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource/kvdb"
	"github.com/TheThingsNetwork/go-app-sdk"
	"github.com/TheThingsNetwork/ttn/core/types"
	"google.golang.org/grpc/status"
)

const (
	name = "ttn"
)

type connect struct {
	AppID     string
	AccessKey string
}

type TTNStatus struct {
	Connected bool          `json:"connected"`
	ErrMsg    string        `json:"err_msg"`
	AppID     string        `json:"app_id"`
	DevID     string        `json:"dev_id"`
	DevEUI    types.DevEUI  `json:"dev_eui"`
	AppEUI    types.AppEUI  `json:"app_eui"`
	AppKey    *types.AppKey `json:"app_key"`
}

type TTNResource struct {
	resource.BaseResource
	desc       resource.Descriptor
	config     config
	kvdb       *kvdb.KVDBResource
	props      properties.Properties
	elog       util.EventLog
	connectCh  chan connect
	listenCh   chan ttnListen
	unlistenCh chan TTNListen
	listDevsCh chan listDevices
	deviceCh   chan string
	uplinkCh   chan *types.UplinkMessage
	client     ttnsdk.Client
	devices    ttnsdk.DeviceManager
	pubsub     ttnsdk.ApplicationPubSub
	devpubsub  ttnsdk.DevicePubSub
	listeners  []ttnListen
	status     TTNStatus
}

// Extenal read-only channels
type TTNListen struct {
	StatusCh <-chan TTNStatus
	UplinkCh <-chan *types.UplinkMessage
}

// Internal read/write channels
type ttnListen struct {
	statusCh chan TTNStatus
	uplinkCh chan *types.UplinkMessage
}

type TTNDeviceList struct {
	ErrMsg  string            `json:"err_msg"`
	Offset  uint64            `json:"offset"`
	Devices ttnsdk.DeviceList `json:"devices"`
}

type listDevices struct {
	limit   uint64
	offset  uint64
	replyCh chan<- TTNDeviceList
}

type config struct {
	AppID     string `json:"app_id"`
	AccessKey string `json:"access_key"`
	DevId     string `json:"dev_id"`
}

func Create(kvdb *kvdb.KVDBResource, props properties.Properties, elog util.EventLog) (*TTNResource, error) {
	r := &TTNResource{
		desc:       resource.BaseDescriptor{Class: name, Name: "The Things Network"},
		kvdb:       kvdb,
		props:      props,
		elog:       elog,
		connectCh:  make(chan connect),
		listenCh:   make(chan ttnListen),
		unlistenCh: make(chan TTNListen),
		listDevsCh: make(chan listDevices),
		deviceCh:   make(chan string),
		uplinkCh:   make(chan *types.UplinkMessage),
	}
	err := props.Get(name, &r.config)
	if err != nil {
		return nil, err
	}

	go r.handler()

	return r, nil
}

func (r *TTNResource) Descriptor() resource.Descriptor {
	return r.desc
}

func (r *TTNResource) storeConfig() error {
	return r.props.Store(name, r.config)
}

func (r *TTNResource) Connect(appID string, accessKey string) {
	r.connectCh <- connect{appID, accessKey}
}

func (r *TTNResource) Listen() TTNListen {
	l := ttnListen{make(chan TTNStatus), make(chan *types.UplinkMessage)}
	r.listenCh <- l
	return TTNListen{l.statusCh, l.uplinkCh}
}

func (r *TTNResource) UnListen(listen TTNListen) {
	r.unlistenCh <- listen
}

func (r *TTNResource) ListDevices(limit uint64, offset uint64, replyCh chan<- TTNDeviceList) {
	r.listDevsCh <- listDevices{limit, offset, replyCh}
}

func (r *TTNResource) SetDevice(devID string) {
	r.deviceCh <- devID
}

func (r *TTNResource) broadcastStatus() {
	for i := range r.listeners {
		r.listeners[i].statusCh <- r.status
	}
}

func (r *TTNResource) broadcastUplink(msg *types.UplinkMessage) {
	for i := range r.listeners {
		r.listeners[i].uplinkCh <- msg
	}
}

func (r *TTNResource) handler() {
	for {
		select {
		case conn := <-r.connectCh:
			if r.status.Connected && r.status.AppID == conn.AppID {
				continue
			}
			r.connect(conn.AppID, conn.AccessKey)
			r.broadcastStatus()
		case ch := <-r.listenCh:
			r.listeners = append(r.listeners, ch)
			if len(r.listeners) == 1 {
				r.connect(r.config.AppID, r.config.AccessKey)
				r.subscribeDevice(r.config.DevId)
			}
			ch.statusCh <- r.status
		case ch := <-r.unlistenCh:
			for i := range r.listeners {
				if r.listeners[i].statusCh == ch.StatusCh {
					close(r.listeners[i].statusCh)
					r.listeners = append(r.listeners[:i], r.listeners[i+1:]...)
					break
				}
			}
			if len(r.listeners) == 0 {
				if r.client != nil {
					r.client.Close()
					r.client = nil
					r.devices = nil
					r.elog.Println("ttn disconnected from app", r.status.AppID)
				}
			}
		case listdevs := <-r.listDevsCh:
			if r.devices == nil {
				listdevs.replyCh <- TTNDeviceList{ErrMsg: "ttn not connected", Offset: listdevs.offset}
				continue
			}
			devs, err := r.devices.List(listdevs.limit, listdevs.offset)
			if err != nil {
				listdevs.replyCh <- TTNDeviceList{ErrMsg: err.Error(), Offset: listdevs.offset}
				continue
			}
			listdevs.replyCh <- TTNDeviceList{Offset: listdevs.offset, Devices: devs}
		case devID := <-r.deviceCh:
			err := r.subscribeDevice(devID)
			if err != nil {
				r.elog.Println(err)
				r.status.ErrMsg = err.Error()
			}
			r.broadcastStatus()
		case msg := <-r.uplinkCh:
			r.broadcastUplink(msg)
			now := time.Now().UTC().Format(time.RFC3339Nano)
			b, err := json.Marshal(msg)
			if err == nil {
				r.kvdb.Create(now, string(b))
			}
		}
	}
}

func (r *TTNResource) connect(appID string, accessKey string) {
	if r.client != nil {
		r.unsubscribeDevice()
		r.client.Close()
		r.elog.Println("ttn disconnected from app", r.status.AppID)
	}
	r.status.AppID = appID
	r.status.DevID = ""
	if appID == "" {
		r.status.Connected = false
		r.config.AppID = ""
		r.config.AccessKey = ""
		r.config.DevId = ""
		r.storeConfig()
		return
	}
	config := ttnsdk.NewCommunityConfig("ide8-io")
	config.ClientVersion = ""
	r.client = config.NewClient(appID, accessKey)
	var err error
	r.devices, err = r.client.ManageDevices()
	if err != nil {
		r.client.Close()
		r.client = nil
		r.devices = nil
		r.status.Connected = false
		serr, ok := status.FromError(err)
		if ok {
			r.status.ErrMsg = serr.Message()
		} else {
			r.status.ErrMsg = err.Error()
		}
		return
	}
	// Run a single read to provoke possible wrong access key error
	_, err = r.devices.List(1, 0)
	if err != nil {
		r.client.Close()
		r.client = nil
		r.devices = nil
		r.status.Connected = false
		serr, ok := status.FromError(err)
		if ok {
			r.status.ErrMsg = serr.Message()
		} else {
			r.status.ErrMsg = err.Error()
		}
		return
	}
	r.elog.Println("ttn connected to app", appID)
	r.status.Connected = true
	r.status.ErrMsg = ""
	if r.config.AppID != appID || r.config.AccessKey != accessKey {
		r.config.AppID = appID
		r.config.AccessKey = accessKey
		r.config.DevId = ""
		r.storeConfig()
	}
}

func (r *TTNResource) subscribeDevice(devID string) error {
	r.unsubscribeDevice()
	if devID == "" {
		r.status.DevID = ""
		r.config.DevId = ""
		r.storeConfig()
		return nil
	}
	if r.client == nil {
		return fmt.Errorf("not connected")
	}
	device, err := r.devices.Get(devID)
	if err != nil {
		return err
	}
	r.pubsub, err = r.client.PubSub()
	if err != nil {
		return err
	}
	r.devpubsub = r.pubsub.Device(devID)
	uplinkCh, err := r.devpubsub.SubscribeUplink()
	if err != nil {
		r.pubsub.Close()
		r.pubsub = nil
		return err
	}

	r.elog.Println("subscribing to device", devID)

	go func() {
		for msg := range uplinkCh {
			r.uplinkCh <- msg
		}
	}()

	r.status.DevID = devID
	r.status.DevEUI = device.DevEUI
	r.status.AppEUI = device.AppEUI
	r.status.AppKey = device.AppKey
	r.config.DevId = devID
	r.storeConfig()

	return nil
}

func (r *TTNResource) unsubscribeDevice() {
	if r.devpubsub != nil {
		r.devpubsub.UnsubscribeEvents()
		r.devpubsub = nil
		r.elog.Println("unsubscribing from device", r.status.DevID)
	}
	if r.pubsub != nil {
		r.pubsub.Close()
		r.pubsub = nil
	}
}
