package resource

import (
	"errors"

	"github.com/golang/protobuf/ptypes/any"
)

var TunnelClientClosedError = errors.New("TunnelClient closed")

type TunnelClient struct {
	done  <-chan struct{}
	inCh  chan<- *any.Any
	outCh <-chan *any.Any
}

func (tc *TunnelClient) Send(msg *any.Any) error {
	select {
	case <-tc.done:
		return TunnelClientClosedError
	case tc.inCh <- msg:
		return nil
	}
}

func (tc *TunnelClient) Recv() (*any.Any, error) {
	select {
	case <-tc.done:
		return nil, TunnelClientClosedError
	case msg := <-tc.outCh:
		return msg, nil
	}
}
