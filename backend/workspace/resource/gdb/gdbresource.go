package gdb

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource/remote"
	"bitbucket.org/ide8io/ide8/backend/workspace/storage"
	gdb_mi "github.com/cyrus-and/gdb"
	"github.com/kr/pty"
)

type Resource struct {
	resource.BaseResource
	desc        resource.BaseDescriptor
	ress        resource.Resources
	storage     *storage.Storage
	elog        util.EventLog
	status      Status
	listenCh    chan listen
	unlistenCh  chan Listen
	stateCh     chan string
	faultCh     chan string
	runningCh   chan bool
	errorCh     chan string
	outputCh    chan []byte
	notifyCh    chan map[string]interface{}
	brkptsCh    chan []Breakpoint
	stoppedCh   chan Stopped
	startCh     chan Start
	stopCh      chan struct{}
	interruptCh chan struct{}
	cmdCh       chan string
	addbrkptCh  chan Breakpoint
	rembrkptCh  chan Breakpoint
	listeners   []listen
	gdbserver   *remote.RemoteResource
	gdb         *gdb_mi.Gdb
	ptm         *os.File
	pts         *os.File
	brkpts      []intBreakpoint
}

const (
	STOPPED    = "stopped"
	CONNECTING = "connecting"
	LOADING    = "loading"
	STARTED    = "started"
	ERROR      = "error"
)

type Status struct {
	State   string `json:"state"`
	Running bool   `json:"running"`
	Message string `json:"message"`
}

type Start struct {
	Project string `json:"project"`
	Target  string `json:"target"`
}

type Breakpoint struct {
	Repo string `json:"repo"`
	File string `json:"file"`
	Line string `json:"line"`
}

type intBreakpoint struct {
	Breakpoint
	number string
}

type Stopped struct {
	Reason     string     `json:"reason"`
	Address    string     `json:"address"`
	Breakpoint Breakpoint `json:"breakpoint"`
}

func New(ress resource.Resources, storage *storage.Storage, elog util.EventLog) (*Resource, error) {
	r := &Resource{
		desc:        resource.BaseDescriptor{Class: "gdb", Name: "The GNU Debugger"},
		ress:        ress,
		storage:     storage,
		elog:        elog,
		status:      Status{State: STOPPED},
		listenCh:    make(chan listen),
		unlistenCh:  make(chan Listen),
		stateCh:     make(chan string),
		faultCh:     make(chan string),
		runningCh:   make(chan bool),
		errorCh:     make(chan string),
		outputCh:    make(chan []byte),
		notifyCh:    make(chan map[string]interface{}),
		brkptsCh:    make(chan []Breakpoint),
		stoppedCh:   make(chan Stopped),
		startCh:     make(chan Start),
		stopCh:      make(chan struct{}),
		interruptCh: make(chan struct{}),
		cmdCh:       make(chan string),
		addbrkptCh:  make(chan Breakpoint),
		rembrkptCh:  make(chan Breakpoint),
	}

	go r.handlerClients()
	go r.handlerGDB()

	return r, nil
}

func (r *Resource) Descriptor() resource.Descriptor {
	return r.desc
}

type Listen struct {
	StatusCh       <-chan Status
	ErrorCh        <-chan string
	OutputCh       <-chan []byte
	NotificationCh <-chan map[string]interface{}
	BreakpointsCh  <-chan []Breakpoint
	StoppedCh      <-chan Stopped
}

type listen struct {
	statusCh       chan Status
	errorCh        chan string
	outputCh       chan []byte
	notificationCh chan map[string]interface{}
	breakpointsCh  chan []Breakpoint
	stoppedCh      chan Stopped
}

func (r *Resource) Listen() Listen {
	l := listen{
		make(chan Status),
		make(chan string),
		make(chan []byte),
		make(chan map[string]interface{}),
		make(chan []Breakpoint),
		make(chan Stopped),
	}
	r.listenCh <- l
	return Listen{
		l.statusCh,
		l.errorCh,
		l.outputCh,
		l.notificationCh,
		l.breakpointsCh,
		l.stoppedCh,
	}
}

func (r *Resource) UnListen(listen Listen) {
	r.unlistenCh <- listen
}

func (r *Resource) Start(start Start) {
	r.startCh <- start
}

func (r *Resource) Stop() {
	r.stopCh <- struct{}{}
}

func (r *Resource) Interrupt() {
	r.interruptCh <- struct{}{}
}

func (r *Resource) AddBreakpoint(brkpt Breakpoint) {
	r.addbrkptCh <- brkpt
}

func (r *Resource) RemoveBreakpoint(brkpt Breakpoint) {
	r.rembrkptCh <- brkpt
}

func (r *Resource) send(operation string, args ...string) (map[string]interface{}, error) {
	if r.gdb == nil {
		return nil, fmt.Errorf("gdb not running")
	}
	reply, err := r.gdb.Send(operation, args...)
	if err != nil {
		return reply, errors.New(err.Error())
	}
	class := reply["class"]
	if class == "error" {
		payload := reply["payload"]
		if payload != nil {
			payloadMap := payload.(map[string]interface{})
			msg := payloadMap["msg"]
			if msg != nil {
				msgString, ok := msg.(string)
				if ok {
					return reply, errors.New(msgString)
				}
			}
		}
		b, _ := json.Marshal(reply)
		return reply, errors.New(string(b))
	}
	return reply, nil
}

func (r *Resource) sendScript(script []string) error {
	for _, s := range script {
		_, err := r.send("interpreter-exec console \"" + strings.Replace(s, `"`, `\"`, -1) + "\"")
		if err != nil {
			return errors.New(s + ": " + err.Error())
		}
	}
	return nil
}

func (r *Resource) start(project string, target string) {
	if r.gdb != nil {
		return
	}

	proj, err := r.storage.FindProject(project)
	if err != nil {
		r.faultCh <- err.Error()
		return
	}

	filename, err := proj.GetFile("out")
	if err != nil {
		r.faultCh <- err.Error()
		return
	}

	targetArr := strings.SplitN(target, "/", 3)
	if len(targetArr) != 3 {
		r.faultCh <- "invalid target"
		return
	}

	gdbserver, err := r.ress.GetByBoardName(targetArr[0]+"/"+targetArr[1], target)
	if err != nil {
		r.faultCh <- err.Error()
		return
	}
	r.gdbserver = gdbserver.(*remote.RemoteResource)
	if r.gdbserver == nil {
		r.faultCh <- "invalid remote resource"
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	err = r.gdbserver.Start(ctx)
	if err != nil {
		r.faultCh <- err.Error()
		return
	}

	time.Sleep(time.Second)

	ptm, pts, err := pty.Open()
	if err != nil {
		r.faultCh <- err.Error()
		return
	}

	// create GDB command
	cmd := []string{"/usr/local/gcc-arm-none-eabi-4_9-2015q3/bin/arm-none-eabi-gdb", "--nx", "--quiet", "--interpreter=mi2", "--tty", pts.Name()}
	if filename != "" {
		cmd = append(cmd, filename)
	}
	r.gdb, err = gdb_mi.NewCmd(cmd, func(notification map[string]interface{}) {
		ntype := notification["type"]
		if ntype == "exec" {
			class := notification["class"]
			if class == "stopped" {
				r.runningCh <- false
				payload := notification["payload"]
				if payload != nil {
					plmap := payload.(map[string]interface{})
					if plmap != nil {
						frame := plmap["frame"]
						if frame != nil {
							framemap := frame.(map[string]interface{})
							reason, _ := plmap["reason"].(string)
							addr, _ := framemap["addr"].(string)
							fullname, _ := framemap["fullname"].(string)
							repo, file, _ := r.storage.ResolveFullname(fullname)
							line, _ := framemap["line"].(string)
							r.stoppedCh <- Stopped{reason, addr, Breakpoint{repo, file, line}}
						}
					}
				}
			} else if class == "running" {
				r.runningCh <- true
			}
		} else if ntype == "notify" {
			class := notification["class"]
			if class == "breakpoint-modified" {

			}
		}
		r.notifyCh <- notification
	})

	if err != nil {
		ptm.Close()
		pts.Close()
		r.faultCh <- err.Error()
		return
	}

	go func() {
		data := make([]byte, 1024)
		for {
			n, err := ptm.Read(data)
			if err != nil {
				r.elog.Println("gdb read:", err)
				break
			}
			r.outputCh <- data[:n]
		}
	}()

	r.ptm = ptm
	r.pts = pts

	r.stateCh <- CONNECTING

	_, err = r.send("target-select", "remote", "localhost:3333")
	if err != nil {
		r.stop("Connecting: " + err.Error())
		return
	}

	r.stateCh <- LOADING

	err = r.sendScript([]string{
		"monitor reset halt",
		"monitor nrf5 mass_erase",
	})
	if err != nil {
		r.stop(err.Error())
		return
	}

	_, err = r.send("target-download")
	if err != nil {
		r.stop("Loading: " + err.Error())
		return
	}

	brkpts := []intBreakpoint{}
	for _, b := range r.brkpts {
		number, err := r.setBreakpoint(b.Breakpoint)
		if err != nil {
			r.errorCh <- "Failed to set breakpoint: " + err.Error()
			continue
		}
		b.number = number
		brkpts = append(brkpts, b)
	}
	r.brkpts = brkpts
	r.publishBreakpoints()

	err = r.sendScript([]string{
		"monitor reset init",
	})
	if err != nil {
		r.stop(err.Error())
		return
	}

	r.stateCh <- STARTED
}

func (r *Resource) stop(errmsg string) {
	if r.gdb != nil {
		go r.gdb.Exit()
		r.gdb = nil
	}
	if r.ptm != nil {
		r.ptm.Close()
		r.ptm = nil
	}
	if r.pts != nil {
		r.pts.Close()
		r.pts = nil
	}

	if r.gdbserver != nil {
		r.gdbserver.Stop(context.Background())
		r.gdbserver = nil
	}

	if errmsg == "" {
		r.stateCh <- STOPPED
	} else {
		r.faultCh <- errmsg
	}
}

func (r *Resource) Command(cmd string) {
	r.cmdCh <- cmd
}

func (r *Resource) ConsoleCommand(cmd string) {
	r.cmdCh <- "interpreter-exec console \"" + strings.Replace(cmd, `"`, `\"`, -1) + "\""
}

func (r *Resource) command(cmd string) {
	reply, err := r.send(cmd)
	if err != nil {
		r.errorCh <- err.Error()
	} else {
		b, _ := json.Marshal(reply)
		r.outputCh <- b
	}
}

func (r *Resource) setBreakpoint(brkpt Breakpoint) (string, error) {
	reply, err := r.send("break-insert " + brkpt.File + ":" + brkpt.Line)
	if err != nil {
		return "", err
	}
	payload := reply["payload"]
	if payload == nil {
		return "", fmt.Errorf("no payload field in reply")
	}
	plmap := payload.(map[string]interface{})
	if plmap == nil {
		return "", fmt.Errorf("payload field not an object")
	}
	bkpt := plmap["bkpt"]
	if bkpt == nil {
		return "", fmt.Errorf("no breakpoint field in reply")
	}
	bkptmap := bkpt.(map[string]interface{})
	if bkptmap == nil {
		return "", fmt.Errorf("breakpoint field not an object")
	}
	number := bkptmap["number"]
	if bkpt == nil {
		return "", fmt.Errorf("no number field in reply")
	}
	numberstr, ok := number.(string)
	if !ok {
		return "", fmt.Errorf("breakpoint number not a string")
	}
	return numberstr, nil
}

func (r *Resource) publishBreakpoints() {
	var brkpts []Breakpoint
	for _, b := range r.brkpts {
		brkpts = append(brkpts, b.Breakpoint)
	}
	r.brkptsCh <- brkpts
}

func (r *Resource) handlerClients() {
	broadcastStatus := func() {
		for _, l := range r.listeners {
			l.statusCh <- r.status
		}
	}

	lastBrkpts := []Breakpoint{}
	for {
		select {
		case ch := <-r.listenCh:
			r.listeners = append(r.listeners, ch)
			ch.statusCh <- r.status
			ch.breakpointsCh <- lastBrkpts
		case ch := <-r.unlistenCh:
			for i := range r.listeners {
				if r.listeners[i].statusCh == ch.StatusCh {
					close(r.listeners[i].statusCh)
					r.listeners = append(r.listeners[:i], r.listeners[i+1:]...)
					break
				}
			}
		case status := <-r.stateCh:
			r.status.State = status
			r.status.Message = ""
			broadcastStatus()
		case errmsg := <-r.faultCh:
			r.status.State = ERROR
			r.status.Message = errmsg
			broadcastStatus()
		case running := <-r.runningCh:
			r.status.Running = running
			broadcastStatus()
		case errmsg := <-r.errorCh:
			for _, l := range r.listeners {
				l.errorCh <- errmsg
			}
		case brkpts := <-r.brkptsCh:
			lastBrkpts = brkpts
			for _, l := range r.listeners {
				l.breakpointsCh <- lastBrkpts
			}
		case data := <-r.outputCh:
			r.elog.Println("output:", string(data))
			for _, l := range r.listeners {
				l.outputCh <- data
			}
		case n := <-r.notifyCh:
			r.elog.Println("notify:", n)
			for _, l := range r.listeners {
				l.notificationCh <- n
			}
		case s := <-r.stoppedCh:
			for _, l := range r.listeners {
				l.stoppedCh <- s
			}
		}
	}
}

// Uses two handler levels since gdb send waits for reply
// and can lock up due to delivering notification.
// Also cleanly separates gdb input and gdb running from client
// mux delivery.
func (r *Resource) handlerGDB() {
	for {
		select {
		case start := <-r.startCh:
			r.start(start.Project, start.Target)
		case _ = <-r.stopCh:
			r.stop("")
		case _ = <-r.interruptCh:
			if r.gdb != nil {
				r.gdb.Interrupt()
			}
		case cmd := <-r.cmdCh:
			r.command(cmd)
		case brkpt := <-r.addbrkptCh:
			ibrkpt := intBreakpoint{Breakpoint: brkpt}
			if r.gdb != nil {
				number, err := r.setBreakpoint(brkpt)
				if err != nil {
					r.errorCh <- "Failed to set breakpoint: " + err.Error()
					break
				}
				ibrkpt.number = number
			}
			r.brkpts = append(r.brkpts, ibrkpt)
			r.publishBreakpoints()
		case brkpt := <-r.rembrkptCh:
			number := ""
			index := -1
			for i, b := range r.brkpts {
				if b.Breakpoint == brkpt {
					number = b.number
					index = i
					break
				}
			}
			if index == -1 {
				r.errorCh <- "Can't find breakpoint to remove"
				break
			}
			if r.gdb != nil {
				r.command("break-delete " + number)
			}
			r.brkpts = append(r.brkpts[:index], r.brkpts[index+1:]...)
			r.publishBreakpoints()
		}
	}
}
