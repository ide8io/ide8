package pty

import (
	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
)

type PtyResource struct {
	resource.BaseResource
	Desc resource.BaseDescriptor
}

func (r *PtyResource) Descriptor() resource.Descriptor {
	return r.Desc
}
