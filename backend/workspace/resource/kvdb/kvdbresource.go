package kvdb

import (
	"fmt"
	"time"

	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
	"github.com/boltdb/bolt"
)

const (
	name = "kvdb"
)

type KVDBResource struct {
	resource.BaseResource
	desc       resource.BaseDescriptor
	elog       util.EventLog
	listenCh   chan kvdbListen
	unlistenCh chan KVDBListen
	updateCh   chan KVDBKeyValue
	listeners  []kvdbListen
	db         *bolt.DB
}

type KVDBKeyValue struct {
	Key   string
	Value string
}

// Extenal read-only channels
type KVDBListen struct {
	UpdateCh <-chan KVDBKeyValue
}

// Internal read/write channels
type kvdbListen struct {
	updateCh chan KVDBKeyValue
}

func Create(filename string, elog util.EventLog) (*KVDBResource, error) {
	db, err := bolt.Open(filename, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return nil, fmt.Errorf("kvdb db open error: %v", err)
	}

	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(name))
		return err
	})

	r := &KVDBResource{
		desc:       resource.BaseDescriptor{Class: name, Name: "Key/Value Database"},
		elog:       elog,
		listenCh:   make(chan kvdbListen),
		unlistenCh: make(chan KVDBListen),
		updateCh:   make(chan KVDBKeyValue),
		db:         db,
	}

	go r.handler()

	return r, nil
}

func (r *KVDBResource) Descriptor() resource.Descriptor {
	return &r.desc
}

func (r *KVDBResource) Listen() KVDBListen {
	l := kvdbListen{make(chan KVDBKeyValue)}
	r.listenCh <- l
	return KVDBListen{l.updateCh}
}

func (r *KVDBResource) UnListen(listen KVDBListen) {
	r.unlistenCh <- listen
}

func (r *KVDBResource) Create(key string, value string) error {
	err := r.db.Update(func(tx *bolt.Tx) error {
		kv := tx.Bucket([]byte(name))
		if rawval := kv.Get([]byte(key)); rawval != nil {
			return fmt.Errorf("can't create already existing key %s", key)
		}
		return kv.Put([]byte(key), []byte(value))
	})
	if err == nil {
		r.updateCh <- KVDBKeyValue{key, value}
	}
	return err
}

func (r *KVDBResource) Read(key string) (string, error) {
	var val string
	err := r.db.View(func(tx *bolt.Tx) error {
		kv := tx.Bucket([]byte(name))
		rawval := kv.Get([]byte(key))
		if rawval != nil {
			val = string(rawval)
		}
		return nil
	})
	return val, err
}

func (r *KVDBResource) ReadTail(count int) ([]string, error) {
	var vals []string
	err := r.db.View(func(tx *bolt.Tx) error {
		kv := tx.Bucket([]byte(name))
		c := kv.Cursor()
		for k, v := c.Last(); k != nil; k, v = c.Prev() {
			vals = append([]string{string(v)}, vals...)
			if len(vals) >= count {
				break
			}
		}
		return nil
	})
	return vals, err
}

func (r *KVDBResource) Update(key string, value string) error {
	err := r.db.Update(func(tx *bolt.Tx) error {
		kv := tx.Bucket([]byte(name))
		return kv.Put([]byte(key), []byte(value))
	})
	if err != nil {
		r.updateCh <- KVDBKeyValue{key, value}
	}
	return err
}

func (r *KVDBResource) Delete(key string) error {
	err := r.db.Update(func(tx *bolt.Tx) error {
		kv := tx.Bucket([]byte(name))
		return kv.Delete([]byte(key))
	})
	return err
}

func (r *KVDBResource) handler() {
	for {
		select {
		case kv := <-r.updateCh:
			for i := range r.listeners {
				r.listeners[i].updateCh <- kv
			}
		case ch := <-r.listenCh:
			r.listeners = append(r.listeners, ch)
		case ch := <-r.unlistenCh:
			for i := range r.listeners {
				if r.listeners[i].updateCh == ch.UpdateCh {
					close(r.listeners[i].updateCh)
					r.listeners = append(r.listeners[:i], r.listeners[i+1:]...)
					break
				}
			}
		}
	}
}
