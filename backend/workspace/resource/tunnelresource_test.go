package resource_test

import (
	"context"
	"testing"

	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewTunnelResource(t *testing.T) {
	tr := resource.NewTunnelResource(context.Background(), "123")
	require.NotNil(t, tr)
	assert.Equal(t, "123", tr.GetTunnelID())
	tr.Destroy()
}

func TestTunnelResourceConnectBadID(t *testing.T) {
	tr := resource.NewTunnelResource(context.Background(), "123")
	require.NotNil(t, tr)
	tc, err := tr.TunnelConnect(context.Background(), "321")
	assert.Nil(t, tc)
	assert.Nil(t, err)
}

func TestTunnelResourceConnect(t *testing.T) {
	tr := resource.NewTunnelResource(context.Background(), "123")
	require.NotNil(t, tr)
	go func() {
		<-tr.ConnectCh
	}()
	tc, err := tr.TunnelConnect(context.Background(), "123")
	require.Nil(t, err)
	go func() {
		_, err := tc.Recv()
		assert.NotNil(t, err)
	}()
	tr.Destroy()
}
