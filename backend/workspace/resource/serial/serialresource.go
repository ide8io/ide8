package serial

import (
	"log"
	"strconv"

	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
	"bitbucket.org/ide8io/ide8/backend/workspace/serialgw"
	"bitbucket.org/ide8io/ide8/backend/wsintapigw/pb"
	"github.com/golang/protobuf/proto"
)

type SerialResource struct {
	resource.BaseResource
	desc SerialDescriptor
}

type SerialDescriptor struct {
	resource.BaseDescriptor
	Status bool   `json:"status"`
	ID     string `json:"id"`
}

func New(boardid string, res *wsintapi.BoardResponse_Board_Resource, tunnelid string, envs resource.Envs) (*SerialResource, error) {
	r := &SerialResource{
		desc: SerialDescriptor{
			BaseDescriptor: resource.BaseDescriptor{
				Class:        "serial",
				Name:         boardid + "/" + res.Name,
				Capabilities: make(map[string]interface{}),
			},
			ID: tunnelid,
		},
	}
	for id, cap := range res.Capabilities {
		if cap.TypeUrl == proto.MessageName(&wsintapi.MapStringString{}) {
			var msscap wsintapi.MapStringString
			err := proto.Unmarshal(cap.Value, &msscap)
			if err != nil {
				log.Println(err)
				continue
			}
			r.desc.Capabilities[id] = msscap.Map
		}
	}
	port, err := serialgw.CreateSerialGW(tunnelid)
	if err != nil {
		return nil, err
	}
	envs.SetEnv("ARDUINO_PROGRAM_PORT", strconv.Itoa(port))
	return r, nil
}

func (r *SerialResource) UpdateStatus(resource *wsintapi.BoardResponse_Board_Resource) {

}

func (r *SerialResource) Descriptor() resource.Descriptor {
	return r.desc
}

func (r *SerialResource) Destroy() {
	serialgw.DestroySerialGW(r.desc.ID)
}
