package www

import (
	"log"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource/kvdb"
	"github.com/gorilla/websocket"
)

const (
	name = "www"
)

type WWWResource struct {
	resource.BaseResource
	desc         resource.Descriptor
	UserShareURL string `json:"usershareurl"`
	wsdir        string
	kvdb         *kvdb.KVDBResource
	dblisten     kvdb.KVDBListen
	wslist       []*websocket.Conn
	wsAddCh      chan *websocket.Conn
	wsRemCh      chan *websocket.Conn
}

func Create(usersharurl string, wsdir string, kvdb *kvdb.KVDBResource, elog util.EventLog) (*WWWResource, error) {
	res := &WWWResource{
		desc:         resource.BaseDescriptor{Class: name, Name: "Workspace Web Server"},
		UserShareURL: usersharurl + "www",
		wsdir:        wsdir,
		kvdb:         kvdb,
		dblisten:     kvdb.Listen(),
		wsAddCh:      make(chan *websocket.Conn),
		wsRemCh:      make(chan *websocket.Conn),
	}

	go res.handler()

	return res, nil
}

func (r *WWWResource) Descriptor() resource.Descriptor {
	return r.desc
}

func (r *WWWResource) Serve(w http.ResponseWriter, req *http.Request, path []string, validUser bool, upgrader websocket.Upgrader) {
	if len(path) >= 1 && path[0] != "www" {
		http.Error(w, "invalid workspace path", http.StatusNotFound)
		return
	}

	if len(path) == 1 {
		http.Redirect(w, req, req.URL.Path+"/", http.StatusMovedPermanently)
		return
	}

	if len(path) >= 2 && path[1] == "data" {
		ws, err := upgrader.Upgrade(w, req, nil)
		if err != nil {
			http.Error(w, "upgrade failure: "+err.Error(), http.StatusInternalServerError)
			return
		}

		countStr := req.FormValue("count")
		count, err := strconv.Atoi(countStr)
		if err != nil {
			count = 10
		}
		vals, err := r.kvdb.ReadTail(count)
		if err == nil {
			for i := range vals {
				err := ws.WriteMessage(websocket.TextMessage, []byte(vals[i]))
				if err != nil {
					log.Println("ws write err:", err)
					ws.Close()
					return
				}
			}
		}

		r.wsAddCh <- ws

		for {
			_, _, err := ws.ReadMessage()
			if err != nil {
				break
			}
		}

		r.wsRemCh <- ws
		ws.Close()
		return
	}

	w.Header().Set("Cache-Control", "public, max-age=0")
	http.ServeFile(w, req, r.wsdir+"/"+strings.Join(path, "/"))
}

func (r *WWWResource) handler() {
	for {
		select {
		case ws := <-r.wsAddCh:
			r.wslist = append(r.wslist, ws)
		case ws := <-r.wsRemCh:
			for i, w := range r.wslist {
				if w == ws {
					r.wslist = append(r.wslist[:i], r.wslist[i+1:]...)
					break
				}
			}
		case data, ok := <-r.dblisten.UpdateCh:
			if !ok {
				return
			}
			for _, ws := range r.wslist {
				ws.WriteMessage(websocket.TextMessage, []byte(data.Value))
			}
		}
	}
}
