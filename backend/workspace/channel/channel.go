package channel

import (
	"bitbucket.org/ide8io/ide8/backend/util"
	"encoding/json"
)

type OutputFunc func(string, json.RawMessage) bool
type DoneFunc func(string)
type ELog util.EventLog

type Channel interface {
	Input(string, json.RawMessage)
	Stop()
	Detach()
}
