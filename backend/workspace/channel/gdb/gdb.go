package gdb

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
	gdbres "bitbucket.org/ide8io/ide8/backend/workspace/resource/gdb"
)

type gdb struct {
	gr      *gdbres.Resource
	errorCh chan error
	listen  gdbres.Listen
	elog    util.EventLog
	output  channel.OutputFunc
}

func (g *gdb) send(action string, payload interface{}) {
	b, err := json.Marshal(payload)
	if err != nil {
		g.elog.Println("send:", err)
		return
	}
	g.output(action, b)
}

func (g *gdb) Input(action string, payload json.RawMessage) {
	g.elog.Println("input:", action, string(payload))
	action, subaction := util.SplitMsgType(action)
	if action == "start" {
		var start gdbres.Start
		if err := json.Unmarshal(payload, &start); err != nil {
			g.elog.Println(err)
			return
		}
		g.gr.Start(start)
	} else if action == "stop" {
		g.gr.Stop()
	} else if action == "interrupt" {
		g.gr.Interrupt()
	} else if action == "command" {
		var data string
		if err := json.Unmarshal(payload, &data); err != nil {
			g.elog.Println(err)
			return
		}
		g.elog.Println("command: ", data)
		g.gr.Command(data)
	} else if action == "console" {
		var data string
		if err := json.Unmarshal(payload, &data); err != nil {
			g.elog.Println(err)
			return
		}
		g.elog.Println("console command: ", data)
		g.gr.ConsoleCommand(data)
	} else if action == "break" {
		var brkpt gdbres.Breakpoint
		if err := json.Unmarshal(payload, &brkpt); err != nil {
			g.elog.Println(err)
			return
		}
		if subaction == "add" {
			g.gr.AddBreakpoint(brkpt)
		} else if subaction == "remove" {
			g.gr.RemoveBreakpoint(brkpt)
		} else {
			g.elog.Println("Unknown gdb breakpoint action type:", subaction)
		}
	} else {
		g.elog.Println("Unknown gdb action type:", action)
	}
}

func (g *gdb) Stop() {
	g.gr.UnListen(g.listen)
}

func (g *gdb) Detach() {
	g.Stop()
}

func (g *gdb) execute() {
	g.listen = g.gr.Listen()
	for {
		select {
		case newstatus, ok := <-g.listen.StatusCh:
			if !ok {
				return
			}
			status := newstatus
			g.send("status", status)
		case errmsg := <-g.listen.ErrorCh:
			g.send("error", errmsg)
		case output := <-g.listen.OutputCh:
			raw := json.RawMessage(output)
			g.send("output", &raw)
		case msg := <-g.listen.NotificationCh:
			g.send("notification", msg)
		case brkpts := <-g.listen.BreakpointsCh:
			g.send("breakpoints", brkpts)
		case s := <-g.listen.StoppedCh:
			g.send("stopped", s)
		}
	}
}

func StartGDB(payload json.RawMessage, output channel.OutputFunc, done channel.DoneFunc, elog util.EventLog,
	ress resource.Resources) (*gdb, error) {
	r, err := ress.Get("gdb")
	if err != nil {
		return nil, err
	}
	gr, ok := r.(*gdbres.Resource)
	if !ok {
		return nil, fmt.Errorf("GDB resource cast failure")
	}
	g := &gdb{
		gr:      gr,
		errorCh: make(chan error),
		elog:    elog,
		output:  output,
	}
	go g.execute()
	return g, nil
}
