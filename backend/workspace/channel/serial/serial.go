package serial

import (
	"encoding/json"

	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel"
	"bitbucket.org/ide8io/ide8/backend/workspace/serialgw"
)

type serChannel struct {
	read   serialgw.SerialGWChan
	write  serialgw.SerialGWChan
	cancel chan struct{}
}

func (ch *serChannel) Input(action string, payload json.RawMessage) {
	ch.write <- serialgw.SerialGWChanData{action, payload}
}

func (ch *serChannel) Stop() {
	select {
	case ch.cancel <- struct{}{}:
	default:
	}
}

func (ch *serChannel) Detach() {

}

func (ch *serChannel) executeSer(output channel.OutputFunc, done channel.DoneFunc, elog util.EventLog) {
loop:
	for {
		select {
		case data, ok := <-ch.read:
			if !ok {
				break loop
			}
			raw := json.RawMessage(data.Data)
			b, err := json.Marshal(&raw)
			if err != nil {
				elog.Println("executeSer:", err)
				continue loop
			}
			ok = output(data.Action, b)
			if !ok {
				break loop
			}
		case <-ch.cancel:
			break loop
		}
	}
	done("")
}

type serData struct {
	TunnelId string `json:"tunnelid"`
}

func StartSer(payload json.RawMessage, output channel.OutputFunc, done channel.DoneFunc, elog util.EventLog) *serChannel {
	var data serData
	if err := json.Unmarshal(payload, &data); err != nil {
		elog.Println("Failed to decode json", err)
		return nil
	}
	readchan, writechan, err := serialgw.ConnectFrontend(data.TunnelId)
	if err != nil {
		elog.Println("startSer:", err)
		return nil
	}
	ch := &serChannel{read: readchan, write: writechan, cancel: make(chan struct{})}
	go ch.executeSer(output, done, elog)
	return ch
}
