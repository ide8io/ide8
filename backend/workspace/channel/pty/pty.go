package pty

import (
	"encoding/json"
	"github.com/kr/pty"
	"os"
	"os/exec"
	"syscall"
	"unsafe"

	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel"
)

type ptyResize struct {
	Rows uint16 `json:"rows"`
	Cols uint16 `json:"cols"`
	X    uint16
	Y    uint16
}

type ptyChannel struct {
	tty  *os.File
	Cmd  *exec.Cmd
	elog util.EventLog
}

func (ch *ptyChannel) Input(action string, payload json.RawMessage) {
	if action == "stdin" {
		var data string
		if err := json.Unmarshal(payload, &data); err != nil {
			ch.elog.Println("Failed to decode json data:", err)
		}
		_, err := ch.tty.WriteString(data)
		if err != nil {
			ch.elog.Println("pty write error:", err)
		}
	} else if action == "resize" {
		var data ptyResize
		if err := json.Unmarshal(payload, &data); err != nil {
			ch.elog.Println("Failed to decode json data:", err)
		}
		_, _, errno := syscall.Syscall(
			syscall.SYS_IOCTL,
			ch.tty.Fd(),
			syscall.TIOCSWINSZ,
			uintptr(unsafe.Pointer(&data)),
		)
		if errno != 0 {
			ch.elog.Println("Unable to resize pty:", syscall.Errno(errno))
		}
	} else {
		ch.elog.Println("Unknown pty action type:", action)
	}
}

func (ch *ptyChannel) Stop() {
	ch.Cmd.Process.Kill()
}

func (ch *ptyChannel) Detach() {

}

func (ch *ptyChannel) executePty(output channel.OutputFunc, done channel.DoneFunc) {
	tty, err := pty.Start(ch.Cmd)
	if err != nil {
		ch.elog.Println("Failed to start pty:", err)
		done("pty failure")
		return
	}
	ch.tty = tty
	defer func() {
		tty.Close()
	}()

	errmsg := ""
	for {
		buf := make([]byte, 1024)
		read, err := tty.Read(buf)
		if err != nil {
			ch.elog.Println("Unable to read from pty/cmd:", err)
			errmsg = "Unable to read from pty/cmd"
			break
		}
		b, _ := json.Marshal(string(buf[:read]))
		ok := output("stdout", b)
		if !ok {
			ch.Stop()
			break
		}
	}
	ch.Cmd.Process.Wait()
	done(errmsg)
}

func StartBashPty(payload json.RawMessage, output channel.OutputFunc, done channel.DoneFunc, elog util.EventLog, path string, envs []string) *ptyChannel {
	cmd := exec.Command("/bin/bash", "-l")
	cmd.Dir = path
	cmd.Env = append(append(os.Environ(), "TERM=xterm"), envs...)

	ch := &ptyChannel{Cmd: cmd, elog: elog}
	go ch.executePty(output, done)
	return ch
}
