package channel

import (
	"encoding/json"
)

type Responder interface {
	Send(id string, action string, payload json.RawMessage) bool
	Done(id string, errmsg string)
}
