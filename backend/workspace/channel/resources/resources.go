package resources

import (
	"encoding/json"
	"log"

	"bitbucket.org/ide8io/ide8/backend/workspace/channel"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
)

type resourcesChannel struct {
	update    chan []resource.Descriptor
	resources resource.Resources
}

func (ch *resourcesChannel) Input(action string, payload json.RawMessage) {
	// nop
}

func (ch *resourcesChannel) Stop() {
	ch.resources.UnSubscribe(ch.update)
}

func (ch *resourcesChannel) Detach() {
	// Always stop when loosing connection
	ch.Stop()
}

func (ch *resourcesChannel) executeResources(output channel.OutputFunc, done channel.DoneFunc) {
	bufchan := make(chan []resource.Descriptor)

	go func() {
	loopOuter:
		for {
			data, ok := <-ch.update
			if !ok {
				break
			}
			// Use non-blocking write to bufchan as we don't care about stale data
		loopInner:
			for {
				select {
				case bufchan <- data:
					break loopInner
				case data, ok = <-ch.update:
					if !ok {
						break loopOuter
					}
				}
			}
		}
		close(bufchan)
	}()

	for {
		data, ok := <-bufchan
		if !ok {
			break
		}
		b, err := json.Marshal(data)
		if err != nil {
			log.Println("r:", err)
			continue
		}
		log.Println("r:", string(b))
		ok = output("update", b)
		if !ok {
			break
		}
	}
	done("")
}

func StartResourcesListen(payload json.RawMessage, output channel.OutputFunc, done channel.DoneFunc,
	resources resource.Resources) *resourcesChannel {
	ch := &resourcesChannel{resources: resources, update: resources.Subscribe()}
	go ch.executeResources(output, done)
	return ch
}
