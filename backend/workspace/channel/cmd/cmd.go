package cmd

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"

	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel"
	"bitbucket.org/ide8io/ide8/backend/workspace/storage"
)

type cmd struct {
	path string
}

func (c cmd) Input(action string, payload json.RawMessage) {

}

func (c cmd) Stop() {

}

func (c cmd) Detach() {

}

type cmdData struct {
	Cmd      string `json:"cmd"`
	Repo     string `json:"repo"`
	Fullpath string `json:"fullpath"`
}

func executeCmd(cmd string, output channel.OutputFunc, done channel.DoneFunc, elog util.EventLog, path string, envs []string) {
	outr, outw, err := os.Pipe()
	if err != nil {
		done("Failed to open stdout pipe!")
		return
	}
	defer outr.Close()
	defer outw.Close()

	inr, inw, err := os.Pipe()
	if err != nil {
		done("Failed to open stdin pipe!")
		return
	}
	defer inr.Close()
	defer inw.Close()

	proc, err := os.StartProcess("/bin/bash", []string{"/bin/bash", "-c", cmd}, &os.ProcAttr{
		Dir:   path,
		Files: []*os.File{inr, outw, outw},
		Env:   append(os.Environ(), envs...),
	})
	if err != nil {
		elog.Println("Failed to start process:", err)
		done(err.Error())
		return
	}

	inr.Close()
	outw.Close()

	s := bufio.NewScanner(outr)
	for s.Scan() {
		//log.Println(string(s.Bytes()))
		b, _ := json.Marshal(string(s.Bytes()))
		output("stdout", b)
	}
	if s.Err() != nil {
		elog.Println("scan:", s.Err())
	}

	procstate, err := proc.Wait()
	if err != nil {
		elog.Println("wait:", err)
	}

	if procstate.Success() {
		done("")
	} else {
		done(procstate.String())
	}
}

func StartCmd(payload json.RawMessage, output channel.OutputFunc, done channel.DoneFunc,
	stor *storage.Storage, elog util.EventLog, envs []string) (*cmd, error) {
	var data cmdData
	if err := json.Unmarshal(payload, &data); err != nil {
		return nil, err
	}
	elog.Println(data)

	if data.Repo == "workspace" {
		proj, err := stor.FindProject(data.Fullpath)
		if err != nil {
			return nil, err
		}
		cmdline, err := proj.GetCommand(data.Cmd)
		if err != nil {
			return nil, err
		}
		go executeCmd(cmdline, output, done, elog, proj.GetDir(), envs)
	} else if data.Repo == "examples" {
		ex, err := stor.FindExample(data.Fullpath)
		if err != nil {
			return nil, err
		}
		cmdline, err := ex.GetCommand(data.Cmd)
		if err != nil {
			return nil, err
		}
		go executeCmd(cmdline, output, done, elog, ex.GetDir(), envs)
	} else {
		return nil, fmt.Errorf("invalid repository type: %v", data.Repo)
	}

	return &cmd{}, nil
}
