package ttn

import (
	"encoding/json"

	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel"
	ttnres "bitbucket.org/ide8io/ide8/backend/workspace/resource/ttn"
)

type ttn struct {
	tr        *ttnres.TTNResource
	errorCh   chan error
	listen    ttnres.TTNListen
	listdevCh chan ttnres.TTNDeviceList
	elog      util.EventLog
	output    channel.OutputFunc
}

func (t *ttn) send(action string, payload interface{}) {
	b, err := json.Marshal(payload)
	if err != nil {
		t.elog.Println("send:", err)
		return
	}
	t.output(action, b)
}

func (t *ttn) Input(action string, payload json.RawMessage) {
	if action == "connect" {
		var data struct {
			AppID     string `json:"app_id"`
			AccessKey string `json:"access_key"`
		}
		if err := json.Unmarshal(payload, &data); err != nil {
			t.elog.Println(err)
			t.errorCh <- err
			return
		}
		t.tr.Connect(data.AppID, data.AccessKey)
	} else if action == "devices" {
		var data struct {
			Limit  uint64 `json:"limit"`
			Offset uint64 `json:"offset"`
		}
		if err := json.Unmarshal(payload, &data); err != nil {
			t.elog.Println(err)
			t.errorCh <- err
			return
		}
		t.tr.ListDevices(data.Limit, data.Offset, t.listdevCh)
	} else if action == "device" {
		var device string
		if err := json.Unmarshal(payload, &device); err != nil {
			t.elog.Println(err)
			t.errorCh <- err
			return
		}
		t.tr.SetDevice(device)
	} else {
		t.elog.Println("Unknown ttn action type:", action)
	}
}

func (t *ttn) Stop() {
	t.tr.UnListen(t.listen)
}

func (t *ttn) Detach() {
	t.Stop()
}

func (t *ttn) executeTTN() {
	t.listen = t.tr.Listen()
	var status ttnres.TTNStatus
	for {
		select {
		case err := <-t.errorCh:
			status.ErrMsg = err.Error()
			t.send("status", status)
		case newstatus, ok := <-t.listen.StatusCh:
			if !ok {
				return
			}
			status := newstatus
			t.send("status", status)
		case devlist := <-t.listdevCh:
			t.send("devices", devlist)
		case msg := <-t.listen.UplinkCh:
			t.send("uplink", msg)
		}
	}
}

func StartTTN(payload json.RawMessage, output channel.OutputFunc, done channel.DoneFunc, elog util.EventLog,
	tr *ttnres.TTNResource) *ttn {
	t := &ttn{
		tr:        tr,
		errorCh:   make(chan error),
		listdevCh: make(chan ttnres.TTNDeviceList),
		elog:      elog,
		output:    output,
	}
	go t.executeTTN()
	return t
}
