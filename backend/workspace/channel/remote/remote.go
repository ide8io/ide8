package remote

import (
	"encoding/json"

	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/channel"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource/remote"
)

type remoteChannel struct {
	elog   util.EventLog
	client *remote.Client
}

func (r *remoteChannel) Input(action string, payload json.RawMessage) {
	if action == "start" {
		r.client.Start()
	} else if action == "stop" {
		r.client.Stop()
	} else {
		r.elog.Println("unknown action:", action)
	}
}

func (r *remoteChannel) Stop() {
	r.client.Close()
}

func (r *remoteChannel) Detach() {

}

func (r *remoteChannel) execute(output channel.OutputFunc, done channel.DoneFunc) {
	for {
		select {
		case status, ok := <-r.client.StatusCh:
			if !ok {
				done("")
				return
			}
			b, err := json.Marshal(status)
			if err != nil {
				r.elog.Println(err)
			}
			output("status", b)
		case data, ok := <-r.client.DataCh:
			if !ok {
				continue
			}
			b, _ := json.Marshal(string(data))
			output("data", b)
		}
	}
}

func StartRemote(payload json.RawMessage, output channel.OutputFunc, done channel.DoneFunc,
	elog util.EventLog, resources resource.Resources) *remoteChannel {
	var data struct {
		BoardID string `json:"boardid"`
		TunnelID string `json:"tunnelid"`
	}
	err := json.Unmarshal(payload, &data)
	if err != nil {
		elog.Println(err)
		return nil
	}
	res, err := resources.GetByBoardTunnel(data.BoardID, data.TunnelID)
	if err != nil {
		elog.Println(err)
		return nil
	}
	rr, ok := res.(*remote.RemoteResource)
	if !ok {
		elog.Println("Remote resource casting failed")
		return nil
	}
	client := rr.FrontendConnect()
	g := &remoteChannel{
		elog:   elog,
		client: client,
	}
	go g.execute(output, done)
	return g
}
