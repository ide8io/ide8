package channel

import (
	"bitbucket.org/ide8io/ide8/backend/util"
	"encoding/json"
	"time"
)

type SessionOutputFunc *func(id string, action string, payload json.RawMessage, elog util.EventLog) bool
type SessionDoneFunc *func(id string, errmsg string, elog util.EventLog)

type sessEventLog struct {
	prefix string
	elog   util.EventLog
}

func (el *sessEventLog) Println(v ...interface{}) {
	el.elog.Println(append([]interface{}{"ch " + el.prefix}, v...)...)
}

type Session struct {
	id         string
	variant    string
	Channel    Channel
	output     SessionOutputFunc
	done       SessionDoneFunc
	syncWake   chan struct{}
	detachtime time.Time
	ELog       util.EventLog
}

type ChannelInfo struct {
	Id      string `json:"id"`
	Variant string `json:"variant"`
	Active  bool   `json:"active"`
}

func CreateSession(id string, variant string, output SessionOutputFunc, done SessionDoneFunc, elog util.EventLog) *Session {
	return &Session{id: id, variant: variant, output: output, done: done, syncWake: make(chan struct{}), ELog: &sessEventLog{id + " " + variant, elog}}
}

func (cs *Session) Attach(output SessionOutputFunc, done SessionDoneFunc) {
	cs.output = output
	cs.done = done
	select {
	case cs.syncWake <- struct{}{}:
	default:
	}
}

func (cs *Session) Detach(output SessionOutputFunc, done SessionDoneFunc) {
	if cs.output == output {
		cs.detachtime = time.Now()
		cs.output = nil
		cs.Channel.Detach()
	}
}

func (cs *Session) Input(action string, payload json.RawMessage) {
	cs.Channel.Input(action, payload)
}

func (cs *Session) Stop() {
	close(cs.syncWake)
	cs.Channel.Stop()
}

func (cs *Session) Output(action string, payload json.RawMessage) bool {
	if cs.output == nil {
		cs.ELog.Println("wait...")
		select {
		case _, ok := <-cs.syncWake:
			if ok {
				cs.ELog.Println("wake")
			} else {
				return false
			}
		}
		if cs.output == nil {
			return false
		}
	}
	return (*cs.output)(cs.id, cs.variant+":"+action, payload, cs.ELog)
}

func (cs *Session) Done(errmsg string) {
	(*cs.done)(cs.id, errmsg, cs.ELog)
}

func (cs *Session) IsDead(limit time.Duration) bool {
	if cs.output == nil {
		inactivetime := time.Now().Sub(cs.detachtime)
		if inactivetime > limit {
			return true
		}
	}
	return false
}

func (cs *Session) IsActive() bool {
	return cs.output != nil
}

func (cs *Session) Info() ChannelInfo {
	return ChannelInfo{Id: cs.id, Variant: cs.variant, Active: cs.IsActive()}
}
