package slack

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
)

type config struct {
	WebhookURL string `json:"webhook_url"`
}

var configfile string

func Init(datadir string) {
	configfile = filepath.Join(datadir, "ide8_slack_config.json")
}

func SetWebhook(url string) error {
	conf := config{url}
	data, err := json.MarshalIndent(conf, "", "  ")
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(configfile, data, 0600)
	if err != nil {
		return err
	}
	return nil
}

func getWebhookURL() (string, error) {
	if _, err := os.Stat(configfile); os.IsNotExist(err) {
		return "", fmt.Errorf("slack webhook not configured")
	}

	data, err := ioutil.ReadFile(configfile)
	if err != nil {
		return "", err
	}
	conf := config{}
	err = json.Unmarshal(data, &conf)
	if err != nil {
		return "", err
	}

	return conf.WebhookURL, nil
}

func IsConfigured() bool {
	url, err := getWebhookURL()
	return err == nil || url != ""
}

func Send(msg string) error {
	url, err := getWebhookURL()
	if err != nil {
		return err
	}

	content := map[string]string{"text": msg}
	b, err := json.Marshal(content)
	if err != nil {
		return err
	}

	_, err = http.Post(url, "application/json", bytes.NewBuffer(b))
	return err
}
