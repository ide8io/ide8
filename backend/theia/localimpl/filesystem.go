package localimpl

import (
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"bitbucket.org/ide8io/ide8/backend/theia"
)

type Filesystem struct {
}

type Node struct {
	URI              string `json:"uri"`
	LastModification int64  `json:"lastModification"`
	IsDirectory      bool   `json:"isDirectory"`
}

type Dir struct {
	Node
	Children []interface{} `json:"children"`
}

type File struct {
	Node
	Size int64 `json:"size"`
}

func getStat(uri string) (theia.FileStat, error) {
	fname := strings.TrimPrefix(uri, "file://")
	rootfi, err := os.Stat(fname)
	if err != nil {
		return nil, err
	}
	if rootfi.IsDir() {
		dir := &theia.FileStatDir{
			Uri:              uri,
			LastModification: rootfi.ModTime().UnixNano() / 1e6,
			IsDirectory:      rootfi.IsDir(),
			Children:         []interface{}{},
		}
		fii, err := ioutil.ReadDir(fname)
		if err != nil {
			return nil, err
		}
		for _, fi := range fii {
			if fi.IsDir() {
				dir.Children = append(dir.Children, &theia.FileStatDir{
					Uri:              uri + string(filepath.Separator) + fi.Name(),
					LastModification: fi.ModTime().UnixNano() / 1e6,
					IsDirectory:      fi.IsDir(),
					Children:         []interface{}{},
				})
			} else {
				dir.Children = append(dir.Children, &theia.FileStatFile{
					Uri:              uri + string(filepath.Separator) + fi.Name(),
					LastModification: fi.ModTime().UnixNano() / 1e6,
					IsDirectory:      fi.IsDir(),
					Size:             fi.Size(),
				})
			}
		}
		return dir, nil
	}
	return &theia.FileStatFile{
		Uri:              uri,
		LastModification: rootfi.ModTime().UnixNano() / 1e6,
		IsDirectory:      rootfi.IsDir(),
		Size:             rootfi.Size(),
	}, nil
}

func (f *Filesystem) Close() {
}

func (f *Filesystem) GetFileStat(uri string) (theia.FileStat, error) {
	stat, err := getStat(uri)
	if err != nil {
		return nil, err
	}
	return &stat, nil
}

func (f *Filesystem) Exists(file string) (bool, error) {
	_, err := os.Stat(strings.TrimPrefix(file, "file://"))
	return !os.IsNotExist(err), nil
}

func (f *Filesystem) ResolveContent(file string) (theia.FileStat, string, error) {
	stat, err := getStat(file)
	if err != nil {
		return nil, "", err
	}
	b, err := ioutil.ReadFile(strings.TrimPrefix(file, "file://"))
	if err != nil {
		return nil, "", err
	}
	return stat, string(b), nil
}

func (f *Filesystem) GetCurrentUserHome() (theia.FileStat, error) {
	u, _ := user.Current()
	stat, err := getStat("file://" + u.HomeDir)
	return stat, err
}
