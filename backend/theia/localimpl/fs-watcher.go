package localimpl

type FSWatcher struct {
	id int
}

func (f *FSWatcher) Close() {
}

func (f *FSWatcher) WatchFileChanges(files string, reply func(result interface{})) {
	f.id++
	reply(f.id)
}

func (f *FSWatcher) UnwatchFileChanges(watchid int, reply func(result interface{})) {
	reply(nil)
}
