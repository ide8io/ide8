package localimpl

type Envs struct {
}

func (e *Envs) Close() {
}

func (e *Envs) GetValue(name string, reply func(result interface{})) {
	reply(nil)
}

func (w *Envs) GetVariables(reply func(result interface{})) {
	reply(nil)
}
