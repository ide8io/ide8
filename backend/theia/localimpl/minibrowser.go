package localimpl

type MiniBrowser struct {
}

func (m *MiniBrowser) Close() {
}

func (m *MiniBrowser) SupportedFileExtensions(reply func(result interface{})) {
	type extension struct {
		Name    string `json:"name"`
		Version string `json:"version"`
	}
	exts := []extension{
		{
			Name:    "ide8",
			Version: "0.1",
		},
	}
	reply(exts)
}
