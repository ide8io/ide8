package localimpl

type EditorConfig struct {
}

func (e *EditorConfig) Close() {
}

func (e *EditorConfig) GetConfig(file string, reply func(result interface{})) {
	reply(make(map[string]interface{}))
}
