package localimpl

type Application struct {
}

type Info struct {
	Name    string `json:"name"`
	Version string `json:"version"`
}

func (a *Application) Close() {
}

func (a *Application) GetApplicationInfo(reply func(result interface{})) {
	info := Info{
		Name:    "ide8",
		Version: "0.1",
	}
	reply(info)
}

func (a *Application) GetExtensionsInfos(reply func(result interface{})) {
	infos := []Info{
		Info{
			Name:    "@theia/core",
			Version: "0.3.13",
		},
	}
	reply(infos)
}
