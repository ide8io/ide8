package localimpl

import (
	"fmt"

	"bitbucket.org/ide8io/ide8/backend/theia"
)

type Factory struct {
}

func (f *Factory) Create(path string, eventCB func(method string, params interface{})) (theia.Service, error) {
	switch path {
	case "logger":
		return &theia.LoggerService{
			Logger: &Logger{},
		}, nil
	case "filesystem":
		return &theia.FilesystemService{
			Filesystem: &Filesystem{},
		}, nil
	case "fs-watcher":
		return &theia.FSWatcherService{
			FSWatcher: &FSWatcher{},
		}, nil
	case "workspace":
		return &theia.WorkspaceService{
			Workspace: &Workspace{},
		}, nil
	case "envs":
		return &theia.EnvsService{
			Envs: &Envs{},
		}, nil
	case "mini-browser-service":
		return &theia.MiniBrowserService{
			MiniBrowser: &MiniBrowser{},
		}, nil
	case "application":
		return &theia.ApplicationService{
			Application: &Application{},
		}, nil
	case "editorconfig":
		return &theia.EditorConfigService{
			EditorConfig: &EditorConfig{},
		}, nil
	default:
		return nil, fmt.Errorf("service path %s not supported", path)
	}
}
