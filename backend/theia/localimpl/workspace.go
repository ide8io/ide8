package localimpl

import "os"

type Workspace struct {
}

func (w *Workspace) Close() {
}

func (w *Workspace) GetWorkspace(reply func(result interface{})) {
	dir, _ := os.Getwd()
	reply("file://" + dir)
}

func (w *Workspace) SetWorkspace(uri string, reply func(result interface{})) {
	reply(nil)
}

func (w *Workspace) GetRecentWorkspaces(reply func(result interface{})) {
	dir, _ := os.Getwd()
	reply([]string{"file://" + dir})
}
