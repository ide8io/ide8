package localimpl

type Logger struct {
}

func (l *Logger) Close() {
}

func (l *Logger) SetLogLevel(name string, level int) error {
	return nil
}

func (l *Logger) GetLogLevel(name string) (int, error) {
	return 30, nil
}

func (l *Logger) Log(name string) error {
	return nil
}

func (l *Logger) Child(name string) error {
	return nil
}
