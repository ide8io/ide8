package theia

import (
	"encoding/json"

	"bitbucket.org/ide8io/ide8/backend/theia/jsonrpc"
)

type FileStat interface {
}

type FileStatFile struct {
	Uri              string `json:"uri"`
	LastModification int64  `json:"lastModification"`
	IsDirectory      bool   `json:"isDirectory"`
	Size             int64  `json:"size"`
}

type FileStatDir struct {
	Uri              string        `json:"uri"`
	LastModification int64         `json:"lastModification"`
	IsDirectory      bool          `json:"isDirectory"`
	Children         []interface{} `json:"children"`
}

type TextDocumentContentChangeEvent struct {
}

type Filesystem interface {
	GetFileStat(uri string) (FileStat, error)
	Exists(uri string) (bool, error)
	// TODO: Add options param
	ResolveContent(uri string) (FileStat, string, error)
	/*
		// TODO: Add options param
		SetContent(file FileStat, content string) (FileStat, error)
		// TODO: Add options param
		UpdateContent(file FileStat, contentChanges []TextDocumentContentChangeEvent) (FileStat, error)
		// TODO: Add options param
		Move(sourceUri string, targetUri string) (FileStat, error)
		// TODO: Add options param
		Copy(sourceUri string, targetUri string) (FileStat, error)
		CreateFile(uri string) (FileStat, error)
		CreateFolder(uri string) (FileStat, error)
		Touch(uri string) (FileStat, error)
		// TODO: Add options param
		Delete(uri string) error
		GetEncoding(uri string) (string, error)
		GetRoots() ([]FileStat, error)
	*/
	GetCurrentUserHome() (FileStat, error)
}

type FilesystemService struct {
	Filesystem Filesystem
}

func (s *FilesystemService) RPC(method string, params json.RawMessage, reply func(result interface{}), replyError func(code int, message string)) {
	switch method {
	case "getFileStat":
		var uri string
		err := json.Unmarshal(params, &uri)
		if err != nil {
			replyError(jsonrpc.PARSE_ERROR, err.Error())
			return
		}
		stat, err := s.Filesystem.GetFileStat(uri)
		if err != nil {
			replyError(jsonrpc.INTERNAL_ERROR, err.Error())
			return
		}
		reply(stat)
	case "exists":
		var uri string
		err := json.Unmarshal(params, &uri)
		if err != nil {
			replyError(jsonrpc.PARSE_ERROR, err.Error())
			return
		}
		exists, err := s.Filesystem.Exists(uri)
		if err != nil {
			replyError(jsonrpc.INTERNAL_ERROR, err.Error())
			return
		}
		reply(exists)
	case "resolveContent":
		var parr []string
		err := json.Unmarshal(params, &parr)
		if err != nil {
			var val string
			err := json.Unmarshal(params, &val)
			if err != nil {
				replyError(jsonrpc.PARSE_ERROR, err.Error())
				return
			}
			parr = append(parr, val)
		}
		stat, content, err := s.Filesystem.ResolveContent(parr[0])
		var result struct {
			Stat    FileStat `json:"stat"`
			Content string   `json:"content"`
		}
		result.Stat = stat
		result.Content = content
		reply(result)
	case "getCurrentUserHome":
		cuh, err := s.Filesystem.GetCurrentUserHome()
		if err != nil {
			replyError(jsonrpc.INTERNAL_ERROR, err.Error())
			return
		}
		reply(cuh)
	default:
		replyError(jsonrpc.METHOD_NOT_FOUND, "unsupported method: "+method)
	}
}
