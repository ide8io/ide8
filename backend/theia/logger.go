package theia

import (
	"encoding/json"

	"bitbucket.org/ide8io/ide8/backend/theia/jsonrpc"
)

type Logger interface {
	SetLogLevel(name string, logLevel int) error
	GetLogLevel(name string) (int, error)
	Log(name string) error
	Child(name string) error
}

type LoggerService struct {
	Logger Logger
}

func (s *LoggerService) RPC(method string, params json.RawMessage, reply func(result interface{}), replyError func(code int, message string)) {
	switch method {
	case "child":
		var name string
		err := json.Unmarshal(params, &name)
		if err != nil {
			replyError(jsonrpc.PARSE_ERROR, err.Error())
			return
		}
		err = s.Logger.Child(name)
		if err != nil {
			replyError(jsonrpc.INTERNAL_ERROR, err.Error())
			return
		}
		reply(nil)
	case "getLogLevel":
		var name string
		err := json.Unmarshal(params, &name)
		if err != nil {
			replyError(jsonrpc.PARSE_ERROR, err.Error())
			return
		}
		level, err := s.Logger.GetLogLevel(name)
		if err != nil {
			replyError(jsonrpc.INTERNAL_ERROR, err.Error())
			return
		}
		reply(level)
	case "log":
		var logMsg []interface{}
		err := json.Unmarshal(params, &logMsg)
		if err != nil {
			replyError(jsonrpc.PARSE_ERROR, err.Error())
			return
		}
		err = s.Logger.Log("")
		if err != nil {
			replyError(jsonrpc.INTERNAL_ERROR, err.Error())
			return
		}
		reply(nil)
	default:
		replyError(jsonrpc.METHOD_NOT_FOUND, "unsupported method: "+method)
	}
}
