package jsonrpc

const (
	PARSE_ERROR      = -32700
	METHOD_NOT_FOUND = -32601
	INTERNAL_ERROR   = -32603
)

type Error struct {
}
