package theia

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"sync"

	"bitbucket.org/ide8io/ide8/backend/theia/jsonrpc"
	"github.com/gorilla/websocket"
)

type FSWatcher interface {
	WatchFileChanges(files string, reply func(result interface{}))
	UnwatchFileChanges(watchid int, reply func(result interface{}))
}

type Workspace interface {
	GetWorkspace(reply func(result interface{}))
	SetWorkspace(uri string, reply func(result interface{}))
	GetRecentWorkspaces(reply func(result interface{}))
}

type Envs interface {
	GetVariables(reply func(result interface{}))
	GetValue(name string, reply func(result interface{}))
}

type MiniBrowser interface {
	SupportedFileExtensions(reply func(result interface{}))
}

type Application interface {
	GetApplicationInfo(reply func(result interface{}))
	GetExtensionsInfos(reply func(result interface{}))
}

type EditorConfig interface {
	GetConfig(file string, reply func(result interface{}))
}

type Service interface {
	RPC(method string, params json.RawMessage, reply func(result interface{}), replyError func(code int, message string))
}

type FSWatcherService struct {
	eventCB   func(method string, params interface{})
	FSWatcher FSWatcher
}

func (s *FSWatcherService) RPC(method string, params json.RawMessage, reply func(result interface{}), replyError func(code int, message string)) {
	switch method {
	case "watchFileChanges":
		s.FSWatcher.WatchFileChanges("", reply)
	case "unwatchFileChanges":
		var watchid int
		err := json.Unmarshal(params, &watchid)
		if err != nil {
			replyError(jsonrpc.PARSE_ERROR, err.Error())
			return
		}
		s.FSWatcher.UnwatchFileChanges(watchid, reply)
	default:
		replyError(jsonrpc.METHOD_NOT_FOUND, "unsupported method: "+method)
	}
}

type DidFilesChangedParams struct {
	Changes []FileChange `json:"changes"`
}

type FileChange struct {
	Uri  string `json:"uri"`
	Type int    `json:"type"`
}

func (s *FSWatcherService) OnDidFilesChanged(changes DidFilesChangedParams) {
	s.eventCB("onDidFilesChanged", changes)
}

type WorkspaceService struct {
	Workspace Workspace
}

func (s *WorkspaceService) RPC(method string, params json.RawMessage, reply func(result interface{}), replyError func(code int, message string)) {
	switch method {
	case "getWorkspace":
		s.Workspace.GetWorkspace(reply)
	case "getRecentWorkspaces":
		s.Workspace.GetRecentWorkspaces(reply)
	default:
		replyError(jsonrpc.METHOD_NOT_FOUND, "unsupported method: "+method)
	}
}

type EnvsService struct {
	Envs Envs
}

func (s *EnvsService) RPC(method string, params json.RawMessage, reply func(result interface{}), replyError func(code int, message string)) {
	switch method {
	case "getValue":
		var what string
		err := json.Unmarshal(params, &what)
		if err != nil {
			replyError(jsonrpc.PARSE_ERROR, err.Error())
			return
		}
		s.Envs.GetValue(what, reply)
	default:
		replyError(jsonrpc.METHOD_NOT_FOUND, "unsupported method: "+method)
	}
}

type MiniBrowserService struct {
	MiniBrowser MiniBrowser
}

func (s *MiniBrowserService) RPC(method string, params json.RawMessage, reply func(result interface{}), replyError func(code int, message string)) {
	switch method {
	case "supportedFileExtensions":
		s.MiniBrowser.SupportedFileExtensions(reply)
	default:
		replyError(jsonrpc.METHOD_NOT_FOUND, "unsupported method: "+method)
	}
}

type ApplicationService struct {
	Application Application
}

func (s *ApplicationService) RPC(method string, params json.RawMessage, reply func(result interface{}), replyError func(code int, message string)) {
	switch method {
	case "getApplicationInfo":
		s.Application.GetApplicationInfo(reply)
	case "getExtensionsInfos":
		s.Application.GetExtensionsInfos(reply)
	default:
		replyError(jsonrpc.METHOD_NOT_FOUND, "unsupported method: "+method)
	}
}

type EditorConfigService struct {
	EditorConfig
}

func (s *EditorConfigService) RPC(method string, params json.RawMessage, reply func(result interface{}), replyError func(code int, message string)) {
	switch method {
	case "getConfig":
		var what string
		err := json.Unmarshal(params, &what)
		if err != nil {
			replyError(jsonrpc.PARSE_ERROR, err.Error())
			return
		}
		s.GetConfig(what, reply)
	default:
		replyError(jsonrpc.METHOD_NOT_FOUND, "unsupported method: "+method)
	}
}

var upgrader = websocket.Upgrader{}

type ServiceFactory interface {
	Create(path string, eventCB func(method string, params interface{})) (Service, error)
}

type Services struct {
	Factory ServiceFactory
}

type channel struct {
	Kind    string `json:"kind"`
	ID      int    `json:"id"`
	Path    string `json:"path,omitempty"`
	Content string `json:"content,omitempty"`
}

func (s *Services) Handle(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		http.Error(w, "websocket upgrade failure: "+err.Error(), http.StatusInternalServerError)
		return
	}
	defer ws.Close()

	var mu sync.Mutex

	send := func(id int, content interface{}) {
		b, err := json.Marshal(content)
		if err != nil {
			log.Println("theia.services: send:", err)
		}
		rdata := channel{
			Kind:    "data",
			ID:      id,
			Content: string(b),
		}
		mu.Lock()
		defer mu.Unlock()
		err = ws.WriteJSON(rdata)
		if err != nil {
			log.Println("theia.services: send:", err)
		}
	}

	services := map[int]Service{}

	for {
		var data channel
		err := ws.ReadJSON(&data)
		if err != nil {
			log.Println("theia.services:", err)
			break
		}

		log.Println("theia.services: incoming:", data)

		switch data.Kind {
		case "open":
			s, err := s.Factory.Create(strings.TrimPrefix(data.Path, "/services/"), func(method string, params interface{}) {
				var eventContent struct {
					JSONRPC string      `json:"jsonrpc"`
					Params  interface{} `json:"params"`
				}
				eventContent.JSONRPC = "2.0"
				eventContent.Params = params
				send(data.ID, eventContent)
			})
			if err != nil {
				log.Println("theia.services:", err)
				//break
			}
			response := channel{
				Kind: "ready",
				ID:   data.ID,
			}
			mu.Lock()
			err = ws.WriteJSON(response)
			mu.Unlock()
			if err != nil {
				log.Println("theia.services:", err)
				break
			}
			mu.Lock()
			if s != nil {
				services[data.ID] = s
			}
			mu.Unlock()
		case "data":
			mu.Lock()
			s, ok := services[data.ID]
			mu.Unlock()
			if !ok {
				log.Println("theia.services: no service with ID:", data.ID)
				break
			}
			type content struct {
				JSONRPC string          `json:"jsonrpc"`
				ID      int             `json:"id"`
				Method  string          `json:"method`
				Params  json.RawMessage `json:"params"`
			}
			var c content
			err := json.Unmarshal([]byte(data.Content), &c)
			if err != nil {
				log.Println("theia.services:", err)
				break
			}
			go s.RPC(c.Method, c.Params,
				func(result interface{}) {
					var replyContent struct {
						JSONRPC string      `json:"jsonrpc"`
						ID      int         `json:"id"`
						Result  interface{} `json:"result"`
					}
					replyContent.JSONRPC = "2.0"
					replyContent.ID = c.ID
					replyContent.Result = result
					send(data.ID, replyContent)
				},
				func(code int, message string) {
					var errorContent struct {
						JSONRPC string `json:"jsonrpc"`
						ID      int    `json:"id"`
						Error   struct {
							Code    int         `json:"code"`
							Message string      `json:"message"`
							Data    interface{} `json:"data,omitempty"`
						} `json:"error"`
					}
					errorContent.JSONRPC = "2.0"
					errorContent.ID = c.ID
					errorContent.Error.Code = code
					errorContent.Error.Message = message
				})
		// TODO: Implement close
		default:
			log.Println("theia.services: invalid kind:", data.Kind)
		}
	}
}
