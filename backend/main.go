package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/ide8io/ide8/backend/api/tunnel"
	"bitbucket.org/ide8io/ide8/backend/auth"
	"bitbucket.org/ide8io/ide8/backend/board"
	board_impl "bitbucket.org/ide8io/ide8/backend/board/impl"
	"bitbucket.org/ide8io/ide8/backend/boardman"
	"bitbucket.org/ide8io/ide8/backend/connect"
	"bitbucket.org/ide8io/ide8/backend/dockertools"
	"bitbucket.org/ide8io/ide8/backend/email"
	"bitbucket.org/ide8io/ide8/backend/event/simple_impl"
	"bitbucket.org/ide8io/ide8/backend/gizmo"
	gizmo_impl "bitbucket.org/ide8io/ide8/backend/gizmo/impl"
	"bitbucket.org/ide8io/ide8/backend/monitor"
	"bitbucket.org/ide8io/ide8/backend/promoter"
	"bitbucket.org/ide8io/ide8/backend/slack"
	"bitbucket.org/ide8io/ide8/backend/stackman"
	"bitbucket.org/ide8io/ide8/backend/urlpath"
	"bitbucket.org/ide8io/ide8/backend/user"
	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/wsextapigw"
	"bitbucket.org/ide8io/ide8/backend/wsintapigw"
	"bitbucket.org/ide8io/ide8/backend/wsman"
	"bitbucket.org/ide8io/ide8/backend/wsrun"
	"github.com/asaskevich/govalidator"
	"github.com/gorilla/context"
	"github.com/gorilla/sessions"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
	"gopkg.in/yaml.v2"
)

var cookiename = "dev.ide8.io"

var store *sessions.CookieStore

var loginTemplate *template.Template
var registerTemplate *template.Template
var regFormTemplate *template.Template
var assignTemplate *template.Template
var agentTemplate *template.Template
var openrepoTemplate *template.Template

func serveJson(w http.ResponseWriter, data interface{}) {
	b, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

func serveJsonErr(w http.ResponseWriter, data interface{}, err error) {
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	serveJson(w, data)
}

func register(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		path := strings.Split(r.URL.Path, "/")
		if len(path) == 3 {
			invite := r.FormValue("invite")
			if path[2] == "" {
				if invite == "" {
					http.ServeFile(w, r, "dist/requestinvite.html")
					return
				}
				emailAdr := ""
				inv, err := user.GetInvite(invite)
				if err == nil {
					emailAdr = inv.Email
				}
				registerTemplate.Execute(w, map[string]string{
					"query": "?invite=" + invite,
					"email": emailAdr,
				})
				return
			} else {
				code := r.FormValue("code")
				state := r.FormValue("state")
				if code == "" || state == "" {
					var handler auth.StateHandler
					if path[2] == "github" {
						handler = auth.CreateGitHubHandler()
					} else if path[2] == "bitbucket" {
						handler = auth.CreateBitBucketHandler()
					} else {
						http.Error(w, "Invalid registration type "+path[2], http.StatusBadRequest)
						return
					}
					replyURL := "https://" + r.Host + r.URL.Path
					if invite != "" {
						replyURL += "?invite=" + invite
					}
					url, err := auth.CreateState(handler, replyURL, time.Now())
					if err != nil {
						http.Error(w, err.Error(), http.StatusInternalServerError)
						return
					}
					http.Redirect(w, r, url, http.StatusTemporaryRedirect)
					return
				}
				handler := auth.PopState(state)
				if handler == nil {
					http.Error(w, "invalid state", http.StatusUnauthorized)
					return
				}
				ui, err := handler.GetUserInfo(code)
				if err != nil {
					user.EventLog.Println("Failed OAuth get user info! error:", err, "from:", util.GetRemoteAddr(r))
					http.Error(w, err.Error(), http.StatusForbidden)
					return
				}
				user.EventLog.Println("Register with", path[2], "remoteuser:", ui.UserName, "email:", ui.Email, "name:", ui.FullName, "from:", util.GetRemoteAddr(r))
				emailadr := ui.Email
				inv, err := user.GetInvite(invite)
				if err == nil && emailadr == "" {
					// Use invite email if missing from user info
					emailadr = inv.Email
				}
				auth.AddUserToCache(ui, time.Now())
				regFormTemplate.Execute(w, map[string]string{
					"remoteuser": ui.UserName,
					"username":   ui.UserName,
					"fullname":   ui.FullName,
					"email":      emailadr,
					"query":      "?invite=" + invite,
				})
				return
			}
		}
	} else if r.Method == "POST" {
		path := strings.Split(r.URL.Path, "/")
		if len(path) == 3 && path[2] == "request" {
			r.ParseForm()
			email := strings.TrimSpace(r.Form.Get("email"))
			if !govalidator.IsEmail(email) {
				http.Error(w, "Not a valid email address: "+email, http.StatusBadRequest)
				return
			}
			remoteaddr := util.GetRemoteAddr(r)
			hostnames, _ := net.LookupAddr(remoteaddr)
			hostnames = append(hostnames, remoteaddr)
			err := user.AddSignup(email, strings.Join(hostnames, " "), time.Now())
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			user.EventLog.Println("Signup request:", email, "from:", hostnames)
			sysevent.Send("Signup request: " + email + " from: " + strings.Join(hostnames, " "))
			w.Write([]byte("Your request for invite have been recorded. Please await approval."))
			return
		}
		r.ParseForm()
		invite := r.FormValue("invite")
		username := strings.TrimSpace(r.Form.Get("username"))
		password := strings.TrimSpace(r.Form.Get("password"))
		remoteuser := strings.TrimSpace(r.Form.Get("remoteuser"))
		fullname := strings.TrimSpace(r.Form.Get("fullname"))
		emailadr := strings.TrimSpace(r.Form.Get("email"))
		_, err := user.GetInvite(invite)
		if err != nil {
			user.EventLog.Println("Registration without invite: email", emailadr, "name:", fullname, "from:", util.GetRemoteAddr(r))
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		if !govalidator.IsEmail(emailadr) {
			http.Error(w, "invalid email address", http.StatusBadRequest)
			return
		}
		if remoteuser != "" {
			ui, err := auth.GetUserFromCache(remoteuser)
			if err != nil {
				http.Error(w, err.Error(), http.StatusForbidden)
				return
			}
			err = user.AddOAuthUser(username, ui.Source, remoteuser, fullname, emailadr, ui.ID, ui.AvatarURL, ui.AccessToken)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			auth.DeleteUserInCache(remoteuser)
			user.EventLog.Println("Added", ui.Source, "user:", username, "from:", util.GetRemoteAddr(r))
		} else {
			err := user.Add(username, fullname, emailadr, password)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			user.EventLog.Println("Added user:", username, "from:", util.GetRemoteAddr(r))
		}
		if invite != "" {
			err := user.DeleteInvite(invite)
			if err != nil {
				log.Println("register DeleteInvite:", err)
				// Not a critical fault so let it pass
			}
		}
		// Let thru and assign session
		session, err := store.Get(r, cookiename)
		if err != nil {
			log.Println("Error when getting session: ", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		session.Values["username"] = username
		session.Save(r, w)
		http.Redirect(w, r, "/users/"+username, http.StatusSeeOther)
		return
	}
	http.Error(w, "Malformed URL", http.StatusBadRequest)
}

func serveLogin(w http.ResponseWriter, r *http.Request, message string) {
	w.Header().Set("cache-control", "no-store")
	query := ""
	githubquery := "?use=github"
	bitbucketquery := "?use=bitbucket"
	if r.URL.RawQuery != "" {
		query = "?" + r.URL.RawQuery
		githubquery += "&" + r.URL.RawQuery
		bitbucketquery += "&" + r.URL.RawQuery
	}
	loginTemplate.Execute(w, map[string]string{"message": message, "query": query, "githubquery": githubquery, "bitbucketquery": bitbucketquery})
}

// Initialize session cookie
func initSession(w http.ResponseWriter, r *http.Request, userinfo user.User) {
	session, err := store.Get(r, cookiename)
	if err != nil {
		log.Println("Error when getting session: ", err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	id, err := user.CreateSession(userinfo.UserName)
	if err != nil {
		log.Println("Error creating session id: ", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	session.Values["id"] = id
	session.Values["username"] = userinfo.UserName
	if userinfo.Admin {
		session.Values["rights"] = "admin"
	}
	session.Save(r, w)
	user.EventLog.Println("logged in", userinfo.UserName, "from:", util.GetRemoteAddr(r))
}

// Forward session to user area or back to discourse sso
func forwardSession(w http.ResponseWriter, r *http.Request, userinfo user.User, from string, sso string, sig string) {
	if sso != "" && sig != "" {
		nonce, err := auth.ValidateDiscourseSSO(sso, sig)
		if err == nil {
			reply, err := auth.GetDiscourseSSOReplyURL(nonce, userinfo.ID, userinfo.UserName, userinfo.Email, userinfo.FullName, userinfo.AvatarURL)
			if err == nil {
				http.Redirect(w, r, reply, http.StatusSeeOther)
				return
			} else {
				log.Println("discourse sso reply error:", err)
			}
		} else {
			log.Println("discourse sso check error:", err)
		}
	}
	if from == "admin" {
		http.Redirect(w, r, "/admin/", http.StatusSeeOther)
		return
	} else if from == "monitor" {
		http.Redirect(w, r, "/monitor/", http.StatusSeeOther)
		return
	} else if from == "assign" {
		http.Redirect(w, r, "/_connect/assign?"+r.URL.RawQuery, http.StatusSeeOther)
		return
	} else if from == "open" {
		http.Redirect(w, r, "/open?"+r.URL.RawQuery, http.StatusSeeOther)
		return
	}

	http.Redirect(w, r, "/users/"+userinfo.UserName, http.StatusSeeOther)
}

func login(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		use := r.FormValue("use")     // Control use of other authenticators
		code := r.FormValue("code")   // OAuth code
		state := r.FormValue("state") // OAuth state
		sso := r.FormValue("sso")     // Discourse SSO
		sig := r.FormValue("sig")     // Discourse SSO signature
		from := r.FormValue("from")   // Login from other page than default
		if use != "" {
			var handler auth.StateHandler
			if use == "github" {
				handler = auth.CreateGitHubHandler()
			} else if use == "bitbucket" {
				handler = auth.CreateBitBucketHandler()
			} else {
				http.Error(w, "Unknown auth use "+use, http.StatusBadRequest)
				return
			}
			replyURL := "https://" + r.Host + r.URL.Path
			if sso != "" && sig != "" {
				replyURL += "?sso=" + sso + "&sig=" + sig
			}
			url, err := auth.CreateState(handler, replyURL, time.Now())
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			http.Redirect(w, r, url, http.StatusTemporaryRedirect)
			return
		} else if state != "" {
			handler := auth.PopState(state)
			if handler == nil {
				http.Error(w, "invalid state", http.StatusUnauthorized)
				return
			}
			ui, err := handler.GetUserInfo(code)
			if err != nil {
				user.EventLog.Println("Failed OAuth get user info! error:", err, "from:", util.GetRemoteAddr(r))
				http.Error(w, err.Error(), http.StatusUnauthorized)
				return
			}
			// TODO: Store access token
			u, err := user.GetByRemoteUser(ui.Source, ui.UserName)
			if err != nil {
				user.EventLog.Println("Failed", ui.Source, "login! remote username:", ui.UserName, "from:", util.GetRemoteAddr(r))
				http.Error(w, err.Error(), http.StatusForbidden)
				return
			}
			initSession(w, r, u)
			forwardSession(w, r, u, from, sso, sig)
		} else {
			// First check for existing session and re-use it
			session, err := store.Get(r, cookiename)
			if err != nil {
				log.Println("Error when getting session: ", err)
			} else {
				username, ok := session.Values["username"].(string)
				if ok {
					u, err := user.Get(username)
					if err == nil {
						// Update session and redirect to user area
						session.Save(r, w)
						forwardSession(w, r, u, from, sso, sig)
						return
					}
				}
			}
			serveLogin(w, r, "")
		}
	} else if r.Method == "POST" {
		sso := r.FormValue("sso")   // Discourse SSO
		sig := r.FormValue("sig")   // Discourse SSO signature
		from := r.FormValue("from") // Login from other page than default
		r.ParseForm()
		username := strings.TrimSpace(r.Form.Get("username"))
		userinfo, err := user.Get(username)
		if err != nil {
			serveLogin(w, r, "Failed to validate user!")
			return
		}
		valid := userinfo.Authenticate(strings.TrimSpace(r.Form.Get("password")))
		if valid {
			initSession(w, r, userinfo)
			forwardSession(w, r, userinfo, from, sso, sig)
		} else {
			user.EventLog.Println("Failed login! username:", username, "from:", util.GetRemoteAddr(r))
			serveLogin(w, r, "Failed to validate user!")
		}
	} else {
		http.Error(w, "Unsupported method: "+r.Method, http.StatusMethodNotAllowed)
	}
}

func logout(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, cookiename)
	username, ok := session.Values["username"].(string)
	if ok {
		user.EventLog.Println("logged out", username, "from:", util.GetRemoteAddr(r))
	}
	session.Options.MaxAge = -1
	session.Save(r, w)
	id, ok := session.Values["id"].(string)
	if ok {
		user.DeleteSession(username, id)
	}
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func validateSession(session *sessions.Session, w http.ResponseWriter, r *http.Request) (string, bool) {
	username, ok := session.Values["username"].(string)
	if !ok {
		log.Println("No cookie in request", r.Method, r.URL)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return "", false
	}
	if !user.IsValid(username) {
		log.Println("Not a valid user: ", username)
		http.Error(w, "Username not valid!", http.StatusUnauthorized)
		return "", false
	}
	return username, true
}

func ping(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, cookiename)
	if err != nil {
		log.Println("Error when getting session: ", err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	username, ok := validateSession(session, w, r)
	if ok {
		id, ok := session.Values["id"].(string)
		if !ok {
			id, err := user.CreateSession(username)
			if err == nil {
				session.Values["id"] = id
			}
		}
		session.Save(r, w)
		if ok {
			user.UpdateSession(username, id)
		}
		w.Write([]byte("pong"))
	}
}

func validate(next http.HandlerFunc, forward bool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		session, err := store.Get(r, cookiename)
		if err != nil {
			log.Println("Error when getting session: ", err)
			if forward {
				serveLogin(w, r, "")
			} else {
				http.Error(w, err.Error(), http.StatusUnauthorized)
			}
			return
		}
		_, ok := validateSession(session, w, r)
		if ok {
			next(w, r)
		}
	}
}

func openRepo(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, cookiename)
	if err != nil {
		log.Println("Error when getting session: ", err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	username, ok := session.Values["username"].(string)
	if !ok || !user.IsValid(username) {
		http.Redirect(w, r, "/login?from=open&"+r.URL.RawQuery, http.StatusSeeOther)
		return
	}

	repo := r.FormValue("repo")
	repoURL, err := url.Parse(repo)
	if err != nil {
		http.Error(w, "Invalid repository URL: "+repo, http.StatusBadRequest)
		return
	}
	if repoURL.Host != "github.com" {
		http.Error(w, "Only github is supported for now", http.StatusBadRequest)
		return
	}
	repoURL.Host = "raw.github.com"
	repoURL.Path += "/master/ide8.yaml"
	resp, err := http.Get(repoURL.String())
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	var def struct {
		Stack string `yaml:"stack"`
	}
	err = yaml.Unmarshal(data, &def)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	openrepoTemplate.Execute(w, map[string]string{"stack": def.Stack})
}

func workspaces(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, cookiename)
	if err != nil {
		log.Println("Error when getting session: ", err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	username, ok := validateSession(session, w, r)
	if !ok {
		return
	}
	if r.Method == "POST" {
		decoder := json.NewDecoder(r.Body)
		var ws struct {
			Name      string `json:"name"`
			Stack     string `json:"stack"`
			CloneRepo string `json:"clonerepo"`
		}
		err := decoder.Decode(&ws)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			w.Write([]byte("{\"status\": \"fail\"}"))
			return
		}
		defer r.Body.Close()
		err = wsman.Create(username, ws.Name, ws.Stack)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			w.Write([]byte("{\"status\": \"fail\"}"))
			return
		}
		stack, err := stackman.GetStack(ws.Stack)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		err = stack.UpdateUsage(username)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			w.Write([]byte("{\"status\": \"fail\"}"))
			return
		}
		init := "bare"
		if ws.CloneRepo != "" {
			init = ws.CloneRepo
		}
		err = wsman.Start(username, ws.Name, init)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		err = wsman.WaitOnlineOrError(r.Context(), username, ws.Name)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write([]byte("{\"status\": \"ok\"}"))
	} else if r.Method == "PUT" {
		path := strings.Split(r.URL.Path, "/")
		if len(path) != 4 {
			http.Error(w, "", http.StatusBadRequest)
			return
		}
		if path[3] == "running" {
			decoder := json.NewDecoder(r.Body)
			defer r.Body.Close()
			var start bool
			err := decoder.Decode(&start)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			if start {
				err := wsman.Start(username, path[2], "")
				if err == wsman.ErrWorkspaceAlreadyRunning {
					return
				}
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				err = wsman.WaitOnlineOrError(r.Context(), username, path[2])
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				return
			} else {
				err := wsman.Stop(username, path[2])
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
			}
			return
		}
	} else if r.Method == "GET" {
		path := strings.Split(r.URL.Path, "/")
		if path[2] == "" {
			wslist, err := wsman.List(username)
			if err != nil {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				w.Write([]byte("{\"status\": \"fail\"}"))
				return
			}
			data, err := json.Marshal(wslist)
			if err != nil {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				w.Write([]byte("{\"status\": \"fail\"}"))
				return
			}
			w.Write(data)
		} else {
			ws, err := wsman.Get(username, path[2])
			if err != nil {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				w.Write([]byte("{\"status\": \"fail\"}"))
				return
			}
			var wsdata struct {
				Running bool   `json:"running"`
				Name    string `json:"name"`
				Order   int    `json:"order"`
				Stack   string `json:"stack"`
				URLS    struct {
					WebSocket string `json:"websocket"`
					DAV       string `json:"dav"`
					Files     string `json:"files"`
					Examples  string `json:"examples"`
					Stack     string `json:"stack"`
					UserShare string `json:"usershare"`
				} `json:"urls"`
			}
			wsdata.Running = ws.Running
			wsdata.Name = ws.Name
			wsdata.Order = ws.Order
			wsdata.Stack = ws.Stack
			wsdata.URLS.WebSocket = "/_ws/" + username + "/" + path[2]
			wsdata.URLS.DAV = "/ws/" + username + "/" + path[2] + "/dav/"
			wsdata.URLS.Files = "/ws/" + username + "/" + path[2] + "/files/"
			wsdata.URLS.Examples = "/ws/" + username + "/" + path[2] + "/examples/"
			wsdata.URLS.Stack = "/ws/" + username + "/" + path[2] + "/stack/"
			wsdata.URLS.UserShare = "/ws/" + username + "/" + path[2] + "/usershare/"
			data, err := json.Marshal(wsdata)
			if err != nil {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				w.Write([]byte("{\"status\": \"fail\"}"))
				return
			}
			w.Write(data)
		}
	} else if r.Method == "DELETE" {
		path := strings.Split(r.URL.Path, "/")
		boardman.ReleaseByWorkspace(username, path[2])
		wsman.Delete(username, path[2])
	} else {
		http.Error(w, "Unsupported method "+r.Method, http.StatusBadRequest)
		w.Write([]byte("{\"status\": \"fail\"}"))
	}
}

func boards(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, cookiename)
	if err != nil {
		log.Println("Error when getting session: ", err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	username, ok := validateSession(session, w, r)
	if !ok {
		return
	}
	if r.Method == "GET" {
		btype := r.URL.Query().Get("board")
		status := r.URL.Query().Get("status")
		var ilist map[string]board.Board
		var err error
		if btype != "" {
			ilist, err = bman.ListByType(btype, "", 20)
		} else if status == "mine" {
			ilist, err = bman.ListMine(username, "", 20)
		} else {
			http.Error(w, "Bad request", http.StatusBadRequest)
			return
		}
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		type boardinfo struct {
			ID        string    `json:"id"`
			Owner     string    `json:"owner"`
			Name      string    `json:"name"`
			Type      string    `json:"type"`
			Info      string    `json:"info"`
			Status    string    `json:"status"`
			User      string    `json:"user"`
			Workspace string    `json:"workspace"`
			Created   time.Time `json:"created"`
			Last      time.Time `json:"last"`
		}
		bii := []boardinfo{}
		for id, b := range ilist {
			owner, name := urlpath.Split2NoError(id)
			u, ws := urlpath.Split2NoError(b.AssignedTo)
			status := "other"
			if u == "" {
				status = "free"
			} else if u == username {
				status = "mine"
			}
			bii = append(bii, boardinfo{
				ID:        id,
				Owner:     owner,
				Name:      name,
				Type:      b.Type,
				Info:      b.Info,
				Status:    status,
				User:      u,
				Workspace: ws,
				Created:   b.Created,
				Last:      b.Last,
			})
		}
		data, err := json.Marshal(bii)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			w.Write([]byte("{\"status\": \"fail\"}"))
			return
		}
		w.Write(data)
	} else {
		path := strings.Split(r.URL.Path, "/")
		if len(path) <= 4 {
			http.Error(w, "Malformed path "+r.URL.Path, http.StatusBadRequest)
			w.Write([]byte("{\"status\": \"fail\"}"))
			return
		}
		boardid := path[2] + "/" + path[3]
		if path[4] == "workspace" {
			if r.Method == "PUT" {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, "Bad request body: "+err.Error(), http.StatusBadRequest)
					w.Write([]byte("{\"status\": \"fail\"}"))
					return
				}
				//target := "/_tunnel/" + username + "/" + string(body) + "/" + boardid
				err = bman.Claim(boardid, username+"/"+string(body))
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
					w.Write([]byte("{\"status\": \"fail\"}"))
					return
				}
				w.Write([]byte("{\"status\": \"ok\"}"))
			} else if r.Method == "DELETE" {
				err = bman.Release(boardid) //, username)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
					w.Write([]byte("{\"status\": \"fail\"}"))
					return
				}
				w.Write([]byte("{\"status\": \"ok\"}"))
			} else {
				http.Error(w, "Unsupported method "+r.Method, http.StatusBadRequest)
				w.Write([]byte("{\"status\": \"fail\"}"))
			}
		} else {
			http.Error(w, "Unsupported board element "+boardid, http.StatusBadRequest)
			w.Write([]byte("{\"status\": \"fail\"}"))
		}
	}
}

func stacks(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Unsupported method "+r.Method, http.StatusBadRequest)
		return
	}
	path := strings.Split(r.URL.Path, "/")

	if len(path) == 2 {
		slist := []string{"elsys:1.0.0", "nRF52:11"}
		data, err := json.Marshal(slist)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(data)
		return
	}

	if len(path) >= 6 {
		stackname := strings.Join(path[2:5], "/")
		if path[5] == "examples" {
			el, err := stackman.LookupExample(stackname, path[6:])
			if err != nil {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			if el.IsCollection {
				exlist, err := el.ListCollection()
				if err != nil {
					log.Println(err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				data, err := json.Marshal(exlist)
				if err != nil {
					log.Println(err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				w.Write(data)
				return
			}
			ex, err := el.ReadFile()
			if err != nil {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.Write(ex)
			return
		}
	}

	http.Error(w, "Bad request "+r.Method+" "+r.URL.Path, http.StatusBadRequest)
}

func users(w http.ResponseWriter, r *http.Request) {
	path := strings.Split(r.URL.Path, "/")
	if len(path) < 3 { // Should really not happen
		http.Error(w, "Bad URL", http.StatusBadRequest)
		return
	}

	if path[2] == "" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	session, err := store.Get(r, cookiename)
	if err != nil {
		log.Println("Error when getting session: ", err)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	username, ok := session.Values["username"].(string)
	if !ok {
		log.Println("No cookie in request", r.Method, r.URL)
		if len(path) == 3 {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
		} else {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
		}
		return
	}
	if !user.IsValid(username) {
		log.Println("Not a valid user: ", username)
		if len(path) == 3 {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
		} else {
			http.Error(w, "Username not valid!", http.StatusUnauthorized)
		}
		return
	}

	if path[2] != username {
		log.Println("Username mismatch (" + path[2] + " != " + username + ")")
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	// URL: /users/<username>
	if len(path) == 3 {
		http.ServeFile(w, r, filepath.Join("dist", "userpage.html"))
		return
	}

	// Implicit for path to be of length higher than 3
	wsname := path[3]
	ws, err := wsman.Get(username, wsname)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	director := func(req *http.Request) {
		req.URL.Scheme = "http"
		req.URL.Host = ws.Server + ":" + ws.HTTPPort
		req.URL.Path = "/files/" + strings.Join(path[4:], "/")
		req.Header.Set("workspace", username+"/"+wsname)
		req.Header.Set("token", "012")
	}
	wsextapigw.ForwardHttp(w, r, director)
}

func serveWebSocket(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, cookiename)
	if err != nil {
		log.Println("Error when getting session: ", err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	username, ok := validateSession(session, w, r)
	if !ok {
		return
	}
	path := strings.Split(r.URL.Path, "/")
	if len(path) < 4 && path[2] == username {
		http.Error(w, "Invalid URL path", http.StatusBadRequest)
		return
	}
	ws, err := wsman.Get(username, path[3])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if ws.Server == "" {
		http.Error(w, "workspace not running", http.StatusInternalServerError)
		return
	}
	wsextapigw.ForwardWebSocket(w, r, ws.Server+":"+ws.GRPCPort)
}

func serveTunnelWs(w http.ResponseWriter, r *http.Request) {
	path := strings.Split(r.URL.Path, "/")
	if len(path) < 4 {
		http.Error(w, "Invalid URL path", http.StatusBadRequest)
		return
	}
	ws, err := wsman.Get(path[2], path[3])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if ws.Server == "" {
		http.Error(w, "workspace not running", http.StatusInternalServerError)
		return
	}
	wsextapigw.ForwardTunnel(w, r, ws.Server+":"+ws.GRPCPort, strings.Join(path[4:], "/"), "456")
}

func setCacheControl(value string, next http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", value)
		next.ServeHTTP(w, r)
	}
}

func admin(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, cookiename)
	if err != nil {
		log.Println("Error when getting session: ", err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	username, ok := validateSession(session, w, r)
	if !ok {
		return
	}
	rights, ok := session.Values["rights"].(string)
	if rights != "admin" {
		log.Println("No admin rights for", username)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	userinfo, err := user.Get(username)
	if err != nil {
		log.Println("Error getting user info: ", err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	if !userinfo.Admin {
		log.Println("No admin rights in user db for", username)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	path := strings.Split(r.URL.Path, "/")
	if len(path) < 3 {
		log.Println("Bad admin url", r.URL)
		http.Error(w, "Invalid URL path", http.StatusBadRequest)
		return
	}

	if path[2] == "" {
		http.ServeFile(w, r, filepath.Join("dist", "admin.html"))
		return
	}

	if path[2] == "users" {
		if len(path) >= 4 {
			if path[3] == "" {
				if r.Method == "GET" {
					users, err := user.List()
					if err != nil {
						log.Println(err)
						http.Error(w, err.Error(), http.StatusInternalServerError)
						return
					}
					data, err := json.Marshal(users)
					if err != nil {
						log.Println(err)
						http.Error(w, err.Error(), http.StatusInternalServerError)
						w.Write([]byte("{\"status\": \"fail\"}"))
						return
					}
					w.Write(data)
					return
				} else if r.Method == "POST" {
					defer r.Body.Close()
					body, err := ioutil.ReadAll(r.Body)
					if err != nil {
						http.Error(w, "Bad request body: "+err.Error(), http.StatusBadRequest)
						w.Write([]byte("{\"status\": \"fail\"}"))
						return
					}
					var newuser struct {
						UserName string `json:"username"`
						FullName string `json:"fullname"`
						Email    string `json:"email"`
						Password string `json:"password"`
					}
					json.Unmarshal(body, &newuser)
					err = user.Add(newuser.UserName, newuser.FullName, newuser.Email, newuser.Password)
					if err != nil {
						http.Error(w, "Bad request body: "+err.Error(), http.StatusBadRequest)
						return
					}
					user.EventLog.Println("admin", username, "added user", newuser.UserName)
					return
				}
			} else if r.Method == "PUT" {
				if len(path) == 5 && path[4] == "admin" {
					defer r.Body.Close()
					body, err := ioutil.ReadAll(r.Body)
					if err != nil {
						http.Error(w, "Bad request body: "+err.Error(), http.StatusBadRequest)
						w.Write([]byte("{\"status\": \"fail\"}"))
						return
					}
					user.SetAdmin(path[3], string(body) == "true")
					user.EventLog.Println("admin", username, "set user", path[3], "admin", string(body) == "true")
					return
				}
			} else if r.Method == "DELETE" {
				user.Delete(path[3])
				user.EventLog.Println("admin", username, "deleted user", path[3])
				return
			}
		}
	} else if path[2] == "invites" {
		if len(path) == 4 {
			if r.Method == "GET" {
				invs, err := user.ListInvites()
				if err != nil {
					log.Println(err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				data, err := json.Marshal(invs)
				if err != nil {
					log.Println(err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				w.Write(data)
				return
			} else if r.Method == "POST" {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, "Bad request body: "+err.Error(), http.StatusBadRequest)
					return
				}
				var newinvite struct {
					Email string `json:"email"`
				}
				err = json.Unmarshal(body, &newinvite)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				if !govalidator.IsEmail(newinvite.Email) {
					http.Error(w, "Invalid email address!", http.StatusBadRequest)
					return
				}
				inv, err := user.CreateInvite(username, newinvite.Email, time.Now(), time.Now().Add(time.Hour*24*30))
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				q := url.Values{}
				q.Set("invite", inv.Token)
				u := url.URL{Scheme: "https", Host: r.Host, Path: "/signup/", RawQuery: q.Encode()}
				err = email.SendInvite(inv.Email, username, u.String())
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				data, err := json.Marshal(inv)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				user.EventLog.Println("admin", username, "invited", inv.Email)
				w.Write(data)
				return
			} else if r.Method == "DELETE" {
				err := user.DeleteInvite(path[3])
				if err != nil {
					log.Println(err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				user.EventLog.Println("admin", username, "deleted invite", path[3])
				return
			}
		}
	} else if path[2] == "requests" {
		if r.Method == "GET" {
			ss, err := user.ListSignups()
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			data, err := json.Marshal(ss)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.Write(data)
			return
		} else if len(path) == 4 && r.Method == "DELETE" {
			err := user.DeleteSignup(path[3])
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.Write([]byte("Invite request deleted"))
			return
		}
	} else if path[2] == "wsman" {
		s := wsman.AdminList()
		data, err := json.Marshal(s)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			w.Write([]byte("{\"status\": \"fail\"}"))
			return
		}
		w.Write(data)
		return
	} else if path[2] == "boards" {
		if r.Method == "GET" {
			if len(path) == 4 {
				bs, err := bman.List("", 50)
				if err != nil {
					log.Println(err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					w.Write([]byte("{\"status\": \"fail\"}"))
					return
				}
				type boardinfo struct {
					board.Board
					Online bool `json:"online"`
				}
				bii := map[string]boardinfo{}
				for id, b := range bs {
					online := false
					for _, r := range b.Resources {
						if r.Status == gizmo.StatusOnline {
							online = true
							break
						}
					}
					bii[id] = boardinfo{
						Board:  b,
						Online: online,
					}
				}
				data, err := json.Marshal(bii)
				if err != nil {
					log.Println(err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					w.Write([]byte("{\"status\": \"fail\"}"))
					return
				}
				w.Write(data)
				return
			}
		} else if r.Method == "PUT" && len(path) >= 6 {
			boardid := path[3] + "/" + path[4]
			if path[5] == "release" {
				err := bman.Release(boardid)
				if err != nil {
					log.Println(err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					w.Write([]byte("{\"status\": \"fail\"}"))
					return
				}
				return
			} else if path[5] == "type" {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, "Bad request body: "+err.Error(), http.StatusBadRequest)
					return
				}
				err = bman.UpdateBoardType(boardid, string(body))
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
					return
				}
				return
			} else if path[5] == "info" {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, "Bad request body: "+err.Error(), http.StatusBadRequest)
					return
				}
				err = bman.UpdateBoardInfo(boardid, string(body))
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
					return
				}
				return
			}
		} else if r.Method == "DELETE" && len(path) == 5 {
			boardid := path[3] + "/" + path[4]
			err := bman.Delete(boardid)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			return
		}
	} else if path[2] == "agents" {
		if r.Method == "GET" {
			agents, err := connect.List()
			serveJsonErr(w, agents, err)
			return
		} else if r.Method == "DELETE" && len(path) == 4 {
			agentid := path[3]
			connect.DeleteAgent(agentid)
			return
		}
	} else if path[2] == "announce" {
		if r.Method == "POST" {
			subj := r.FormValue("subject")
			if subj == "" {
				http.Error(w, "Empty subject not allowed", http.StatusBadRequest)
				return
			}
			defer r.Body.Close()
			body, err := ioutil.ReadAll(r.Body)
			if err != nil || string(body) == "" {
				http.Error(w, "Bad request body: "+err.Error(), http.StatusBadRequest)
				return
			}
			uu, err := user.List()
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			count := 0
			length := 0
			for _, u := range uu {
				if u.Email != "" {
					err := email.Send("news", u.Email, subj, string(body))
					if err != nil {
						log.Println("announce send email error:", err)
					} else {
						count++
					}
					length++
				}
			}
			log.Printf("sent announce emails. Success rate %d/%d", count, length)
			return
		}
	}

	log.Println("Bad admin request", r.Method, r.URL)
	http.Error(w, "Invalid URL request", http.StatusBadRequest)
}

func handleMonitor(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, cookiename)
	if err != nil {
		log.Println("Error when getting session: ", err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	username, ok := validateSession(session, w, r)
	if !ok {
		return
	}
	rights, ok := session.Values["rights"].(string)
	if rights != "admin" {
		log.Println("No admin rights for", username)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	userinfo, err := user.Get(username)
	if err != nil {
		log.Println("Error getting user info: ", err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	if !userinfo.Admin {
		log.Println("No admin rights in user db for", username)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	path := strings.Split(r.URL.Path, "/")
	if len(path) < 3 {
		log.Println("Bad monitor url", r.URL)
		http.Error(w, "Invalid URL path", http.StatusBadRequest)
		return
	}

	if path[2] == "" {
		http.ServeFile(w, r, filepath.Join("dist", "monitor.html"))
		return
	}

	if path[2] == "stats" && r.Method == "GET" {
		if len(path) > 3 {
			if path[3] == "users" {
				serveJson(w, monitor.GetUsersStatsLog())
				return
			} else if path[3] == "stacks" {
				if len(path) == 4 {
					serveJson(w, stackman.GetStats())
				} else {
					data, err := stackman.GetStatsLog(path[4])
					serveJsonErr(w, data, err)
				}
				return
			}
		} else {
			serveJson(w, monitor.GetStats())
			return
		}
	}

	log.Println("Bad monitor request", r.Method, r.URL)
	http.Error(w, "Invalid URL request", http.StatusBadRequest)
}

func handleAssign(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, cookiename)
	if err != nil {
		log.Println("Error when getting session: ", err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	username, ok := session.Values["username"].(string)
	if !ok || !user.IsValid(username) {
		http.Redirect(w, r, "/login?from=assign&"+r.URL.RawQuery, http.StatusSeeOther)
		return
	}
	assignTemplate.Execute(w, map[string]string{
		"username": username,
	})
}

type downloadAgentHandler struct {
	datadir string
}

func downloadAgent(datadir string) http.Handler {
	return &downloadAgentHandler{datadir: datadir}
}

func (h *downloadAgentHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	updatefrom := r.FormValue("updatefrom")

	agents := "IDE8 Agent download:<br>"
	if updatefrom != "" {
		agents = "Your IDE8 agent version " + updatefrom + " is outdated.<br><br>Please download latest release:<br>"
	}

	ua := r.Header.Get("User-Agent")

	archos := ""
	if strings.Contains(ua, "Windows") {
		archos = "win32"
	} else if strings.Contains(ua, "Macintosh") {
		archos = "macos"
	} else if strings.Contains(ua, "Linux") {
		archos = "linux-x86_64"
	}

	files, err := ioutil.ReadDir(filepath.Join(h.datadir, "download"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	assumed := ""
	headless := []string{}
	others := []string{}
	for _, f := range files {
		if strings.HasPrefix(f.Name(), "ide8agent") {
			if strings.Contains(f.Name(), "headless") {
				headless = append(headless, f.Name())
			} else if archos != "" && strings.Contains(f.Name(), archos) {
				assumed = f.Name()
			} else {
				others = append(others, f.Name())
			}
		}
	}

	if assumed != "" {
		agents += "<a href=\"/download/" + assumed + "\">" + assumed + "</a><br><br>Other os downloads:<br>"
	}
	for _, other := range others {
		agents += "<a href=\"/download/" + other + "\">" + other + "</a><br>"
	}

	if len(headless) > 0 {
		agents += "<br><br>Headless (no systray) downloads:<br>"
		for _, h := range headless {
			agents += "<a href=\"/download/" + h + "\">" + h + "</a><br>"
		}
	}

	agentTemplate.Execute(w, template.HTML(agents))
}

func workspace(w http.ResponseWriter, r *http.Request) {
	path := strings.Split(r.URL.Path, "/")
	if len(path) < 6 {
		http.Error(w, "Invalid path: "+r.URL.Path, http.StatusNotFound)
		return
	}
	username := path[2]
	wsname := path[3]

	validUser := false
	session, err := store.Get(r, cookiename)
	if err == nil {
		sessionUsername, ok := session.Values["username"].(string)
		if ok && username == sessionUsername && user.IsValid(username) {
			validUser = true
		}
	}
	ws, err := wsman.Get(username, wsname)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	token := ""
	if validUser {
		token = "789"
	}
	isws := strings.ToLower(r.Header.Get("Connection")) == "upgrade" && strings.ToLower(r.Header.Get("Upgrade")) == "websocket"
	dest := r.Header.Get("Destination")
	host := r.Host
	director := func(req *http.Request) {
		if isws {
			req.URL.Scheme = "ws"
		} else {
			req.URL.Scheme = "http"
		}
		req.URL.Host = ws.Server + ":" + ws.HTTPPort
		req.URL.Path = "/" + strings.Join(path[4:], "/")
		req.Header.Set("workspace", username+"/"+wsname)
		req.Header.Set("token", token)
		if dest != "" {
			// Rewrite WebDAV MOVE Destination url
			sd := strings.Split(dest, "/")
			u := url.URL{
				Host: host,
				Path: "/" + strings.Join(sd[4:], "/"),
			}
			req.Header.Set("Destination", u.String())
		}
		log.Println("wsextapigw: proxy:", util.GetRemoteAddr(r)+r.URL.Path+"->"+req.URL.String())
	}
	if isws {
		wsextapigw.ForwardUserWebSocket(w, r, director)
	} else {
		wsextapigw.ForwardHttp(w, r, director)
	}
}

func runnerInit(server string, port string) {
	runner, err := wsrun.New(server, port)
	if err != nil {
		log.Fatalln(err)
	}
	wsman.AddRunnerClient(runner)
}

type tsysevent struct{}

var sysevent tsysevent

func (s tsysevent) Send(msg string) {
	if slack.IsConfigured() {
		err := slack.Send(msg)
		if err != nil {
			log.Println("sysevent.Send:", err)
		}
	}
}

func getMyDockerIP() string {
	ifaces, err := net.Interfaces()
	if err != nil {
		log.Fatalln("get interfaces:", err)
	}
	for _, i := range ifaces {
		addrs, err := i.Addrs()
		if err != nil {
			log.Fatalln("get addresses error:", err)
		}
		for _, addr := range addrs {
			switch v := addr.(type) {
			case *net.IPNet:
				ip4 := v.IP.To4()
				if ip4 != nil && ip4[0] == 172 {
					return ip4.String()
				}
			case *net.IPAddr:
				ip4 := v.IP.To4()
				if ip4 != nil && ip4[0] == 172 {
					return ip4.String()
				}
			}
		}
	}
	log.Fatalln("found no docker ip address")
	return ""
}

type grpcTunnelServer struct {
}

func (s *grpcTunnelServer) Tunnel(server api_tunnel.TunnelApi_TunnelServer) error {
	autherr := fmt.Errorf("unauthorized")
	md, ok := metadata.FromIncomingContext(server.Context())
	if !ok {
		return autherr
	}
	wsid, ok := md["workspace"]
	if !ok || len(wsid) == 0 {
		return autherr
	}
	wsarr := strings.SplitN(wsid[0], "/", 2)
	if len(wsarr) != 2 {
		return autherr
	}
	ws, err := wsman.Get(wsarr[0], wsarr[1])
	if err != nil {
		return err
	}
	if ws.Server == "" {
		return fmt.Errorf("workspace not running")
	}
	tunnelid, ok := md["tunnelid"]
	if !ok || len(tunnelid) == 0 {
		return autherr
	}
	return wsextapigw.ForwardAPITunnel(server, ws.Server+":"+ws.GRPCPort, tunnelid[0], "321")
}

// Link time configurable domain for secure cookie
var domain string
var gitver string

var bman board.Manager

func main() {
	loginTemplate = template.Must(template.ParseFiles("dist/login.html"))
	registerTemplate = template.Must(template.ParseFiles("dist/register.html"))
	regFormTemplate = template.Must(template.ParseFiles("dist/regform.html"))
	assignTemplate = template.Must(template.ParseFiles("dist/assign.html"))
	agentTemplate = template.Must(template.ParseFiles("dist/agent.html"))
	openrepoTemplate = template.Must(template.ParseFiles("dist/openrepo.html"))

	if gitver == "" {
		log.Println(os.Args[0], "starting")
	} else {
		log.Println(os.Args[0], "version", gitver, "starting")
	}

	datadir := flag.String("datadir", "data", "Data directory")
	listen := flag.String("listen", ":8080", "http server listen address")
	flag.Parse()

	if f, err := os.Stat(*datadir); os.IsNotExist(err) || !f.IsDir() {
		panic("No data directory found! Looked for: " + *datadir + "/\n" +
			"Please specify an existing data directory or create a new one.")
	}

	wsdir := filepath.Join(*datadir, "users")
	if _, err := os.Stat(wsdir); os.IsNotExist(err) {
		os.Mkdir(wsdir, 0755)
	}

	// Stop any dangling bash docker containers running
	err := dockertools.StopImage("workspace-bash")
	if err != nil {
		log.Println("Stopping any workspace-bash image failed:", err)
	}

	dockerhostaddr := getMyDockerIP()

	bootstrapped := auth.Init(*datadir)
	store = sessions.NewCookieStore(auth.GetCookieKey())
	monitor.Init(*datadir)
	email.Init(*datadir)
	slack.Init(*datadir)
	user.Init(*datadir)
	user.InitInvite(*datadir)
	stackman.Init(*datadir)
	wsman.Init(wsdir)
	broker := simple_impl.New()
	gizmostore := gizmo_impl.NewGizmoStore(filepath.Join(*datadir, "ide8_gizmo.db"), broker)
	connect.Init(*datadir, gizmostore)
	bman = board_impl.New(filepath.Join(*datadir, "ide8_board.db"), gizmostore, broker)
	promoter.New(gizmostore, bman)
	if domain == "" {
		runnerInit(dockerhostaddr, "7777")
	} else {
		runnerInit(domain, "7777")
	}
	wsintapigw.Init(dockerhostaddr+":7777", bman)

	sysevent.Send(os.Args[0] + " version " + gitver + " starting")

	if bootstrapped {
		inv, err := user.CreateInvite("", "", time.Now(), time.Now().Add(time.Hour*24*365))
		if err != nil {
			log.Fatal("Failed to create bootstrap invite!")
		}
		log.Println("Use this bootstrap invite to create initial user: /signup/?invite=" + inv.Token)
	}

	store.Options = &sessions.Options{
		Path: "/",
		/*Domain:   "http://mydomain.com/",*/
		MaxAge:   3600 * 24 * 31,
		Secure:   false,
		HttpOnly: true,
	}
	if domain != "" {
		store.Options.Domain = domain
		store.Options.Secure = true
	}

	grpclisten, err := net.Listen("tcp", ":7070")
	if err != nil {
		log.Fatalln("failed to listen:", err)
	}
	var s *grpc.Server
	if domain != "" {
		certbase := filepath.Join("certs", domain)
		creds, err := credentials.NewServerTLSFromFile(certbase+".crt", certbase+".key")
		if err != nil {
			log.Fatal("could not load TLS keys:", err)
		}
		s = grpc.NewServer(grpc.Creds(creds))
	} else {
		s = grpc.NewServer()
	}
	api_tunnel.RegisterTunnelApiServer(s, &grpcTunnelServer{})
	reflection.Register(s)
	go func() {
		if err := s.Serve(grpclisten); err != nil {
			log.Println("Failed to serve:", err)
		}
	}()

	http.HandleFunc("/users/", users)
	http.HandleFunc("/admin/", admin)
	http.HandleFunc("/monitor/", handleMonitor)
	http.HandleFunc("/signup/", register)
	http.HandleFunc("/login", login)
	http.HandleFunc("/logout", logout)
	http.HandleFunc("/open", openRepo)
	http.HandleFunc("/_ping", ping)
	http.HandleFunc("/_workspaces/", workspaces)
	http.HandleFunc("/_boards/", boards)
	http.HandleFunc("/_stacks/", validate(stacks, false))
	http.HandleFunc("/_connect", connect.ServeConnect)
	http.HandleFunc("/_connect/assign", handleAssign)
	http.Handle("/", setCacheControl("public, max-age=0", http.FileServer(http.Dir("dist"))))
	http.Handle("/download/agent", downloadAgent(*datadir))
	http.Handle("/download/", http.FileServer(http.Dir(*datadir)))
	http.HandleFunc("/_ws/", serveWebSocket)
	http.HandleFunc("/_tunnel/", serveTunnelWs)
	http.HandleFunc("/ws/", workspace)

	if err := http.ListenAndServe(*listen, context.ClearHandler(http.DefaultServeMux)); err != nil {
		log.Fatalf("Error with WebDAV server: %v", err)
	}
}
