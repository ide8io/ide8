package user

import (
	"crypto/rand"
	"sync"
	"time"

	"bitbucket.org/ide8io/ide8/backend/monitor"
	"github.com/oklog/ulid"
)

type session struct {
	created time.Time
	last    time.Time
}

var sessionsMu sync.Mutex
var sessions = make(map[string]map[string]session)

func CreateSession(username string) (string, error) {
	t := time.Now()
	key, err := ulid.New(ulid.Timestamp(t), rand.Reader)
	if err != nil {
		return "", err
	}
	id := key.String()
	sessionsMu.Lock()
	defer sessionsMu.Unlock()
	u, ok := sessions[username]
	if !ok {
		u = make(map[string]session)
		sessions[username] = u
	}
	u[id] = session{
		created: t,
		last:    t,
	}
	go publishActiveUsers()
	return id, err
}

func UpdateSession(username string, id string) {
	t := time.Now()
	sessionsMu.Lock()
	defer sessionsMu.Unlock()
	u, ok := sessions[username]
	if !ok {
		u = make(map[string]session)
		sessions[username] = u
	}
	s, ok := u[id]
	if !ok {
		u[id] = session{
			created: t,
			last:    t,
		}
	} else {
		s.last = t
		u[id] = s
	}
	go publishActiveUsers()
}

func DeleteSession(username string, id string) {
	sessionsMu.Lock()
	defer sessionsMu.Unlock()
	u, ok := sessions[username]
	if !ok {
		return
	}
	delete(u, id)
	if len(u) == 0 {
		delete(sessions, username)
	}
	go publishActiveUsers()
}

func publishActiveUsers() {
	active := 0
	t := time.Now()
	sessionsMu.Lock()
	for _, u := range sessions {
		for _, s := range u {
			if t.Sub(s.last) < time.Duration(time.Minute*2) {
				active++
				break
			}
		}
	}
	sessionsMu.Unlock()
	monitor.UpdateUsersActive(active)
}
