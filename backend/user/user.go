package user

import (
	"bitbucket.org/ide8io/ide8/backend/auth"
	"bitbucket.org/ide8io/ide8/backend/monitor"
	"bitbucket.org/ide8io/ide8/backend/util"
	"encoding/json"
	"fmt"
	"github.com/asaskevich/govalidator"
	"golang.org/x/oauth2"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"time"
)

const (
	dbfilename = "ide8_user_db.json"
)

type User struct {
	ID        int    `json:"id"`
	UserName  string `json:"username"`
	FullName  string `json:"fullname"`
	Email     string `json:"email"`
	AvatarURL string `json:"avatarurl"`
	Admin     bool   `json:"admin"`
	password  PasswordHash
	userdir   string
}

type Users []User

type PasswordHash struct {
	Type string `json:"type,omitempty"`
	Hash string `json:"hash,omitempty"`
}

type intUser struct {
	ID       int          `json:"id"`
	UserName string       `json:"username"`
	FullName string       `json:"fullname"`
	Email    string       `json:"email"`
	Admin    bool         `json:"admin"`
	Password PasswordHash `json:"passwordhash,omitempty"`
	GitHub   struct {
		Login       string       `json:"login,omitempty"`
		ID          int64        `json:"id,omitempty"`
		AccessToken oauth2.Token `json:"accesstoken,omitempty"`
	} `json:"github,omitempty"`
	BitBucket struct {
		Username    string       `json:"username,omitempty"`
		AccessToken oauth2.Token `json:"accesstoken,omitempty"`
	} `json:"bitbucket,omitempty"`
	RegDate   time.Time `json:"regdate"`
	AvatarURL string    `json:"avatarurl,omitempty"`
}

type intUsers []intUser

type userDB struct {
	NextID int      `json:"nextid"`
	Users  intUsers `json:"users"`
}

var EventLog = util.EventLogImpl{"user:"}

var user_dir string
var user_db_filename string

func Init(datadir string) {
	user_dir = filepath.Join(datadir, "users")
	user_db_filename = filepath.Join(datadir, dbfilename)
	initSignup(datadir)
	users, _ := List()
	monitor.UpdateUsersTotal(len(users))
}

func readUsers() (userDB, error) {
	if _, err := os.Stat(user_db_filename); os.IsNotExist(err) {
		return userDB{NextID: 1}, nil
	}
	data, err := ioutil.ReadFile(user_db_filename)
	if err != nil {
		return userDB{}, err
	}
	udb := userDB{}
	err = json.Unmarshal(data, &udb)
	if err != nil {
		return userDB{}, err
	}
	return udb, err
}

func writeUsers(udb userDB) {
	data, _ := json.MarshalIndent(udb, "", "  ")
	ioutil.WriteFile(user_db_filename, data, 0600)
	monitor.UpdateUsersTotal(len(udb.Users))
}

// Convert internal user representation to external one
func createUser(intuser intUser) User {
	return User{ID: intuser.ID, UserName: intuser.UserName, FullName: intuser.FullName, Email: intuser.Email, AvatarURL: intuser.AvatarURL, Admin: intuser.Admin, password: intuser.Password, userdir: filepath.Join(user_dir, intuser.UserName)}
}

var usermu sync.RWMutex

func isValidUserName(username string) error {
	if len(username) < 2 {
		return fmt.Errorf("Too short username. Must be between 2 and 15 characters long.")
	}
	if len(username) > 15 {
		return fmt.Errorf("Too long username. Must be between 2 and 15 characters long.")
	}
	// If adjusted; make sure any docker container name is adjusted to fit container naming rules
	matched, err := regexp.MatchString("^[a-zA-Z0-9][a-zA-Z0-9_.-]+$", username)
	if err != nil || !matched {
		return fmt.Errorf("Invalid characters in username. Can be alphanumeric characters pluss underscore (_), dot (.) and dash (-)")
	}
	lusername := strings.ToLower(username)
	if username == "ide8" || lusername == "ide8.io" || lusername == "ide8io" {
		return fmt.Errorf("Username is reserved")
	}
	return nil
}

func isValidPassword(password string) error {
	if len(password) < 8 {
		return fmt.Errorf("Too short password. Must be at least 8 characters long.")
	}
	return nil
}

func Add(username string, fullname string, email string, password string) error {
	if err := isValidUserName(username); err != nil {
		return err
	}
	if err := isValidPassword(password); err != nil {
		return err
	}
	if !govalidator.IsEmail(email) {
		return fmt.Errorf("invalid email address")
	}
	passhash := auth.HashPW(username, password)
	usermu.Lock()
	defer usermu.Unlock()
	udb, err := readUsers()
	if err != nil {
		return err
	}
	for i := range udb.Users {
		if strings.ToLower(udb.Users[i].UserName) == strings.ToLower(username) {
			return fmt.Errorf("user \"%s\" already present in db", username)
		}
	}
	admin := false
	if udb.NextID == 1 && len(udb.Users) == 0 {
		admin = true
	}
	udb.Users = append(udb.Users, intUser{ID: udb.NextID, UserName: username, FullName: fullname, Email: email, Admin: admin, Password: PasswordHash{"sha256", passhash}, RegDate: time.Now()})
	udb.NextID++
	writeUsers(udb)
	return nil
}

func AddOAuthUser(username string, source string, remoteuser string, fullname string, email string, id int64, avatarURL string, accessToken *oauth2.Token) error {
	if err := isValidUserName(username); err != nil {
		return err
	}
	if !govalidator.IsEmail(email) {
		return fmt.Errorf("invalid email address")
	}
	usermu.Lock()
	defer usermu.Unlock()
	udb, err := readUsers()
	if err != nil {
		return err
	}
	for i := range udb.Users {
		if strings.ToLower(udb.Users[i].UserName) == strings.ToLower(username) {
			return fmt.Errorf("username \"%s\" already present in db", username)
		}
		if source == "github" && udb.Users[i].GitHub.Login == remoteuser {
			return fmt.Errorf(" GitHub user \"%s\" already present in db", remoteuser)
		}
		if source == "bitbucket" && udb.Users[i].BitBucket.Username == remoteuser {
			return fmt.Errorf(" BitBucket user \"%s\" already present in db", remoteuser)
		}
	}
	admin := false
	if udb.NextID == 1 && len(udb.Users) == 0 {
		admin = true
	}
	u := intUser{ID: udb.NextID, UserName: username, Admin: admin, FullName: fullname, Email: email, RegDate: time.Now(), AvatarURL: avatarURL}
	if source == "github" {
		u.GitHub.Login = remoteuser
		u.GitHub.ID = id
		u.GitHub.AccessToken = *accessToken
	} else if source == "bitbucket" {
		u.BitBucket.Username = remoteuser
		u.BitBucket.AccessToken = *accessToken
	} else {
		return fmt.Errorf("unsupported authentication source type: %s", source)
	}
	udb.Users = append(udb.Users, u)
	udb.NextID++
	writeUsers(udb)
	return nil
}

func SetAdmin(username string, admin bool) error {
	usermu.Lock()
	defer usermu.Unlock()
	udb, err := readUsers()
	if err != nil {
		return err
	}
	for i := range udb.Users {
		if udb.Users[i].UserName == username {
			udb.Users[i].Admin = admin
		}
	}
	writeUsers(udb)
	return nil
}

func Delete(username string) error {
	usermu.Lock()
	defer usermu.Unlock()
	udb, err := readUsers()
	if err != nil {
		return err
	}
	for i := range udb.Users {
		if udb.Users[i].UserName == username {
			udb.Users = append(udb.Users[:i], udb.Users[i+1:]...)
			break
		}
	}
	writeUsers(udb)
	return nil
}

func Get(username string) (User, error) {
	usermu.RLock()
	udb, err := readUsers()
	usermu.RUnlock()
	if err != nil {
		return User{}, err
	}
	for i := range udb.Users {
		if udb.Users[i].UserName == username {
			return createUser(udb.Users[i]), nil
		}
	}
	return User{}, fmt.Errorf("no user with username: %s", username)
}

func GetByRemoteUser(source string, remoteuser string) (User, error) {
	usermu.RLock()
	udb, err := readUsers()
	usermu.RUnlock()
	if err != nil {
		return User{}, err
	}
	for i := range udb.Users {
		if source == "github" {
			if udb.Users[i].GitHub.Login == remoteuser {
				return createUser(udb.Users[i]), nil
			}
		} else if source == "bitbucket" {
			if udb.Users[i].BitBucket.Username == remoteuser {
				return createUser(udb.Users[i]), nil
			}
		} else {
			return User{}, fmt.Errorf("invalid remote source %s", source)
		}
	}
	return User{}, fmt.Errorf("no user with %s username: %s", source, remoteuser)
}

func List() (Users, error) {
	usermu.Lock()
	defer usermu.Unlock()
	udb, err := readUsers()
	if err != nil {
		return Users{}, err
	}
	extusers := Users{}
	for i := range udb.Users {
		extusers = append(extusers, createUser(udb.Users[i]))
	}
	return extusers, nil
}

func (u *User) Authenticate(password string) bool {
	passhash := auth.HashPW(u.UserName, password)
	if u.password.Hash == passhash {
		return true
	}
	return false
}

func (u *User) GetDir() string {
	return u.userdir
}

func IsValid(username string) bool {
	usermu.RLock()
	udb, err := readUsers()
	usermu.RUnlock()
	if err != nil {
		return false
	}
	for i := range udb.Users {
		if udb.Users[i].UserName == username {
			return true
		}
	}
	return false
}
