package user

import (
	"bitbucket.org/ide8io/ide8/backend/util"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sync"
	"time"
)

const (
	invitedbfilename = "ide8_invite_db.json"
)

type Invitation struct {
	Email   string    `json:"email"`
	Token   string    `json:"token"`
	Created time.Time `json:"created"`
	Expires time.Time `json:"expires"`
	Issuer  string    `json:"issuer"`
}

type Invitations []Invitation

var invite_db_filename string

func InitInvite(datadir string) {
	invite_db_filename = filepath.Join(datadir, invitedbfilename)
}

func readInvites() (Invitations, error) {
	if _, err := os.Stat(invite_db_filename); os.IsNotExist(err) {
		return Invitations{}, nil
	}
	data, err := ioutil.ReadFile(invite_db_filename)
	if err != nil {
		return nil, err
	}
	invs := Invitations{}
	err = json.Unmarshal(data, &invs)
	if err != nil {
		return nil, err
	}
	return invs, nil
}

func writeInvites(invs Invitations) error {
	data, err := json.Marshal(invs)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(invite_db_filename, data, 0600)
	if err != nil {
		return err
	}
	return nil
}

var invitemu sync.RWMutex

func CreateInvite(issuer string, email string, now time.Time, expires time.Time) (Invitation, error) {
	token, err := util.GenerateRandomString(32)
	if err != nil {
		log.Println("CreateInvite generate token:", err)
		return Invitation{}, err
	}
	inv := Invitation{email, token, now, expires, issuer}
	invitemu.Lock()
	defer invitemu.Unlock()
	invs, err := readInvites()
	if err != nil {
		return Invitation{}, err
	}
	invs = append(invs, inv)
	err = writeInvites(invs)
	if err != nil {
		return Invitation{}, err
	}
	return inv, nil
}

func DeleteInvite(token string) error {
	invitemu.Lock()
	defer invitemu.Unlock()
	invs, err := readInvites()
	if err != nil {
		return err
	}
	for i := range invs {
		if invs[i].Token == token {
			invs = append(invs[:i], invs[i+1:]...)
			break
		}
	}
	return writeInvites(invs)
}

func GetInvite(token string) (Invitation, error) {
	invitemu.RLock()
	invs, err := readInvites()
	invitemu.RUnlock()
	if err != nil {
		return Invitation{}, err
	}
	for i := range invs {
		if invs[i].Token == token {
			return invs[i], nil
		}
	}
	return Invitation{}, fmt.Errorf("No invite with token: %s", token)
}

func ListInvites() (Invitations, error) {
	invitemu.Lock()
	defer invitemu.Unlock()
	return readInvites()
}
