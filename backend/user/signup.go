package user

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
	"time"
)

const (
	signupdbfilename = "ide8_signup_db.json"
)

type Signup struct {
	Email   string    `json:"email"`
	From    string    `json:"from"`
	Created time.Time `json:"created"`
}

type Signups []Signup

var signup_db_filename string

func initSignup(datadir string) {
	signup_db_filename = filepath.Join(datadir, signupdbfilename)
}

func readSignups() (Signups, error) {
	if _, err := os.Stat(signup_db_filename); os.IsNotExist(err) {
		return Signups{}, nil
	}
	data, err := ioutil.ReadFile(signup_db_filename)
	if err != nil {
		return nil, err
	}
	ss := Signups{}
	err = json.Unmarshal(data, &ss)
	if err != nil {
		return nil, err
	}
	return ss, nil
}

func writeSignups(ss Signups) error {
	data, err := json.Marshal(ss)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(signup_db_filename, data, 0600)
	if err != nil {
		return err
	}
	return nil
}

var signupmu sync.RWMutex

func AddSignup(email string, from string, now time.Time) error {
	s := Signup{email, from, now}
	signupmu.Lock()
	defer signupmu.Unlock()
	ss, err := readSignups()
	if err != nil {
		return err
	}
	ss = append(ss, s)
	err = writeSignups(ss)
	if err != nil {
		return err
	}
	return nil
}

func DeleteSignup(email string) error {
	signupmu.Lock()
	defer signupmu.Unlock()
	ss, err := readSignups()
	if err != nil {
		return err
	}
	for i := range ss {
		if ss[i].Email == email {
			ss = append(ss[:i], ss[i+1:]...)
			break
		}
	}
	return writeSignups(ss)
}

func ListSignups() (Signups, error) {
	signupmu.Lock()
	defer signupmu.Unlock()
	return readSignups()
}
