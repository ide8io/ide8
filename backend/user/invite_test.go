package user

import (
	"io/ioutil"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNoInit(t *testing.T) {
	_, err := CreateInvite("issuer", "me@me.me", time.Now(), time.Now().Add(time.Hour*24*30))
	if err == nil {
		t.Error("No fail on missing init")
	}
}

func TestCreate(t *testing.T) {
	tmp, _ := ioutil.TempDir("", "")
	InitInvite(tmp)
	now := time.Now()
	expires := now.Add(time.Hour * 24 * 30)
	invite, err := CreateInvite("issuer", "me@me.me", now, expires)
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, "issuer", invite.Issuer)
	assert.Equal(t, now, invite.Created)
	assert.Equal(t, 43, len(invite.Token))
	assert.Equal(t, "me@me.me", invite.Email)
	assert.Equal(t, expires, invite.Expires)
}

func TestList(t *testing.T) {
	tmp, _ := ioutil.TempDir("", "")
	InitInvite(tmp)
	_, err := CreateInvite("issuer1", "me@me.me", time.Now(), time.Now().Add(time.Hour*24*30))
	if err != nil {
		t.Error(err)
	}
	_, err = CreateInvite("issuer2", "you@me.me", time.Now(), time.Now().Add(time.Hour*24*30))
	if err != nil {
		t.Error(err)
	}
	invs, err := ListInvites()
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, 2, len(invs))
}

func TestDelete(t *testing.T) {
	tmp, _ := ioutil.TempDir("", "")
	InitInvite(tmp)
	inv1, err := CreateInvite("issuer1", "me@me.me", time.Now(), time.Now().Add(time.Hour*24*30))
	if err != nil {
		t.Error(err)
	}
	_, err = CreateInvite("issuer2", "you@me.me", time.Now(), time.Now().Add(time.Hour*24*30))
	if err != nil {
		t.Error(err)
	}
	err = DeleteInvite(inv1.Token)
	if err != nil {
		t.Error(err)
	}
	invs, err := ListInvites()
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, 1, len(invs))
}

func TestGet(t *testing.T) {
	tmp, _ := ioutil.TempDir("", "")
	InitInvite(tmp)
	inv1, err := CreateInvite("issuer1", "me@me.me", time.Now(), time.Now().Add(time.Hour*24*30))
	if err != nil {
		t.Error(err)
	}
	inv, err := GetInvite(inv1.Token)
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, inv1.Email, inv.Email)
	assert.Equal(t, inv1.Token, inv.Token)
	assert.True(t, inv1.Created.Equal(inv.Created))
	assert.True(t, inv1.Expires.Equal(inv.Expires))
	assert.Equal(t, inv1.Issuer, inv.Issuer)
}

func TestBadGet(t *testing.T) {
	tmp, _ := ioutil.TempDir("", "")
	InitInvite(tmp)
	_, err := CreateInvite("issuer1", "me@me.me", time.Now(), time.Now().Add(time.Hour*24*30))
	if err != nil {
		t.Error(err)
	}
	_, err = GetInvite("badtoken")
	if err == nil {
		t.Error("No error when getting with bad token!")
	}
}
