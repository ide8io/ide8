package email

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/smtp"
	"os"
	"path/filepath"
	"strconv"
)

type config struct {
	Server string `json:"server"`
	Port   uint16 `json:"port"`
	Login  string `json:"login"`
	Passwd string `json:"passwd"`
	From   string `json:"from"`
}

var configfile string

func Init(datadir string) {
	configfile = filepath.Join(datadir, "ide8_email_config.json")
}

func SetServer(account string, server string, port uint16, login string, passwd string, from string) error {
	var confs map[string]config
	if _, err := os.Stat(configfile); !os.IsNotExist(err) {
		data, err := ioutil.ReadFile(configfile)
		if err != nil {
			return err
		}
		err = json.Unmarshal(data, &confs)
		if err != nil {
			return err
		}
	}
	confs[account] = config{server, port, login, passwd, from}
	data, err := json.Marshal(confs)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(configfile, data, 0600)
	if err != nil {
		return err
	}
	return nil
}

func Send(account string, to string, subject string, body string) error {
	if _, err := os.Stat(configfile); os.IsNotExist(err) {
		return fmt.Errorf("email server not configured")
	}

	data, err := ioutil.ReadFile(configfile)
	if err != nil {
		return err
	}
	var confs map[string]config
	err = json.Unmarshal(data, &confs)
	if err != nil {
		// Convert old single instance into new map one
		var conf config
		err := json.Unmarshal(data, &conf)
		if err != nil {
			return err
		}
		if conf.Server != "" {
			confs = map[string]config{"account": conf}
			data, err := json.Marshal(confs)
			if err != nil {
				return err
			}
			err = ioutil.WriteFile(configfile, data, 0600)
			if err != nil {
				return err
			}
		}
	}

	conf, ok := confs[account]
	if !ok {
		return fmt.Errorf("email server not configured for account: %v", account)
	}

	msg := "From: " + conf.From + "\n" +
		"To: " + to + "\n" +
		"Subject: " + subject + "\n" +
		"\n" +
		body
	err = smtp.SendMail(conf.Server+":"+strconv.Itoa(int(conf.Port)),
		smtp.PlainAuth("", conf.Login, conf.Passwd, conf.Server),
		conf.From, []string{to}, []byte(msg))
	return err
}

func SendInvite(email string, from string, inviteURL string) error {
	msg := from + "@IDE8 have invited you to sign up on IDE8.\n\n" +
		"Please use the following link to register:\n\n" +
		inviteURL
	return Send("account", email, "IDE8 signup invitation", msg)
}
