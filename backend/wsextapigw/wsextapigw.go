package wsextapigw

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"strings"
	"time"

	"bitbucket.org/ide8io/ide8/backend/api/tunnel"
	"bitbucket.org/ide8io/ide8/backend/comm/message"
	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/workspace/wsapi"
	"github.com/gorilla/websocket"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/transport"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Maximum message size allowed from peer.
	maxMessageSize = 8192

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10
)

func ping(ws *websocket.Conn, ctx context.Context) {
	ticker := time.NewTicker(pingPeriod)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			if err := ws.WriteControl(websocket.PingMessage, []byte{}, time.Now().Add(writeWait)); err != nil {
				log.Println("wsextapige: ping:", err)
			}
		case <-ctx.Done():
			return
		}
	}
}

var upgrader = websocket.Upgrader{}

func ForwardWebSocket(w http.ResponseWriter, r *http.Request, server string) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	elog := util.EventLogImpl{Prefix: "wsextapigw: " + util.GetRemoteAddr(r) + r.URL.Path + "->" + server + ":"}
	elog.Println("connecting")
	conn, err := grpc.DialContext(ctx, server, grpc.WithInsecure())
	if err != nil {
		http.Error(w, "websocket forwarding failed: "+err.Error(), http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	client := wsapi.NewWorkspaceApiClient(conn)
	comms, err := client.Comms(ctx)
	if err != nil {
		http.Error(w, "websocket forwarding failed: "+err.Error(), http.StatusInternalServerError)
		return
	}

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		http.Error(w, "websocket upgrade failure: "+err.Error(), http.StatusInternalServerError)
		return
	}
	defer ws.Close()
	go ping(ws, ctx)

	go func() {
		for {
			msg, err := comms.Recv()
			if err != nil {
				if err != transport.ErrConnClosing {
					elog.Println("Failed to receive:", err)
				}
				break
			}
			var payload json.RawMessage = msg.Payload
			response := message.Message{Type: msg.Type, Id: msg.ID, Payload: &payload}
			b, _ := json.Marshal(response)
			err = ws.SetWriteDeadline(time.Now().Add(writeWait))
			if err != nil {
				break
			}
			err = ws.WriteMessage(websocket.TextMessage, b)
			if err != nil {
				elog.Println("websocket write error:", err)
				break
			}
		}
		ws.Close() // To break read loop
	}()

	ws.SetReadLimit(maxMessageSize)
	ws.SetReadDeadline(time.Now().Add(pongWait))
	ws.SetPongHandler(func(string) error {
		ws.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})

	for {
		_, rawmsg, err := ws.ReadMessage()
		if err != nil {
			break
		}

		var payload json.RawMessage
		msg := message.Message{Payload: &payload}
		if err := json.Unmarshal(rawmsg, &msg); err != nil {
			elog.Println(err)
			continue
		}

		err = comms.Send(&wsapi.Message{Type: msg.Type, ID: msg.Id, Payload: payload})
		if err != nil {
			elog.Println("Failed to forward message:", err)
			break
		}
	}
}

type tunnelcreds struct {
	token string
}

func (c *tunnelcreds) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{
		"token": c.token,
	}, nil
}

func (c *tunnelcreds) RequireTransportSecurity() bool {
	return false
}

func ForwardTunnel(w http.ResponseWriter, r *http.Request, server string, tunnelid string, token string) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	elog := util.EventLogImpl{Prefix: "wsextapigw: tunnel: " + tunnelid + " " + util.GetRemoteAddr(r) + r.URL.Path + "->" + server + ":"}
	elog.Println("connecting")
	creds := tunnelcreds{token}
	conn, err := grpc.DialContext(ctx, server, grpc.WithInsecure(), grpc.WithPerRPCCredentials(&creds))
	if err != nil {
		http.Error(w, "websocket forwarding failed: "+err.Error(), http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	client := wsapi.NewWorkspaceApiClient(conn)
	tctx := metadata.NewOutgoingContext(ctx, metadata.Pairs("tunnelid", tunnelid))
	tunnel, err := client.OldTunnel(tctx)
	if err != nil {
		http.Error(w, "websocket forwarding failed: "+err.Error(), http.StatusInternalServerError)
		return
	}

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		http.Error(w, "websocket upgrade failure: "+err.Error(), http.StatusInternalServerError)
		return
	}
	defer ws.Close()

	go func() {
		for {
			msg, err := tunnel.Recv()
			if err != nil {
				if err != transport.ErrConnClosing {
					elog.Println("Failed to receive:", err)
				}
				break
			}
			var payload json.RawMessage = msg.Payload
			id := msg.ID
			if id == "" {
				id = tunnelid
			}
			response := message.Message{Type: msg.Type, Id: msg.ID, Payload: &payload}
			b, _ := json.Marshal(response)
			err = ws.SetWriteDeadline(time.Now().Add(writeWait))
			if err != nil {
				break
			}
			err = ws.WriteMessage(websocket.TextMessage, b)
			if err != nil {
				elog.Println("websocket write error:", err)
				break
			}
		}
		ws.Close() // To break read loop
	}()

	// Client pings every minute, so set read deadline for every ping in order
	// to close connection on missing network. Uses client ping instead of sending
	// ping in both directions.
	orgph := ws.PingHandler()
	ws.SetPingHandler(func(appData string) error {
		ws.SetReadDeadline(time.Now().Add(time.Minute + time.Second*10))
		return orgph(appData)
	})

	for {
		_, rawmsg, err := ws.ReadMessage()
		if err != nil {
			break
		}

		var payload json.RawMessage
		msg := message.Message{Payload: &payload}
		if err := json.Unmarshal(rawmsg, &msg); err != nil {
			elog.Println(err)
			continue
		}

		err = tunnel.Send(&wsapi.Message{Type: msg.Type, ID: msg.Id, Payload: payload})
		if err != nil {
			elog.Println("Failed to forward message:", err)
			break
		}
	}
	elog.Println("closing")
}

type errwriter struct {
}

func (w *errwriter) Write(p []byte) (int, error) {
	log.Println(strings.TrimSuffix(string(p), "\n"))
	return len(p), nil
}

func ForwardUserWebSocket(w http.ResponseWriter, r *http.Request, director func(req *http.Request)) {
	req := r.WithContext(r.Context())
	req.Header = http.Header{}
	director(req)

	connInt, _, err := websocket.DefaultDialer.Dial(req.URL.String(), req.Header)
	if err != nil {
		log.Println("wsextapigw: fwduws dial:", err)
		return
	}
	defer connInt.Close()

	connExt, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("wsextapigw: fwduws upgrade:", err)
		return
	}
	defer connExt.Close()

	copy := func(dst *websocket.Conn, src *websocket.Conn) {
		buf := make([]byte, 32*1024)
		for {
			mType, r, err := src.NextReader()
			if err != nil {
				log.Println("wsextapigw: fwduws read:", err)
				break
			}
			w, err := dst.NextWriter(mType)
			if err != nil {
				log.Println("wsextapigw: fwduws write:", err)
				break
			}
			_, err = io.CopyBuffer(w, r, buf)
			w.Close()
			if err != nil {
				log.Println("wsextapigw: fwduws copy:", err)
				break
			}
		}
	}
	go func() {
		copy(connInt, connExt)
		// Make sure below copy is stopped and not leaking
		connInt.Close()
	}()
	copy(connExt, connInt)
}

func ForwardHttp(w http.ResponseWriter, r *http.Request, director func(req *http.Request)) {
	proxy := httputil.ReverseProxy{
		Director: director,
		ErrorLog: log.New(&errwriter{}, "wsextapigw: "+util.GetRemoteAddr(r)+r.URL.Path+": ", 0),
	}
	proxy.ServeHTTP(w, r)
}

func ForwardAPITunnel(server api_tunnel.TunnelApi_TunnelServer, wsserver string, tunnelid string, token string) error {
	log.Println("wsextapigw: GRPC:", wsserver, tunnelid)
	conn, err := grpc.DialContext(server.Context(), wsserver, grpc.WithInsecure())
	if err != nil {
		return err
	}
	defer conn.Close()

	client := wsapi.NewWorkspaceApiClient(conn)
	tctx, cancel := context.WithCancel(server.Context())
	md := metadata.MD{
		"tunnelid": []string{tunnelid},
		"token":    []string{token},
	}
	tctx = metadata.NewOutgoingContext(tctx, md)
	tunnel, err := client.Tunnel(tctx)
	if err != nil {
		return err
	}

	go func() {
		for {
			msg, err := server.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Println("wsextapigw:", err)
				break
			}
			err = tunnel.Send(msg)
			if err != nil {
				log.Println("wsextapigw:", err)
				break
			}
		}
		cancel()
	}()

	for {
		msg, err := tunnel.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			log.Println("wsextapigw:", err)
			return err
		}
		err = server.Send(msg)
		if err != nil {
			log.Println("wsextapigw:", err)
			return err
		}
	}
}
