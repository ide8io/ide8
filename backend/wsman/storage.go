package wsman

import "time"

type dbitem struct {
	Created    time.Time `json:"created"`
	LastAlive  time.Time `json:"lastalive"`
	LastActive time.Time `json:"lastactive"`
	Status     string    `json:"status"`
	Order      int       `json:"order"`
	Stack      string    `json:"stack"`
	Server     string    `json:"server,omitempty"`
	GRPCPort   string    `json:"grpcport,omitempty"`
	HTTPPort   string    `json:"httpport,omitempty"`
	Token      string    `json:"token,omitempty"`
}
