package wsman

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"time"

	"bitbucket.org/ide8io/ide8/backend/monitor"
	"bitbucket.org/ide8io/ide8/backend/user"
	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/wsman/runner"
	"github.com/boltdb/bolt"
)

const (
	dbfilename    = "wsman_db.json"
	newdbfilename = "wsman.db"
)

const (
	STOPPED  = "STOPPED"
	STARTING = "STARTING"
	STARTED  = "STARTED" // Started but haven't connected back yet
	ONLINE   = "ONLINE"  // Started and have called back
	STOPPING = "STOPPING"
	ERROR    = "ERROR"
)

type Manager struct {
	datadir             string
	db                  *bolt.DB
	runners             []runner.Runner
	runnersMu           sync.Mutex
	stateChangeListen   map[string][]chan string
	stateChangeListenMu sync.Mutex
}

type WS struct {
	Running  bool   `json:"running"`
	Name     string `json:"name"`
	Order    int    `json:"order"`
	Stack    string `json:"stack"`
	Server   string `json:"server"`
	GRPCPort string `json:"grpcport"`
	HTTPPort string `json:"httpport"`
	WSURL    string `json:"wsurl"`
	DAVURL   string `json:"davurl"`
	USERURL  string `json:"userurl"`
}

type WSList []WS

var m *Manager

func Init(datadir string) {
	if _, err := os.Stat(datadir); os.IsNotExist(err) {
		panic("No data directory found! Looked for: " + datadir)
	}

	m = &Manager{
		datadir:           datadir,
		stateChangeListen: make(map[string][]chan string),
	}

	upgradeOldDB := false
	dbfilename := filepath.Join(datadir, newdbfilename)
	if _, err := os.Stat(dbfilename); os.IsNotExist(err) {
		upgradeOldDB = true
	}

	var err error
	m.db, err = bolt.Open(dbfilename, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Fatal("wsman: Init:", err)
	}
	err = m.db.Update(func(tx *bolt.Tx) error {
		tx.CreateBucketIfNotExists([]byte("workspaces"))
		return nil
	})
	if err != nil {
		log.Fatal("wsman: Init:", err)
	}

	if upgradeOldDB {
		files, err := ioutil.ReadDir(datadir)
		if err != nil {
			log.Fatalln("wsman: Init:", err)
		}
		for _, f := range files {
			if f.IsDir() {
				wslist, _ := readOld(f.Name())
				if len(wslist) == 0 {
					continue
				}
				// Re-order old file structure into new for mounting into home dir
				for _, ws := range wslist {
					wsdir := filepath.Join(datadir, f.Name(), ws.Name)
					tmpdir := filepath.Join(datadir, f.Name(), "__"+ws.Name)
					if err := os.Mkdir(tmpdir, 0700); err != nil {
						log.Println(err)
					}
					if err := os.Rename(wsdir, filepath.Join(tmpdir, ws.Name)); err != nil {
						log.Println(err)
					}
					if err := os.Rename(tmpdir, wsdir); err != nil {
						log.Println(err)
					}
					propfile := filepath.Join(datadir, f.Name(), ws.Name+"_properties.json")
					if _, err := os.Stat(propfile); !os.IsNotExist(err) {
						if err := os.Rename(propfile, filepath.Join(wsdir, ".properties.json")); err != nil {
							log.Println(err)
						}
					}
					kvdbfile := filepath.Join(datadir, f.Name(), ws.Name+"_kvdb.db")
					if _, err := os.Stat(kvdbfile); !os.IsNotExist(err) {
						if err := os.Rename(kvdbfile, filepath.Join(wsdir, ".kvdb.db")); err != nil {
							log.Println(err)
						}
					}
				}
				err := m.db.Update(func(tx *bolt.Tx) error {
					b := tx.Bucket([]byte("workspaces"))
					u, err := b.CreateBucket([]byte(f.Name()))
					if err != nil {
						return err
					}
					for _, ws := range wslist {
						order, _ := u.NextSequence()
						item := dbitem{Status: "STOPPED", Stack: ws.Stack, Order: int(order)}
						b, _ := json.Marshal(item)
						u.Put([]byte(ws.Name), b)
					}
					return nil
				})
				if err != nil {
					log.Println("wsman: Init: upgradeold:", err)
				}
			}
		}
	}

	updateStats()
}

func AddRunnerClient(runner runner.Runner) {
	m.runnersMu.Lock()
	m.runners = append(m.runners, runner)
	m.runnersMu.Unlock()
	go func() {
		for {
			ldata := <-runner.Listen()
			log.Println("listen:", ldata)
		}
	}()
}

func updateStats() {
	total := 0
	diskusage := int64(0)
	users, _ := user.List()
	for i := range users {
		wsl, _ := List(users[i].UserName)
		total += len(wsl)
		for j := range wsl {
			size, _ := util.DirSize(GetHomeDir(users[i].UserName, wsl[j].Name))
			diskusage += size
		}
	}
	monitor.UpdateWorkspacesTotal(total)
	monitor.UpdateWorkspacesDiskUsage(diskusage)
}

func GetHomeDir(username string, wsname string) string {
	return filepath.Join(m.datadir, username, wsname)
}

func GetWSDir(username string, wsname string) string {
	return filepath.Join(GetHomeDir(username, wsname), wsname)
}

func Create(username string, wsname string, stack string) error {
	// If adjusted; make sure any docker container name is adjusted to fit container naming rules
	matched, err := regexp.MatchString("^[a-zA-Z0-9_.-]+$", wsname)
	if err != nil || !matched {
		return fmt.Errorf("bad workspace name")
	}

	err = m.db.Update(func(tx *bolt.Tx) error {
		u, err := tx.Bucket([]byte("workspaces")).CreateBucketIfNotExists([]byte(username))
		if err != nil {
			return err
		}
		ws := u.Get([]byte(wsname))
		if ws != nil {
			return fmt.Errorf("workspace \"%s\" already present in db", wsname)
		}
		order, _ := u.NextSequence()
		item := dbitem{Created: time.Now(), Status: STOPPED, Order: int(order), Stack: stack}
		b, err := json.Marshal(item)
		if err != nil {
			return err
		}
		return u.Put([]byte(wsname), b)
	})
	if err != nil {
		return err
	}
	userdir := filepath.Join(m.datadir, username)
	if _, err = os.Stat(userdir); os.IsNotExist(err) {
		os.Mkdir(userdir, 0755)
	}
	err = os.Mkdir(filepath.Join(userdir, wsname), 0700)
	if err != nil {
		return err
	}
	err = os.Mkdir(filepath.Join(userdir, wsname, wsname), 0700)
	if err != nil {
		return err
	}
	go updateStats()
	return nil
}

func viewWS(username string, wsname string, f func(item dbitem) error) error {
	return m.db.View(func(tx *bolt.Tx) error {
		u := tx.Bucket([]byte("workspaces")).Bucket([]byte(username))
		if u == nil {
			return fmt.Errorf("can't find workspace %v for user %v", wsname, username)
		}
		b := u.Get([]byte(wsname))
		if b == nil {
			return fmt.Errorf("can't find workspace %v for user %v", wsname, username)
		}
		var item dbitem
		if err := json.Unmarshal(b, &item); err != nil {
			return err
		}
		return f(item)
	})
}

func updateWS(username string, wsname string, f func(item *dbitem) error) error {
	return m.db.Update(func(tx *bolt.Tx) error {
		u := tx.Bucket([]byte("workspaces")).Bucket([]byte(username))
		if u == nil {
			return fmt.Errorf("can't find workspace %v for user %v", wsname, username)
		}
		b := u.Get([]byte(wsname))
		if b == nil {
			return fmt.Errorf("can't find workspace %v for user %v", wsname, username)
		}
		var item dbitem
		if err := json.Unmarshal(b, &item); err != nil {
			return err
		}
		if err := f(&item); err != nil {
			return err
		}
		wb, err := json.Marshal(item)
		if err != nil {
			return err
		}
		return u.Put([]byte(wsname), wb)
	})
}

var ErrWorkspaceAlreadyRunning = errors.New("workspace already running")

func Start(username string, wsname string, init string) error {
	err := updateWS(username, wsname, func(item *dbitem) error {
		if item.Status == ONLINE {
			return ErrWorkspaceAlreadyRunning
		}
		if item.Status == STOPPED || item.Status == ERROR {
			item.Status = STARTING
			return nil
		}
		return fmt.Errorf("workspace %v is in bad state (%v)", wsname, item.Status)
	})
	if err == ErrWorkspaceAlreadyRunning {
		running := m.runners[0].Probe(username + "/" + wsname)
		if !running {
			err = nil
		}
	}
	if err != nil {
		return err
	}
	response, err := m.runners[0].Start(username + "/" + wsname, init)
	if err != nil {
		updateWS(username, wsname, func(item *dbitem) error {
			item.Status = ERROR
			return nil
		})
		return err
	}
	err = updateWS(username, wsname, func(item *dbitem) error {
		item.Status = STARTED
		item.Server = response.Server
		item.GRPCPort = response.GRPCPort
		item.HTTPPort = response.HTTPPort
		return nil
	})
	go func() {
		listench := stateChangeSubscribe(username, wsname)
		timeout := false
		select {
		case <-listench:
		case <-time.After(time.Second * 10):
			timeout = true
		}
		stateChangeUnsubscribe(username, wsname, listench)
		if timeout {
			state := ""
			updateWS(username, wsname, func(item *dbitem) error {
				if item.Status == STARTED {
					item.Status = ERROR
					state = ERROR
				}
				return nil
			})
			if state != "" {
				stateChangePublish(username, wsname, state)
			}
		}
	}()

	return err
}

func stateChangeSubscribe(username string, wsname string) chan string {
	listenCh := make(chan string)
	m.stateChangeListenMu.Lock()
	defer m.stateChangeListenMu.Unlock()
	m.stateChangeListen[username+"/"+wsname] = append(m.stateChangeListen[username+"/"+wsname], listenCh)
	return listenCh
}

func stateChangeUnsubscribe(username string, wsname string, listenCh chan string) error {
	go func() {
		// Make sure to flush chan to avoid dead-lock
		for range listenCh {
		}
	}()
	m.stateChangeListenMu.Lock()
	defer m.stateChangeListenMu.Unlock()
	close(listenCh)
	id := username + "/" + wsname
	list := m.stateChangeListen[id]
	for i, l := range m.stateChangeListen[id] {
		if l == listenCh {
			if len(list) <= 1 {
				delete(m.stateChangeListen, id)
			} else {
				m.stateChangeListen[id] = append(list[:i], list[i+1:]...)
			}
			return nil
		}
	}
	return fmt.Errorf("unable to find subscriber %v/%v", username, wsname)
}

func stateChangePublish(username string, wsname string, state string) {
	m.stateChangeListenMu.Lock()
	defer m.stateChangeListenMu.Unlock()
	for _, l := range m.stateChangeListen[username+"/"+wsname] {
		l <- state
	}
}

func WaitOnlineOrError(ctx context.Context, username string, wsname string) error {
	l := stateChangeSubscribe(username, wsname)
	defer stateChangeUnsubscribe(username, wsname, l)
	state := ""
	err := viewWS(username, wsname, func(item dbitem) error {
		state = item.Status
		return nil
	})
	if err != nil {
		return err
	}
	if state == ONLINE {
		return nil
	} else if state == ERROR {
		return fmt.Errorf("wait for online failed")
	}
	for {
		select {
		case <-ctx.Done():
			return context.Canceled
		case state := <-l:
			if state == ONLINE {
				return nil
			} else if state == ERROR {
				return fmt.Errorf("wait for online failed")
			}
		}
	}
}

func Stop(username string, wsname string) error {
	err := updateWS(username, wsname, func(item *dbitem) error {
		// Always put it in stopping to avoid any lock-up state
		item.Status = STOPPING
		return nil
	})
	if err != nil {
		return err
	}
	err = m.runners[0].Stop(username + "/" + wsname)
	err2 := updateWS(username, wsname, func(item *dbitem) error {
		item.Status = STOPPED
		item.Server = ""
		return nil
	})
	if err != nil {
		return err
	}
	return err2
}

func SetStatus(username string, wsname string, online bool) error {
	state := ""
	err := updateWS(username, wsname, func(item *dbitem) error {
		if online {
			if item.Status == ONLINE {
				return nil
			}
			if item.Status == STARTED {
				item.Status = ONLINE
				state = ONLINE
				return nil
			}
		} else {

		}
		return fmt.Errorf("SetStatus: invalid workspace run state %v", item.Status)
	})
	if err != nil {
		return err
	}
	stateChangePublish(username, wsname, state)
	return nil
}

func dbitem2WS(wsname string, item dbitem) WS {
	running := false
	if item.Status == ONLINE {
		running = true
	}
	return WS{
		running,
		wsname,
		item.Order,
		item.Stack,
		item.Server,
		item.GRPCPort,
		item.HTTPPort,
		"ws://" + item.Server + "/_ws",
		"http://" + item.Server + "/files/",
		"http://" + item.Server + "/usershare/",
	}
}

func List(username string) (WSList, error) {
	wslist := WSList{}
	err := m.db.View(func(tx *bolt.Tx) error {
		u := tx.Bucket([]byte("workspaces")).Bucket([]byte(username))
		if u == nil {
			return nil
		}
		c := u.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			var item dbitem
			err := json.Unmarshal(v, &item)
			if err != nil {
				return err
			}
			wslist = append(wslist, dbitem2WS(string(k), item))
		}
		return nil
	})
	return wslist, err
}

type AdminWS struct {
	Username   string    `json:"username"`
	WSName     string    `json:"wsname"`
	Created    time.Time `json:"created"`
	LastAlive  time.Time `json:"lastalive"`
	LastActive time.Time `json:"lastactive"`
	Status     string    `json:"status"`
	Stack      string    `json:"stack"`
	Server     string    `json:"server"`
}

func AdminList() []AdminWS {
	var list []AdminWS
	err := m.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("workspaces"))
		uc := b.Cursor()
		for uk, _ := uc.First(); uk != nil; uk, _ = uc.Next() {
			u := b.Bucket(uk)
			if u == nil {
				return fmt.Errorf("no user bucket named %v", string(uk))
			}
			c := u.Cursor()
			for k, v := c.First(); k != nil; k, v = c.Next() {
				var item dbitem
				err := json.Unmarshal(v, &item)
				if err != nil {
					return err
				}
				list = append(list, AdminWS{
					string(uk),
					string(k),
					item.Created,
					item.LastAlive,
					item.LastActive,
					item.Status,
					item.Stack,
					item.Server,
				})
			}
		}
		return nil
	})
	if err != nil {
		log.Println("AdminList:", err)
	}
	return list
}

func Get(username string, wsname string) (WS, error) {
	var item dbitem
	err := m.db.View(func(tx *bolt.Tx) error {
		u := tx.Bucket([]byte("workspaces")).Bucket([]byte(username))
		if u == nil {
			return fmt.Errorf("no workspaces for user %v", username)
		}
		v := u.Get([]byte(wsname))
		if v == nil {
			return fmt.Errorf("workspace \"%s\" not found", wsname)
		}
		return json.Unmarshal(v, &item)
	})
	return dbitem2WS(wsname, item), err
}

func Delete(username string, wsname string) error {
	if wsname == "" {
		return fmt.Errorf("empty workspace name")
	}

	Stop(username, wsname)

	err := m.db.Update(func(tx *bolt.Tx) error {
		u := tx.Bucket([]byte("workspaces")).Bucket([]byte(username))
		if u == nil {
			return fmt.Errorf("can't find workspace for user %v", username)
		}
		return u.Delete([]byte(wsname))
	})
	if err != nil {
		return err
	}

	go updateStats()
	return os.RemoveAll(filepath.Join(m.datadir, username, wsname))
}

func mkDBName(username string) string {
	return filepath.Join(m.datadir, username, dbfilename)
}

func readOld(username string) (WSList, error) {
	if username == "" {
		return nil, fmt.Errorf("Empty username!")
	}
	db := mkDBName(username)
	if _, err := os.Stat(db); os.IsNotExist(err) {
		return WSList{}, nil
	}
	data, err := ioutil.ReadFile(db)
	if err != nil {
		return nil, err
	}
	wslist := WSList{}
	err = json.Unmarshal(data, &wslist)
	if err != nil {
		return nil, err
	}
	// Fix old style stack naming using only name
	for i := range wslist {
		split := strings.SplitN(wslist[i].Stack, "/", 3)
		if len(split) == 1 {
			if split[0] == "nRF52" {
				wslist[i].Stack = "NordicSemi/" + split[0] + "/0.1"
			} else if split[0] == "Telenor" {
				wslist[i].Stack = split[0] + "/ee-0x/0.1"
			} else {
				wslist[i].Stack = split[0] + "/" + split[0] + "/0.1"
			}
		}
	}
	return wslist, nil
}
