package runner

type Runner interface {
	Start(name string, init string) (StartResponse, error)
	Stop(name string) error
	Probe(name string) bool
	Listen() chan struct{}
}

type Client struct {
	Ready   chan<- struct{}
	Start   <-chan Start
	Started chan<- Started
	Stop    <-chan struct{}
}

type Start struct {
	Name string
}

type StartResponse struct {
	Server   string
	HTTPPort string
	GRPCPort string
}

type Started struct {
	Err error
}
