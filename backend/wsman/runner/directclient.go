package runner

import "log"

type DirectClient struct {
	ready chan struct{}
	start chan Start
	started chan Started
	stop chan struct{}
}

func NewDirectClient() *Client {
	client := DirectClient{
		make(chan struct{}),
		make(chan Start),
		make(chan Started),
		make(chan struct{}),
	}


	go client.handler()

	return &Client{
		client.ready,
		client.start,
		client.started,
		client.stop,
	}
}

func (c *DirectClient) handler() {
	for {
		select {
		case <- c.ready:
			log.Println("ready")
		case <- c.started:
			log.Println("started")
		}
	}
}
