package impl

import (
	"context"
	"log"

	"bitbucket.org/ide8io/ide8/backend/board"
)

type wsClient struct {
	wsid     string
	man      *manager
	ctx      context.Context
	cancel   context.CancelFunc
	updateCh chan map[string]board.Board
	addCh    chan string
	remCh    chan string
}

func newWSClient(ctx context.Context, wsid string, boardids []string, man *manager) *wsClient {
	ctx, cancel := context.WithCancel(ctx)
	c := &wsClient{
		wsid:     wsid,
		man:      man,
		ctx:      ctx,
		cancel:   cancel,
		updateCh: make(chan map[string]board.Board),
		addCh:    make(chan string),
		remCh:    make(chan string),
	}
	go c.handler()
	for _, boardid := range boardids {
		c.addBoard(boardid)
	}
	return c
}

func (c *wsClient) UpdateCh() <-chan map[string]board.Board {
	return c.updateCh
}

func (c *wsClient) Close() {
	c.cancel()
}

func (c *wsClient) addBoard(boardid string) error {
	select {
	case c.addCh <- boardid:
		return nil
	case <-c.ctx.Done():
		return c.ctx.Err()
	}
}

func (c *wsClient) remBoard(boardid string) error {
	select {
	case c.remCh <- boardid:
		return nil
	case <-c.ctx.Done():
		return c.ctx.Err()
	}
}

func (c *wsClient) handler() {
	type merge struct {
		boardid string
		board   board.Board
	}
	mergeCh := make(chan merge)
	removeCh := make(chan string)
	boards := map[string]board.Board{}
	bcc := map[string]board.BoardClient{}
	for {
		select {
		case boardid := <-c.addCh:
			_, ok := bcc[boardid]
			if ok {
				break
			}
			bc, err := c.man.BoardClient(c.ctx, boardid)
			if err != nil {
				log.Println("board.WorkspaceClient.handler:", err)
				break
			}
			bcc[boardid] = bc
			go func() {
				for {
					select {
					case u, ok := <-bc.UpdateCh():
						if !ok {
							select {
							case removeCh <- boardid:
							case <-c.ctx.Done():
							}
							return
						}
						select {
						case mergeCh <- merge{boardid: boardid, board: u}:
						case <-c.ctx.Done():
							return
						}
					case <-c.ctx.Done():
						return
					}
				}
			}()
		case boardid := <-c.remCh:
			bc, ok := bcc[boardid]
			if !ok {
				break
			}
			bc.Target("")
			bc.Close()
			delete(bcc, boardid)
		case m := <-mergeCh:
			boards[m.boardid] = m.board
			select {
			case c.updateCh <- boards:
			case <-c.ctx.Done():
			}
		case boardid := <-removeCh:
			delete(boards, boardid)
			select {
			case c.updateCh <- boards:
			case <-c.ctx.Done():
			}
		case <-c.ctx.Done():
			c.man.closeWorkspaceClient(c)
			close(c.updateCh)
			return
		}
	}
}
