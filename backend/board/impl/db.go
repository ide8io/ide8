package impl

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/ide8io/ide8/backend/board"
	"github.com/boltdb/bolt"
)

var boardsBucket = []byte("boards")
var assignBucket = []byte("assign")

func openBuckets(dbfunc func(func(tx *bolt.Tx) error) error, cb func(boards *bolt.Bucket, assign *bolt.Bucket) error) error {
	return dbfunc(func(tx *bolt.Tx) error {
		boards := tx.Bucket(boardsBucket)
		if boards == nil {
			return fmt.Errorf("no boards bucket in db")
		}
		assign := tx.Bucket(assignBucket)
		if assign == nil {
			return fmt.Errorf("no assign bucket in db")
		}
		return cb(boards, assign)
	})
}

func (m *manager) view(cb func(boards *bolt.Bucket, assign *bolt.Bucket) error) error {
	return openBuckets(m.db.View, cb)
}

func (m *manager) update(cb func(boards *bolt.Bucket, assign *bolt.Bucket) error) error {
	return openBuckets(m.db.Update, cb)
}

func modifyBoard(boards *bolt.Bucket, boardid string, cb func(board *board.Board) error) error {
	bid := []byte(boardid)
	boardRaw := boards.Get(bid)
	if boardRaw == nil {
		return fmt.Errorf("no board with id: %v", boardid)
	}
	var board board.Board
	err := json.Unmarshal(boardRaw, &board)
	if err != nil {
		return err
	}
	err = cb(&board)
	if err != nil {
		return err
	}
	boardRaw, err = json.Marshal(board)
	if err != nil {
		return err
	}
	return boards.Put(bid, boardRaw)
}

func modifyAssign(assign *bolt.Bucket, wsid string, cb func(assigns []string) ([]string, error)) error {
	wsbid := []byte(wsid)
	assigns := []string{}
	wsa := assign.Get(wsbid)
	if wsa != nil {
		err := json.Unmarshal(wsa, &assigns)
		if err != nil {
			return err
		}
	}
	var err error
	assigns, err = cb(assigns)
	if err != nil {
		return err
	}
	if len(assigns) == 0 {
		return assign.Delete(wsbid)
	}
	wsa, err = json.Marshal(assigns)
	if err != nil {
		return err
	}
	return assign.Put(wsbid, wsa)
}

func (m *manager) updateBoard(boardid string, cb func(board *board.Board) error) error {
	err := m.update(func(boards *bolt.Bucket, assign *bolt.Bucket) error {
		return modifyBoard(boards, boardid, cb)
	})
	return err
}
