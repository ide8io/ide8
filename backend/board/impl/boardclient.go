package impl

import (
	"context"

	"bitbucket.org/ide8io/ide8/backend/board"
	"bitbucket.org/ide8io/ide8/backend/gizmo"
)

type boardClient struct {
	boardid  string
	board    board.Board
	rc       gizmo.ResourcesClient
	man      *manager
	cancel   context.CancelFunc
	updateCh chan board.Board
}

func newBoardClient(ctx context.Context, boardid string, b board.Board, rc gizmo.ResourcesClient, man *manager) *boardClient {
	ctx, cancel := context.WithCancel(ctx)
	c := &boardClient{
		boardid:  boardid,
		rc:       rc,
		man:      man,
		cancel:   cancel,
		updateCh: make(chan board.Board),
	}

	go func() {
		for u := range rc.UpdateCh() {
			b.Resources = u
			c.updateCh <- b
		}
		close(c.updateCh)
	}()

	go func() {
		<-ctx.Done()
		rc.Close()
	}()

	return c
}

func (c *boardClient) UpdateCh() <-chan board.Board {
	return c.updateCh
}

func (c *boardClient) Close() {
	c.man.closeBoardClient(c)
}

func (c *boardClient) Target(target string) error {
	return c.rc.Target(target)
}

func (c *boardClient) destroy() {
	c.cancel()
}
