package impl_test

import (
	"context"
	"testing"
	"time"

	"bitbucket.org/ide8io/ide8/backend/event"

	"bitbucket.org/ide8io/ide8/backend/board"
	"bitbucket.org/ide8io/ide8/backend/board/impl"
	"bitbucket.org/ide8io/ide8/backend/filename"
	"bitbucket.org/ide8io/ide8/backend/gizmo"
	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type subMock struct {
}

func (s *subMock) Subscribe(topic string, cb func(msg proto.Message)) event.Subscription {
	return nil
}

type gizmoStoreMock struct {
	gizmo.Store
	rc *resourcesClientMock
}

func (s *gizmoStoreMock) ResourcesClient(resources []string) (gizmo.ResourcesClient, error) {
	s.rc = &resourcesClientMock{
		updateCh: make(chan map[string]gizmo.Resource),
	}
	return s.rc, nil
}

type resourcesClientMock struct {
	gizmo.ResourcesClient
	updateCh chan map[string]gizmo.Resource
}

func (c *resourcesClientMock) UpdateCh() <-chan map[string]gizmo.Resource {
	return c.updateCh
}

func (c *resourcesClientMock) Target(target string) error {
	return nil
}

func (c *resourcesClientMock) Close() {
	close(c.updateCh)
}

func setupTestBoardManager(test func(man board.Manager, gsmock *gizmoStoreMock)) {
	tmp := filename.NewTemp()
	defer tmp.Remove()
	gsmock := &gizmoStoreMock{}
	man := impl.New(tmp.Filename(), gsmock, &subMock{})
	test(man, gsmock)
}

func TestNewManagerBadFilename(t *testing.T) {
	assert.Panics(t, func() { impl.New("/", &gizmoStoreMock{}, &subMock{}) })
}

func TestNewManager(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		assert.NotNil(t, man)
	})
}

func TestBoardCreate(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		id, err := man.Create("user", board.Board{Type: "board"})
		require.Nil(t, err)
		assert.Equal(t, "user/board", id)
		id, err = man.Create("user", board.Board{Type: "board"})
		require.Nil(t, err)
		assert.Equal(t, "user/board-1", id)
	})
}

func TestBoardDelete(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		id, err := man.Create("user", board.Board{Type: "board"})
		assert.Nil(t, err)
		err = man.Delete(id)
		assert.Nil(t, err)
		err = man.Delete(id)
		assert.NotNil(t, err)
	})
}

func TestBoardClaimRelease(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		id1, err := man.Create("user", board.Board{Type: "board"})
		assert.Nil(t, err)
		id2, err := man.Create("user", board.Board{Type: "board"})
		assert.Nil(t, err)
		err = man.Claim(id1, "user/ws")
		assert.Nil(t, err)
		err = man.Claim(id2, "user/ws")
		assert.Nil(t, err)
		err = man.Release(id1)
		assert.Nil(t, err)
	})
}

func TestBoardClaimReleaseInvalidBoard(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		err := man.Claim("user/board", "user/ws")
		assert.NotNil(t, err)
		err = man.Release("user/board")
		assert.NotNil(t, err)
	})
}

func TestBoardDoubleClaimRelease(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		id, err := man.Create("user", board.Board{Type: "board"})
		assert.Nil(t, err)
		err = man.Claim(id, "user/ws")
		assert.Nil(t, err)
		err = man.Claim(id, "user/ws")
		assert.Nil(t, err)
		err = man.Release(id)
		assert.Nil(t, err)
		err = man.Release(id)
		assert.Nil(t, err)
	})
}

func TestList(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		boards, err := man.List("", 10)
		require.Nil(t, err)
		assert.Empty(t, boards)
		id, err := man.Create("user", board.Board{Type: "board"})
		assert.Nil(t, err)
		boards, err = man.List("", 10)
		require.Nil(t, err)
		_, ok := boards[id]
		assert.True(t, ok)
	})
}

func TestBoardClientBadBoard(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		_, err := man.BoardClient(context.Background(), "user/board")
		require.NotNil(t, err)
	})
}

func TestBoardClient(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		id, err := man.Create("user", board.Board{Type: "board"})
		require.Nil(t, err)
		bc, err := man.BoardClient(context.Background(), id)
		require.Nil(t, err)
		bc.Close()
		select {
		case _, ok := <-bc.UpdateCh():
			assert.False(t, ok)
		case <-time.After(time.Second):
			assert.Fail(t, "board update chan not closed")
		}
	})
}

func TestBoardClientUpdateCloseOnDelete(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		id, err := man.Create("user", board.Board{Type: "board"})
		require.Nil(t, err)
		bc, err := man.BoardClient(context.Background(), id)
		require.Nil(t, err)
		err = man.Delete(id)
		assert.Nil(t, err)
		select {
		case _, ok := <-bc.UpdateCh():
			assert.False(t, ok)
		case <-time.After(time.Second):
			assert.Fail(t, "board update chan not closed")
		}
	})
}

func TestMultiBoardClient(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		id, err := man.Create("user", board.Board{Type: "board"})
		require.Nil(t, err)
		bc1, err := man.BoardClient(context.Background(), id)
		require.Nil(t, err)
		bc2, err := man.BoardClient(context.Background(), id)
		require.Nil(t, err)
		bc3, err := man.BoardClient(context.Background(), id)
		require.Nil(t, err)
		bc1.Close()
		select {
		case _, ok := <-bc1.UpdateCh():
			assert.False(t, ok)
		case <-time.After(time.Second):
			assert.Fail(t, "board update chan not closed")
		}
		err = man.Delete(id)
		assert.Nil(t, err)
		select {
		case _, ok := <-bc2.UpdateCh():
			assert.False(t, ok)
		case <-time.After(time.Second):
			assert.Fail(t, "board update chan not closed")
		}
		select {
		case _, ok := <-bc3.UpdateCh():
			assert.False(t, ok)
		case <-time.After(time.Second):
			assert.Fail(t, "board update chan not closed")
		}
	})
}

func TestBoardClientUpdate(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		id, err := man.Create("user", board.Board{
			Type:      "board",
			Resources: map[string]gizmo.Resource{"agent/gizmo": {}},
		})
		require.Nil(t, err)
		bc, err := man.BoardClient(context.Background(), id)
		require.Nil(t, err)
		gsmock.rc.updateCh <- map[string]gizmo.Resource{
			"agent/gizmo": {
				Class:  "class",
				Status: gizmo.StatusOnline,
			},
		}
		select {
		case u := <-bc.UpdateCh():
			r, ok := u.Resources["agent/gizmo"]
			require.True(t, ok)
			assert.Equal(t, gizmo.StatusOnline, r.Status)
		case <-time.After(time.Second):
			assert.Fail(t, "board update chan not closed")
		}
		bc.Close()
	})
}

func TestWorkspaceClient(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		wc, err := man.WorkspaceClient(context.Background(), "user/ws")
		require.Nil(t, err)
		wc.Close()
		select {
		case _, ok := <-wc.UpdateCh():
			assert.False(t, ok)
		case <-time.After(time.Second):
			assert.Fail(t, "workspace update chan not closed")
		}
	})
}

func TestMultiWorkspaceClient(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		wc1, err := man.WorkspaceClient(context.Background(), "user/ws")
		require.Nil(t, err)
		wc2, err := man.WorkspaceClient(context.Background(), "user/ws")
		require.Nil(t, err)
		wc1.Close()
		select {
		case _, ok := <-wc1.UpdateCh():
			assert.False(t, ok)
		case <-time.After(time.Second):
			assert.Fail(t, "board update chan not closed")
		}
		wc2.Close()
		select {
		case _, ok := <-wc2.UpdateCh():
			assert.False(t, ok)
		case <-time.After(time.Second):
			assert.Fail(t, "board update chan not closed")
		}
	})
}

func TestWorkspaceClientWithBoard(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		id, err := man.Create("user", board.Board{Type: "board"})
		assert.Nil(t, err)
		err = man.Claim(id, "user/ws")
		assert.Nil(t, err)
		wc, err := man.WorkspaceClient(context.Background(), "user/ws")
		require.Nil(t, err)
		gsmock.rc.updateCh <- map[string]gizmo.Resource{}
		select {
		case u, ok := <-wc.UpdateCh():
			assert.True(t, ok)
			b, ok := u[id]
			assert.True(t, ok)
			assert.Empty(t, b.Resources)
		case <-time.After(time.Second):
			assert.Fail(t, "workspace update chan not closed")
		}
		wc.Close()
		select {
		case _, ok := <-wc.UpdateCh():
			assert.False(t, ok)
		case <-time.After(time.Second):
			assert.Fail(t, "board update chan not closed")
		}
	})
}

func TestWorkspaceClientWithClaimRelease(t *testing.T) {
	setupTestBoardManager(func(man board.Manager, gsmock *gizmoStoreMock) {
		require.NotNil(t, man)
		id, err := man.Create("user", board.Board{Type: "board"})
		assert.Nil(t, err)
		wc, err := man.WorkspaceClient(context.Background(), "user/ws")
		require.Nil(t, err)
		err = man.Claim(id, "user/ws")
		assert.Nil(t, err)
		time.Sleep(time.Second / 10)
		gsmock.rc.updateCh <- map[string]gizmo.Resource{}
		select {
		case u, ok := <-wc.UpdateCh():
			assert.True(t, ok)
			b, ok := u[id]
			assert.True(t, ok)
			assert.Empty(t, b.Resources)
		case <-time.After(time.Second):
			assert.Fail(t, "workspace update chan not closed")
		}
		err = man.Release("user/board")
		select {
		case u, ok := <-wc.UpdateCh():
			assert.True(t, ok)
			assert.Empty(t, u)
		case <-time.After(time.Second):
			assert.Fail(t, "workspace update chan not closed")
		}
	})
}
