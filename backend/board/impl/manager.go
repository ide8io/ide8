package impl

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"bitbucket.org/ide8io/ide8/backend/board"
	"bitbucket.org/ide8io/ide8/backend/boltpaging"
	"bitbucket.org/ide8io/ide8/backend/event"
	"bitbucket.org/ide8io/ide8/backend/event/message"
	"bitbucket.org/ide8io/ide8/backend/gizmo"
	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
)

type manager struct {
	db           *bolt.DB
	subscriber   event.Subscriber
	clientsMu    sync.Mutex
	boardClients map[string][]*boardClient
	wsClients    map[string][]*wsClient
	gizmosStore  gizmo.Store
}

func New(dbfilename string, gizmoStore gizmo.Store, subscriber event.Subscriber) board.Manager {
	m := &manager{
		subscriber:   subscriber,
		boardClients: make(map[string][]*boardClient),
		wsClients:    make(map[string][]*wsClient),
		gizmosStore:  gizmoStore,
	}
	var err error
	m.db, err = bolt.Open(dbfilename, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Panicln("board.Manager: New:", err)
	}
	err = m.db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(boardsBucket)
		if err != nil {
			return err
		}
		_, err = tx.CreateBucketIfNotExists(assignBucket)
		return err
	})
	if err != nil {
		log.Panicln("board.Manager: New:", err)
	}

	subscriber.Subscribe("resource.update", m.updateResourceStatus)
	return m
}

func (m *manager) updateResourceStatus(msg proto.Message) {
	su := msg.(*event_resource.StatusUpdate)
	if su == nil {
		log.Println("board.Manager: unknown resource update message type:", reflect.TypeOf(msg))
	}
	err := m.update(func(boards *bolt.Bucket, assign *bolt.Bucket) error {
		return boards.ForEach(func(k, v []byte) error {
			var sb board.Board
			err := json.Unmarshal(v, &sb)
			if err != nil {
				return err
			}
			found := false
			for id := range su.Resources {
				for sid := range sb.Resources {
					if id == sid {
						res := sb.Resources[sid]
						res.Status = su.Resources[id].Status
						sb.Resources[sid] = res
						found = true
					}
				}
			}
			if found {
				boardRaw, err := json.Marshal(sb)
				if err != nil {
					return err
				}
				err = boards.Put(k, boardRaw)
				return err
			}
			return nil
		})
	})
	if err != nil {
		log.Println("board.Manager: failed to update resource status:", err)
	}
}

func (m *manager) Create(owner string, brd board.Board) (string, error) {
	baseid := owner + "/" + brd.Type
	id := baseid
	err := m.update(func(boards *bolt.Bucket, assign *bolt.Bucket) error {
		for i := 0; i < 1000; i++ {
			if i > 0 {
				id = baseid + "-" + strconv.Itoa(i)
			}
			bid := []byte(id)
			boardRaw := boards.Get(bid)
			if boardRaw != nil {
				continue
			}
			now := time.Now()
			brd.Created = now
			brd.Last = now
			b, err := json.Marshal(brd)
			if err != nil {
				return err
			}
			return boards.Put(bid, b)
		}
		return fmt.Errorf("failed to create unique name for board: %v", id)
	})
	return id, err
}

func (m *manager) FindByResources(resources map[string]gizmo.Resource) (string, board.Board, error) {
	boardid := ""
	var brd board.Board
	err := m.view(func(boards *bolt.Bucket, assign *bolt.Bucket) error {
		return boards.ForEach(func(k, v []byte) error {
			var sb board.Board
			err := json.Unmarshal(v, &sb)
			if err != nil {
				return err
			}
			currentboardid := string(k)
			for id := range resources {
				for sid := range sb.Resources {
					if id == sid {
						if boardid != "" && boardid != currentboardid {
							return fmt.Errorf("inconclusive mapping of board resources")
						}
						boardid = currentboardid
						brd = sb
					}
				}
			}
			return nil
		})
	})
	return boardid, brd, err
}

func (m *manager) Update(owner string, brd board.Board) (string, error) {
	boardid := ""
	err := m.update(func(boards *bolt.Bucket, assign *bolt.Bucket) error {
		err := boards.ForEach(func(k, v []byte) error {
			var sb board.Board
			err := json.Unmarshal(v, &sb)
			if err != nil {
				return err
			}
			currentboardid := string(k)
			for id := range brd.Resources {
				for sid := range sb.Resources {
					if id == sid {
						if boardid != "" && boardid != currentboardid {
							return fmt.Errorf("inconclusive mapping of board resources")
						}
						boardid = currentboardid
					}
				}
			}
			return nil
		})
		if err != nil {
			return err
		}
		if boardid == "" {
			return nil
		}
		err = modifyBoard(boards, boardid, func(sbrd *board.Board) error {
			sbrd.Last = time.Now()
			sbrd.Type = brd.Type
			sbrd.Info = brd.Info
			sbrd.Resources = brd.Resources
			return nil
		})
		return err
	})
	return boardid, err
}

func (m *manager) Delete(boardid string) error {
	err := m.update(func(boards *bolt.Bucket, assign *bolt.Bucket) error {
		bid := []byte(boardid)
		boardRaw := boards.Get(bid)
		if boardRaw == nil {
			return fmt.Errorf("no such board: %v", boardid)
		}
		return boards.Delete([]byte(boardid))
	})
	if err != nil {
		return err
	}
	m.clientsMu.Lock()
	defer m.clientsMu.Unlock()
	bcc, ok := m.boardClients[boardid]
	if ok {
		for _, bc := range bcc {
			bc.destroy()
		}
		delete(m.boardClients, boardid)
	}
	return nil
}

func (m *manager) UpdateBoardType(boardid string, btype string) error {
	// TODO: Input validation
	err := m.updateBoard(boardid, func(board *board.Board) error {
		board.Type = btype
		return nil
	})
	return err
}

func (m *manager) UpdateBoardInfo(boardid string, info string) error {
	// TODO: Input validation
	err := m.updateBoard(boardid, func(board *board.Board) error {
		board.Info = info
		return nil
	})
	return err
}

func (m *manager) List(previd string, count int) (map[string]board.Board, error) {
	boardmap := make(map[string]board.Board)
	err := m.view(func(boards *bolt.Bucket, assign *bolt.Bucket) error {
		boltpaging.IterPage(boards, previd, count, func(k, v []byte) error {
			var board board.Board
			if v != nil {
				err := json.Unmarshal(v, &board)
				if err != nil {
					return err
				}
			}
			boardmap[string(k)] = board
			return nil
		})
		return nil
	})
	return boardmap, err
}

func (m *manager) ListByType(btype string, previd string, count int) (map[string]board.Board, error) {
	boardmap := make(map[string]board.Board)
	err := m.view(func(boards *bolt.Bucket, assign *bolt.Bucket) error {
		k, v, c := boltpaging.PageCursor(boards, previd)
		for i := 0; k != nil && i < count; k, v = c.Next() {
			if v != nil {
				var board board.Board
				err := json.Unmarshal(v, &board)
				if err != nil {
					return err
				}
				if btype == board.Type {
					for _, r := range board.Resources {
						if r.Status == gizmo.StatusOnline {
							boardmap[string(k)] = board
							break
						}
					}
				}
				i++
			}
		}
		return nil
	})
	return boardmap, err
}

func (m *manager) ListMine(username string, previd string, count int) (map[string]board.Board, error) {
	boardmap := make(map[string]board.Board)
	if previd == "" {
		previd = username + "/"
	} else if !strings.HasPrefix(previd, username+"/") {
		return nil, fmt.Errorf("invalid previous id for username %s", username)
	}
	err := m.view(func(boards *bolt.Bucket, assign *bolt.Bucket) error {
		k, v, c := boltpaging.PageCursor(boards, previd)
		for i := 0; k != nil && i < count; k, v = c.Next() {
			if v != nil {
				if strings.HasPrefix(string(k), username+"/") {
					var board board.Board
					err := json.Unmarshal(v, &board)
					if err != nil {
						return err
					}
					boardmap[string(k)] = board
					i++
				} else {
					break
				}
			}
		}
		return nil
	})
	return boardmap, err
}

func (m *manager) Claim(boardid string, wsid string) error {
	err := m.update(func(boards *bolt.Bucket, assign *bolt.Bucket) error {
		err := modifyBoard(boards, boardid, func(board *board.Board) error {
			board.AssignedTo = wsid
			return nil
		})
		if err != nil {
			return err
		}
		err = modifyAssign(assign, wsid, func(assigns []string) ([]string, error) {
			for _, v := range assigns {
				if v == boardid {
					return assigns, nil
				}
			}
			return append(assigns, boardid), nil
		})
		return err
	})
	if err != nil {
		return err
	}
	m.clientsMu.Lock()
	defer m.clientsMu.Unlock()
	wscc, ok := m.wsClients[wsid]
	if ok {
		for _, wsc := range wscc {
			wsc.addBoard(boardid)
		}
	}
	return nil
}

func (m *manager) Release(boardid string) error {
	assignedTo := ""
	err := m.update(func(boards *bolt.Bucket, assign *bolt.Bucket) error {
		err := modifyBoard(boards, boardid, func(board *board.Board) error {
			assignedTo = board.AssignedTo
			board.AssignedTo = ""
			return nil
		})
		if err != nil {
			return err
		}
		err = modifyAssign(assign, assignedTo, func(assigns []string) ([]string, error) {
			for i, v := range assigns {
				if v == boardid {
					return append(assigns[:i], assigns[i+1:]...), nil
				}
			}
			return assigns, nil
		})
		return err
	})
	if err != nil {
		return err
	}
	m.clientsMu.Lock()
	defer m.clientsMu.Unlock()
	wscc, ok := m.wsClients[assignedTo]
	if ok {
		for _, wsc := range wscc {
			wsc.remBoard(boardid)
		}
	}
	return nil
}

func (m *manager) BoardClient(ctx context.Context, boardid string) (board.BoardClient, error) {
	resources := []string{}
	var board board.Board
	err := m.view(func(boards *bolt.Bucket, assign *bolt.Bucket) error {
		boardRaw := boards.Get([]byte(boardid))
		if boardRaw == nil {
			return fmt.Errorf("no board with id: %v", boardid)
		}
		err := json.Unmarshal(boardRaw, &board)
		if err != nil {
			return err
		}
		for id := range board.Resources {
			resources = append(resources, id)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	rc, err := m.gizmosStore.ResourcesClient(resources)
	if err != nil {
		return nil, err
	}
	bc := newBoardClient(ctx, boardid, board, rc, m)
	bc.Target(board.AssignedTo)
	m.clientsMu.Lock()
	defer m.clientsMu.Unlock()
	bcc, ok := m.boardClients[boardid]
	if ok {
		m.boardClients[boardid] = append(bcc, bc)
	} else {
		m.boardClients[boardid] = []*boardClient{bc}
	}
	return bc, err
}

func (m *manager) closeBoardClient(bc *boardClient) {
	m.clientsMu.Lock()
	defer m.clientsMu.Unlock()
	bcc, ok := m.boardClients[bc.boardid]
	if ok {
		for i, c := range bcc {
			if c == bc {
				if len(bcc) == 1 {
					delete(m.boardClients, bc.boardid)
				} else {
					m.boardClients[bc.boardid] = append(bcc[:i], bcc[i+1:]...)
				}
				break
			}
		}
	}
	bc.destroy()
}

func (m *manager) WorkspaceClient(ctx context.Context, wsid string) (board.WorkspaceClient, error) {
	boardids := []string{}
	err := m.view(func(boards *bolt.Bucket, assign *bolt.Bucket) error {
		assignRaw := assign.Get([]byte(wsid))
		if assignRaw == nil {
			// No assignment; just return
			return nil
		}
		return json.Unmarshal(assignRaw, &boardids)
	})
	if err != nil {
		return nil, err
	}
	wc := newWSClient(ctx, wsid, boardids, m)
	m.clientsMu.Lock()
	defer m.clientsMu.Unlock()
	wcc, ok := m.wsClients[wsid]
	if ok {
		m.wsClients[wsid] = append(wcc, wc)
	} else {
		m.wsClients[wsid] = []*wsClient{wc}
	}
	return wc, nil
}

func (m *manager) closeWorkspaceClient(wc *wsClient) {
	m.clientsMu.Lock()
	defer m.clientsMu.Unlock()
	wcc, ok := m.wsClients[wc.wsid]
	if ok {
		for i, c := range wcc {
			if c == wc {
				if len(wcc) == 1 {
					delete(m.wsClients, wc.wsid)
				} else {
					m.wsClients[wc.wsid] = append(wcc[:i], wcc[i+1:]...)
				}
				break
			}
		}
	}
}
