package board

import (
	"context"
	"time"

	"bitbucket.org/ide8io/ide8/backend/gizmo"
)

type Board struct {
	Created    time.Time                 `json:"created"`
	Last       time.Time                 `json:"last"`
	Type       string                    `json:"type"`
	Info       string                    `json:"info"`
	Resources  map[string]gizmo.Resource `json:"resources"`
	AssignedTo string                    `json:"assignedto"`
}

type Type struct {
}

type BoardClient interface {
	UpdateCh() <-chan Board
	Target(target string) error
	Close()
}

type WorkspaceClient interface {
	UpdateCh() <-chan map[string]Board
	Close()
}

type Manager interface {
	Create(owner string, board Board) (string, error)
	FindByResources(resources map[string]gizmo.Resource) (string, Board, error)
	Update(owner string, board Board) (string, error)
	Delete(boardid string) error

	UpdateBoardType(boardid string, btype string) error
	UpdateBoardInfo(boardid string, info string) error

	List(previd string, count int) (map[string]Board, error)
	ListByType(btype string, previd string, count int) (map[string]Board, error)
	ListMine(username string, previd string, count int) (map[string]Board, error)

	BoardClient(ctx context.Context, boardid string) (BoardClient, error)
	WorkspaceClient(ctx context.Context, wsid string) (WorkspaceClient, error)
	Claim(boardid string, wsid string) error
	Release(boardid string) error
}

type Types interface {
	Get(id string) (Type, error)
	List(previd string, count int) (map[string]Type, error)
	Search(term string, count int) (map[string]Type, error)
}
