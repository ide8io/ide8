package event

import "github.com/golang/protobuf/proto"

type Subscription interface {
	Unsubscribe()
}

type Subscriber interface {
	Subscribe(topic string, cb func(msg proto.Message)) Subscription
}
