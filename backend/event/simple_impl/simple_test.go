package simple_impl_test

import (
	"testing"
	"time"

	"bitbucket.org/ide8io/ide8/backend/event/message"
	"bitbucket.org/ide8io/ide8/backend/event/simple_impl"
	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/assert"
)

func TestPubSub(t *testing.T) {
	b := simple_impl.New()
	go b.Publish("lard", &event_resource.StatusUpdate{})
	msgCh := make(chan proto.Message)
	sub := b.Subscribe("lard", func(msg proto.Message) {
		msgCh <- msg
	})
	select {
	case msg := <-msgCh:
		assert.Equal(t, "", msg.String())
	case <-time.After(time.Second):
		assert.Fail(t, "timeout waiting for subscribed message")
	}
	sub.Unsubscribe()
}

func TestPubDualSub(t *testing.T) {
	b := simple_impl.New()
	msgCh1 := make(chan proto.Message)
	sub1 := b.Subscribe("lard", func(msg proto.Message) {
		msgCh1 <- msg
	})
	msgCh2 := make(chan proto.Message)
	sub2 := b.Subscribe("lard", func(msg proto.Message) {
		msgCh2 <- msg
	})
	b.Publish("lard", &event_resource.StatusUpdate{})
	select {
	case msg := <-msgCh1:
		assert.Equal(t, "", msg.String())
	case <-time.After(time.Second):
		assert.Fail(t, "timeout waiting for subscribed message")
	}
	select {
	case msg := <-msgCh2:
		assert.Equal(t, "", msg.String())
	case <-time.After(time.Second):
		assert.Fail(t, "timeout waiting for subscribed message")
	}
	sub1.Unsubscribe()
	sub2.Unsubscribe()
}
