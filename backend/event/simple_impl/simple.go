package simple_impl

import (
	"sync"

	"bitbucket.org/ide8io/ide8/backend/event"
	"github.com/golang/protobuf/proto"
)

type Broker struct {
	mmMu sync.Mutex
	mm   map[string]*handler
}

func New() *Broker {
	return &Broker{
		mm: make(map[string]*handler),
	}
}

func (b *Broker) getHandler(topic string) *handler {
	b.mmMu.Lock()
	defer b.mmMu.Unlock()
	h, ok := b.mm[topic]
	if !ok {
		h = &handler{
			pubCh:   make(chan proto.Message),
			subCh:   make(chan *sub),
			unsubCh: make(chan *sub),
		}
		go h.handle()
		b.mm[topic] = h
	}
	return h
}

func (b *Broker) Publish(topic string, msg proto.Message) {
	b.getHandler(topic).publish(msg)
}

func (b *Broker) Subscribe(topic string, cb func(msg proto.Message)) event.Subscription {
	h := b.getHandler(topic)
	s := &sub{
		h:  h,
		cb: cb,
	}
	h.subCh <- s
	return s
}

type handler struct {
	pubCh   chan proto.Message
	subCh   chan *sub
	unsubCh chan *sub
}

func (h *handler) publish(msg proto.Message) {
	h.pubCh <- msg
}

func (h *handler) handle() {
	subs := []*sub{}
	for {
		sub := <-h.subCh
		subs = append(subs, sub)
	loop:
		for {
			select {
			case sub := <-h.subCh:
				subs = append(subs, sub)
			case unsub := <-h.unsubCh:
				for i, s := range subs {
					if s == unsub {
						subs = append(subs[:i], subs[i+1:]...)
						break
					}
				}
				if len(subs) == 0 {
					break loop
				}
			case msg := <-h.pubCh:
				for _, s := range subs {
					s.cb(msg)
				}
			}
		}
	}
}

type sub struct {
	h  *handler
	cb func(msg proto.Message)
}

func (s *sub) Unsubscribe() {
	s.h.unsubCh <- s
}
