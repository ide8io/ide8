package event

import "github.com/golang/protobuf/proto"

type Publisher interface {
	Publish(topic string, msg proto.Message)
}
