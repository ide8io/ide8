package wsrun

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"bitbucket.org/ide8io/ide8/backend/stackman"
	"bitbucket.org/ide8io/ide8/backend/user"
	"bitbucket.org/ide8io/ide8/backend/wsman"
	"bitbucket.org/ide8io/ide8/backend/wsman/runner"
	"github.com/fsouza/go-dockerclient"
)

const (
	container_prefix = "workspace_"
)

type Runner struct {
	expose  bool
	server  string
	port    string
	wsmap   map[string]*ws
	dclient *docker.Client
	listen  chan struct{}
}

type ws struct {
}

func New(server string, port string) (*Runner, error) {
	r := &Runner{
		expose: false,
		server: server,
		port:   port,
		listen: make(chan struct{}),
	}
	var err error
	r.dclient, err = docker.NewClient("unix:///var/run/docker.sock")
	if err != nil {
		return nil, fmt.Errorf("runner: failed to create docker client: %v", err)
	}

	return r, nil
}

func containerName(name string) string {
	return container_prefix + strings.Replace(name, "/", "_", -1)
}

func mkGitConfig(homedir string, fullname string, email string) error {
	gitconfig := filepath.Join(homedir, ".gitconfig")
	if _, err := os.Stat(gitconfig); !os.IsNotExist(err) {
		return nil
	}
	cfg := "[user]\n" +
		"\tname = " + fullname + "\n" +
		"\temail = " + email + "\n"
	return ioutil.WriteFile(gitconfig, []byte(cfg), 0644)
}

func (r *Runner) Start(name string, init string) (runner.StartResponse, error) {
	log.Println("starting workspace:", name)
	var response runner.StartResponse
	cname := containerName(name)
	wsname := strings.SplitN(name, "/", 2)
	if len(wsname) != 2 {
		return response, fmt.Errorf("invalid workspace name: %v", name)
	}
	exposeHTTPPort := docker.Port("8080/tcp")
	exposeGRPCPort := docker.Port("7777/tcp")
	exposedPorts := make(map[docker.Port]struct{})
	publishAllPorts := false
	if r.expose {
		exposedPorts[exposeHTTPPort] = struct{}{}
		exposedPorts[exposeGRPCPort] = struct{}{}
		publishAllPorts = true
	}
	con, err := r.dclient.InspectContainer(cname)
	if err == nil {
		log.Println("deleting container..", con.State)
		err := r.dclient.KillContainer(docker.KillContainerOptions{ID: cname})
		if err != nil {
			log.Println("failed to stop container:", err)
		}
		err = r.dclient.RemoveContainer(docker.RemoveContainerOptions{ID: cname})
		if err != nil {
			log.Println("failed to remove container:", err)
		}
	}
	ws, err := wsman.Get(wsname[0], wsname[1])
	if err != nil {
		return response, err
	}
	stack, err := stackman.GetStack(ws.Stack)
	if err != nil {
		return response, err
	}
	wsdir, err := filepath.Abs(wsman.GetHomeDir(wsname[0], wsname[1]))
	if err != nil {
		return response, err
	}
	u, err := user.Get(wsname[0])
	if err != nil {
		return response, err
	}
	err = mkGitConfig(wsdir, u.FullName, u.Email)
	if err != nil {
		return response, err
	}
	err = exec.Command("chown", "-hR", "1000:1000", wsdir).Run()
	if err != nil {
		log.Println("chown", wsdir, err)
	}
	var cmds []string
	if init != "" {
		cmds = []string{"-init", init}
	}
	var links []string
	var extrahosts []string
	if r.server == "172.17.0.1" {
		// Special handling if server is not running in container but directly on host
		extrahosts = []string{"backend:" + r.server}
	} else {
		links = []string{r.server + ":backend"}
	}
	wsagent, err := filepath.Abs("workspace/workspace")
	if err != nil {
		return response, err
	}
	memory := 256
	if stack.Memory > memory {
		memory = stack.Memory
	}
	_, err = r.dclient.CreateContainer(docker.CreateContainerOptions{
		Name: cname,
		Config: &docker.Config{
			Env: []string{
				"SERVER=" + "backend:" + r.port,
				"WORKSPACE=" + name,
				"TOKEN=123",
				"WSDIR=/home/user/" + wsname[1],
			},
			Image:        stack.Image,
			User:         "user",
			Hostname:     "ide8",
			ExposedPorts: exposedPorts,
			Entrypoint:   []string{"/ide8/wsagent"},
			Cmd:          cmds,
		},
		HostConfig: &docker.HostConfig{
			PublishAllPorts: publishAllPorts,
			Binds: []string{
				wsdir + ":/home/user",
				wsagent + ":/ide8/wsagent:ro",
			},
			Memory:      int64(memory) * 1024 * 1024,
			OomScoreAdj: 100,
			AutoRemove:  true,
			Links:       links,
			ExtraHosts:  extrahosts,
		},
	})
	if err != nil {
		return response, fmt.Errorf("failed to create container from image %v: %v", stack.Image, err)
	}
	err = r.dclient.StartContainer(cname, nil)
	if err != nil {
		return response, fmt.Errorf("failed to start container %v: %v", cname, err)
	}
	cont, err := r.dclient.InspectContainer(cname)
	if err != nil {
		return response, err
	}
	log.Println("status:", cont.State.String())
	if r.expose {
		portlist, ok := cont.NetworkSettings.Ports[exposeHTTPPort]
		if ok && len(portlist) > 0 {
			response.HTTPPort = portlist[0].HostPort
		}
		portlist, ok = cont.NetworkSettings.Ports[exposeGRPCPort]
		if ok && len(portlist) > 0 {
			response.GRPCPort = portlist[0].HostPort
		}
		log.Println("exposed ports: grpc:", response.GRPCPort, "http:", response.HTTPPort)
	} else {
		response.Server = cont.NetworkSettings.IPAddress
		response.HTTPPort = "8080"
		response.GRPCPort = "7777"
	}
	return response, nil
}

func (r *Runner) Stop(name string) error {
	cname := containerName(name)
	return r.dclient.StopContainer(cname, 10)
}

func (r *Runner) Probe(name string) bool {
	cname := containerName(name)
	con, err := r.dclient.InspectContainer(cname)
	if err != nil {
		log.Println("wsrun: probe:", err)
		return false
	}
	return con.State.Running
}

func (r *Runner) Listen() chan struct{} {
	return r.listen
}
