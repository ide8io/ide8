package wsintapigw

import (
	"context"
	"fmt"
	"log"
	"net"
	"strings"

	"bitbucket.org/ide8io/ide8/backend/board"
	"bitbucket.org/ide8io/ide8/backend/gizmo"
	"bitbucket.org/ide8io/ide8/backend/stackman"
	"bitbucket.org/ide8io/ide8/backend/util"
	"bitbucket.org/ide8io/ide8/backend/wsintapigw/pb"
	"bitbucket.org/ide8io/ide8/backend/wsman"
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes/any"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
)

func Init(server string, bman board.Manager) {
	listen, err := net.Listen("tcp", server)
	if err != nil {
		log.Fatalln("wsintapigw: failed to listen:", err)
	}
	gs := grpcserver{
		elog: &util.EventLogImpl{Prefix: "wsintapigw:"},
		bman: bman,
	}
	s := grpc.NewServer()
	wsintapi.RegisterWorkspaceInternalApiServer(s, &gs)
	reflection.Register(s)
	go func() {
		if err := s.Serve(listen); err != nil {
			log.Println("wsintapigw: Failed to serve:", err)
		}
	}()
}

type grpcserver struct {
	elog util.EventLog
	bman board.Manager
}

func authorize(ctx context.Context, username *string, wsname *string) error {
	autherr := fmt.Errorf("unauthorized")
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return autherr
	}
	ws, ok := md["workspace"]
	if !ok || len(ws) == 0 {
		return autherr
	}
	wsarr := strings.SplitN(ws[0], "/", 2)
	if len(wsarr) != 2 {
		return autherr
	}
	// TODO: check token
	if username != nil {
		*username = wsarr[0]
	}
	if wsname != nil {
		*wsname = wsarr[1]
	}
	return nil
}

func (s *grpcserver) Status(ctx context.Context, r *wsintapi.StatusRequest) (*wsintapi.StatusResponse, error) {
	var username string
	var wsname string
	err := authorize(ctx, &username, &wsname)
	if err != nil {
		return nil, err
	}
	md, _ := metadata.FromIncomingContext(ctx)
	s.elog.Println("ws:", md)
	s.elog.Println("status:", r.Status)
	err = wsman.SetStatus(username, wsname, r.Status == wsintapi.StatusRequest_ONLINE)
	if err != nil {
		return nil, err
	}
	return &wsintapi.StatusResponse{}, nil
}

func (s *grpcserver) GetStack(ctx context.Context, r *wsintapi.StackRequest) (*wsintapi.Stack, error) {
	var username string
	var wsname string
	err := authorize(ctx, &username, &wsname)
	if err != nil {
		return nil, err
	}
	log.Println("getstack:", r)
	ws, err := wsman.Get(username, wsname)
	if err != nil {
		return nil, err
	}
	stack, err := stackman.GetStack(ws.Stack)
	if err != nil {
		return nil, err
	}
	rstack := wsintapi.Stack{
		Name:      stack.Name,
		Resources: make(map[string]*wsintapi.Stack_Resource),
		Commands:  make(map[string]*wsintapi.Stack_Command),
		Examples:  make(map[string]*wsintapi.Stack_Examples),
	}
	for res := range stack.Resources {
		rstack.Resources[res] = nil
	}
	for i := range stack.Commands {
		rstack.Commands[stack.Commands[i].Name] = &wsintapi.Stack_Command{Cmdline: stack.Commands[i].CommandLine}
	}
	for i := range stack.ExampleCommands {
		rstack.Commands[stack.ExampleCommands[i].Name] = &wsintapi.Stack_Command{Cmdline: stack.ExampleCommands[i].CommandLine}
	}
	for i := range stack.Examples {
		rstack.Examples[stack.Examples[i].Name] = &wsintapi.Stack_Examples{Type: stack.Examples[i].Type, Root: stack.Examples[i].Root, Path: stack.ExpandEnv(stack.Examples[i].Path)}
	}
	return &rstack, nil
}

func (s *grpcserver) ListenBoard(breq *wsintapi.BoardRequest, stream wsintapi.WorkspaceInternalApi_ListenBoardServer) error {
	s.elog.Println("start listen")
	var username string
	var wsname string
	err := authorize(stream.Context(), &username, &wsname)
	if err != nil {
		return err
	}
	wsc, err := s.bman.WorkspaceClient(stream.Context(), username+"/"+wsname)
	if err != nil {
		return err
	}
	sendCh := make(chan map[string]board.Board, 10)
	go func() {
		for ldata := range wsc.UpdateCh() {
			select {
			case sendCh <- ldata:
			default:
				// Send overflow; close down
				wsc.Close()
			}
		}
		close(sendCh)
	}()
	for ldata := range sendCh {
		boards := make(map[string]*wsintapi.BoardResponse_Board)
		for id, b := range ldata {
			rr := make(map[string]*wsintapi.BoardResponse_Board_Resource)
			for rid, r := range b.Resources {
				status := wsintapi.BoardResponse_Board_Resource_UNKNOWN
				switch r.Status {
				case gizmo.StatusNotFound:
					status = wsintapi.BoardResponse_Board_Resource_NOT_FOUND
				case gizmo.StatusDisconnected:
					status = wsintapi.BoardResponse_Board_Resource_DISCONNECTED
				case gizmo.StatusOffline:
					status = wsintapi.BoardResponse_Board_Resource_OFFLINE
				case gizmo.StatusError:
					status = wsintapi.BoardResponse_Board_Resource_ERROR
				case gizmo.StatusOnline:
					status = wsintapi.BoardResponse_Board_Resource_ONLINE
				}
				caps := map[string]*any.Any{}
				for cid, cap := range r.Capabilities {
					pb := &wsintapi.MapStringString{}
					if len(cap) > 0 {
						pb.Map = make(map[string]string)
					}
					for mid, ms := range cap {
						pb.Map[mid] = ms
					}
					b, err := proto.Marshal(pb)
					if err != nil {
						s.elog.Println("Failed to marshal capability", cid, err)
						continue
					}
					caps[cid] = &any.Any{
						TypeUrl: proto.MessageName(pb),
						Value:   b,
					}
				}
				rr[rid] = &wsintapi.BoardResponse_Board_Resource{
					Class:        r.Class,
					Name:         r.Name,
					Status:       status,
					Capabilities: caps,
				}
			}
			boards[id] = &wsintapi.BoardResponse_Board{
				Type:      b.Type,
				Resources: rr,
			}
		}
		br := wsintapi.BoardResponse{Boards: boards}
		if err := stream.Send(&br); err != nil {
			s.elog.Println("send error:", err)
		}
	}
	s.elog.Println("closing listen")
	return nil
}
