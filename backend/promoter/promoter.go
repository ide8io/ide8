package promoter

import (
	"log"
	"strings"

	"bitbucket.org/ide8io/ide8/backend/board"
	"bitbucket.org/ide8io/ide8/backend/gizmo"
)

type Promoter interface {
}

type promoter struct {
	client gizmo.PromoteClient
	bman   board.Manager
}

func New(promotestore gizmo.PromoteStore, bman board.Manager) Promoter {
	client, err := promotestore.PromoteClient()
	if err != nil {
		log.Fatalln(err)
	}
	p := &promoter{
		client: client,
		bman:   bman,
	}
	go p.handler()
	return p
}

func findOther(gizmos map[string]gizmo.Gizmo, class string, gizmo gizmo.Gizmo) (string, *gizmo.Gizmo) {
	for id, g := range gizmos {
		if g.Class == class && gizmo.VID == g.VID && gizmo.PID == g.PID && gizmo.Serial == g.Serial {
			return id, &g
		}
	}
	return "", nil
}

func (p *promoter) handler() {
	for u := range p.client.UpdateCh() {
		boards := []board.Board{}
		proms := map[string]gizmo.Promotion{}
		for id, g := range u.Gizmos {
			vid := strings.ToLower(g.VID)
			pid := strings.ToLower(g.PID)
			var info string
			if g.Serial != "" && g.Serial != "0" {
				info += "#" + g.Serial + " "
			}
			info += "Agent: " + u.AgentID
			switch g.Class {
			case "vendor":
				if vid == "1366" && pid == "1015" {
					promo := gizmo.Promotion{
						Class: "remote",
						Name:  "J-Link Debug Probe",
						Capabilities: map[string]map[string]string{
							"gdbserver": nil,
						},
						Plugin: "openocd",
					}
					proms[id] = promo
					rr := map[string]gizmo.Resource{}
					rr[u.AgentID+"/"+id] = gizmo.Resource{
						Class:        promo.Class,
						Name:         promo.Name,
						Status:       gizmo.StatusUnknown,
						Capabilities: promo.Capabilities,
					}
					oid, og := findOther(u.Gizmos, "serial", g)
					if og != nil {
						promo := gizmo.Promotion{
							Name: "J-Link VCOM",
							Capabilities: map[string]map[string]string{
								"baudrate": map[string]string{
									"default": "115200",
								},
							},
						}
						proms[oid] = promo
						rr[u.AgentID+"/"+oid] = gizmo.Resource{
							Class:  og.Class,
							Name:   promo.Class,
							Status: gizmo.StatusUnknown,
						}
					}
					boards = append(boards, board.Board{
						Type:      "nRF52 DK",
						Info:      "nRF52 Development Kit (" + info + ")",
						Resources: rr,
					})
				}
			case "serial":
				if (vid == "2341" && (pid == "0043" || pid == "0001" || pid == "0243")) ||
					(vid == "2a03" && pid == "0043") || (vid == "1a86" && pid == "7523") {
					promo := gizmo.Promotion{
						Class: g.Class,
						Name:  g.Class,
						Capabilities: map[string]map[string]string{
							"baudrate": map[string]string{
								"default": "9600",
							},
							"reset": map[string]string{
								"type": "dtr",
							},
						},
					}
					proms[id] = promo
					rr := map[string]gizmo.Resource{}
					rr[u.AgentID+"/"+id] = gizmo.Resource{
						Class:        promo.Class,
						Name:         promo.Name,
						Capabilities: promo.Capabilities,
						Status:       gizmo.StatusUnknown,
					}
					boards = append(boards, board.Board{
						Type:      "Arduino UNO",
						Info:      "Arduino UNO (" + info + ")",
						Resources: rr,
					})
				}
			}
		}
		if len(proms) > 0 {
			log.Println("promoter: promoting", u, proms)
			err := p.client.Promote(u.AgentID, proms)
			if err != nil {
				log.Println("promoter: promote:", err)
			}
		}
		for _, b := range boards {
			owner := u.Owner
			if owner == "" {
				owner = "public"
			}
			name, brd, err := p.bman.FindByResources(b.Resources)
			if err != nil {
				log.Println("promoter: autoboard:", err)
				continue
			}
			if name != "" {
				if brd.Type != b.Type {
					continue
				}
				b.Info = brd.Info
				_, err = p.bman.Update(owner, b)
				if err != nil {
					log.Println("promoter: autoboard:", err)
				}
				continue
			}
			_, err = p.bman.Create(owner, b)
			if err != nil {
				log.Println("promoter: autoboard:", err)
				continue
			}
		}
	}
}
