package promoter_test

import (
	"testing"
	"time"

	"bitbucket.org/ide8io/ide8/backend/board"
	"bitbucket.org/ide8io/ide8/backend/gizmo"
	"bitbucket.org/ide8io/ide8/backend/promoter"
	"github.com/stretchr/testify/assert"
)

type gizmoStoreMock struct {
	gizmo.PromoteStore
	updateCh chan gizmo.PromoteUpdate
}

func (s *gizmoStoreMock) PromoteClient() (gizmo.PromoteClient, error) {
	return &promoteClientMock{
		updateCh: s.updateCh,
	}, nil
}

type promoteClientMock struct {
	gizmo.PromoteClient
	updateCh <-chan gizmo.PromoteUpdate
}

func (c *promoteClientMock) UpdateCh() <-chan gizmo.PromoteUpdate {
	return c.updateCh
}

func (c *promoteClientMock) Promote(agentid string, proms map[string]gizmo.Promotion) error {
	return nil
}

type bmanMock struct {
	board.Manager
}

func (m *bmanMock) Create(owner string, board board.Board) (string, error) {
	return owner + "/" + board.Type, nil
}

func (m *bmanMock) FindByResources(resources map[string]gizmo.Resource) (string, board.Board, error) {
	return "user/type", board.Board{}, nil
}

func (m *bmanMock) Update(owner string, board board.Board) (string, error) {
	return owner + "/" + board.Type, nil
}

func TestNewPromoter(t *testing.T) {
	updateCh := make(chan gizmo.PromoteUpdate)
	p := promoter.New(&gizmoStoreMock{updateCh: updateCh}, &bmanMock{})
	assert.NotNil(t, p)
}

func TestUpdate(t *testing.T) {
	updateCh := make(chan gizmo.PromoteUpdate)
	p := promoter.New(&gizmoStoreMock{updateCh: updateCh}, &bmanMock{})
	assert.NotNil(t, p)
	select {
	case updateCh <- gizmo.PromoteUpdate{
		AgentID: "agent",
		Gizmos:  map[string]gizmo.Gizmo{"gizmo": {Class: "serial", VID: "2341", PID: "0001"}},
	}:
	case <-time.After(time.Second):
		assert.Fail(t, "promoter update listen not consumed")
	}
}

func TestUpdateJLink(t *testing.T) {
	updateCh := make(chan gizmo.PromoteUpdate)
	p := promoter.New(&gizmoStoreMock{updateCh: updateCh}, &bmanMock{})
	assert.NotNil(t, p)
	select {
	case updateCh <- gizmo.PromoteUpdate{
		AgentID: "agent",
		Gizmos: map[string]gizmo.Gizmo{
			"gizmo1": {Class: "vendor", VID: "1366", PID: "1015"},
			"gizmo2": {Class: "serial", VID: "1366", PID: "1015"},
		},
	}:
	case <-time.After(time.Second):
		assert.Fail(t, "promoter update listen not consumed")
	}
}
