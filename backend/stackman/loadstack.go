package stackman

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
)

const (
	stackdeffilename = "stack.json"
)

func loadStackdefs(dir string) ([]Stack, error) {
	stacks := []Stack{}
	groupentries, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}
	for _, groupentry := range groupentries {
		if !groupentry.IsDir() {
			continue
		}
		gd := filepath.Join(dir, groupentry.Name())
		entries, err := ioutil.ReadDir(gd)
		if err != nil {
			return nil, err
		}
		for _, entry := range entries {
			if !entry.IsDir() {
				continue
			}
			sd := filepath.Join(gd, entry.Name())
			verentries, err := ioutil.ReadDir(sd)
			if err != nil {
				return nil, err
			}
			for _, verentry := range verentries {
				if !verentry.IsDir() {
					continue
				}
				stack := Stack{Owner: groupentry.Name(), Name: entry.Name(), Version: verentry.Name()}
				err := loadStackdef(sd, &stack)
				if err != nil {
					return nil, err
				}
				vsd := filepath.Join(sd, verentry.Name())
				err = loadStackdef(vsd, &stack)
				if err != nil {
					return nil, err
				}
				stacks = append(stacks, stack)
			}
		}
	}
	return stacks, nil
}

func loadStackdef(stackdir string, stack *Stack) error {
	sdf := filepath.Join(stackdir, stackdeffilename)
	if _, err := os.Stat(sdf); !os.IsNotExist(err) {
		b, err := ioutil.ReadFile(sdf)
		if err != nil {
			return err
		}
		err = json.Unmarshal(b, stack)
		if err != nil {
			return err
		}
	}
	return nil
}
