package stackman

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

const (
	stackdefsdir = "stackdefs"
)

type Stack struct {
	Owner           string                 `json:"owner"`
	Name            string                 `json:"name"`
	Version         string                 `json:"version"`
	Description     string                 `json:"description"`
	Image           string                 `json:"image"`
	Memory          int                    `json:"memory"`
	RootPath        string                 `json:"rootpath"`
	Env             map[string]string      `json:"env"`
	Resources       map[string]interface{} `json:"resources"`
	Examples        []ExamplesNode         `json:"examples"`
	Commands        []Command              `json:"commands"`
	ExampleCommands []Command              `json:"examplecommands"`
	Public          bool                   `json:"public"`
	Beta            bool                   `json:"beta"`
	Deprecated      bool                   `json:"deprecated"`
}

type Stacks []Stack

type ExamplesNode struct {
	Name string `json:"name"`
	Type string `json:"type"`
	Root string `json:"root"`
	Path string `json:"path"`
}

type Command struct {
	Name          string `json:"name"`
	FileExtension string `json:"fileext"`
	CommandLine   string `json:"cmdline"`
}

var stacks Stacks

type stackMan struct {
	stackdir string
}

var stackman *stackMan

func Init(datadir string) {
	stackman = &stackMan{filepath.Join(datadir, "stacks")}
	if _, err := os.Stat(stackman.stackdir); os.IsNotExist(err) {
		os.Mkdir(stackman.stackdir, 0755)
	}
	initStats(datadir)

	if _, err := os.Stat(stackdefsdir); os.IsNotExist(err) {
		log.Println("No stack definition dir in", stackdefsdir)
	} else {
		stacks, err = loadStackdefs(stackdefsdir)
		if err != nil {
			log.Fatalf("Failed to load stack: %s", err)
		}
	}
}

func (s *Stack) UpdateUsage(username string) error {
	incrementStackCount(s.Owner, s.Name, s.Version, username)
	return nil
}

func (s *Stack) GetCommandDir() string {
	return filepath.Join(s.GetStackDefDir(), "commands")
}

func (s *Stack) GetStackDefDir() string {
	return filepath.Join(stackdefsdir, s.Owner, s.Name, s.Version)
}

func (s *Stack) GetStackDir() string {
	return filepath.Join(stackman.stackdir, s.Owner, s.Name, s.Version)
}

func (s *Stack) ExpandEnv(input string) string {
	for k, v := range s.Env {
		input = strings.Replace(input, "${"+k+"}", v, -1)
	}
	return input
}

func (s *Stack) GetExamplePath(name string) (string, string, error) {
	for i := range s.Examples {
		if s.Examples[i].Name == name {
			root := ""
			if s.Examples[i].Root == "stackdir" {
				root = s.GetStackDir()
			} else if s.Examples[i].Root == "stackdefdir" {
				root = s.GetStackDefDir()
			} else {
				return "", "", fmt.Errorf("unknown example root type: %s", s.Examples[i].Root)
			}
			return filepath.Join(root, s.ExpandEnv(s.Examples[i].Path)), s.Examples[i].Type, nil
		}
	}
	return "", "", fmt.Errorf("no stack examples for %s found", name)
}

func GetStack(stackname string) (*Stack, error) {
	split := strings.SplitN(stackname, "/", 3)
	owner := ""
	name := ""
	ver := ""
	if len(split) == 1 {
		// Handle old use with only stack name
		if split[0] == "nRF52" {
			name = split[0]
			owner = "NordicSemi"
		} else if split[0] == "Telenor" {
			name = "ee-0x"
			owner = split[0]
		} else {
			owner = split[0]
			name = split[0]
		}
		ver = "0.1"
	} else if len(split) == 3 {
		owner = split[0]
		name = split[1]
		ver = split[2]
	} else {
		return nil, fmt.Errorf("invalid stack name: %s", stackname)
	}
	for i := range stacks {
		if stacks[i].Owner == owner && stacks[i].Name == name && stacks[i].Version == ver {
			return &stacks[i], nil
		}
	}
	return nil, fmt.Errorf("No stack named %s", stackname)
}
