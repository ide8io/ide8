package stackman

import (
	"encoding/json"
	"fmt"
	"github.com/boltdb/bolt"
	"log"
	"path/filepath"
	"strconv"
	"time"
)

var db *bolt.DB

func initStats(datadir string) {
	var err error
	db, err = bolt.Open(filepath.Join(datadir, "ide8_stats_stack.db"), 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Fatal("stackman: initStats:", err)
	}
}

type countLogItem struct {
	Version  string `json:"version"`
	Count    int    `json:"count"`
	Username string `json:"username"`
}

func incVal(b *bolt.Bucket, name string) (int, error) {
	count := b.Get([]byte(name))
	val := 0
	if count != nil {
		var err error
		val, err = strconv.Atoi(string(count))
		if err != nil {
			return 0, err
		}
	}
	val++
	valb := []byte(strconv.Itoa(val))
	err := b.Put([]byte(name), valb)
	return val, err
}

func moveBucket(from *bolt.Bucket, dest *bolt.Bucket) error {
	err := from.ForEach(func(k, v []byte) error {
		if v == nil {
			// Nested bucket
			newchild, err := dest.CreateBucket(k)
			if err != nil {
				return err
			}
			return moveBucket(from.Bucket(k), newchild)
		} else {
			// Regular value
			return dest.Put(k, v)
		}
	})
	if err != nil {
		return err
	}
	return nil
}

func incrementStackCount(owner string, name string, version string, username string) {
	err := db.Update(func(tx *bolt.Tx) error {
		// Convert old name-only buckets
		// TODO: Remove when converted
		oldstack := tx.Bucket([]byte(name))
		if oldstack != nil {
			newbucket, err := tx.CreateBucket([]byte(owner + "/" + name))
			if err != nil {
				return err
			}
			err = moveBucket(oldstack, newbucket)
			if err != nil {
				return err
			}
			err = tx.DeleteBucket([]byte(name))
			if err != nil {
				return err
			}
		}
		stack, err := tx.CreateBucketIfNotExists([]byte(owner + "/" + name))
		if err != nil {
			return err
		}

		// Increment total stack count
		count, err := incVal(stack, "count")
		if err != nil {
			return err
		}

		// Increment stack version count
		verstack, err := stack.CreateBucketIfNotExists([]byte("vercount"))
		if err != nil {
			return err
		}
		_, err = incVal(verstack, version)
		if err != nil {
			return err
		}

		// Append to log
		slog, err := stack.CreateBucketIfNotExists([]byte("log"))
		if err != nil {
			return err
		}
		now := time.Now().UTC().Format(time.RFC3339Nano)
		b, err := json.Marshal(countLogItem{version, count, username})
		if err != nil {
			return err
		}
		err = slog.Put([]byte(now), b)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		log.Println("stackman: incrementStackCount:", err)
	}
}

type LogItem struct {
	Time     string `json:"time"`
	Version  string `json:"version"`
	Count    int    `json:"count"`
	Username string `json:"username"`
}

func GetStatsLog(name string) ([]LogItem, error) {
	ss := []LogItem{}
	err := db.View(func(tx *bolt.Tx) error {
		stack := tx.Bucket([]byte(name))
		if stack == nil {
			return fmt.Errorf("no stats for stack %s", name)
		}
		slog := stack.Bucket([]byte("log"))
		if slog == nil {
			return fmt.Errorf("no stats log for stack %s", name)
		}
		slog.ForEach(func(k, v []byte) error {
			data := countLogItem{}
			err := json.Unmarshal(v, &data)
			if err != nil {
				return err
			}
			ss = append(ss, LogItem{string(k), data.Version, data.Count, data.Username})
			return nil
		})
		return nil
	})
	return ss, err
}

type Stats struct {
	Name     string     `json:"name"`
	Count    string     `json:"count"`
	Versions []VerStats `json:"versions"`
}

type VerStats struct {
	Version string `json:"version"`
	Count   string `json:"count"`
}

func GetStats() []Stats {
	s := []Stats{}
	err := db.View(func(tx *bolt.Tx) error {
		tx.ForEach(func(name []byte, b *bolt.Bucket) error {
			val := b.Get([]byte("count"))
			stats := Stats{Name: string(name), Count: string(val)}
			vers := b.Bucket([]byte("vercount"))
			if vers != nil {
				err := vers.ForEach(func(k []byte, v []byte) error {
					stats.Versions = append(stats.Versions, VerStats{Version: string(k), Count: string(v)})
					return nil
				})
				if err != nil {
					return err
				}
			}
			s = append(s, stats)
			return nil
		})
		return nil
	})
	if err != nil {
		log.Println("stackman: GetStats:", err)
	}
	return s
}
