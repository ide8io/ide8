package stackman

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

type Collection struct {
	Type string `json:"type"`
	Name string `json:"name"`
}

type Example struct {
	Type        string   `json:"type"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	MainFile    string   `json:"mainfile"`
	OtherFiles  []string `json:"otherfiles"`
}

type Examples []Example

type ExampleLookup struct {
	fullpath     string
	examples     []ExamplesNode
	exType       string
	IsCollection bool
	IsExample    bool
}

func scanArduinoIde(path string) ([]interface{}, error) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}
	listing := []interface{}{}
	for _, f := range files {
		if f.IsDir() {
			_, err := os.Stat(filepath.Join(path, f.Name(), f.Name()+".ino"))
			if os.IsNotExist(err) {
				listing = append(listing, Collection{"collection", f.Name()})
			} else {
				var desc string
				txtfile := f.Name() + ".txt"
				descb, err := ioutil.ReadFile(filepath.Join(path, f.Name(), txtfile))
				if err != nil {
					desc = err.Error()
				} else {
					desc = string(descb)
				}
				mainfile := f.Name() + ".ino"
				efiles, err := ioutil.ReadDir(filepath.Join(path, f.Name()))
				otherfiles := []string{}
				for _, ef := range efiles {
					if ef.Name() != mainfile && ef.Name() != txtfile {
						otherfiles = append(otherfiles, ef.Name())
					}
				}
				listing = append(listing, Example{Type: "example", Name: f.Name(), Description: desc, MainFile: mainfile, OtherFiles: otherfiles})
			}
		}
	}
	return listing, err
}

func LookupExample(stackname string, path []string) (*ExampleLookup, error) {
	stack, err := GetStack(stackname)
	if err != nil {
		return nil, err
	}

	if len(path) == 0 || path[0] == "" {
		return &ExampleLookup{stack.GetStackDir(), stack.Examples, "", true, false}, nil
	}

	expath, extype, err := stack.GetExamplePath(path[0])
	if err != nil {
		return nil, err
	}

	abspath := append([]string{expath}, path[1:]...)

	fullpath := filepath.Join(abspath...)

	isdir := false
	isex := false
	if extype == "" {
		if path[len(path)-1] == "" {
			isdir = true
		} else {
			isex = true
		}
	} else {
		fi, err := os.Stat(fullpath)
		if err != nil {
			return nil, err
		}
		if fi.Mode().IsDir() {
			if path[len(path)-1] == "" {
				isdir = true
			} else {
				isex = true
			}
		}
	}

	return &ExampleLookup{fullpath, []ExamplesNode{}, extype, isdir, isex}, nil
}

func (el *ExampleLookup) ListCollection() ([]interface{}, error) {
	if len(el.examples) > 0 {
		examples := []interface{}{}
		for i := range el.examples {
			examples = append(examples, Collection{Type: "collection", Name: el.examples[i].Name})
		}
		return examples, nil
	}

	if !el.IsCollection {
		return nil, fmt.Errorf("Example path '%s' is not a collection!", el.fullpath)
	}

	if el.exType == "arduino" {
		exs, err := scanArduinoIde(el.fullpath)
		if err != nil {
			return nil, err
		}
		return exs, nil
	}

	listing := []interface{}{}
	exdeffilename := filepath.Join(el.fullpath, "examples.json")
	if _, err := os.Stat(exdeffilename); os.IsNotExist(err) {
		files, err := ioutil.ReadDir(el.fullpath)
		if err != nil {
			return nil, err
		}
		for _, f := range files {
			if f.IsDir() {
				listing = append(listing, Collection{"collection", f.Name()})
			} else {
				listing = append(listing, Collection{"example", f.Name()})
			}
		}
	} else {
		data, err := ioutil.ReadFile(exdeffilename)
		if err != nil {
			return nil, err
		}
		var exs Examples
		err = json.Unmarshal(data, &exs)
		if err != nil {
			return nil, err
		}
		for i := range exs {
			exs[i].Type = "example"
			listing = append(listing, exs[i])
		}
	}
	return listing, nil
}

func (el *ExampleLookup) ReadFile() ([]byte, error) {
	if el.exType == "" {
		exbase := filepath.Dir(filepath.Dir(el.fullpath))
		exname := filepath.Base(filepath.Dir(el.fullpath))
		exdeffilename := filepath.Join(exbase, "examples.json")
		if _, err := os.Stat(exdeffilename); !os.IsNotExist(err) {
			data, err := ioutil.ReadFile(exdeffilename)
			if err != nil {
				return nil, err
			}
			var exs Examples
			err = json.Unmarshal(data, &exs)
			if err != nil {
				return nil, err
			}
			for i := range exs {
				if exs[i].Name == exname {
					return ioutil.ReadFile(filepath.Join(exbase, exs[i].MainFile))
				}
			}
		}
	}
	return ioutil.ReadFile(el.fullpath)
}
