package stackdef

import (
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/mattn/go-zglob"
	"gopkg.in/yaml.v2"
)

type StackDef struct {
	Init struct {
		Bare string
	}
	Envs    map[string]string
	Targets []struct {
		Name string
		Envs map[string]string
	}
	Projects []Project
	Commands []Command
	Examples struct {
		Groups   []ExampleGroup
		Commands []Command
	}
	Shares []string
}

type Command struct {
	Name      string
	FileMatch string
	CmdLine   string
	Resource  string
}

type ExampleGroup struct {
	Name     string
	Type     string
	Path     string
	Groups   []ExampleGroup
	Examples []Example
}

type Example struct {
	Name        string
	Path        string
	Description string
	Files       map[string]string
	Commands    []Command
}

type Project struct {
	FileMatch string
	FileMap   map[string]string
	Commands  []Command
}

const (
	NODE_TYPE_COLLECTION = "collection"
	NODE_TYPE_PROJECT    = "project"
	NODE_TYPE_EXAMPLE    = "example"
	NODE_TYPE_DIR        = "dir"
	NODE_TYPE_FILE       = "file"
)

type Node interface {
}

type baseNode struct {
	Name string `json:"name"`
	Dir  bool   `json:"dir"`
	Type string `json:"type"`
}

type projectNode struct {
	baseNode
	Commands []nodeCommand     `json:"commands"`
	Files    map[string]string `json:"files"`
}

type nodeCommand struct {
	Name     string `json:"name"`
	Resource string `json:"resource,omitempty"`
	Params   string `json:"params,omitempty"`
}

func Parse(r io.Reader) (*StackDef, error) {
	var buf bytes.Buffer
	_, err := io.Copy(&buf, r)
	if err != nil {
		return nil, err
	}
	var def StackDef
	err = yaml.Unmarshal(buf.Bytes(), &def)
	if err != nil {
		return nil, err
	}

	// Expand any env vars used in shares
	for i := range def.Shares {
		def.Shares[i] = def.expandEnvs(def.Shares[i])
	}

	// Build examples tree and expand any env vars
	for i := range def.Examples.Groups {
		def.Examples.Groups[i].Path = def.expandEnvs(def.Examples.Groups[i].Path)
		if def.Examples.Groups[i].Type == "nrf5sdkscan" {
			def.Examples.Groups[i].Groups, def.Examples.Groups[i].Examples, err = def.nrf5sdkscan(def.Examples.Groups[i].Path)
		} else if def.Examples.Groups[i].Type == "arduinoscan" {
			def.Examples.Groups[i].Groups, def.Examples.Groups[i].Examples, err = def.arduinoscan(def.Examples.Groups[i].Path)
		} else if def.Examples.Groups[i].Type == "dirs" {
			def.Examples.Groups[i].Groups, def.Examples.Groups[i].Examples, err = def.dirscan(def.Examples.Groups[i].Path)
		} else {
			log.Println("Unknown scan type:", def.Examples.Groups[i].Type)
		}
	}

	return &def, nil
}

func expandEnv(input string, env string, value string) string {
	return strings.Replace(input, "${"+env+"}", value, -1)
}

func expandEnvs(input string, envs map[string]string) string {
	for k, v := range envs {
		input = expandEnv(input, k, v)
	}
	return input
}

func (sd *StackDef) expandEnvs(input string) string {
	return expandEnvs(input, sd.Envs)
}

func (sd *StackDef) arduinoscan(path string) (groups []ExampleGroup, examples []Example, err error) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return
	}
	for _, f := range files {
		if f.IsDir() {
			_, staterr := os.Stat(filepath.Join(path, f.Name(), f.Name()+".ino"))
			if os.IsNotExist(staterr) {
				eg := ExampleGroup{Name: f.Name(), Path: f.Name()}
				eg.Groups, eg.Examples, err = sd.arduinoscan(filepath.Join(path, f.Name()))
				if err != nil {
					return
				}
				groups = append(groups, eg)
			} else {
				var desc string
				txtfile := f.Name() + ".txt"
				descb, readerr := ioutil.ReadFile(filepath.Join(path, f.Name(), txtfile))
				if readerr != nil {
					desc = readerr.Error()
				} else {
					desc = string(descb)
				}
				examples = append(examples, Example{Name: f.Name(), Path: f.Name(), Description: desc})
			}
		}
	}
	return
}

func (sd *StackDef) nrf5sdkscan(path string) (groups []ExampleGroup, examples []Example, err error) {
	sdkboard := "pca10040"
	found, err := zglob.Glob(filepath.Join(path, "**", sdkboard))
	if err != nil {
		return
	}

	prefix := path
	if path[len(path)-1] != os.PathSeparator {
		prefix += string(os.PathSeparator)
	}
	suffix := string(os.PathSeparator) + sdkboard

	var setexample func(path []string, groups *[]ExampleGroup, examples *[]Example)
	setexample = func(path []string, groups *[]ExampleGroup, example *[]Example) {
		if len(path) == 1 {
			*example = append(*example, Example{Name: path[0], Path: path[0]})
			return
		}
		for i := range *groups {
			if path[0] == (*groups)[i].Name {
				setexample(path[1:], &(*groups)[i].Groups, &(*groups)[i].Examples)
				return
			}
		}
		eg := ExampleGroup{Name: path[0], Path: path[0]}
		setexample(path[1:], &eg.Groups, &eg.Examples)
		*groups = append(*groups, eg)
	}

	for _, f := range found {
		parr := strings.Split(strings.TrimSuffix(strings.TrimPrefix(f, prefix), suffix), string(os.PathSeparator))
		setexample(parr, &groups, &examples)
	}
	return
}

func (sd *StackDef) dirscan(path string) (groups []ExampleGroup, examples []Example, err error) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return
	}
	for _, f := range files {
		if f.IsDir() {
			examples = append(examples, Example{Name: f.Name(), Path: f.Name()})
		}
	}
	return
}

func (sd *StackDef) GetNodes(path string) ([]Node, error) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}
	nodes := []Node{}
	for _, file := range files {
		var node Node
		if file.IsDir() {
			pfiles, err := ioutil.ReadDir(filepath.Join(path, file.Name()))
			if err != nil {
				log.Println(err)
				// Don't abort listing
			} else {
			loop:
				for _, pfile := range pfiles {
					if pfile.IsDir() {
						continue
					}
					for _, projdef := range sd.Projects {
						matched, err := regexp.MatchString(projdef.FileMatch, pfile.Name())
						if err != nil {
							log.Println(err)
							continue
						}
						if !matched {
							continue
						}
						var cmds []nodeCommand
						for _, cmd := range projdef.Commands {
							params := ""
							resarr := strings.SplitN(cmd.Resource, " ", 2)
							if len(resarr) >= 2 {
								params = expandEnv(resarr[1], "project", file.Name())
							}
							cmds = append(cmds, nodeCommand{cmd.Name, resarr[0], params})
						}
						node = projectNode{baseNode: baseNode{file.Name(), true, NODE_TYPE_PROJECT}, Commands: cmds}
						break loop
					}
				}
			}
		}
		if node == nil {
			if file.IsDir() {
				node = baseNode{file.Name(), true, NODE_TYPE_DIR}
			} else {
				node = baseNode{file.Name(), false, NODE_TYPE_FILE}
			}
		}
		nodes = append(nodes, node)
	}
	return nodes, nil
}

func (sd *StackDef) LookupExample(path []string) (ExampleLookup, error) {
	fspath := []string{}
	var findEx func(groups []ExampleGroup, examples []Example, i int) (ExampleLookup, error)
	findEx = func(groups []ExampleGroup, examples []Example, i int) (ExampleLookup, error) {
		if i >= len(path) || path[i] == "" {
			var exs = examples
			for i := range exs {
				exs[i].Commands = sd.Examples.Commands
			}
			return &collectionLookup{filepath.Join(fspath...), groups, exs}, nil
		}
		for _, g := range groups {
			if path[i] == g.Name {
				fspath = append(fspath, g.Path)
				return findEx(g.Groups, g.Examples, i+1)
			}
		}
		for _, ex := range examples {
			if path[i] == ex.Name {
				fspath = append(fspath, ex.Path)
				if i+1 < len(path) {
					fspath = append(fspath, path[i+1:]...)
				}
				return &fileLookup{filepath.Join(fspath...)}, nil
			}
		}
		fspath = append(fspath, path[i])
		return &fileLookup{filepath.Join(fspath...)}, nil
	}
	return findEx(sd.Examples.Groups, []Example{}, 0)
}

type ExampleLookup interface {
	GetNodes() ([]Node, error)
	GetPath() string
}

type collectionLookup struct {
	path     string
	groups   []ExampleGroup
	examples []Example
}

func (l *collectionLookup) GetNodes() ([]Node, error) {
	nodes := []Node{}
	for _, g := range l.groups {
		nodes = append(nodes, baseNode{g.Name, true, NODE_TYPE_COLLECTION})
	}
	for _, ex := range l.examples {
		var cmds []nodeCommand
		for _, cmd := range ex.Commands {
			params := ""
			resarr := strings.SplitN(cmd.Resource, " ", 2)
			if len(resarr) >= 2 {
				params = expandEnv(resarr[1], "example", ex.Name)
			}
			cmds = append(cmds, nodeCommand{cmd.Name, resarr[0], params})
		}
		nodes = append(nodes, projectNode{
			baseNode{ex.Name, true, NODE_TYPE_EXAMPLE},
			cmds,
			ex.Files,
		})
	}
	// Add any other loose files
	if len(l.path) == 0 {
		// Skip root
		return nodes, nil
	}
	fi, err := os.Stat(l.path)
	if os.IsNotExist(err) {
		return nil, err
	}
	if !fi.IsDir() {
		return nil, nil
	}
	files, err := ioutil.ReadDir(l.path)
	if err != nil {
		return nil, err
	}
	for _, f := range files {
		if f.IsDir() {
			// Should already be handled by collections and examples
			continue
		} else {
			nodes = append(nodes, baseNode{f.Name(), false, NODE_TYPE_FILE})
		}
	}
	return nodes, nil
}

func (l *collectionLookup) GetPath() string {
	return ""
}

type fileLookup struct {
	path string
}

func (l *fileLookup) GetNodes() ([]Node, error) {
	fi, err := os.Stat(l.path)
	if os.IsNotExist(err) {
		return nil, err
	}

	if !fi.IsDir() {
		return nil, nil
	}

	files, err := ioutil.ReadDir(l.path)
	if err != nil {
		return nil, err
	}
	nodes := []Node{}
	for _, f := range files {
		if f.IsDir() {
			nodes = append(nodes, baseNode{f.Name(), true, NODE_TYPE_DIR})
		} else {
			nodes = append(nodes, baseNode{f.Name(), false, NODE_TYPE_FILE})
		}
	}
	return nodes, nil
}

func (l *fileLookup) GetPath() string {
	return l.path
}
