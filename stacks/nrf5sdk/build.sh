#!/bin/bash

gitver=$(git describe --always --tags --dirty)

set -e

nrf5sdkmajor=14
nrf5sdkminor=2.0
nrf5sdkhash=17b948a
nrf5sdkverstring=${nrf5sdkmajor}.${nrf5sdkminor}_${nrf5sdkhash}
stackname=nrf5sdk-${nrf5sdkmajor}.${nrf5sdkminor}

docker build -t stack-${stackname} --build-arg GITVER=${gitver} --build-arg NRF5SDKVERSTRING=${nrf5sdkverstring} --build-arg NRF5SDKMAJOR=${nrf5sdkmajor} .
docker tag stack-${stackname} ide8/${stackname}

echo "To publish on docker hub run: docker push ide8/${stackname}"
