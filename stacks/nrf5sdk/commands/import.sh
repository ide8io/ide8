#!/bin/bash
set -ex

projdir=${WSDIR}/$(basename ${PWD})

mkdir ${projdir}
cp *.c ${projdir}/
cp pca10040/blank/armgcc/Makefile ${projdir}/
cp pca10040/blank/armgcc/*.ld ${projdir}/
cp pca10040/blank/config/sdk_config.h ${projdir}/

sed -i 's/\(SDK_ROOT := \).*/\1${NRF5SDKROOT}/' ${projdir}/Makefile
sed -i 's/\(PROJ_DIR := \).*/\1./' ${projdir}/Makefile
sed -i '/^ \+\.\.\/config \\\r$/d' ${projdir}/Makefile
