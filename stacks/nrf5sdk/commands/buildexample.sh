#!/bin/bash
set -e

builddir=$(mktemp -d)

make OUTPUT_DIRECTORY=${builddir}

rm --preserve-root -rf ${builddir}
