#!/bin/bash
set -ex

example=${NRF5SDKROOT}/examples/peripheral/blinky
projdir=my-project

mkdir ${projdir}
cp ${example}/*.c ${projdir}/
cp ${example}/pca10040/blank/armgcc/Makefile ${projdir}/
cp ${example}/pca10040/blank/armgcc/*.ld ${projdir}/
cp ${example}/pca10040/blank/config/sdk_config.h ${projdir}/

sed -i 's/\(SDK_ROOT := \).*/\1${NRF5SDKROOT}/' ${projdir}/Makefile
sed -i 's/\(PROJ_DIR := \).*/\1./' ${projdir}/Makefile
sed -i '/^ \+\.\.\/config \\\r$/d' ${projdir}/Makefile
