#!/bin/bash

gitver=$(git describe --always --tags --dirty)

set -e

arduinoversion=arduino-1.8.5

docker build -t stack-${arduinoversion} --build-arg GITVER=${gitver} --build-arg ARDUINOVERSION=${arduinoversion} .
docker tag stack-${arduinoversion} ide8/${arduinoversion}

echo "To publish on docker hub run: docker push ide8/${arduinoversion}"

