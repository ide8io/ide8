#!/bin/bash

set -e

example=*.ino
builddir=$(mktemp -d)

hexfile=${builddir}/$(basename ${example}).hex

if [ -z "${ARDUINO_PROGRAM_PORT}" ]; then
    echo -e "\e[31mError: No Arduino programming port available. Please claim board to program.\e[0m"
    exit 1
fi

libraries=
if [ -d "${WSDIR}/libraries" ]; then
    libraries="-libraries ${WSDIR}/libraries"
fi

set -x
${ARDUINOROOT}/arduino-builder -compile -hardware ${ARDUINOROOT}/hardware -tools ${ARDUINOROOT}/tools-builder -tools ${ARDUINOROOT}/hardware/tools/avr -built-in-libraries ${ARDUINOROOT}/libraries ${libraries} -fqbn=arduino:avr:uno -build-path ${builddir} -verbose ${example}

timeout 60 ${ARDUINOROOT}/hardware/tools/avr/bin/avrdude -C ${ARDUINOROOT}/hardware/tools/avr/etc/avrdude.conf -p atmega328p -c arduino -P net:localhost:${ARDUINO_PROGRAM_PORT} -U flash:w:${hexfile}:i

rm --preserve-root -rf ${builddir}
