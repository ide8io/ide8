#!/bin/bash
set -e
echo args $@

sketch=$1
builddir=$(dirname ${sketch})/_build

if [ ! -d "${builddir}" ]; then
    mkdir ${builddir}
fi

libraries=
if [ -d "${WSDIR}/libraries" ]; then
    libraries="-libraries ${WSDIR}/libraries"
fi

set -x
${ARDUINOROOT}/arduino-builder -compile -hardware ${ARDUINOROOT}/hardware -tools ${ARDUINOROOT}/tools-builder -tools ${ARDUINOROOT}/hardware/tools/avr -built-in-libraries ${ARDUINOROOT}/libraries ${libraries} -fqbn=arduino:avr:uno -build-path ${PWD}/${builddir} -verbose ${sketch}

