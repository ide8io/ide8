#!/bin/bash
set -ex

project="my-project"

mkdir ${project}
cp ${ARDUINOROOT}/examples/01.Basics/BareMinimum/BareMinimum.ino ${project}/my-sketch.ino
mkdir libraries
#mkdir www
