#!/bin/bash
set -e

if [ -z "${ARDUINO_PROGRAM_PORT}" ]; then
    echo -e "\e[31mError: No Arduino programming port available. Please claim board to program.\e[0m"
    exit 1
fi

sketch=$1
hexfile=$(dirname ${sketch})/_build/$(basename ${sketch}).hex

if [ "${sketch}" -nt "${hexfile}" ]; then
    $(dirname $0)/build.sh ${sketch}
fi

set -x
timeout 60 ${ARDUINOROOT}/hardware/tools/avr/bin/avrdude -C ${ARDUINOROOT}/hardware/tools/avr/etc/avrdude.conf -p atmega328p -c arduino -P net:localhost:${ARDUINO_PROGRAM_PORT} -U flash:w:${hexfile}:i
