#!/bin/bash

set -e

echo "Copying example into project:" ${PWD}
cp -r ${PWD} ${WSDIR}/
