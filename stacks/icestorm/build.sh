#!/bin/bash

gitver=$(git describe --always --tags --dirty)

set -e

docker build -t stack-icestorm --build-arg GITVER=${gitver} .
docker tag stack-icestorm ide8/icestorm

echo "To publish on docker hub run: docker push ide8/icestorm"
