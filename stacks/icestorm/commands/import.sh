#!/bin/bash
set -ex

projdir=${WSDIR}/$(basename ${PWD})

mkdir ${projdir}
cp * ${projdir}/

# Fix bad aracnhe-pnr path in rot example
sed -i 's/\.\.\/\.\.\/bin\/\(arachne-pnr\)/\1/' ${projdir}/Makefile
