#!/bin/bash

dest=$1

if [ ! -d /usr/local/gcc-arm-none-eabi-4_9-2015q3 ]; then
    wget -qO- "https://launchpad.net/gcc-arm-embedded/4.9/4.9-2015-q3-update/+download/gcc-arm-none-eabi-4_9-2015q3-20150921-linux.tar.bz2" | tar -jx -C /usr/local
fi

if [ ! -f /usr/local/bin/gdb_prog.sh ]; then
    cp utils/gdb_prog.sh /usr/local/bin/
fi

nrfsdkmajor="11"
if [ $nrfsdkmajor -eq "11" ]; then
    nrfsdkzip="nRF5_SDK_11.0.0_89a8197.zip"
elif [ $nrfsdkmajor -eq "13" ]; then
    nrfsdkzip="nRF5_SDK_13.0.0_04a0bfd.zip"
fi

if [ ! -f $nrfsdkzip ]; then
    wget -qO- "https://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v${nrfsdkmajor}.x.x/${nrfsdkzip}" -O $nrfsdkzip
fi

unzip -d ${dest}/nrf_sdk/ $nrfsdkzip

(cd ${dest}; git clone https://github.com/telenordigital/ee0x-firmware.git)

sed -i 's/\(GNU_INSTALL_ROOT := \).*/\1\/usr\/local\/gcc-arm-none-eabi-4_9-2015q3/' ${dest}/ee0x-firmware/Makefile
sed -i 's/\(NRF52_SDK_ROOT := \).*/\1..\/nrf_sdk/' ${dest}/ee0x-firmware/Makefile
cat >${dest}/Makefile <<EOL
GNU_INSTALL_ROOT := /usr/local/gcc-arm-none-eabi-4_9-2015q3
GNU_PREFIX       := arm-none-eabi

all:
    make -C ee0x-firmware

clean:
    make -C ee0x-firmware clean

prog:
    gdb_prog.sh \$(GNU_INSTALL_ROOT)/bin/\$(GNU_PREFIX)-gdb ee0x-firmware/nrf52lora.out
EOL

# Fix indentation to be tabs (Required by make)
sed -i 's/^    /\t/' ${dest}/Makefile
