#!/bin/bash

set -e

site=$1

version=$(git describe --always --tags --dirty)

builddir="ide8io-${version}"
if [ -z "${site}" ]; then
    site="staging.ide8.io"
    builddir="ide8io-staging-${version}"
elif [ "${site}" != "ide8.io" ]; then
    builddir="ide8io-${site%%.*}-${version}"
fi

archive=${builddir}.tar.bz2

scp ${archive} root@${site}:
ssh root@${site} << EOF
docker stop ${site}
docker logs ${site} > ${site}-\$(date +"%Y%m%d-%H%M%S").log 2>&1
docker rm ${site}
tar xjf ${archive}
cd ${builddir}
./start_docker_backend.sh ${site}
docker rmi \$(docker images -f dangling=true -q) || true
EOF
