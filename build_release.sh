#!/bin/bash

set -e

site=$1

version=$(git describe --always --tags --dirty)

builddir="ide8io-${version}"
if [ -z "${site}" ]; then
    site="staging.ide8.io"
    builddir="ide8io-staging-${version}"
elif [ "${site}" != "ide8.io" ]; then
    builddir="ide8io-${site%%.*}-${version}"
fi

mkdir ${builddir}

(cd backend; vgo build -ldflags "-X main.domain=${site} -X main.cookiename=${site} -X main.gitver=${version}")
mkdir ${builddir}/backend
mv backend/backend ${builddir}/backend

(cd workspace; vgo build -ldflags "-X main.gitver=${version}")
mkdir ${builddir}/workspace
cp workspace/workspace ${builddir}/workspace/

npm run build-prod
cp -a dist ${builddir}/

cp Dockerfile ${builddir}/

cp -a stackdefs ${builddir}/

cp -a stacks ${builddir}/

cp start_docker_backend.sh ${builddir}/

tar cjf ${builddir}.tar.bz2 ${builddir}

rm -rf --preserve-root ${builddir}
