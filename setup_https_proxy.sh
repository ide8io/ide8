#!/bin/bash

mkdir certs

docker pull jwilder/nginx-proxy
docker build -t nginx-proxy nginx-proxy

docker run -d -p 80:80 -p 443:443 \
    --name nginx-proxy \
    --restart unless-stopped \
    -v $PWD/certs:/etc/nginx/certs:ro \
    -v /etc/nginx/vhost.d \
    -v /usr/share/nginx/html \
    -v /var/run/docker.sock:/tmp/docker.sock:ro \
    --label com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy=true \
    nginx-proxy

docker pull jrcs/letsencrypt-nginx-proxy-companion
docker run -d \
    --name nginx-letsencrypt \
    --restart unless-stopped \
    -v $PWD/certs:/etc/nginx/certs:rw \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    --volumes-from nginx-proxy \
    jrcs/letsencrypt-nginx-proxy-companion

docker pull lukehowelldev/redirect:non-www
docker run -d \
    --name www-redirect \
    --restart unless-stopped \
    -e VIRTUAL_HOST=www.ide8.io \
    -e LETSENCRYPT_HOST=www.ide8.io \
    -e LETSENCRYPT_EMAIL=drift@ide8.io \
    lukehowelldev/redirect:non-www
