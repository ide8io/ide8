var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var CleanObsoleteChunks = require('webpack-clean-obsolete-chunks');
var GitRevisionPlugin = require('git-revision-webpack-plugin');
var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const PRODUCTION = process.env.NODE_ENV === 'production';

module.exports = {
    //devtool: 'cheap-module-eval-source-map',
    mode: PRODUCTION ? 'production' : 'development',
    entry: {
        userpage: './frontend/userpage.js',
        admin: './frontend/admin.js',
        monitor: './frontend/monitor.js',
        januskiosk: './frontend/januskiosk.js'
    },
    output: {
        path: __dirname + '/dist',
        filename: '[name].js',
        chunkFilename: '[name].js',
        publicPath: '/',
    },
    plugins: [
/*
        new BundleAnalyzerPlugin({
            analyzerMode: 'static'
        }),
*/
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './frontend/frontpage.html',
            inject: false,
        }),
        new HtmlWebpackPlugin({
            title: 'ide8.io',
            favicon: 'frontend/favicon.ico',
            chunks: ['userpage', 'manifest', 'node-static'],
            filename: 'userpage.html',
        }),
        new HtmlWebpackPlugin({
            title: 'ide8.io admin',
            favicon: 'frontend/favicon.ico',
            chunks: ['admin', 'manifest', 'node-static'],
            filename: 'admin.html',
        }),
        new HtmlWebpackPlugin({
            title: 'ide8.io monitor',
            favicon: 'frontend/favicon.ico',
            chunks: ['monitor', 'manifest', 'node-static'],
            filename: 'monitor.html',
            template: 'frontend/monitor.html',
        }),
        new HtmlWebpackPlugin({
            title: 'ide8.io video kiosk',
            favicon: 'frontend/favicon.ico',
            chunks: ['januskiosk', 'manifest', 'node-static'],
            filename: 'januskiosk.html',
            template: 'frontend/januskiosk.html',
        }),
        new CopyWebpackPlugin([
            {from: 'frontend/frontpage.css'},
            {from: 'frontend/login.html'},
            {from: 'frontend/requestinvite.html'},
            {from: 'frontend/register.html'},
            {from: 'frontend/regform.html'},
            {from: 'frontend/assign.html'},
            {from: 'frontend/agent.html'},
            {from: 'frontend/openrepo.html'},
            {from: 'frontend/privacy.html'},
            {from: 'frontend/privacy.css'},
            {from: 'frontend/terms.html'},
            {from: 'frontend/terms.css'},
            {from: 'frontend/anim.gif'},
        ]),
        /*
        new webpack.optimize.CommonsChunkPlugin({
            name: 'node-static',
            minChunks(module, count) {
                var context = module.context;
                return context && context.indexOf('node_modules') >= 0;
            },
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'manifest'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'userpage',
            async: 'xterm',
            minChunks(module, count) {
                var context = module.context;
                return context && context.indexOf('node_modules/xterm') >= 0;
            },
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'userpage',
            async: 'brace/worker',
            minChunks(module, count) {
                var context = module.context;
                return context && context.indexOf('node_modules/brace/worker') >= 0;
            },
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'userpage',
            async: 'userpage_common',
            children: true,
            minChunks(module, count) {
                return count >= 2;
            },
        }),*/
        new CleanObsoleteChunks(),
        new webpack.DefinePlugin({
            'VERSION': JSON.stringify(new GitRevisionPlugin({
                versionCommand: 'describe --always --tags --dirty',
            }).version()),
        }),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            },
            {
                test: /\.(jpg|png)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                },
            },
            {
                test: /\.svg$/,
                loader: 'svg-inline-loader',
                options: {
                    name: '[name].[ext]',
                },
            },
            {
                test: /\.js$/,
                exclude: [/node_modules/],
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [['@babel/preset-env', {modules: false, targets: {chrome: 69, firefox: 62}}]],
                        plugins: ['@babel/plugin-syntax-dynamic-import'],
                    },
                },
            },
        ]
    }
};
