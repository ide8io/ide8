FROM debian
RUN apt-get update && \
    apt-get install -y git
ARG CACHEBUST=1
RUN apt-get update -y && apt-get upgrade -y && apt-get clean && apt-get -y autoremove && rm -rf /var/lib/apt/lists/*
WORKDIR /app
ADD dist/ dist/
ADD stackdefs/ stackdefs/
COPY backend/backend .
CMD ./backend
EXPOSE 8080
