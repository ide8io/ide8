package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/ide8io/ide8/backend/stackdef"
	"bitbucket.org/ide8io/ide8/backend/workspace"
	"bitbucket.org/ide8io/ide8/backend/workspace/resource"
	"bitbucket.org/ide8io/ide8/backend/workspace/serialgw"
	"bitbucket.org/ide8io/ide8/backend/workspace/wsapi"
	"bitbucket.org/ide8io/ide8/backend/wsintapigw/pb"
	"golang.org/x/net/context"
	"golang.org/x/net/webdav"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
	grpc_status "google.golang.org/grpc/status"
)

func grpcErrorString(err error) string {
	s, ok := grpc_status.FromError(err)
	if ok {
		return s.Message()
	} else {
		return err.Error()
	}
}

type wshandler struct {
	sd       *stackdef.StackDef
	ws       *workspace.Workspace
	httpsrv  *http.Server
	httpdone chan struct{}
}

func createWSHandler(wsname string, sd *stackdef.StackDef, stack *wsintapi.Stack, wsdir string, httpport string, grpcport string) (*wshandler, error) {
	wsh := &wshandler{
		sd:       sd,
		httpdone: make(chan struct{}),
	}
	var err error
	wsh.ws, err = workspace.New(wsdir, sd, stack)
	if err != nil {
		return nil, fmt.Errorf("workspace creation error: %v", err)
	}

	dav := webdav.Handler{
		Prefix:     "/dav",
		FileSystem: webdav.Dir(wsdir),
		LockSystem: webdav.NewMemLS(),
		Logger: func(r *http.Request, err error) {
			if err != nil {
				log.Printf("WEBDAV: %v, ERROR: %v", r, err)
			}
		},
	}
	validate := func(serve func(w http.ResponseWriter, r *http.Request)) func(w http.ResponseWriter, r *http.Request) {
		return func(w http.ResponseWriter, r *http.Request) {
			if wsname != r.Header.Get("workspace") {
				http.Error(w, "invalid workspace", http.StatusBadRequest)
				return
			}
			serve(w, r)
		}
	}
	http.HandleFunc("/dav/", validate(dav.ServeHTTP))
	reducePath := func(serve func(w http.ResponseWriter, r *http.Request, path []string)) func(w http.ResponseWriter, r *http.Request) {
		return func(w http.ResponseWriter, r *http.Request) {
			path := strings.Split(r.URL.Path, "/")
			if len(path) < 3 {
				http.Error(w, "Invalid path: "+r.URL.Path, http.StatusNotFound)
				return
			}
			serve(w, r, path[2:])
		}
	}
	http.HandleFunc("/files/", validate(reducePath(wsh.ws.ServeFiles)))
	http.HandleFunc("/stack/", validate(reducePath(wsh.ws.ServeStackShares)))
	http.HandleFunc("/examples/", validate(reducePath(wsh.ws.ServeExamples)))
	http.HandleFunc("/projects/", validate(reducePath(wsh.ws.ServeProjects)))
	http.HandleFunc("/usershare/", validate(reducePath(wsh.ws.ServeUserShare)))
	wsh.httpsrv = &http.Server{Addr: ":" + httpport}
	go func() {
		if err := wsh.httpsrv.ListenAndServe(); err != nil {
			log.Printf("http serve error: %v", err)
		}
		close(wsh.httpdone)
	}()

	log.Println("listening gprc on port:", grpcport)
	listen, err := net.Listen("tcp", ":"+grpcport)
	if err != nil {
		log.Fatalln("wsintapigw: failed to listen:", err)
	}
	s := grpc.NewServer()
	wsapi.RegisterWorkspaceApiServer(s, wsh)
	reflection.Register(s)
	go func() {
		if err := s.Serve(listen); err != nil {
			log.Println("wsintapigw: Failed to serve:", err)
		}
	}()

	return wsh, nil
}

func (wsh *wshandler) Comms(server wsapi.WorkspaceApi_CommsServer) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	sendCB := func(msgType string, id string, payload json.RawMessage) {
		err := server.Send(&wsapi.Message{
			Type:    msgType,
			ID:      id,
			Payload: payload,
		})
		if err != nil {
			log.Println(err)
			cancel()
		}
	}
	session := wsh.ws.NewSession(ctx, sendCB)
	for {
		msg, err := server.Recv()
		if err != nil {
			if err != context.Canceled {
				log.Println(err)
			}
			break
		}
		session.Input(msg.Type, msg.ID, msg.Payload)
	}
	return nil
}

func (wsh *wshandler) OldTunnel(server wsapi.WorkspaceApi_OldTunnelServer) error {
	md, _ := metadata.FromIncomingContext(server.Context())
	log.Println("tunnel meta:", md)
	tunnelid, ok := md["tunnelid"]
	if !ok || len(tunnelid) == 0 {
		return fmt.Errorf("no tunnel id")
	}
	ctx, cancel := context.WithCancel(context.Background())
	readchan, writechan, err := serialgw.ConnectTunnel(tunnelid[0])
	if err != nil {
		log.Println("tunnel:", err)
		return err
	}
	log.Println("tunnel: Opened", tunnelid[0])

	writechan <- serialgw.SerialGWChanData{"status", []byte("\"online\"")}

	go func() {
	loop:
		for {
			select {
			case data, ok := <-readchan:
				if !ok {
					break loop
				}
				err = server.Send(&wsapi.Message{Type: "uart:" + data.Action, Payload: data.Data})
			case <-ctx.Done():
				return
			}
		}
		cancel()
	}()

	for {
		msg, err := server.Recv()
		if err != nil {
			log.Println(err)
			break
		}
		action := strings.TrimPrefix(msg.Type, "uart:")
		writechan <- serialgw.SerialGWChanData{action, msg.Payload}
	}
	cancel()

	select {
	case writechan <- serialgw.SerialGWChanData{"status", []byte("\"offline\"")}:
	case <-time.After(time.Second):
	}

	log.Println("tunnel: Closed", tunnelid[0])
	return nil
}

type GW struct {
}

func (gw *GW) ConnectTunnel(tunnelid string) {

}

func (wsh *wshandler) Tunnel(server wsapi.WorkspaceApi_TunnelServer) error {
	md, _ := metadata.FromIncomingContext(server.Context())
	log.Println("tunnel meta:", md)
	tunnelid, ok := md["tunnelid"]
	if !ok || len(tunnelid) == 0 {
		return fmt.Errorf("no tunnel id")
	}

	ctx, cancel := context.WithCancel(server.Context())
	client, err := wsh.ws.ConnectTunnel(ctx, tunnelid[0])
	if err != nil {
		return err
	}
	log.Println("tunnel connected")
	defer log.Println("tunnel disonnected")
	go func() {
		for {
			msg, err := server.Recv()
			if err != nil {
				break
			}
			err = client.Send(msg)
			if err != nil {
				break
			}
		}
		// Make sure context is cancelled
		cancel()
	}()

	for {
		msg, err := client.Recv()
		if err == resource.TunnelClientClosedError {
			return nil
		}
		if err != nil {
			return err
		}
		err = server.Send(msg)
		if err != nil {
			return err
		}
	}
}

func (wsh *wshandler) stop() {
	log.Println("Shutting down workspace")
	wsh.ws.Stop()
	time.Sleep(time.Second) // Give it some time to close stuff
	ctx, _ := context.WithTimeout(context.Background(), time.Second*5)
	wsh.httpsrv.Shutdown(ctx)
	<-wsh.httpdone
}

type wscreds struct {
	workspace string
	token     string
}

func (c *wscreds) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{
		"workspace": c.workspace,
		"token":     c.token,
	}, nil
}

func (c *wscreds) RequireTransportSecurity() bool {
	return false
}

func connect(ctx context.Context, wsdir string, wsname string, token string, server string, httpport string, grpcport string, sd *stackdef.StackDef) <-chan struct{} {
	done := make(chan struct{})
	go func() {
		var startTime time.Time
		var conn *grpc.ClientConn
		var wsh *wshandler
	loop:
		for {
			if time.Now().Sub(startTime).Seconds() < 10 {
				select {
				case <-ctx.Done():
					break loop
				case <-time.After(time.Second * 10):
				}
			}
			startTime = time.Now()
			log.Println("Connecting to runner", server, "...")
			var err error
			creds := wscreds{wsname, token}
			conn, err = grpc.DialContext(ctx, server, grpc.WithInsecure(), grpc.WithPerRPCCredentials(&creds))
			if err != nil {
				if err == context.Canceled {
					break
				}
				log.Println("Failed to connect:", err)
				continue
			}
			log.Println("Connected ro runner")

			client := wsintapi.NewWorkspaceInternalApiClient(conn)

			if wsh == nil {
				stack, err := client.GetStack(ctx, &wsintapi.StackRequest{})
				if err != nil {
					conn.Close()
					if err == context.Canceled {
						break
					}
					log.Println("Failed to get stack:", grpcErrorString(err))
					continue
				}

				wsh, err = createWSHandler(wsname, sd, stack, wsdir, httpport, grpcport)
				if err != nil {
					conn.Close()
					log.Println(err)
					break
				}
			}

			status := &wsintapi.StatusRequest{
				Status: wsintapi.StatusRequest_ONLINE,
			}
			_, err = client.Status(ctx, status)
			if err != nil {
				conn.Close()
				if err == context.Canceled {
					break
				}
				log.Println("Failed to send status:", grpcErrorString(err))
				continue
			}

			boardReq := wsintapi.BoardRequest{}
			stream, err := client.ListenBoard(ctx, &boardReq)
			if err != nil {
				conn.Close()
				log.Println(err)
				continue
			}
			for {
				log.Println("waiting for board events")
				board, err := stream.Recv()
				if err != nil {
					log.Println(err)
					break
				}
				log.Println("got board:", board)
				wsh.ws.SetBoards(board)
			}
			conn.Close()
		}
		log.Println("Connection cancelled")
		if wsh != nil {
			wsh.stop()
		}
		done <- struct{}{}
	}()
	return done
}

var gitver string

func main() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGTERM, syscall.SIGINT)

	if gitver == "" {
		log.Println(os.Args[0], "starting")
	} else {
		log.Println(os.Args[0], "version", gitver, "starting")
	}

	init := flag.String("init", "", "initialize")
	flag.Parse()

	wsdir := os.Getenv("WSDIR")
	wsname := os.Getenv("WORKSPACE")
	token := os.Getenv("TOKEN")
	server := os.Getenv("SERVER")
	httpport := os.Getenv("HTTPPORT")
	if httpport == "" {
		httpport = "8080"
	}

	grpcport := os.Getenv("GRPCPORT")
	if grpcport == "" {
		grpcport = "7777"
	}

	err := os.Chdir(wsdir)
	if err != nil {
		log.Fatal(err)
	}

	sdfile, err := os.Open("/ide8/stack.yaml")
	if err != nil {
		log.Fatal(err)
	}
	sd, err := stackdef.Parse(sdfile)
	if err != nil {
		log.Fatal(err)
	}
	for k, v := range sd.Envs {
		err := os.Setenv(k, v)
		if err != nil {
			log.Fatal(err)
		}
	}

	if *init == "bare" {
		if sd.Init.Bare != "" {
			cmd := exec.Command(sd.Init.Bare)
			out, err := cmd.CombinedOutput()
			if err != nil {
				log.Println(err)
			}
			log.Println("bare init: ", string(out))
		}
	} else if strings.HasPrefix(*init, "git") || strings.HasPrefix(*init, "http") {
		cmd := exec.Command("git", "clone", *init)
		out, err := cmd.CombinedOutput()
		if err != nil {
			log.Println(err)
		}
		log.Println("git init: ", string(out))
	} else if *init != "" {
		log.Println("unknown init:", *init)
	}

	ctx, cancel := context.WithCancel(context.Background())
	done := connect(ctx, wsdir, wsname, token, server, httpport, grpcport, sd)
	sig := <-sigs
	log.Println("Stopping due to signal:", sig)
	cancel()
	<-done
	log.Println("Stopped. Exiting")
}
