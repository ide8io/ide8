module bitbucket.org/ide8io/ide8/workspace

require (
	bitbucket.org/ide8io/ide8/backend v0.0.0-20180726120641-8757bbba480c
	golang.org/x/net v0.0.0-20180724234803-3673e40ba225
	google.golang.org/grpc v1.13.0
)

replace bitbucket.org/ide8io/ide8/backend v0.0.0-20180726120641-8757bbba480c => ../backend
