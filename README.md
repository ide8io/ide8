# IDE8.io #

## Building ##

Prerequisites:
1. Go compiler and node.js
2. Run `go get` in the `backend` target to fetch needed libs
3. Run `npm install` to fetch dependencies
4. Run `npm run build` to generate js bundle files needed

## Running ##

Prerequisites:
1. Docker. As workspaces are run in docker containers.
2. Build workspace agent: `(cd workspace; go build)`
3. By default it requires a data dir to be present. So just run `mkdir data` the first time.

Run backend: `go run backend/main.go`

It starts a web server on port 8080

For first time admin user registration, you will find a special invite in the backend stdout.

## File structure ##

* `backend`: Backend main in go
* `frontend`: html/javascript frontend
* `dist`: Output of frontend build

## Developing ##

For continuous frontend bundle build run: `npm run build-watch`

## Production build ##

For production bundle build: `npm run build-prod`
