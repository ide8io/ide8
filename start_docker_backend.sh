#!/bin/bash
# Expects to be run inside unpacked archive
# Supply an argument to specify other domain

set -e

if [ -z "$1" ]; then
    name=ide8.io
else
    name=$1
fi

docker pull debian

(cd stacks/arduino; ./build.sh)
(cd stacks/nrf5sdk; ./build.sh)
(cd stacks/icestorm; ./build.sh)

# copy stackdefs into /app so workspace containers will find them
# Should be removed once stack images are self-contained with defs
cp -a stackdefs /app/

# Makes sure to remove destination to avoid file busy
cp --remove-destination -a workspace /app/

docker build -t $name --build-arg CACHEBUST=$(date +%s) .

docker run -d \
    --name $name \
    --hostname backend \
    --restart unless-stopped \
    -p 7070:7070 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /usr/bin/docker:/usr/bin/docker \
    -v /usr/lib/x86_64-linux-gnu/libltdl.so.7:/usr/lib/libltdl.so.7 \
    -v /app/data:/app/data \
    -v $HOME/certs:/app/certs:ro \
    -e "VIRTUAL_HOST=${name}" \
    -e "VIRTUAL_PORT=8080" \
    -e "LETSENCRYPT_HOST=${name}" \
    -e "LETSENCRYPT_EMAIL=drift@ide8.io" \
    $name
