package main

import (
	"archive/tar"
	"bytes"
	"flag"
	"log"
	"os"
	"path/filepath"
	"strings"

	"bitbucket.org/ide8io/ide8/backend/stackdef"
	"github.com/fgrehm/go-dockerpty"
	"github.com/fsouza/go-dockerclient"
)

func getImageStackDef(image string) (*stackdef.StackDef, error) {
	dclient, err := docker.NewClient("unix:///var/run/docker.sock")
	if err != nil {
		return nil, err
	}

	c, err := dclient.CreateContainer(docker.CreateContainerOptions{
		Config: &docker.Config{
			Image: image,
		},
		HostConfig: &docker.HostConfig{
			AutoRemove: true,
		},
	})
	if err != nil {
		return nil, err
	}
	defer func() {
		err := dclient.RemoveContainer(docker.RemoveContainerOptions{
			ID: c.ID,
		})
		if err != nil {
			log.Print(err)
		}
	}()

	var b bytes.Buffer
	err = dclient.DownloadFromContainer(c.ID, docker.DownloadFromContainerOptions{
		OutputStream: &b,
		Path:         "/ide8/stack.yaml",
	})
	if err != nil {
		return nil, err
	}
	r := tar.NewReader(bytes.NewReader(b.Bytes()))
	_, err = r.Next()
	if err != nil {
		return nil, err
	}
	return stackdef.Parse(r)
}

func runImage(image string, pwd string) error {
	dclient, err := docker.NewClient("unix:///var/run/docker.sock")
	if err != nil {
		return err
	}

	dirname := filepath.Base(pwd)

	c, err := dclient.CreateContainer(docker.CreateContainerOptions{
		Config: &docker.Config{
			Image:        image,
			Entrypoint:   []string{"bash", "-l"},
			User:         "user",
			Hostname:     "ide8cli",
			WorkingDir:   "/home/user/" + dirname,
			OpenStdin:    true,
			StdinOnce:    true,
			AttachStdin:  true,
			AttachStdout: true,
			AttachStderr: true,
			Tty:          true,
		},
		HostConfig: &docker.HostConfig{
			Binds: []string{
				pwd + ":/home/user/" + dirname,
			},
			AutoRemove: true,
		},
	})
	if err != nil {
		return err
	}
	if err = dockerpty.Start(dclient, c, &docker.HostConfig{}); err != nil {
		return err
	}
	return nil
}

func command(image string, pwd string, cmd string) error {
	dclient, err := docker.NewClient("unix:///var/run/docker.sock")
	if err != nil {
		return err
	}

	dirname := filepath.Base(pwd)

	c, err := dclient.CreateContainer(docker.CreateContainerOptions{
		Config: &docker.Config{
			Image:      image,
			Cmd:        []string{"/bin/bash", "-c", cmd},
			User:       "user",
			WorkingDir: "/home/user/" + dirname,
		},
		HostConfig: &docker.HostConfig{
			Binds: []string{
				pwd + ":/home/user/" + dirname,
			},
			AutoRemove: true,
		},
	})
	if err != nil {
		return err
	}

	err = dclient.StartContainer(c.ID, nil)
	if err != nil {
		return err
	}
	err = dclient.AttachToContainer(docker.AttachToContainerOptions{
		Container:    c.ID,
		OutputStream: os.Stdout,
		ErrorStream:  os.Stderr,
		Logs:         true,
		Stream:       true,
		Stdout:       true,
		Stderr:       true,
	})
	if err != nil {
		return err
	}

	return nil
}

var gitver string

func main() {
	image := flag.String("image", "", "Docker image to use")
	//dockerfile := flag.String("dockerfile", "", "Dockerfile to build image from")
	flag.Parse()

	log.Println("args:", flag.Args())

	log.Println("exec:", os.Args[0])

	if image != nil {

	} // else read from ide8.yaml ???

	if len(flag.Args()) == 0 {
		log.Fatal("No command argument given!")
	}

	arg := flag.Args()[0]
	if arg == "show" {
		sd, err := getImageStackDef(*image)
		if err != nil {
			log.Fatal(err)
		}
		log.Print(sd)
	} else if arg == "bash" {
		pwd, err := os.Getwd()
		if err != nil {
			log.Fatal(err)
		}
		err = runImage(*image, pwd)
		if err != nil {
			log.Fatal(err)
		}
	} else if arg == "cmd" {
		if len(flag.Args()) < 2 {
			log.Fatal("Please specify which command to execute")
		}
		sd, err := getImageStackDef(*image)
		if err != nil {
			log.Fatal(err)
		}
		cmd, err := sd.GetCommand(flag.Args()[1])
		if err != nil {
			log.Fatal(err)
		}
		pwd, err := os.Getwd()
		if err != nil {
			log.Fatal(err)
		}
		err = command(*image, pwd, cmd+" "+strings.Join(flag.Args()[2:], " "))
		if err != nil {
			log.Fatal(err)
		}
	} else if arg == "exec" {
		if len(flag.Args()) < 2 {
			log.Fatal("Please specify which command to execute")
		}
		pwd, err := os.Getwd()
		if err != nil {
			log.Fatal(err)
		}
		err = command(*image, pwd, strings.Join(flag.Args()[1:], " "))
		if err != nil {
			log.Fatal(err)
		}
	} else {
		log.Fatal("Unknown operation:", arg)
	}
}
