#!/bin/bash

# Create server with docker installed

useradd -ms /bin/bash user

ide8arc=$1
site=$2

./setup_https_proxy.sh

mkdir -p /app/data

tar xjf ide8arc
cd ${ide8arc%%.*}
./start_docker_backend.sh $site

