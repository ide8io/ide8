#!/bin/sh

if [ -z $CLOUD_DEBUGGER_IP ]; then
    CLOUD_DEBUGGER_IP=localhost
fi
if [ -z $CLOUD_DEBUGGER_PORT ]; then
    CLOUD_DEBUGGER_PORT=3333
fi

gdb_script=$(mktemp)
gdb_quit=$(mktemp)

cat > $gdb_script <<EOL
target remote ${CLOUD_DEBUGGER_IP}:${CLOUD_DEBUGGER_PORT}
monitor reset halt
monitor nrf52 mass_erase
load
monitor reset run
EOL

echo "quit" > $gdb_quit

gdb_exe=$1
shift
$gdb_exe -x $gdb_script -x $gdb_quit "$@"

rm $gdb_script $gdb_quit

