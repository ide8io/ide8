"use strict";

import './admin.css';
import { StartPing, addel, addtext } from './utils';
import formatDistance from 'date-fns/formatDistance';

const view = {};

view.announce = (model, intents) => {
    const form = addel(null, 'form');
    form.onsubmit = e => {
        e.preventDefault();
        intents.announce(subject.value, body.value);
    };
    addtext(form, 'Subject: ');
    const subject = addel(form, 'input', {type: 'text', autofocus: true});
    addel(form, 'br');
    const body = addel(form, 'textarea', {cols: '80', rows: '10'});
    addel(form, 'br');
    addel(form, 'input', {name: 'Submit', type: 'Submit', value: 'Send to all'});
    return {content: form};
};

view.users = (model, intents) => {
    if (model.users === undefined) {
        const text = document.createTextNode('loading...');
        return {content: text};
    }
    const frag = document.createDocumentFragment();
    for (const user of model.users) {
        console.log("user:" + JSON.stringify(user));
        const div = document.createElement('div');
        frag.appendChild(div);
        const name = document.createElement('span');
        div.appendChild(name);
        name.innerHTML = user.username;
        const admin = document.createElement('input');
        div.appendChild(admin);
        div.appendChild(document.createTextNode('Admin'));
        admin.type = 'checkbox';
        admin.checked = user.admin;
        admin.onchange = () => intents.setAdmin(user.username, admin.checked);
        const del = document.createElement('button');
        div.appendChild(del);
        del.innerHTML = 'Delete';
        del.onclick = () => intents.delUser(user.username);
    }
    return {content: frag};
};

view.addUser = (model, intents) => {
    const form = document.createElement('form');
    form.onsubmit = e => {e.preventDefault(); intents.addUser(name.value, fullname.value, email.value, pw.value)};
    form.appendChild(document.createTextNode('User Name: '));
    const name = document.createElement('input');
    form.appendChild(name);
    name.type = 'text';
    name.autofocus = true;
    form.appendChild(document.createTextNode('Full Name: '));
    const fullname = document.createElement('input');
    form.appendChild(fullname);
    fullname.type = 'text';
    form.appendChild(document.createTextNode('email: '));
    const email = document.createElement('input');
    form.appendChild(email);
    email.type = 'text';
    form.appendChild(document.createTextNode('Password: '));
    const pw = document.createElement('input');
    form.appendChild(pw);
    pw.type = 'password';
    const submit = document.createElement('input');
    form.appendChild(submit);
    submit.name = 'Submit';
    submit.type = 'Submit';
    submit.value = 'Create';
    return {content: form};
};

view.invites = (model, intents) => {
    if (model.invites === undefined) {
        const text = document.createTextNode('loading...');
        return {content: text};
    } else if (model.invites.length === 0) {
        const text = document.createTextNode('No invites found');
        return {content: text};
    }
    const frag = document.createDocumentFragment();
    const table = document.createElement('table');
    frag.appendChild(table);
    const htr = document.createElement('tr');
    table.appendChild(htr);
    for (const h of ['Token', 'email', 'Issuer', 'Created', 'Expires', 'Actions']) {
        const th = document.createElement('th');
        th.innerHTML = h;
        htr.appendChild(th);
    }
    for (const inv of model.invites) {
        console.log("invite:" + JSON.stringify(inv));
        const tr = document.createElement('tr');
        table.appendChild(tr);
        for (const h of ['token', 'email', 'issuer', 'created', 'expires']) {
            const th = document.createElement('td');
            th.innerHTML = inv[h];
            tr.appendChild(th);
        }
        const del = document.createElement('button');
        tr.appendChild(del);
        del.innerHTML = 'Delete';
        del.onclick = () => intents.delInvite(inv.token);
    }
    return {content: frag};
};

view.requests = (model, intents) => {
    if (model.requests === undefined) {
        const text = document.createTextNode('loading...');
        return {content: text};
    } else if (model.requests.length === 0) {
        const text = document.createTextNode('No invite requests found');
        return {content: text};
    }
    const frag = document.createDocumentFragment();
    const table = document.createElement('table');
    frag.appendChild(table);
    const htr = document.createElement('tr');
    table.appendChild(htr);
    for (const h of ['email', 'From', 'Created', 'Actions']) {
        const th = document.createElement('th');
        th.innerHTML = h;
        htr.appendChild(th);
    }
    for (const req of model.requests) {
        const tr = document.createElement('tr');
        table.appendChild(tr);
        for (const h of ['email', 'from', 'created']) {
            const th = document.createElement('td');
            th.innerHTML = req[h];
            tr.appendChild(th);
        }
        const inv = document.createElement('button');
        tr.appendChild(inv);
        inv.innerHTML = 'Invite';
        inv.onclick = () => intents.approveRequest(req.email);
        const del = document.createElement('button');
        tr.appendChild(del);
        del.innerHTML = 'Delete';
        del.onclick = () => intents.delRequest(req.email);
    }
    return {content: frag};
};

view.inviteUser = (model, intents) => {
    const form = document.createElement('form');
    form.onsubmit = e => {e.preventDefault(); intents.inviteUser(email.value)};
    form.appendChild(document.createTextNode('Email: '));
    const email = document.createElement('input');
    form.appendChild(email);
    email.type = 'email';
    email.autofocus = true;
    const submit = document.createElement('input');
    form.appendChild(submit);
    submit.name = 'Submit';
    submit.type = 'Submit';
    submit.value = 'Send Invite';
    return {content: form};
};

view.workspaces = (model, intents) => {
    if (model.workspaces === undefined) {
        const text = document.createTextNode('loading...');
        return {content: text};
    }
    const frag = document.createDocumentFragment();
    for (const ws of model.workspaces) {
        const div = document.createElement('div');
        frag.appendChild(div);
        const name = document.createElement('span');
        div.appendChild(name);
        name.innerHTML = ws.username + '/' + ws.wsname;
        const chans = document.createElement('span');
        div.appendChild(chans);
        chans.innerHTML = JSON.stringify(ws);
    }
    return {content: frag};
};

view.boards = (model, intents) => {
    if (model.boards === undefined) {
        const text = document.createTextNode('loading...');
        return {content: text};
    } else if (model.boards.length === 0) {
        const text = document.createTextNode('No boards found');
        return {content: text};
    }
    const frag = document.createDocumentFragment();
    const table = document.createElement('table');
    frag.appendChild(table);
    const htr = document.createElement('tr');
    table.appendChild(htr);
    for (const h of ['ID', 'Online', 'Created', 'Last', 'Type', 'Info', 'AssingedTo', 'Actions']) {
        const th = document.createElement('th');
        th.innerHTML = h;
        htr.appendChild(th);
    }
    for (const id in model.boards) {
        const b = model.boards[id];
        console.log("b:" + JSON.stringify(b));
        const tr = document.createElement('tr');
        table.appendChild(tr);
        addel(tr, 'td', {innerHTML: id});
        for (const h of ['online', 'created', 'last', 'type', 'info', 'assignedto']) {
            const td = addel(tr, 'td', {innerHTML: h === 'last' ? formatDistance(b[h], new Date()) : b[h]});
            if (h === 'type') {
                const edit = addel(td, 'button', {innerHTML: 'edit'});
                edit.onclick = () => intents.edittype(id, b[h]);
            } else if (h === 'info') {
                const edit = addel(td, 'button', {innerHTML: 'edit'});
                edit.onclick = () => intents.editinfo(id, b[h]);
            }
        }
        const release = document.createElement('button');
        tr.appendChild(release);
        release.innerHTML = 'Forced Release';
        release.onclick = () => intents.releaseBoard(id);
        const del = addel(tr, 'button', {innerHTML: 'Delete'});
        del.onclick = () => intents.deleteBoard(id);
}
    return {content: frag};
};

view.agents = (model, intents) => {
    if (model.agents === undefined) {
        const text = document.createTextNode('loading...');
        return {content: text};
    } else if (model.agents.length === 0) {
        const text = document.createTextNode('No agents found');
        return {content: text};
    }
    const frag = document.createDocumentFragment();
    const table = document.createElement('table');
    frag.appendChild(table);
    const htr = document.createElement('tr');
    table.appendChild(htr);
    for (const h of ['ID', 'Version', 'OS', 'RemoteAddr', 'Owner', 'Created', 'Last', 'Online', 'Actions']) {
        const th = document.createElement('th');
        th.innerHTML = h;
        htr.appendChild(th);
    }
    for (const b of model.agents) {
        console.log("b:" + JSON.stringify(b));
        const tr = document.createElement('tr');
        table.appendChild(tr);
        for (const h of ['id', 'version', 'os', 'raddr', 'owner', 'created', 'last', 'online']) {
            const th = document.createElement('td');
            th.innerHTML = h === 'last' ? formatDistance(b[h], new Date()) : b[h];
            tr.appendChild(th);
        }
        const del = addel(tr, 'button', {innerHTML: 'Delete'});
        del.onclick = () => intents.deleteAgent(b.id);
    }
    return {content: frag};
};

view.display = repr => {
    for (const r in repr) {
        if (!repr.hasOwnProperty(r)) {
            continue;
        }
        document.getElementById(r).innerHTML = '';
        if (repr[r] !== undefined) {
            document.getElementById(r).appendChild(repr[r]);
        }
    }
};


const state = {};

state.representation = (model, intents) => {
    let repr = {};
    if (model.select === "Announce") {
        repr = view.announce(model, intents);
    } else if (model.select === "Users") {
        repr = view.users(model, intents);
    } else if (model.select === "Add User") {
        repr = view.addUser(model, intents);
    } else if (model.select === "Invites") {
        repr = view.invites(model, intents);
    } else if (model.select === "Invite Requests") {
        repr = view.requests(model, intents);
    } else if (model.select === "Invite User") {
        repr = view.inviteUser(model, intents);
    } else if (model.select === "Workspaces") {
        repr = view.workspaces(model, intents);
    } else if (model.select === "Boards") {
        repr = view.boards(model, intents);
    } else if (model.select === "Agents") {
        repr = view.agents(model, intents);
    }
    view.display(repr);
};

state.nextAction = (model, intents) => {

};

state.render = (model, intents) => {
    state.representation(model, intents);
    state.nextAction(model, intents);
};


const model = {};

model.present = (data, intents) => {
    console.log("present: " + JSON.stringify(data));
    for (const id in data) {
        const payload = data[id];
        if (id === "select") {
            model.select = payload;
        } else if (id === "users") {
            model.users = payload;
        } else if (id === "invites") {
            model.invites = payload;
        } else if (id === "requests") {
            model.requests = payload;
        } else if (id === "workspaces") {
            model.workspaces = payload;
        } else if (id === "boards") {
            model.boards = payload;
        } else if (id === "agents") {
            model.agents = payload;
        }
    }
    state.render(model, intents);
};


function get(url) {
    return fetch(url, {credentials: 'include'})
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('GET ' + url + ' failed with: ' + response.status + ' ' + response.statusText);
        })
}

function encodeBody(data) {
    if (typeof data === 'string') {
        return data;
    } else {
        return JSON.stringify(data);
    }
}

function post(url, data) {
    const body = encodeBody(data);
    return fetch(url, {method: 'POST', body: body, credentials: 'include'})
        .then(response => {
            if (response.ok) {
                return;
            }
            throw new Error('POST ' + url + ' failed with: ' + response.status + ' ' + response.statusText);
        })
}

function put(url, data) {
    const body = encodeBody(data);
    return fetch(url, {method: 'PUT', body: body, credentials: 'include'})
        .then(response => {
            if (response.ok) {
                return;
            }
            throw new Error('PUT ' + url + ' failed with: ' + response.status + ' ' + response.statusText);
        })
}

function del(url) {
    return fetch(url, {method: 'DELETE', credentials: 'include'})
        .then(response => {
            if (response.ok) {
                return;
            }
            throw new Error('DELETE ' + url + ' failed with: ' + response.status + ' ' + response.statusText);
        })
}

const actions = {};

actions.announce = (subject, body) => {
    if (confirm('Are you sure you want send email to all users?')) {
        post('announce/?subject=' + encodeURIComponent(subject), body);
    }
};

actions.updateUsers = () => {
    get('users/').then(users => {
        model.present({users: users}, actions);
    })
};

actions.updateInvites = () => {
    get('invites/').then(invs => {
        model.present({invites: invs}, actions);
    })
};

actions.updateRequests = () => {
    get('requests/').then(reqs => {
        model.present({requests: reqs}, actions);
    })
};

actions.updateWorkspaces = () => {
    get('wsman/').then(s => {
        model.present({workspaces: s}, actions);
    })
};

actions.updateBoards = () => {
    get('boards/').then(bs => {
        model.present({boards: bs}, actions);
    })
};

actions.updateAgents = () => {
    get('agents/').then(agents => {
        model.present({agents: agents}, actions);
    })
};

actions.select = menu => {
    if (menu === "Users") {
        actions.updateUsers();
    } else if (menu === "Invites") {
        actions.updateInvites();
    } else if (menu === "Invite Requests") {
        actions.updateRequests();
    } else if (menu === "Workspaces") {
        actions.updateWorkspaces();
    } else if (menu === "Boards") {
        actions.updateBoards();
    } else if (menu === "Agents") {
        actions.updateAgents();
    }

    model.present({select: menu}, actions);
};

actions.addUser = (username, fullname, email, password) => {
    post('users/', {username: username, fullname: fullname, email: email, password: password});
};

actions.setAdmin = (username, admin) => {
    put('users/' + username + '/admin', admin);
};

actions.delUser = username => {
    if (confirm('Are you sure you want to delete user ' + username + '?')) {
        del('users/' + username)
            .then(() => actions.updateUsers())
    }
};

actions.inviteUser = email => {
    post('invites/', {email: email});
};

actions.delInvite = token => {
    if (confirm('Are you sure you want to delete invite token ' + token + '?')) {
        del('invites/' + token)
            .then(() => actions.updateInvites())
    }
};

actions.approveRequest = async email => {
    await post('invites/', {email: email});
    actions.updateInvites();
    actions.delRequest(email);
};

actions.delRequest = async email => {
    if (confirm('Are you sure you want to delete request from ' + email + '?')) {
        await del('requests/' + email);
        actions.updateRequests();
    }
};

actions.deleteAgent = async agentid => {
    if (confirm('Are you sure you want to delete agent ' + agentid + '?')) {
        await del('agents/' + agentid);
        actions.updateAgents();
    }
};

actions.releaseBoard = id => {
    put('boards/' + encodeURIComponent(id) + '/release', '');
};

actions.edittype = async (id, btype) => {
    const newtype = prompt('Enter new board type for ' + id + ':', btype);
    if (newtype) {
        await put('boards/' + encodeURIComponent(id) + '/type', newtype);
        actions.updateBoards();
    }
};

actions.editinfo = async (id, info) => {
    const newinfo = prompt('Enter new board info for ' + id + ':', info);
    if (newinfo) {
        await put('boards/' + encodeURIComponent(id) + '/info', newinfo);
        actions.updateBoards();
    }
};

actions.deleteBoard = async id => {
    if (confirm('Are you sure you want to delete board ' + id + '?')) {
        await del('boards/' + encodeURIComponent(id));
        actions.updateBoards();
    }
};

class Admin {
    constructor(actions) {
        this.root = document.createDocumentFragment();

        const sidebar = document.createElement('div');
        this.root.appendChild(sidebar);
        sidebar.id = 'sidebar';

        for (const menu of ['Announce', 'Users', 'Add User', 'Invites', 'Invite User', 'Invite Requests', 'Workspaces', 'Boards', 'Agents']) {
            const item = document.createElement('div');
            sidebar.appendChild(item);
            item.classList.add('sidebar-item');
            item.innerHTML = menu;
            item.onclick = () => actions.select(menu);
        }

        const content = document.createElement('div');
        this.root.appendChild(content);
        content.id = 'content';
    }
}

const admin = new Admin(actions);

document.body.appendChild(admin.root);

StartPing('admin');
