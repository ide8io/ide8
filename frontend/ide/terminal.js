"use strict";

import './terminal.css';
import { Terminal } from 'xterm';
import * as fit from 'xterm/lib/addons/fit/fit';
import 'xterm/dist/xterm.css';

Terminal.applyAddon(fit);

export default class {
    constructor(name, comms) {
        this.name = name;
        this.root = document.createElement('div');
        this.root.id = 'terminal';
        this.comms = comms;
    }

    getRoot(visible) {
        this.root.style.display = visible ? 'block' : 'none';
        return this.root;
    }

    init() {
        if (this.xterm === undefined) {
            this.xterm = new Terminal();
            const ondata = data => this.pty.write(data);
            const onresize = e => this.pty.resize(e.rows, e.cols);
            this.pty = this.comms.remotePty(data => this.xterm.write(data), error => {
                if (error) {
                    console.log('pty error: ' + error);
                }
                if (this.pty) {
                    delete this.pty;
                }
                // Release event callbacks
                this.xterm.off('data', ondata);
                this.xterm.off('resize', onresize);
                this.xterm.write('\r\n\n<<< Terminal disconnected >>>\r\n');
            });
            this.xterm.on('data', ondata);
            this.xterm.on('resize', onresize);
            this.xterm.open(this.root);
        }
        this.resize();
        this.focus();
    }

    cleanup() {
        if (this.pty) {
            this.pty.stop();
            delete this.pty;
        }
    }

    focus() {
        this.xterm.focus();
    }

    resize() {
        this.xterm.fit();
    }
}
