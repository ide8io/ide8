"use strict";

import './remote.css';
import { Terminal } from 'xterm';
import * as fit from 'xterm/lib/addons/fit/fit';
import 'xterm/dist/xterm.css';
import { mkel } from '../utils';

Terminal.applyAddon(fit);

export default class {
    constructor(name, boardid, tunnelid, comms) {
        this.name = name;
        this.boardid = boardid;
        this.tunnelid = tunnelid;
        this.root = document.createElement('div');
        this.root.id = 'remote';
        const bar = document.createElement('div');
        this.root.appendChild(bar);
        bar.id = 'remote-bar';
        const start = mkel('button', {innerHTML: "Start"}, bar);
        start.onclick = () => {
            this.remote.start();
        };
        const stop = mkel('button', {innerHTML: "Stop"}, bar);
        stop.onclick = () => {
            this.remote.stop();
        };
        this.status = document.createElement('div');
        bar.appendChild(this.status);
        this.status.id = 'remote-bar-status';
        this.status.classList.add('remote-bar-item');
        this.status.innerHTML = "Connecting...";
        this.term = document.createElement('div');
        this.root.appendChild(this.term);
        this.term.id = "remote-term";
        this.comms = comms;
    }

    _onstatus(status) {
        console.log('Remote status ' + JSON.stringify(status));
        this.status.innerHTML = status.error;
    }

    _connect() {
        if (this.remote) {
            return;
        }
        this.remote = this.comms.remotePlugin(this.boardid, this.tunnelid,
            status => this._onstatus(status),
            (type, data) => this.xterm.write(data.replace(/\n/g, '\r\n')),
            error => {
                if (error) {
                    console.log('remote error: ' + error);
                }
                if (this.remote) {
                    delete this.remote;
                }
                //this._statusOffline();
                this.xterm.write('\r\n\n<<< Remote plugin disconnected >>>\r\n');
                //setTimeout(() => {if (!this.remote) {this._connect()}}, 10000);
            }
        );
        this._clear();
    }

    getRoot(visible) {
        this.root.style.display = visible ? 'block' : 'none';
        return this.root;
    }

    init() {
        if (this.xterm === undefined) {
            this.xterm = new Terminal();
            this._connect();
            this.xterm.open(this.term);
        }
        this.resize();
        this.focus();
    }

    cleanup() {
        if (this.remote) {
            this.remote.close();
            delete this.remote;
        }
    }

    focus() {
        this.xterm.focus();
    }

    resize() {
        this.xterm.fit();
    }

    _clear() {
        if (this.xterm) {
            this.xterm.clear();
        }
    }
}
