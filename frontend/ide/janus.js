"use strict";

import './janus.css';
import { mkel } from '../utils';
import loading from '../components/loading';
import adapter from 'webrtc-adapter';
import Janus from '../../external/janus.es';


const list = (streamList, intents) => {
    if (streamList === undefined) {
        return loading('Fetching stream list ...');
    }
    const sel = mkel('select');
    mkel('option', {value: -1, innerHTML: '&lt;None selected&gt;'}, sel);
    for (const stream of streamList) {
        const opt = document.createElement('option');
        opt.value = stream.id;
        opt.innerHTML = stream.description;
        sel.appendChild(opt);
    }
    sel.onclick = e => intents.selectStream(parseInt(e.target.value));
    return sel;
};

const video = (stream) => {
    if (stream === undefined) {
        return mkel('div', {innerHTML: 'No stream selected'});
    }
    const video = mkel('video', {width: 640, height: 480, autoplay: true, controls: true});
    Janus.attachMediaStream(video, stream);
    return video;
};

class View {
    constructor() {
        this._els = {};
        this.root = mkel('div', {className: 'janus-root'});
        this._els['error'] = mkel('div', {}, this.root);
        this._els['list'] = mkel('div', {}, this.root);
        this._els['video'] = mkel('div', {className: 'janus-content'}, this.root);
    }

    getRoot(visible) {
        this.root.style.display = visible ? 'block' : 'none';
        return this.root;
    }

    static list(model, intents) {
        return {list: list(model.list, intents)};
    }

    static video(model, intents) {
        return{video: video(model.stream)};
    }

    display(repr) {
        for (const r in repr) {
            if (!repr.hasOwnProperty(r)) {
                continue;
            }
            const el = this._els[r];
            el.innerHTML = '';
            if (repr[r] !== undefined) {
                el.appendChild(repr[r]);
            }
        }
    }
}


const state = {
    representation(model, view, intents) {
        let repr = {};
        if (model.error) {
            repr['error'] = document.createTextNode(model.error);
        }

        if (model.connected === true) {
            if (model.updateList) {
                repr = Object.assign({}, repr, view.constructor.list(model, intents));
            }
            if (model.updateStream) {
                repr = Object.assign({}, repr, view.constructor.video(model, intents));
            }
        } else {
            repr['list'] = loading('Connecting to WebRTC server ...');
        }
        view.display(repr);
    },

    nextAction(model, view, intents) {
    },

    render(model, view, intents) {
        state.representation(model, view, intents);
        state.nextAction(model, view, intents);
    },

};


class Model {
    constructor() {
    }

    present(data, view, intents) {
        this.updateList = false;
        this.updateStream = false;

        for (const id in data) {
            if (!data.hasOwnProperty(id)) {
                continue;
            }

            const payload = data[id];
            if (id === 'connected') {
                this.connected = payload;
            } else if (id === 'list') {
                this.list = payload;
                this.updateList = true;
            } else if (id === 'stream') {
                this.stream = payload;
                this.updateStream = true;
            } else if (id === 'error') {
                this.error = payload;
            } else {
                console.log('ttn: Unhandled model present id: ' + id);
            }
        }

        state.render(this, view, intents)
    }
}


class Actions {
    constructor(view) {
        this.view = view;
        this.model = new Model();
        this.intents = {
            selectStream: this.selectStream.bind(this),
        };
        this.present({connected: false});
        this._init();
    }

    async _init() {
        await new Promise(resolve => Janus.init({
            debug: true,
            dependencies: Janus.useDefaultDependencies({adapter: adapter}),
            callback: () => {
                resolve();
            }
        }));
        this.janus = await new Promise(resolve => {
            const janus = new Janus({
                server: 'https://stream.ide8.io/janus',
                iceServers: [{urls: 'turn:stream.ide8.io:3478',
			        credential: 'turnsecret',
			        username: 'ide8'
		        }],
                success: () => resolve(janus),
                error: cause => {
                    console.log('janus error: ', cause);
                    this.present({error: 'Janus error: ' + cause});
                    throw cause;
                }
            })
        });
        this.janus.attach({
            plugin: "janus.plugin.streaming",
			opaqueId: "streamingtest-" + Janus.randomString(12),
			success: this._onsuccess.bind(this),
            error: error => {
                console.log('attach error: ' + error);
            },
            onmessage: this._onmessage.bind(this),
            onremotestream: this._onremotestream.bind(this),
            oncleanup: () => {
            }
        });
    }

    _onsuccess(pluginHandle) {
        this.present({connected: true, list: undefined});
        this.streaming = pluginHandle;
        this.streaming.send({"message": {"request": "list"}, success: result => {
                if (result === null || result === undefined) {
                    console.log('janus stream list failed!');
                    return;
                }
                const list = result['list'];
                if (list !== null && list !== undefined) {
                    console.log('stream list: ' + JSON.stringify(list));
                    this.present({list: list})
                }
            }
        });
    }

    _onmessage(msg, jsep) {
        console.log('onmessage: ' + JSON.stringify(msg));
        const result = msg["result"];
        if (result !== null && result !== undefined) {
            if (result["status"] !== undefined && result["status"] !== null) {
                const status = result["status"];
                if (status === 'stopped') {
                    this.streaming.send({"message": {"request": "stop"}});
                    this.streaming.hangup();
                }
            }
        } else if (msg["error"] !== undefined && msg["error"] !== null) {
            this.streaming.send({"message": {"request": "stop"}});
            this.streaming.hangup();
            return;
        }
        if (jsep !== undefined && jsep !== null) {
            this.streaming.createAnswer({
                jsep: jsep,
                media: {audioSend: false, videoSend: false},
                success: jsep => {
                    this.streaming.send({"message": {"request": "start"}, "jsep": jsep});
                },
                error: error => {
                    console.log('createAnswer error: ' + error);
                }
            });
        }
    }

    _onremotestream(stream) {
        this.present({stream: stream});
    }

    cleanup() {
        if (this.streaming) {
            this.streaming.send({"message": {"request": "stop"}});
            this.streaming.hangup();
            this.streaming.detach();
        }
        if (this.janus) {
            this.janus.destroy();
        }
    }

    present(data) {
        this.model.present(data, this.view, this.intents);
    }

    selectStream(id) {
        if (this.streamId === id) {
            return;
        }
        this.streamId  = id;
        this.streaming.send({"message": {"request": "stop"}});
        this.streaming.hangup();
        if (id >= 0) {
            this.streaming.send({"message": {"request": "watch", id: id}});
        }
    }
}


export default class {
    constructor() {
        this.view = new View();
    }

    getRoot(visible) {
        return this.view.getRoot(visible);
    }

    init() {
        if (!this.actions) {
            this.actions = new Actions(this.view);
        }
    }

    cleanup() {
        this.actions.cleanup();
    }

    focus() {
    }

    resize() {
    }
}
