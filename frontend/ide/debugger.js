"use strict";

import './debugger.css';
import { Terminal } from 'xterm';
import * as fit from 'xterm/lib/addons/fit/fit';
import 'xterm/dist/xterm.css';
import { addel } from '../utils';
import spinnericon from "../img/spinnericon.svg";
import { FileInfo } from '../comms/workspace';
import playicon from '../img/playicon.svg';
import stopicon from '../img/stopicon.svg';
import pauseicon from '../img/pauseicon.svg';
import { reloadicon } from '../components/icons';

Terminal.applyAddon(fit);

const header = (status, projects, selectedproject, targets, selectedtarget, intents) => {
    const frag = document.createDocumentFragment();
    if (status.state === 'stopped') {
        const notselval = '___not_selected___';
        const start = addel(frag, 'button', {innerHTML: playicon, className: 'debugger-start'});
        start.onclick = () => {
            let project = pro.value;
            if (pro.value === notselval) {
                project = null;
            }
            let target = tar.value;
            if (tar.value === notselval) {
                target = null;
            }
            intents.start(project, target);
        };
        addel(start, 'span', {className: 'tooltip', innerHTML: 'Start debugging'});
        addel(frag, 'span', {innerHTML: 'Project:', className: 'debugger-label'});
        const pro = addel(frag, 'select');
        const selected = selectedproject !== null || selectedproject !== undefined;
        addel(pro, 'option', {innerHTML: '&lt;not selected&gt;', value: notselval, selected: !selected});
        if (projects) {
            for (const p of projects) {
                let name = '';
                if (p === '') {
                    name = '&lt;workspace root&gt;'
                } else {
                    name = p;
                }
                addel(pro, 'option', {innerHTML: name, value: p, selected: p === selectedproject});
            }
        }
        pro.onclick = () => {
            if (pro.value !== selectedproject) {
                let val = pro.value;
                if (pro.value === notselval) {
                    val = null;
                }
                intents.storeSelectedProject(val);
            }
        };
        addel(frag, 'span', {innerHTML: 'Board:', className: 'debugger-label'});
        const tar = addel(frag, 'select');
        const selectedt = selectedtarget !== null || selectedtarget !== undefined;
        addel(tar, 'option', {innerHTML: '&lt;not selected&gt;', value: notselval, selected: !selectedt});
        if (targets) {
            for (const t of targets) {
                addel(tar, 'option', {innerHTML: t, value: t, selected: t === selectedtarget});
            }
        }
        tar.onclick = () => {
            if (tar.value !== selectedtarget) {
                let val = tar.value;
                if (tar.value === notselval) {
                    val = null;
                }
                intents.storeSelectedTarget(val);
            }
        };
    } else {
        const stop = addel(frag, 'button', {innerHTML: stopicon, className: 'debugger-stop'});
        stop.onclick = () => intents.stop();
        addel(stop, 'span', {className: 'tooltip', innerHTML: 'Stop debugging'});
        if (status.state === 'started') {
            const run = addel(frag, 'button', {innerHTML: playicon});
            run.onclick = () => intents.run();
            addel(run, 'span', {className: 'tooltip', innerHTML: 'Continue'});
            const brk = addel(frag, 'button', {innerHTML: pauseicon});
            brk.onclick = () => intents.brk();
            addel(brk, 'span', {className: 'tooltip', innerHTML: 'Break'});
            const stepover = addel(frag, 'button', {innerHTML: 'Step Over'});
            stepover.onclick = () => intents.stepOver();
            addel(stepover, 'span', {className: 'tooltip', innerHTML: 'Step over function'});
            const stepinto = addel(frag, 'button', {innerHTML: 'Step Into'});
            stepinto.onclick = () => intents.stepInto();
            addel(stepinto, 'span', {className: 'tooltip', innerHTML: 'Step into function'});
            const stepout = addel(frag, 'button', {innerHTML: 'Step Out'});
            stepout.onclick = () => intents.stepOut();
            addel(stepout, 'span', {className: 'tooltip', innerHTML: 'Step out of function'});
            const reset = addel(frag, 'button', {innerHTML: reloadicon});
            reset.onclick = () => intents.reset();
            addel(reset, 'span', {className: 'tooltip', innerHTML: 'Reset'});
            if (status.running) {
                run.disabled = true;
                brk.className = 'debugger-stop';
                stepover.disabled = true;
                stepinto.disabled = true;
                stepout.disabled = true;
                reset.disabled = true;
            } else {
                run.className = 'debugger-start';
                brk.disabled = true;
                stepover.className = 'debugger-stepover';
                stepinto.className = 'debugger-stepinto';
                stepout.className = 'debugger-stepout';
                reset.className = 'debugger-reset';
            }
        } else {
            addel(frag, 'span', {innerHTML: spinnericon});
            addel(frag, 'span', {innerHTML: status.state});
        }
    }
    return frag;
};

class View {
    constructor() {
        this._els = {};
        this.root = addel(null, 'div', {className: 'debugger-root'});
        const hdr = addel(this.root, 'div', {className: 'debugger-header'});
        this._els['hdr'] = hdr;
        this._els['console'] = addel(this.root, 'div', {className: 'debugger-console'});
    }

    getConsoleElement() {
        return this._els['console'];
    }

    getRoot(visible) {
        this.root.style.display = visible ? 'block' : 'none';
        return this.root;
    }

    static loading(message) {
        return {content: loading(message)};
    }

    static header(model, intents) {
        return {hdr: header(model.status, model.projects, model.selectedproject, model.targets, model.selectedtarget, intents)};
    }

    display(repr) {
        for (const r in repr) {
            if (!repr.hasOwnProperty(r)) {
                continue;
            }
            const el = this._els[r];
            el.innerHTML = '';
            if (repr[r] !== undefined) {
                el.appendChild(repr[r]);
            }
        }
    }
}

const state = {
    representation(model, view, intents) {
        let repr = {};
        if (model.updateHeader) {
            repr = Object.assign({}, repr, view.constructor.header(model, intents));
        }

        view.display(repr);
    },

    nextAction(model, view, intents) {
        if (model.connectionOk && !(model.device && model.device.dev_id) && !model.devices) {
            intents.getDeviceList(10, 0);
        }
    },

    render(model, view, intents) {
        state.representation(model, view, intents);
        state.nextAction(model, view, intents);
    },

};

class Model {
    constructor() {
    }

    present(data, view, intents) {
        this.updateHeader = false;

        for (const id in data) {
            if (!data.hasOwnProperty(id)) {
                continue;
            }

            const payload = data[id];
            if (id === 'setStatus') {
                this.status = payload;
                this.updateHeader = true;
            } else if (id === 'selectProject') {
                this.selectedproject = payload;
                this.updateHeader = true;
            } else if (id === 'storeSelectedProject') {
                this.selectedproject = payload;
            } else if (id === 'projects') {
                this.projects = payload;
                this.updateHeader = true;
            } else if (id === 'targets') {
                this.targets = payload;
                this.updateHeader = true;
            } else if (id === 'selectTarget') {
                this.selectedtarget = payload;
                this.updateHeader = true;
            } else if (id === 'storeSelectedTarget') {
                this.selectedtarget = payload;
            } else {
                console.log('debugger: Unhandled model present id: ' + id);
            }
        }

        state.render(this, view, intents)
    }
}

class Actions {
    constructor(view, comms, services) {
        this.view = view;
        this.comms = comms;
        this.services = services;
        this.model = new Model();
        this.intents = {
            storeSelectedProject: project => this.present({storeSelectedProject: project}),
            storeSelectedTarget: target => this.present({storeSelectedTarget: target}),
            start: this.start.bind(this),
            stop: this.stop.bind(this),
            brk: () => this.send('interrupt'),
            run: () => this.send('command', 'exec-continue'),
            stepOver: () => this.send('command', 'exec-next'),
            stepInto: () => this.send('command', 'exec-step'),
            stepOut: () => this.send('command', 'exec-finish'),
            reset: () => this.send('command', 'interpreter-exec console \"monitor reset init\"'),
        };
        this.present({setStatus: {state: 'stopped', runnning: false}});
        this.xterm = new Terminal();
        this.xterm.open(view.getConsoleElement());
        this.loadProjects();
        this.loadTargets();
        this._connect();
        this.comms.setBreakpointHandler({add: this.addbreakpoint.bind(this), remove: this.rembreakpoint.bind(this)});
    }

    writeError(errmsg) {
        this.xterm.write('\x1b[41mERROR: ' + errmsg.replace(/\n/g, '\r\n') + '\x1b[0m\r\n');
    }

    _ondata(type, data) {
        if (type === 'status') {
            console.log('debugger status ' + JSON.stringify(data));
            this.status = data;
            if (data.state === 'error') {
                this.writeError(data.message);
            }
            this.present({setStatus: data});
        } else if (type === 'breakpoints') {
            console.log('breakpoints: ' + JSON.stringify(data));
            const breakpoints = {};
            if (data) {
                for (const bp of data) {
                    const fi = new FileInfo(bp.repo, bp.file);
                    if (fi in breakpoints) {
                        breakpoints[fi].push(bp.line);
                    } else {
                        breakpoints[fi] = [bp.line];
                    }
                }
            }
            this.comms.publishBreakpoints(breakpoints);
        } else if (type === 'stopped') {
            console.log('stopped: ' + JSON.stringify(data));
            if (data.breakpoint.file && data.breakpoint.line) {
                const fileinfo = new FileInfo(data.breakpoint.repo, data.breakpoint.file);
                this.services.highlightSource(fileinfo, data.breakpoint.line);
            }
        } else if (type === 'notification') {
            if (data.type === 'console') {
                this.xterm.write(data.payload.replace(/\n/g, '\r\n'));
            } else {
                this.xterm.write('notifcation: ' + JSON.stringify(data) + '\r\n');
            }
        } else if (type === 'output') {
            this.xterm.write('output: ' + JSON.stringify(data) + '\r\n');
        } else if (type === 'error') {
            this.writeError(data);
        } else {
            console.log('debugger: unknown remote message type: ' + type);
        }
    }

    _connect() {
        if (this.debugger) {
            return;
        }
        let lineBuf = '';
        const ondata = data => {
            const end = data.indexOf('\r');
            if (end === -1) {
                this.xterm.write(data);
                lineBuf += data;
            } else {
                this.xterm.write('\r\n');
                lineBuf += data.substr(0, end);
                this.debugger.send('console', lineBuf);
                lineBuf = data.substr(end + 1);
            }
        };
        this.debugger = this.comms.connectResource('gdb', {},
            this._ondata.bind(this),
            error => {
                if (error) {
                    console.log('gdb error: ' + error);
                }
                if (this.debugger) {
                    delete this.debugger;
                }
                // Release event callbacks
                this.xterm.off('data', ondata);
                this.xterm.write('\r\n\n<<< Debugger disconnected >>>\r\n');
            }
        );
        this.xterm.on('data', ondata);
    }

    loadProjects() {
        this.comms.listProjects().then(projects => this.present({projects: projects}));
    }

    loadTargets() {
        const ress = this.comms.getResourcesByCapability('gdbserver');
        let sel;
        if (ress.length > 0) {
            sel = ress[0];
        }
        this.present({targets: ress, selectTarget: sel});
    }

    start(project, target) {
        if (project === null || project === undefined) {
            this.writeError('No project selected!');
            return;
        }
        if (target === null || target === undefined) {
            this.writeError('No target selected!');
            return;
        }
        this.send('start', {project: project, target: target});
    }

    stop() {
        this.send('stop');
    }

    send(type, data) {
        this.debugger.send(type, data);
    }

    addbreakpoint(fileinfo, row) {
        if (this.status && this.status.state === 'started' && this.status.running === false) {
            this.debugger.send('break:add', {repo: fileinfo.repo, file: fileinfo.path, line: row.toString()});
            return true;
        }
        return false;
    }

    rembreakpoint(fileinfo, row) {
        if (this.status && this.status.state === 'started' && this.status.running === false) {
            this.debugger.send('break:remove', {repo: fileinfo.repo, file: fileinfo.path, line: row.toString()});
            return true;
        }
        return false;
    }

    command(params) {
        console.log('gdb cmd: ' + params);
        const arr = params.split(' ', 2);
        const operation = arr[0];
        const project = arr[1];
        this.present({selectProject: project})
    }

    cleanup() {
        this.comms.clearBreakpointHandler();
        if (this.debugger) {
            this.debugger.close();
            delete this.debugger;
        }
    }

    focus() {
        this.xterm.focus();
    }

    resize() {
        this.xterm.fit();
    }

    present(data) {
        this.model.present(data, this.view, this.intents);
    }
}

export default class {
    constructor(name, comms, services) {
        this.view = new View();
        this.name = name;
        this.comms = comms;
        this.services = services;
    }

    command(params) {
        this.actions.command(params);
    }

    getRoot(visible) {
        return this.view.getRoot(visible);
    }

    init() {
        if (!this.actions) {
            this.actions = new Actions(this.view, this.comms, this.services);
        }
        this.resize();
        this.focus();
    }

    cleanup() {
        this.actions.cleanup();
    }

    focus() {
        this.actions.focus();
    }

    resize() {
        this.actions.resize();
    }
}
