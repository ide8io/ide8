"use strict";

import './tabs.css';
import { mkel } from '../utils';

export default function(tabs, selected, intents, index, showDragOverlay) {
    const frag = document.createDocumentFragment();
    for (const t of tabs) {
        const tab = mkel('span', {className: 'tabs-tab', innerHTML: t.name, draggable: true}, frag);
        if (t === selected) {
            tab.classList.add('tabs-tab-selected');
        }
        tab.onclick = e => intents.select(t.id, index);
        tab.ondragstart = e => {
            e.dataTransfer.setData('text', t.id);
            showDragOverlay(true);
        };
        tab.ondragend = e => {
            showDragOverlay(false);
        };
        if (intents.contextmenu) {
            tab.oncontextmenu = e => {
                e.preventDefault();
                intents.contextmenu(e, t.id, index);
            }
        }
        const dot = mkel('span', {className: 'tabs-tab-dot'}, tab);
        if (t.writeable) {
            if (t.saved) {
                dot.classList.add('tabs-tab-dot-saved');
                dot.innerHTML = "&#9679;";
            } else {
                dot.classList.add('tabs-tab-dot-unsaved');
                dot.innerHTML = "&#9679;";
            }
        }
        const x = mkel('div', {className: 'tabs-tab-x', innerHTML: '&#10005;'}, tab);
        x.onclick = e => {e.stopPropagation(); intents.close(t.id, index)};
    }
    const tab = mkel('span', {className: 'tabs-pluss', innerHTML: '+'}, frag);
    tab.onclick = e => {
        e.stopPropagation();
        e.preventDefault();
        intents.pluss(e, index);
    };
    return frag;
}
