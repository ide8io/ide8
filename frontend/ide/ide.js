"use strict";

import './ide.css';
import tree from './tree';
import tabs from './tabs';
import treetabs from './treetabs';
import Editor from './editor';
import ImageViewer from './imageviewer';
import HelpViewer from './helpviewer';
import Browser from './browser';
import { FileInfo, Workspace } from '../comms/workspace';
import modal from './modal';
import loading from '../components/loading';
import { addel } from '../utils';

class View {
    constructor() {
        this._els = {};
        this.root = addel(null, 'div');
        const tree = addel(this.root, 'div', {id: 'ide-tree'});
        this._els['ide-tree-tabs'] = addel(tree, 'div', {id: 'ide-tree-tabs'});
        this._els['ide-tree-content'] = addel(tree, 'div', {id: 'ide-tree-content'});
        addel(this.root, 'div', {id: 'ide-left-sizer'});
        this._els['ide-middle'] = addel(this.root, 'div', {id: 'ide-middle'});
        this._els['ide-modal'] = addel(this.root, 'div', {id: 'ide-modal'});
    }

    treetabs(model, intents) {
        return {'ide-tree-tabs': treetabs(['Files', 'Examples', 'Resources'], model.selectedTreeTab, intents.treetabs)};
    }

    files(model, intents) {
        return {'ide-tree-content': tree(model.tree, intents.files)};
    }

    examples(model, intents) {
        return {'ide-tree-content': tree(model.examples, intents.examples)};
    }

    resources(model, intents) {
        if (model.resources === undefined) {
            const l = loading("Loading resources ...");
            l.style.height = '40px';
            return {'ide-tree-content': l};
        }
        const items = [];
        for (const res of model.resources) {
            items.push({name: res.name, id: res.name, type: 'file', info: res});
        }
        return {'ide-tree-content': tree({children: items}, intents.resources)};
    }

    subpanes(subpanes, paneindex, intents) {
        const frag = document.createDocumentFragment();
        const length = subpanes.length;
        const widths = new Array(length);
        for (let i = 0; i < length; i++) {
            widths[i] = subpanes[i].width;
        }
        const resize = index => {
            if (subpanes[index].selected && subpanes[index].selected.plugin && subpanes[index].selected.plugin.resize) {
                subpanes[index].selected.plugin.resize();
            }
        };
        let prev = null;
        for (let index = 0; index < length; index++) {
            const subpane = subpanes[index];
            if (prev) {
                const leftel = prev;
                const sizer = addel(frag, 'div', {className: 'ide-subpane-sizer'});
                sizer.onmousedown = e => {
                    e.preventDefault();
                    const width = widths[index - 1] + widths[index];
                    const left = leftel.getBoundingClientRect().left;
                    const right = splitel.getBoundingClientRect().right;
                    window.onmouseup = e => {
                        window.onmouseup = null;
                        window.onmousemove = null;
                        intents.storeWidths(paneindex, widths);
                    };
                    window.onmousemove = e => {
                        const xpos = e.pageX - 3;
                        const leftpercent = width * Math.max(Math.min(xpos, right - 40) - left, 30) / (right - left);
                        if (index === 1) {
                            leftel.style.width = leftpercent + '%';
                        } else {
                            leftel.style.width = 'calc(' + leftpercent + '% - 6px)';
                        }
                        splitel.style.width = 'calc(' + (width - leftpercent) + '% - 6px)';
                        widths[index - 1] = leftpercent;
                        widths[index] = width - leftpercent;
                        resize(index -  1);
                        resize(index);
                    };
                };
            }
            const splitel = addel(frag, 'div', {className: 'ide-subpane'});
            if (index > 0) {
                splitel.style.width = 'calc(' + subpane.width + '% - 6px)';
            } else {
                splitel.style.width = subpane.width + '%';
            }
            prev = splitel;
            const drag = addel(splitel, 'div', {className: 'ide-subpane-drag'});
            addel(drag, 'div', {innerHTML: 'Drop here to move tab'});
            drag.ondragover = e => e.preventDefault();
            drag.ondrop = e => {
                e.preventDefault();
                intents.paneTabs.moveTab(e.dataTransfer.getData('text'), {pane: paneindex, subpane: index})
            };
            const showDragOverlay = show => {
                const dragels = document.getElementsByClassName('ide-subpane-drag');
                for (const dragel of dragels) {
                    if (dragel.isSameNode(drag)) {
                        continue;
                    }
                    dragel.style.display = show ? 'block' : 'none';
                }
            };
            const hdr = addel(splitel, 'div', {className: 'ide-subpane-hdr'});
            const tabsel = addel(hdr, 'div', {className: 'ide-subpane-tabs'});
            tabsel.appendChild(tabs(subpane.tabs, subpane.selected, intents.paneTabs, {pane: paneindex, subpane: index}, showDragOverlay));
            addel(hdr, 'div', {className: 'ide-subpane-head'});
            const content = addel(splitel, 'div', {className: 'ide-subpane-content'});
            for (const type in subpane.commons) {
                if (subpane.commons.hasOwnProperty(type)) {
                    content.appendChild(subpane.commons[type].getRoot(false));
                }
            }
            for (const tab of subpane.tabs) {
                if (tab.plugin) {
                    if (tab.plugin.getRoot) {
                        const root = tab.plugin.getRoot(tab === subpane.selected);
                        content.appendChild(root);
                    } else if (tab.plugin.getCommonsType) {
                        if (tab === subpane.selected) {
                            const type = tab.plugin.getCommonsType();
                            subpane.commons[type].getRoot(true);
                        }
                    } else {
                        console.log('Bad plugin interface: ' + tab.plugin.constructor.name);
                    }
                } else if (tab === subpane.selected) {
                    content.appendChild(loading('Loading  plugin ...'));
                }
            }
        }
        return frag;
    }

    panes(model, intents) {
        const frag = document.createDocumentFragment();
        const length = model.panes.length;
        const heights = new Array(length);
        for (let i = 0; i < length; i++) {
            heights[i] = model.panes[i].height;
        }
        const resize = index => {
            for (const subpane of model.panes[index].subpanes) {
                if (subpane.selected && subpane.selected.plugin && subpane.selected.plugin.resize) {
                    subpane.selected.plugin.resize();
                }
            }
        };
        let prev = null;
        for (let index = 0; index < length; index++) {
            const pane = model.panes[index];
            if (prev) {
                const aboveel = prev;
                const sizer = addel(frag, 'div', {className: 'ide-pane-sizer'});
                sizer.onmousedown = e => {
                    e.preventDefault();
                    const height = heights[index - 1] + heights[index];
                    const top = aboveel.getBoundingClientRect().top;
                    const bottom = paneel.getBoundingClientRect().bottom;
                    window.onmouseup = e => {
                        window.onmouseup = null;
                        window.onmousemove = null;
                        intents.storeHeights(heights);
                    };
                    window.onmousemove = e => {
                        const ypos = e.pageY;
                        const abovepercent = height * Math.max(Math.min(ypos, bottom - 44) - top, 37) / (bottom - top);
                        if (index === 1) {
                            aboveel.style.height = abovepercent + '%';
                        } else {
                            aboveel.style.height = 'calc(' + abovepercent + '% - 6px)';
                        }
                        paneel.style.height = 'calc(' + (height - abovepercent) + '% - 6px)';
                        heights[index - 1] = abovepercent;
                        heights[index] = height - abovepercent;
                        resize(index -  1);
                        resize(index);
                    };
                };
            }
            const paneel = addel(frag, 'div', {className: 'ide-pane'});
            prev = paneel;
            if (index > 0) {
                paneel.style.height = 'calc(' + pane.height + '% - 6px';
            } else {
                paneel.style.height = pane.height + '%';
            }
            paneel.appendChild(this.subpanes(pane.subpanes, index, intents));
        }
        return {'ide-middle': frag};
    }

    modal(model, intents) {
        if (model.modal) {
            return {'ide-modal': modal(model.modal, intents.modal)};
        } else {
            return {'ide-modal': undefined};
        }
    }

    display(repr) {
        for (const r in repr) {
            if (!repr.hasOwnProperty(r)) {
                continue;
            }
            const el = this._els[r];
            el.innerHTML = '';
            if (repr[r] !== undefined) {
                el.appendChild(repr[r]);
            }
        }
    }
}

const state = {};

state.representation = (model, view, intents) => {
    let repr = view.treetabs(model, intents);
    if (model.selectedTreeTab === 'Files') {
        repr = Object.assign({}, repr, view.files(model, intents));
    } else if (model.selectedTreeTab === 'Examples') {
        repr = Object.assign({}, repr, view.examples(model, intents));
    } else if (model.selectedTreeTab === 'Resources') {
        repr = Object.assign({}, repr, view.resources(model, intents));
    } else {
        repr['ide-tree-content'] = undefined;
    }

    let updatepane = false;
    let updatesubpane = false;
    for (const pane of model.panes) {
        updatepane = updatepane || pane.update;
        for (const subpane of pane.subpanes) {
            updatesubpane = updatesubpane || subpane.update;
        }
    }
    if (model.updatePanes || updatepane || updatesubpane) {
        repr = Object.assign({}, repr, view.panes(model, intents));
    }

    if (model.updateModal) {
        repr = Object.assign({}, repr, view.modal(model, intents));
    }

    view.display(repr);
};

state.nextAction = (model, intents) => {
    for (const pane of model.panes) {
        for (const subpane of pane.subpanes) {
            if (subpane.update && subpane.selected && subpane.selected.plugin) {
                subpane.selected.plugin.init();
            }
        }
    }
};

state.render = (model, view, intents) => {
    state.representation(model, view, intents);
    state.nextAction(model, intents);
};

const _findInTree = (children, path) => {
    const found = children.find(item => item.name === path[0]);
    if (found !== undefined) {
        if ((path.length === 1) || ((path.length === 2) && !path[1])) {
            return found;
        }
        if (found.children) {
            return _findInTree(found.children, path.slice(1));
        }
    }
    return undefined;
};

const findInTree = (tree, path) => {
    if (path.length === 1 && path[0] === '') {
        return tree;
    }
    return _findInTree(tree.children, path);
};

class Model {
    constructor() {
        this.selectedTreeTab = 'Files';
        this.panes = [
            {
                update: true,
                height: 60,
                subpanes: [{selected: null, width: 100, commons: {}, tabs: []}],
            },
            {
                update: true,
                height: 40,
                subpanes: [{selected: null, width: 100, commons: {}, tabs: []}],
            },
        ];
    }

    iterTabs(func) {
        for (const pane of this.panes) {
            for (const subpane of pane.subpanes) {
                for (const tab of subpane.tabs) {
                    const result = func(tab);
                    if (!result) {
                        return result;
                    }
                }
            }
        }
    }

    _findTab(id, func) {
        for (const pane of this.panes) {
            for (const subpane of pane.subpanes) {
                for (const tab of subpane.tabs) {
                    if (tab.id === id) {
                        return func(pane, subpane, tab);
                    }
                }
            }
        }
        return null;
    }

    findTab(id) {
        return this._findTab(id, (pane, subpane, tab) => tab);
    }

    _createTab(tab, index) {
        const paneindex = index.pane >= 0 ? index.pane : this.panes.length - 1;
        const subpaneindex = index.subpane >= 0 ?
            index.subpane : this.panes[paneindex].subpanes.length - 1;
        const subpane = this.panes[paneindex].subpanes[subpaneindex];
        subpane.tabs.push(tab);
        subpane.selected = tab;
        subpane.update = true;
        if (tab.plugin && tab.plugin.getCommonsType) {
            // Set up common for plugin
            const type = tab.plugin.getCommonsType();
            if (type in subpane.commons) {
                tab.plugin.initCommons(subpane.commons[type]);
            } else {
                subpane.commons[type] = tab.plugin.initCommons();
            }
        }
    }

    _deleteTab(id) {
        for (const pane of this.panes) {
            for (const subpane of pane.subpanes) {
                const length = subpane.tabs.length;
                for (let index = 0; index < length; index++) {
                    if (subpane.tabs[index].id === id) {
                        const tab = subpane.tabs.splice(index, 1);
                        if (tab[0] === subpane.selected) {
                            if (subpane.tabs.length === 0) {
                                subpane.selected = null;
                            } else if (index < subpane.tabs.length) {
                                subpane.selected = subpane.tabs[index];
                            } else {
                                subpane.selected = subpane.tabs[index - 1];
                            }
                        }
                        subpane.update = true;
                        return tab[0];
                    }
                }
            }
        }
    }

    present(data, view, intents) {
        this.updateModal = false;
        for (const pane of this.panes) {
            pane.update = false;
            for (const subpane of pane.subpanes) {
                subpane.update = false;
            }
        }
        this.updatePanes = false;

        for (const id in data) {
            if (!data.hasOwnProperty(id)) {
                continue;
            }

            const payload = data[id];
            if (id === 'selectTreeTab') {
                this.selectedTreeTab = payload;
            } else if (id === 'tree') {
                this.tree = payload;
            } else if (id === 'updateTreeBranch') {
                let branch = null;
                const parent = findInTree(this.tree, payload.parent.split('/'));
                if (parent !== undefined) {
                    if (parent.children === undefined) {
                        parent.children = [];
                    }
                    branch = parent.children;
                }
                if (branch !== null) {
                    const newTree = [];
                    for (const item of payload.branch) {
                        let old;
                        for (const index in branch) {
                            if (branch[index].name === item.name) {
                                old = branch.splice(index, 1)[0];
                                break;
                            }
                        }
                        if (old) {
                            newTree.push(old);
                        } else {
                            newTree.push(item)
                        }
                    }
                    branch.length = 0;
                    branch.push(...newTree);
                } else {
                    console.log('Failed to locate branch: ' + payload.parent);
                }
            } else if (id === 'addTreeBranch') {
                const parent = findInTree(this.tree, payload.parent.split('/'));
                if (parent !== undefined) {
                    parent.children = payload.branch;
                }
            } else if (id === 'delTreeBranch') {
                const parent = findInTree(this.tree, payload.split('/'));
                if (parent !== undefined) {
                    parent.children = undefined;
                }
            } else if (id === 'examples') {
                this.examples = payload;
            } else if (id === 'addExamplesBranch') {
                const parent = findInTree(this.examples, payload.parent.split('/'));
                if (parent) {
                    parent.children = payload.branch;
                }
            } else if (id === 'delExamplesBranch') {
                const parent = findInTree(this.examples, payload.split('/'));
                if (parent) {
                    parent.children = undefined;
                }
            } else if (id === 'resources') {
                this.resources = payload;
            } else if (id === 'setTabSaveState') {
                this._findTab(payload.id, (pane, subpane, tab) => {
                    tab.saved = payload.saved;
                    subpane.update = true;
                });
            } else if (id === 'initPanes') {
                this.updatePanes = true;
            } else if (id === 'splitSubPane') {
                let symmetric = false;
                const pane = this.panes[payload.pane];
                const length = pane.subpanes.length;
                if (length >= 2) {
                    symmetric = true;
                    for (let i = 0; i < length - 1; i++) {
                        if (pane.subpanes[i].width !== pane.subpanes[i + 1].width) {
                            symmetric = false;
                            break;
                        }
                    }
                }
                if (symmetric) {
                    for (const subpane of pane.subpanes) {
                        subpane.width = 100 / (length + 1);
                    }
                } else {
                    pane.subpanes[payload.subpane].width /= 2;
                }
                pane.subpanes.splice(payload.subpane + 1, 0, {
                    update: true,
                    selected: null,
                    width: pane.subpanes[payload.subpane].width,
                    commons: {},
                    tabs: [],
                });
            } else if (id === 'deleteSubPane') {
                const subpanes = this.panes[payload.pane].subpanes;
                if (subpanes.length > 1) {
                    const old = subpanes.splice(payload.subpane, 1);
                    const move2index = payload.subpane < 1 ? 0 : payload.subpane - 1;
                    subpanes[move2index].tabs.push(...old[0].tabs);
                    subpanes[move2index].width += old[0].width;
                    if (old[0].selected) {
                        subpanes[move2index].selected = old[0].selected;
                    }
                    subpanes[move2index].update = true;
                    this.updatePanes = true;
                }
            } else if (id === 'storeHeights') {
                if (this.panes.length === payload.length) {
                    for (let i = 0; i < payload.length; i++) {
                        this.panes[i].height = payload[i];
                    }
                }
            } else if (id === 'storeWidths') {
                if (this.panes[payload.index].subpanes.length === payload.widths.length) {
                    for (let i = 0; i < payload.widths.length; i++) {
                        this.panes[payload.index].subpanes[i].width = payload.widths[i];
                    }
                }
            } else if (id === 'createTab') {
                this._createTab(payload, payload.index);
            } else if (id === 'setTabPlugin') {
                this._findTab(payload.id, (pane, subpane, tab) => {
                    tab.plugin = payload.plugin;
                    subpane.update = true;
                });
            } else if (id === 'selectTab') {
                this._findTab(payload, (pane, subpane, tab) => {
                    subpane.selected = tab;
                    subpane.update = true;
                });
            } else if (id === 'deleteTab') {
                this._deleteTab(payload);
            } else if (id === 'moveTab') {
                const tab = this._deleteTab(payload.fromid);
                this._createTab(tab, payload.toindex);
            } else if (id === 'modal') {
                this.modal = payload;
                this.updateModal = true;
            } else {
                console.log('IDE: Unknown model present id: ' + id);
            }
        }
        state.render(this, view, intents);
    }
}

class Actions {
    constructor(view, wsname, stack, services) {
        this.view = view;
        this.wsname = wsname;
        this.services = services;
        this.conCounter = 0;
        this.model = new Model();
        this.ws = new Workspace(wsname, stack);
        this.ws.on('error', e => console.log(e));
        this.ws.on('resource', this._onresource.bind(this));
        this.intents = {
            treetabs: {
                select: name => this.present({selectTreeTab: name}),
            },
            files: {
                openFile: this.openFile.bind(this),
                expandDir: this.expandDir.bind(this),
                collapseDir: this.collapseDir.bind(this),
                contextmenu: this.fileContextMenu.bind(this),
                uploadFiles: this.uploadFiles.bind(this),
                dropFile: this.dropFile.bind(this),
            },
            examples: {
                openFile: this.openFile.bind(this),
                expandDir: this.expandExample.bind(this),
                collapseDir: fileinfo => this.present({delExamplesBranch: fileinfo.path}),
                contextmenu: this.exampleContextMenu.bind(this),
            },
            resources: {
                openFile: this.openResource.bind(this),
                contextmenu: this.resourceContextMenu.bind(this),
            },
            paneTabs: {
                select: this.selectTab.bind(this),
                close: this.closeTab.bind(this),
                pluss: this.newTab.bind(this),
                contextmenu: this.tabContextMenu.bind(this),
                moveTab: this.moveTab.bind(this),
            },
            modal: {
                close: () => this.closeModal(),
                upload: (file, path) => {
                    this.closeModal();
                    if (file) {
                        this.uploadFiles([file], path);
                    }
                },
            },
            storeWidths: (index, widths) => this.present({storeWidths: {index: index, widths: widths}}),
            storeHeights: widths => this.present({storeHeights: widths}),
        };

        //state.render(this.model, this.intents);
        const wsroot = FileInfo.WorkspaceFileInfo();
        const exroot = FileInfo.ExamplesFileInfo();
        this.present({
            initPanes: true,
            tree: {name: '', type: 'dir', id: wsroot.id, info: wsroot},
            examples: {name: '', type: 'dir', id: exroot.id, info: exroot},
        });
        this.expandDir(wsroot);
        this.expandExample(exroot);
    }

    present(data) {
        this.model.present(data, this.view, this.intents);
    }

    reinit() {
        this.present({});
    }

    destroy() {
        if (this.ws) {
            this.ws.destroy();
        }
    }

    async expandDir(fileinfo) {
        const branch = await this.ws.listDir(fileinfo.path);
        this.present({addTreeBranch: {parent: fileinfo.path, branch: branch}});
    }

    collapseDir(fileinfo) {
        this.present({delTreeBranch: fileinfo.path});
    }

    _createEditorPlugin(fileinfo) {
        const plugin = new Editor(fileinfo, this.services, this.ws);
        plugin.on('savechange', saved => {
            this.present({setTabSaveState: {id: fileinfo.id, saved: saved}});
        });
        plugin.on('command', this.command.bind(this));
        return plugin;
    }

    openFile(fileinfo) {
        const found = this.model.findTab(fileinfo.id);
        if (found) {
            this.present({selectTab: fileinfo.id});
            return;
        }
        const ext = fileinfo.path.split('.').pop().toLowerCase();
        let plugin;
        let writeable = false;
        if ((ext === 'png') || (ext === 'jpg') || (ext === 'jpeg') || (ext === 'gif') || (ext === 'svg')) {
            const url = this.ws.getURL(fileinfo);
            plugin = new ImageViewer(url);
        } else {
            writeable = true;
            plugin = this._createEditorPlugin(fileinfo);
        }
        const name = fileinfo.path.split('/').pop();
        this.present({createTab: {name: name, index: {pane: 0, subpane: 0}, id: fileinfo.id, info: fileinfo, plugin: plugin, writeable: writeable, saved: true}});
    }

    async newFile(parentfileinfo, index) {
        const name = prompt('New file name:');
        if (name) {
            const fileinfo = FileInfo.WorkspaceFileInfo(parentfileinfo.path + name);
            const hasfile = await this.ws.hasFile(fileinfo);
            if (hasfile) {
                this.services.status("File " + name + " already exists!", 30);
                return;
            }
            const plugin = this._createEditorPlugin(fileinfo);
            await this.ws.saveFile(fileinfo, '');
            if (!index) {
                index = {pane: 0, subpane: 0};
            }
            this.present({createTab: {name: name, id: fileinfo.id, info: fileinfo, index: index, plugin: plugin, writeable: true, saved: false}});
            await this.refreshPath(parentfileinfo.path);
        }
    }

    async newDir(fileinfo) {
        const name = prompt('New folder name:');
        if (name) {
            const newfi = new FileInfo(fileinfo.repo, fileinfo.path + name);
            await this.ws.mkdir(newfi);
            await this.refreshPath(fileinfo.path);
        }
    }

    async refreshPath(path) {
        if ((path !== '') && !path.endsWith('/')) {
            path += '/';
        }
        const branch = await this.ws.listDir(path);
        this.present({updateTreeBranch: {parent: path, branch: branch}});
    }

    async renameFile(fileinfo) {
        const dest = prompt('Rename: ' + fileinfo.path, fileinfo.path);
        if (dest) {
            if (await this.ws.hasFile(FileInfo.WorkspaceFileInfo(dest))) {
                if (!confirm('File "' + dest + '" already exists!\n\nDo you want to overwrite?')) {
                    return;
                }
            }
            await this.ws.renameFile(fileinfo, dest);
            let fullpath = fileinfo.path;
            if (fullpath.endsWith('/')) {
                fullpath = fullpath.slice(0, -1);
            }
            const parent = Actions._getPathParent(fullpath);
            await this.refreshPath(parent);
        }
    }

    async deleteFile(fileinfo) {
        if (confirm('Are you sure you want to delete ' + fileinfo.path + ' ?')) {
            await this.ws.deleteFile(fileinfo);
            let fullpath = fileinfo.path;
            if (fullpath.endsWith('/')) {
                fullpath = fullpath.slice(0, -1);
            }
            const parent = fullpath.substring(0, fullpath.lastIndexOf('/'));
            await this.refreshPath(parent);
        }
    }

    openOutput(index) {
        const name = 'Output';
        const found = this.model.findTab(name);
        if (found) {
            this.selectTab(name);
            return Promise.resolve();
        }
        const plugin = import(/* webpackChunkName: "output" */ './output')
            .then(outterm => {
                this.present({setTabPlugin: {id: name, plugin: new outterm.default(name, this.ws)}});
            });
        if (!index) {
            index = {pane: -1, subpane: 0}; // Default to bottom pane
        }
        this.present({createTab: {name: name, id: name, index: index}});
        return plugin;
    }

    static _getPathParent(fullpath) {
        let parent = fullpath.substring(0, fullpath.lastIndexOf('/'));
        if (parent !== '') {
            parent += '/';
        }
        return parent;
    }

    async command(cmd, fileinfo) {
        if (cmd.resource === 'gdb') {
            const gdb = await this.openDebugger();
            gdb.command(cmd.params);
            return;
        }
        await this.openOutput();
        const tab = this.model.findTab(fileinfo.id);
        if (tab && tab.writable && !tab.saved && tab.plugin && tab.plugin.save) {
            tab.plugin.save();
        }
        await this.ws.remoteCommand(cmd.name, fileinfo);
        if (fileinfo.repo === 'workspace') {
            const parent = Actions._getPathParent(fileinfo.path);
            this.refreshPath(parent);
        } else {
            this.refreshPath('');
        }
    }

    fileContextMenu(event, type, fileinfo) {
        const dirfi = fileinfo.getDir();
        const menu = [];
        if (fileinfo.path) {
            if (type === "file") {
                menu.push({name: "Open", intent: () => this.openFile(fileinfo)});
                const name = fileinfo.path.substr(fileinfo.path.lastIndexOf('/') + 1);
                const url = this.ws.getURL(fileinfo);
                menu.push({name: '<a href="' + url + '" download="' + name + '">Download</a>', intent: () => {}});
                } else {
                menu.push({name: "Open Dir", intent: () => this.expandDir(fileinfo)});
            }
            if (fileinfo.path.startsWith("www")) {
                const wwwress = this.ws.getResourcesByClass('www');
                if (wwwress.length > 0) {
                    const url = this.ws.getUserShareURL(fileinfo.path);
                    menu.push({name: "Open User Share URL", intent: () => this.openBrowser(url)});
                }
            }
            for (const cmd of fileinfo.commands) {
                menu.push({name: cmd.name, intent: () => this.command(cmd, fileinfo)});
            }
            menu.push({name: "--"});
        }
        menu.push({name: "Refresh", intent: () => this.refreshPath(fileinfo.path)});
        if (fileinfo.path) {
            menu.push({name: "Rename...", intent: () => this.renameFile(fileinfo)});
            menu.push({name: "Delete...", intent: () => this.deleteFile(fileinfo)});
        }
        menu.push({name: "--"});
        menu.push({name: "Upload File...", intent: () => this.openFileDialog(fileinfo.path)});
        menu.push({name: "New File...", intent: () => this.newFile(dirfi)});
        menu.push({name: "New Folder...", intent: () => this.newDir(dirfi)});
        this.services.menu.on(event.clientX, event.clientY, menu);
    }

    async uploadFiles(files, path) {
        for (const f of files) {
            const fileinfo = FileInfo.WorkspaceFileInfo(path + f.name);
            if (await this.ws.hasFile(fileinfo)) {
                if (!confirm('File "' + fullpath.path + '" already exists!\n\nDo you want to overwrite?')) {
                    continue;
                }
            }
            const fr = new FileReader();
            fr.onload = async e => {
                await this.ws.saveFile(fileinfo, e.currentTarget.result);
                this.refreshPath(path);
            };
            fr.readAsArrayBuffer(f);
        }
    }

    async dropFile(absfrompath, toinfo) {
        const baseurl = this.ws.getBaseURL();
        if (absfrompath.startsWith(baseurl)) {
            // Same workspace; move file
            const fullpath = absfrompath.slice(baseurl.length);
            let destfile = toinfo.path;
            if (fullpath.endsWith('/')) {
                destfile += fullpath.slice(0, -1).split('/').pop();
            } else {
                destfile += fullpath.split('/').pop();
            }
            if (destfile === (fullpath.endsWith('/') ? fullpath.slice(0, -1) : fullpath)) {
                // Same file; skip
                return;
            }
            const destinfo = FileInfo.WorkspaceFileInfo(destfile)
            if (await this.ws.hasFile(destinfo)) {
                if (!confirm('File "' + destfile + '" already exists!\n\nDo you want to overwrite?')) {
                    return;
                }
            }
            const srcinfo = FileInfo.WorkspaceFileInfo(fullpath);
            await this.ws.renameFile(srcinfo, destfile);
            await this.refreshPath(Actions._getPathParent(fullpath.endsWith('/') ? fullpath.slice(0, -1) : fullpath));
        } else {
            // Other workspace; copy file
            const data = await fetch(absfrompath, {
                credentials: 'include',
                headers: {'cache-control': 'no-cache'}
            })
                .then(response => {
                    if (response.ok) {
                        return response.blob();
                    }
                    throw new Error('Failed to load file: ' + absfrompath);
                });
            const destinfo = FileInfo.WorkspaceFileInfo(topath + absfrompath.split('/').pop());
            if (await this.ws.hasFile(destinfo)) {
                if (!confirm('File "' + destfile + '" already exists!\n\nDo you want to overwrite?')) {
                    return;
                }
            }
            await this.ws.saveFile(destinfo, data);
        }
        await this.refreshPath(toinfo.path);
    }

    openFileDialog(path) {
        this.present({modal: {type: 'file', path: path}});
    }

    closeModal() {
        this.present({modal: undefined});
    }

    tabContextMenu(event, id) {
        const menu = [];
        const found = this.model.findTab(id);
        if (found) {
            if (found.plugin && found.plugin.save && found.plugin.writeable) {
                menu.push({name: "Save", intent: () => found.plugin.save()});
            }
            menu.push({name: "Close", intent: () => this.closeTab(id)});
        }
        this.services.menu.on(event.clientX, event.clientY, menu);
    }

    async expandExample(example) {
        const branch = await this.ws.listExamples(example.path);
        this.present({addExamplesBranch: {parent: example.path, branch: branch}});
    }

    exampleContextMenu(event, type, exinfo) {
        const menu = [];
        if (exinfo.path) {
            if (type === "file") {
                menu.push({name: "Open", intent: () => this.openFile(exinfo)});
                const name = exinfo.path.substr(exinfo.path.lastIndexOf('/') + 1);
                const url = this.ws.getURL(exinfo);
                menu.push({name: '<a href="' + url + '" download="' + name + '">Download</a>', intent: () => {}});
            } else {
                menu.push({name: "Open Dir", intent: () => this.expandExample(exinfo)});
            }
            for (const cmd of exinfo.commands) {
                menu.push({name: cmd.name, intent: () => this.command(cmd, exinfo)});
            }
            this.services.menu.on(event.clientX, event.clientY, menu);
        }
    }

    _onresource(resources) {
        this.present({resources: resources});
    }

    openResource(resource) {
        if (resource.class === 'ttn') {
            this.openTTN();
        } else if (resource.class === 'pty') {
            this.newTerminal();
        } else if (resource.class === 'www') {
            const url = this.ws.getUserShareURL('www/');
            this.openBrowser(url);
        } else if (resource.class === 'serial') {
            this.openSerial(resource);
        } else if (resource.class === 'remoteexec') {
            this.openRemote(resource.boardid, resource.tunnelid);
        } else if (resource.class === 'gdb') {
            this.openDebugger();
        }
    }

    resourceContextMenu(event, type, resource) {
        const menu = [];
        if (resource) {
            menu.push({name: 'Open', intent: () => this.openResource(resource)});
            if (resource.class === 'www') {
                const url = this.ws.getUserShareURL('www/');
                menu.push({name: 'Open User Share URL', intent: () => this.openBrowser(url)});
            }
            this.services.menu.on(event.clientX, event.clientY, menu);
        }
    }

    selectTab(id) {
        this.present({selectTab: id});
    }

    closeTab(id) {
        const tab = this.model.findTab(id);
        if (tab && tab.plugin) {
            if (tab.writeable && !tab.saved) {
                const dosave = confirm('There are unsaved changes in "' + tab.info.path + '"\n\nDo you want to save them?');
                if (dosave) {
                    tab.plugin.save();
                }
            }
            tab.plugin.cleanup();
        }
        this.present({deleteTab: id});
    }

    moveTab(fromid, toindex) {
        this.present({moveTab: {fromid: fromid, toindex: toindex}});
    }

    splitSubPane(index) {
        this.present({splitSubPane: index});
    }

    deleteSubPane(index) {
        this.present({deleteSubPane: index});
    }

    newTab(e, index) {
        const menu = [];
        if (index.pane === 0) {
            menu.push({name: "New File ...", intent: () => this.newFile(FileInfo.WorkspaceFileInfo(), index)});
        }
        if (index.pane !== 0 || !(index.pane === 0 && index.subpane === 0)) {
            menu.push({name: "New Bash Terminal", intent: () => this.newTerminal(index)});
            menu.push({name: "Open Command Ouput", intent: () => this.openOutput(index)});
            if (this.ws.getResourcesByClass('ttn').length > 0) {
                menu.push({name: "Open The Things Network Tool", intent: () => this.openTTN(index)});
            }
            menu.push({name: "Open Video", intent: () => this.openJanus(index)});
            const serialResources = this.ws.getResourcesByClass('serial');
            if (serialResources.length > 0) {
                menu.push({name: "Open Serial Port", disable: !serialResources[0].online,
                    intent: () => this.openSerial(serialResources[0], index)
                });
            }
            menu.push({name: "Open Remote", disable: false,
                intent: () => this.openRemote("tunid", "", index)
            });
        }
        menu.push({name: "--"});
        menu.push({name: "Split", intent: () => this.splitSubPane(index)});
        menu.push({name: "Close Split", intent: () => this.deleteSubPane(index)});
        const pos = e.target.getBoundingClientRect();
        this.services.menu.on(pos.left, pos.top + pos.height, menu);
    }

    newTerminal(index) {
        this.conCounter++;
        const name = 'bash' + this.conCounter;
        import(/* webpackChunkName: "terminal" */ './terminal')
            .then(xterm => {
                this.present({setTabPlugin: {id: name, plugin: new xterm.default(name, this.ws)}});
            });
        if (!index) {
            index = {pane: -1, subpane: 0}; // Default to bottom pane
        }
        this.present({createTab: {name: name, id: name, index: index}});
    }

    openSerial(resource, index) {
        const name = 'Serial';
        const found = this.model.findTab(name);
        if (found) {
            found.plugin.reconnect(id);
            this.selectTab(name);
            return Promise.resolve();
        }
        const ser = import(/* webpackChunkName: "serterm" */ './serterm')
            .then(sterm => {
                this.present({setTabPlugin: {id: name, plugin: new sterm.default(name, resource, this.ws)}});
            });
        if (!index) {
            index = {pane: -1, subpane: 0}; // Default to bottom pane
        }
        this.present({createTab: {name: name, id: name, index: index}});
        return ser;
    }

    openDebugger(index) {
        const name = 'Debugger';
        const found = this.model.findTab(name);
        if (found) {
            this.selectTab(name);
            return Promise.resolve(found.plugin);
        }
        const plugin = import(/* webpackChunkName: "debugger" */ './debugger')
            .then(dterm => {
                const plugin = new dterm.default(name, this.ws, {highlightSource: this.highlightSource.bind(this)});
                this.present({setTabPlugin: {id: name, plugin: plugin}});
                return plugin;
            });
        if (!index) {
            index = {pane: -1, subpane: 0}; // Default to bottom pane
        }
        this.present({createTab: {name: name, id: name, index: index}});
        return plugin;
    }

    highlightSource(fileinfo, line) {
        this.openFile(fileinfo);
        const found = this.model.findTab(fileinfo.id);
        this.model.iterTabs(tab => {
            if (tab.plugin && tab.plugin.highlightClear && tab != found) {
                tab.plugin.highlightClear();
            }
            return true;
        });
        if (found) {
            found.plugin.highlightLine(line);
        }
    }

    openRemote(boardid, tunnelid, index) {
        const name = 'Remote ' + boardid;
        const found = this.model.findTab(name);
        if (found) {
            this.selectTab(name);
            return Promise.resolve();
        }
        const remote = import(/* webpackChunkName: "remote" */ './remote')
            .then(rterm => {
                this.present({setTabPlugin: {id: name, plugin: new rterm.default(name, boardid, tunnelid, this.ws)}});
            });
        if (!index) {
            index = {pane: -1, subpane: 0}; // Default to bottom pane
        }
        this.present({createTab: {name: name, id: name, index: index}});
        return remote;
    }

    openTTN(index) {
        const name = 'TTN';
        const found = this.model.findTab(name);
        if (found) {
            this.selectTab(name);
            return Promise.resolve();
        }
        const plugin = import(/* webpackChunkName: "ttn" */ './ttn')
            .then(ttn => {
                this.present({setTabPlugin: {id: name, plugin: new ttn.default(this.ws, {openHelp: this.openHelp.bind(this)})}});
            });
        if (!index) {
            index = {pane: -1, subpane: 0}; // Default to bottom pane
        }
        this.present({createTab: {name: name, id: name, index: index}});
        return plugin;
    }

    openHelp(id, title, content) {
        const found = this.model.findTab(id);
        if (found) {
            this.present({selectTab: id});
            return;
        }
        const plugin = new HelpViewer(content);
        this.present({createTab: {name: title, index: {pane: 0, subpane: -1}, id: id, plugin: plugin}});
    }

    openBrowser(url) {
        const plugin = new Browser(url);
        this.present({createTab: {name: 'usershare', index: {pane: 0, subpane: -1}, id: url, plugin: plugin}});
    }

    openJanus(index) {
        const name = 'Video';
        const found = this.model.findTab(name);
        if (found) {
            this.selectTab(name);
            return Promise.resolve();
        }
        const plugin = import(/* webpackChunkName: "janus" */ './janus')
            .then(janus => {
                this.present({setTabPlugin: {id: name, plugin: new janus.default()}});
            });
        if (!index) {
            index = {pane: -1, subpane: 0}; // Default to bottom pane
        }
        this.present({createTab: {name: name, id: name, index: index}});
        return plugin;
    }

    assignBoard(id) {
        this.openSerial(id);
    }

    resize() {
        if (this.model.selectedEditor) {
            this.model.selectedEditor.plugin.resize();
        }
        for (const pane of this.model.panes) {
            for (const subpane of pane.subpanes) {
                if (subpane.selected && subpane.selected.plugin) {
                    subpane.selected.plugin.resize();
                }
            }
        }
    }
}

export class Ide {
    constructor(name, stack, services) {
        this.name = name;
        this.stack = stack;
        this.services = services;
        this.view = new View();
        this.root = this.view.root;
    }

    init() {
        const lsizer = document.getElementById('ide-left-sizer');
        lsizer.onmousedown = e => {
            e.preventDefault();
            const tree = document.getElementById('ide-tree');
            const treeXpos = tree.getBoundingClientRect().left;
            const middle = document.getElementById('ide-middle');
            window.onmouseup = e => {
                window.onmouseup = null;
                window.onmousemove = null;
            };
            window.onmousemove = e => {
                const width = Math.max(Math.min(e.pageX, window.innerWidth - 20) - treeXpos, 22);
                lsizer.style.left = width - 4 + 'px';
                tree.style.width = width + 'px';
                middle.style.left = width + 'px';
                this.actions.resize();
            };
        };

        if (this.actions) {
            this.actions.reinit();
        } else {
            this.actions = new Actions(this.view, this.name, this.stack, this.services);
        }
    }

    destroy() {
        if (this.actions) {
            this.actions.destroy();
            delete this.actions;
        }
    }

    assignBoard(id) {
        this.actions.assignBoard(id);
    }

    resize() {
        if (this.actions) {
            this.actions.resize();
        }
    }
}
