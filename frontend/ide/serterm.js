"use strict";

import './serterm.css';
import { Terminal } from 'xterm';
import * as fit from 'xterm/lib/addons/fit/fit';
import 'xterm/dist/xterm.css';

Terminal.applyAddon(fit);

export default class {
    constructor(name, resource, comms) {
        this.name = name;
        this.tunnelid = resource.id;
        this.root = document.createElement('div');
        this.root.id = 'serterm';
        const bar = document.createElement('div');
        this.root.appendChild(bar);
        bar.id = 'serterm-bar';
        this.status = document.createElement('div');
        bar.appendChild(this.status);
        this.status.id = 'serterm-bar-status';
        this.status.classList.add('serterm-bar-item');
        this.status.innerHTML = "Connecting...";
        if (resource.capabilities && resource.capabilities.reset) {
            const reset = document.createElement('button');
            bar.appendChild(reset);
            reset.classList.add('serterm-bar-item');
            reset.innerHTML = "Reset Arduino";
            reset.onclick = () => {this._reset(); this.focus()};
        }
        const baudsel = document.createElement('select');
        bar.appendChild(baudsel);
        baudsel.classList.add('serterm-bar-item');
        for (const br of [9600, 19200, 38400, 57600, 115200]) {
            const opt = document.createElement('option');
            opt.value = br;
            opt.innerHTML = br;
            baudsel.appendChild(opt);
        }
        baudsel.onclick = e => {
            this._setBaudrate(e.target.value);
        };
        this.term = document.createElement('div');
        this.root.appendChild(this.term);
        this.term.id = "serterm-term";
        this.comms = comms;
    }

    _reset() {
        if (this.ser) {
            this.ser.DTR(0);
            this.ser.DTR(1);
        }
    }

    _setBaudrate(baudrate) {
        if (this.ser) {
            this.ser.setBaudrate(baudrate);
        }
    }

    _ondata(data) {
        this.ser.write(data);
    }

    _statusOnline() {
        this.status.innerHTML = 'Connected';
        this.status.style.color = 'black';
        this.status.style.backgroundColor = 'green';
    }

    _statusOffline() {
        this.status.innerHTML = 'Disconnected';
        this.status.style.color = 'black';
        this.status.style.backgroundColor = 'red';
    }

    _onstatus(status) {
        if (status === 'online') {
            this._statusOnline();
        } else if (status === 'offline') {
            this._statusOffline();
        } else {
            console.log('Unknown status ' + status);
        }
    }

    _connect() {
        if (this.ser) {
            return;
        }
        const ondata = this._ondata.bind(this);
        this.ser = this.comms.remoteSer(this.tunnelid,
            status => this._onstatus(status),
            data => this.xterm.write(data),
            error => {
                if (error) {
                    console.log('ser error: ' + error);
                }
                if (this.ser) {
                    delete this.ser;
                }
                // Release event callbacks
                this.xterm.off('data', ondata);
                this._statusOffline();
                this.xterm.write('\r\n\n<<< Serial console disconnected >>>\r\n');
                setTimeout(() => {if (this.ser) {this._connect()}}, 10000);
            }
        );
        this.ser.setBaudrate('9600');
        this._statusOnline();
        this.xterm.on('data', ondata);
        this._clear();
    }

    getRoot(visible) {
        this.root.style.display = visible ? 'block' : 'none';
        return this.root;
    }

    init() {
        if (this.xterm === undefined) {
            this.xterm = new Terminal();
            this._connect();
            this.xterm.open(this.term);
        }
        this.resize();
        this.focus();
    }

    cleanup() {
        if (this.ser) {
            this.ser.stop();
            delete this.ser;
        }
    }

    focus() {
        this.xterm.focus();
    }

    resize() {
        this.xterm.fit();
    }

    _clear() {
        if (this.xterm) {
            this.xterm.clear();
        }
    }

    reconnect(tunnelid) {
        if (this.tunnelid !== tunnelid) {
            if (this.ser) {
                this.ser.stop();
                delete this.ser;
            }
            this.tunnelid = tunnelid;
        }
        this._connect();
    }
}
