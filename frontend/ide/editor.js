"use strict";

import './editor.css';
import loading from '../components/loading';

class EditorCommons {
    constructor(comms, services) {
        this.comms = comms;
        this.services = services;
        this.root = document.createElement('div');
        this.root.id = 'editor';
        this.root.oncontextmenu = e => {
            if (this.menufunc) {
                this.menufunc(e);
            }
        };
        this.root.appendChild(loading('Loading editor'));
    }

    getRoot(visible) {
        this.root.style.display = visible ? 'block' : 'none';
        return this.root;
    }

    async _init() {
        if (this._instance === undefined) {
            this._instance = await import(/* webpackChunkName: "brace" */ 'brace');
            await Promise.all([
                import(/* webpackChunkName: "brace/theme/clouds" */ 'brace/theme/clouds'),
                import(/* webpackChunkName: "brace/ext/searchbox" */ 'brace/ext/searchbox'),
            ]);
            this.Range = this._instance.acequire('ace/range').Range;
            this.editor = this._instance.edit(this.root);
            this.editor.$blockScrolling = Infinity;
            this.editor.setTheme('ace/theme/clouds');
            this.editor.setFontSize('15px');
            this.editor.renderer.setShowGutter(true);
            this.editor.on("guttermousedown", e => {
	            const target = e.domEvent.target;

	            if (target.className.indexOf("ace_gutter-cell") === -1) {
	                return;
                }

                if (!this.editor.isFocused()) {
	                return;
	            }

	            if (e.clientX > 25 + target.getBoundingClientRect().left) {
	                return;
	            }
	            const row = e.getDocumentPosition().row;
	            const breakpointsArray = e.editor.session.getBreakpoints();
	            if (!(row in breakpointsArray)) {
                    if (this.comms.addBreakpoint(this.fileinfo, row + 1)) {
                        e.stop();
                    }
                } else {
                    if (this.comms.removeBreakpoint(this.fileinfo, row + 1)) {
                        e.stop();
                    }
                }
            });
            this.editor.commands.addCommand({
                name: 'save', bindKey: {win: 'Ctrl-S', mac: 'Cmd-S'}, exec: () => {
                    if (this.savefunc) {
                        this.savefunc();
                    }
                }
            });
        }
    }

    async createEditSession(fullpath, data) {
        await this._init();
        const session = this._instance.createEditSession(data);
        const filename = fullpath.split('/').pop();
        const ext = filename.split('.').pop().toLowerCase();
        if (filename === "Makefile") {
            import(/* webpackChunkName: "brace/mode/makefile" */ 'brace/mode/makefile')
                .then(() => session.setMode("ace/mode/makefile"));
        } else if (ext === 'py') {
            import(/* webpackChunkName: "brace/mode/python" */ 'brace/mode/python')
                .then(() => session.setMode("ace/mode/python"));
        } else if (ext === 'js') {
            import(/* webpackChunkName: "brace/mode/javascript" */ 'brace/mode/javascript')
                .then(() => session.setMode("ace/mode/javascript"));
        } else if (ext === 'html') {
            import(/* webpackChunkName: "brace/mode/html" */ 'brace/mode/html')
                .then(() => session.setMode("ace/mode/html"));
        } else if (ext === 'json') {
            import(/* webpackChunkName: "brace/mode/json" */ 'brace/mode/json')
                .then(() => session.setMode("ace/mode/json"));
        } else if (ext === 'md') {
            import(/* webpackChunkName: "brace/mode/markdown" */ 'brace/mode/markdown')
                .then(() => session.setMode("ace/mode/markdown"));
        } else if (ext === 'yaml') {
            import(/* webpackChunkName: "brace/mode/yaml" */ 'brace/mode/yaml')
                .then(() => session.setMode("ace/mode/yaml"));
        } else if (ext === 'xml') {
            import(/* webpackChunkName: "brace/mode/xml" */ 'brace/mode/xml')
                .then(() => session.setMode("ace/mode/xml"));
        } else if (ext === 'v') {
            import(/* webpackChunkName: "brace/mode/verilog" */ 'brace/mode/verilog')
                .then(() => session.setMode("ace/mode/verilog"));
        } else {
            import(/* webpackChunkName: "brace/mode/c_cpp" */ 'brace/mode/c_cpp')
                .then(() => session.setMode("ace/mode/c_cpp"));
        }
        return session;
    }

    async setSession(session, fileinfo, savefunc, menufunc) {
        this.fileinfo = fileinfo;
        this.savefunc = savefunc;
        this.menufunc = menufunc;
        await this._init();
        this.editor.setSession(session);
        this.editor.setReadOnly(!fileinfo.writeable);
    }

    focus() {
        if (this.editor) {
            this.editor.focus();
        }
    }

    resize() {
        if (this.editor) {
            this.editor.resize();
        }
    }
}

export default class {
    constructor(fileinfo, services, comms) {
        this.fileinfo = fileinfo;
        this.services = services;
        this.comms = comms;
        this.saved = true;
    }

    getCommonsType() {
        return 'brace';
    }

    initCommons(commons) {
        this.commons = commons;
        if (!this.commons) {
            this.commons = new EditorCommons(this.comms, this.services);
        }
        return this.commons;
    }

    async init() {
        if (this.session === undefined) {
            const data = await this.comms.loadFile(this.fileinfo);
            this.session = await this.commons.createEditSession(this.fileinfo.path, data);
            this.session.on('change', e => {
                this._setSaved(false);
            });
            this.session.on('changeBreakpoint', () => {
                const rows = this.session.getBreakpoints().reduce((a, e, i) => {
                    if (e) {
                        a.push(i);
                    }
                    return a;
                }, []);
                console.log('changebrkpt: ' + this.fileinfo + " " + rows);
            });
            this.comms.addBreakpointListener(this.fileinfo, this._setBreakpoints.bind(this));
        }
        this.commons.setSession(this.session, this.fileinfo, this.save.bind(this), this.menu.bind(this));
        this.focus();
        if (this.markerline !== undefined) {
            this.highlightLine(this.markerline);
            delete this.markerline;
        }
    }

    on(id, func) {
        if (id === 'savechange') {
            this.savechange = func;
        } else if (id === 'command') {
            this.command = func;
        } else {
            throw Error('Unknown event type: ' + id);
        }
    }

    _setSaved(saved) {
        if (this.saved !== saved) {
            this.saved = saved;
            if (this.savechange) {
                this.savechange(saved);
            }
        }
    }

    _setBreakpoints(lines) {
        const rows = [];
        for (const l of lines) {
            rows.push(l - 1);
        }
        this.session.setBreakpoints(rows);
    }

    save() {
        if (this.fileinfo.writeable) {
            this.comms.saveFile(this.fileinfo, this.session.getValue())
                .then(() => {
                    this._setSaved(true);
                    console.log('Successfully saved file ' + this.fileinfo.path);
                })
                .catch(e => console.log(e))
        }
    }

    menu(event) {
        event.preventDefault();
        const menu = [];
        if (this.fileinfo.writeable) {
            menu.push({name: "Save", intent: () => this.save()});
        }
        if (this.fileinfo.command) {
            for (const cmd of this.fileinfo.commands) {
                menu.push({name: cmd, intent: () => this.command(cmd, this.fileinfo.path)});
            }
        }
        if (menu) {
            this.commons.services.menu.on(event.clientX, event.clientY, menu);
        }
    }

    highlightLine(line) {
        if (this.session) {
            this.highlightClear();
            const r = new this.commons.Range(line - 1, 0, line - 1, 1);
            console.log("highlighting line: " + line + "   " + JSON.stringify(r));
            this.markerID = this.session.addMarker(r, 'debugger-highlight', 'fullLine', true);
            // Needs to run a bit later when called just after opening file
            setTimeout(() => this.commons.editor.scrollToLine(line, true, true, null), 0);
        } else {
            // Pre session init
            this.markerline = line;
        }
    }

    highlightClear() {
        if (this.markerID !== undefined) {
            this.session.removeMarker(this.markerID);
            delete this.markerID;
        }
        if (this.markerline) {
            delete this.markerline;
        }
    }

    cleanup() {
        this.comms.removeBreakpointListener(this.fileinfo, this._setBreakpoints.bind(this));
    }

    focus() {
        this.commons.focus();
    }

    resize() {
        this.commons.resize();
    }
}
