"use strict";

export default class {
    constructor(url) {
        this.root = document.createElement('div');
        this.root.style.overflow = 'auto';
        this.root.style.height = '100%';
        const img = document.createElement('img');
        this.root.appendChild(img);
        img.src = url;
    }

    getRoot() {
        return this.root;
    }

    async init() {
    }

    cleanup() {
    }

    focus() {
    }

    resize() {
    }
}
