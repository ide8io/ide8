"use strict";

import './modal.css';

export default function(modal, intents) {
    const mback = document.createElement('div');
    mback.classList.add('modal');
    mback.onclick = e => {
        // Limit to own element
        if (e.target === mback) {
            intents.close();
        }
    };
    const mcont = document.createElement('div');
    mback.appendChild(mcont);
    mcont.classList.add('modal-content');
    const close = document.createElement('span');
    mcont.appendChild(close);
    close.classList.add('modal-close');
    close.innerHTML = '&times;';
    close.onclick = () => intents.close();
    const form = document.createElement('form');
    mcont.appendChild(form);
    const label = document.createElement('label');
    form.appendChild(label);
    label.innerHTML = 'Select file to upload:';
    const file = document.createElement('input');
    form.appendChild(file);
    file.type = 'file';
    const upload = document.createElement('button');
    form.appendChild(upload);
    upload.type = 'submit';
    upload.innerHTML = 'Upload';
    upload.onclick = e => {
        e.preventDefault();
        intents.upload(file.files[0], modal.path);
    };
    return mback;
}
