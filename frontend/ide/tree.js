"use strict";

import './tree.css';
import fileicon from '../img/fileicon.svg';
import foldericon from '../img/foldericon.svg';
import openfoldericon from '../img/openfoldericon.svg';
import loading from '../components/loading';

export default function(root, intents) {
    const treediv = document.createElement('div');
    treediv.classList.add('tree');
    if (root.children === undefined) {
        const l = loading("Fetching files ...");
        l.style.height = '40px';
        treediv.appendChild(l);
        return treediv;
    }
    if (intents.uploadFiles && intents.dropFile) {
        treediv.ondragover = e => {
            e.preventDefault();
            if (e.dataTransfer.types.includes('Files')) {
                // External drag source
                e.dataTransfer.dropEffect = 'copy';
            } else {
                e.dataTransfer.dropEffect = 'move';
            }
        };
        treediv.ondrop = e => {
            e.preventDefault();
            if (e.dataTransfer.files.length) {
                intents.uploadFiles(e.dataTransfer.files, '');
            } else {
                const uri = e.dataTransfer.getData('text/uri-list');
                intents.dropFile(uri, '');
            }
        };
    }
    if (intents.contextmenu) {
        treediv.oncontextmenu = e => {
            e.preventDefault();
            intents.contextmenu(e, root.type, root.info);
        }
    }
    const append = (arr, indent=0) => {
        for (const node of arr) {
            const div = document.createElement('div');
            div.ondragstart = e => {
                e.dataTransfer.setData('text/uri-list', node.abspath);
            };
            if (intents.uploadFiles && intents.dropFile) {
                div.ondragover = e => {
                    e.preventDefault();
                    e.stopPropagation();
                    if (node.type === 'file') {
                        e.dataTransfer.dropEffect = 'none';
                    } else if (e.dataTransfer.types.includes('Files')) {
                        // External drag source
                        e.dataTransfer.dropEffect = 'copy';
                    } else {
                        e.dataTransfer.dropEffect = 'move';
                    }
                };
                div.ondrop = e => {
                    e.preventDefault();
                    e.stopPropagation();
                    if (node.type === 'file') {
                    } else if (e.dataTransfer.files.length) {
                        intents.uploadFiles(e.dataTransfer.files, node.info.path);
                    } else {
                        const uri = e.dataTransfer.getData('text/uri-list');
                        intents.dropFile(uri, node.info);
                    }
                };
            }
            div.draggable = true;
            const flag = document.createElement('span');
            div.appendChild(flag);
            const icon = document.createElement('span');
            div.appendChild(icon);
            const name = document.createElement('span');
            div.appendChild(name);
            treediv.appendChild(div);
            if (node.type === 'file') {
                icon.innerHTML = fileicon;
                div.ondblclick = e => intents.openFile(node.info);
            } else if (!node.children) {
                flag.innerHTML = "&#9656;";
                flag.style.fontSize = '15px';
                flag.onclick = e => {
                    e.stopPropagation();
                    intents.expandDir(node.info)
                };
                icon.innerHTML = foldericon;
                div.ondblclick = e => intents.expandDir(node.info);
            } else {
                flag.innerHTML = "&#9660;";
                flag.style.fontSize = '12px';
                flag.onclick = e => {
                    e.stopPropagation();
                    intents.collapseDir(node.info);
                };
                icon.innerHTML = openfoldericon;
                div.ondblclick = e => intents.collapseDir(node.info);
            }
            name.innerHTML = node.name;
            if (intents.select) {
                div.onclick = () => intents.select(node.info);
            }
            if (intents.contextmenu) {
                div.oncontextmenu = e => {
                    e.preventDefault();
                    e.stopPropagation();
                    intents.contextmenu(e, node.type, node.info);
                };
            }
            div.style.paddingLeft = indent * 20 + 'px';
            if (node.children) {
                append(node.children, indent + 1);
            }
        }
    };
    append(root.children);
    return treediv;
}
