"use strict";

export default class {
    constructor(content) {
        this.root = document.createElement('div');
        this.root.style.overflow = 'auto';
        this.root.style.height = '100%';
        this.root.style.margin = '5px';
        this.root.appendChild(content);
    }

    getRoot(visible) {
        this.root.style.display = visible ? 'block' : 'none';
        return this.root;
    }

    async init() {
    }

    cleanup() {
    }

    focus() {
    }

    resize() {
    }
}
