"use strict";

import './ttn.css';
import ttnlogo from '../img/ttn-logo.png';
import ttnhelp from './ttnhelp';
import pagenav from '../components/pagenav';
import { mkel, toHexString } from '../utils';
import loading from '../components/loading';
import base64js from 'base64-js';
import { LeftArrowIcon, RightArrowIcon, DBIcon } from "../components/icons";

const info = (app, device, intents) => {
    const frag = document.createDocumentFragment();

    const appdiv = mkel('div', {}, frag);
    mkel('span', {innerHTML: 'Application ID'}, appdiv);
    const appspan = mkel('span', {innerHTML: app ? app : ""}, appdiv);
    if (app) {
        const butt = mkel('button', {innerHTML: '&#10005;'}, appspan);
        butt.onclick = () => intents.clearApp();
    }

    const devdiv = mkel('div', {}, frag);
    mkel('span', {innerHTML: 'Device ID'}, devdiv);
    if (device && device.dev_id) {
        const devspan = mkel('span', {innerHTML: device.dev_id}, devdiv);
        const butt = mkel('button', {innerHTML: '&#10005;'}, devspan);
        butt.onclick = () => intents.clearDevice();

        const deveuidiv = mkel('div', {}, frag);
        mkel('span', {innerHTML: 'Device EUI'}, deveuidiv);
        mkel('span', {innerHTML: device.dev_eui}, deveuidiv);
        const appeuidiv = mkel('div', {}, frag);
        mkel('span', {innerHTML: 'Application EUI'}, appeuidiv);
        mkel('span', {innerHTML: device.app_eui}, appeuidiv);
        const appkeydiv = mkel('div', {}, frag);
        mkel('span', {innerHTML: 'Application Key'}, appkeydiv);
        mkel('span', {innerHTML: device.app_key}, appkeydiv);
    }

    return frag;
};

const selectApp = (app, error, connecting, intents) => {
    if (connecting) {
        return loading('Connecting to TTN application ...');
    }
    const form = mkel('form', {className: 'ttn-select-app'});
    form.onsubmit = evt => {
        evt.preventDefault();
        intents.connectToApp(appinput.value.trim(), keyinput.value.trim());
    };
    const divinfo = mkel('div', {}, form);
    divinfo.innerHTML = 'Please enter your application details from The Things Network:';
    const divappinput = mkel('div', {}, form);
    const appinput = mkel('input', {placeholder: 'Enter Application ID', value: app}, divappinput);
    const divkeyinput =  mkel('div', {}, form);
    const keyinput = mkel('input', {placeholder: 'Enter Application Access Key'}, divkeyinput);
    if (error) {
        mkel('div', {className: 'ttn-select-app-error', innerHTML: error}, form);
    }
    mkel('input', {type: 'submit', value: 'Connect to TTN Application'}, form);
    const footer = mkel('div', {}, form);
    footer.innerHTML = 'Application details can be found or created on ' +
        '<a href="https://console.thethingsnetwork.org/applications" target="_blank">The Things Network Console</a>.';
    return form;
};


const selectDevices = (devices, limit, offset, error, intents) => {
    if (devices === undefined) {
        return loading('Retrieving device list ...');
    }
    const root = document.createElement('div');
    root.classList.add('ttn-dev');
    if (error) {
        const errmsg = document.createElement('div');
        errmsg.classList.add('ttn-dev-error');
        root.appendChild(errmsg);
        errmsg.innerHTML = error;
    }
    const list = document.createElement('div');
    root.appendChild(list);
    list.classList.add('ttn-dev-list');
    const hdr = document.createElement('div');
    list.appendChild(hdr);
    hdr.innerHTML = 'Registered Devices:';
    hdr.appendChild(pagenav(devices ? devices.length : 0, limit, offset, intents.getDeviceList));
    const devlist = document.createElement('div');
    list.appendChild(devlist);
    devlist.classList.add('ttn-dev-list-devs');
    if (devices) {
        for (const dev of devices) {
            const divdev = document.createElement('div');
            devlist.appendChild(divdev);
            const devid = document.createElement('span');
            divdev.appendChild(devid);
            devid.innerHTML = dev.dev_id;
            const desc = document.createElement('span');
            divdev.appendChild(desc);
            desc.innerHTML = dev.description ? dev.description : '';
            const eui = document.createElement('span');
            divdev.appendChild(eui);
            eui.innerHTML = dev.dev_eui;
            divdev.onclick = () => intents.selectDevice(dev.dev_id);
        }
    }
    const addform = document.createElement('form');
    root.appendChild(addform);
    addform.classList.add('ttn-dev-add');
    const addhdr = document.createElement('div');
    addform.appendChild(addhdr);
    addhdr.innerHTML = 'Register New Device:';
    const divid = document.createElement('div');
    addform.appendChild(divid);
    divid.innerHTML = 'Device ID:';
    const addid = document.createElement('input');
    divid.appendChild(addid);
    addid.classList.add('ttn-dev-add-id');
    const diveui = document.createElement('div');
    addform.appendChild(diveui);
    diveui.innerHTML = 'Device EUI';
    const addeui = document.createElement('input');
    diveui.appendChild(addeui);
    addeui.classList.add('ttn-dev-add-eui');
    const submit = document.createElement('input');
    addform.appendChild(submit);
    submit.type = 'submit';
    submit.value = 'Register';
    addform.onsubmit = evt => {
        evt.preventDefault();
        intents.registerDevice(addid.value);
    };
    return root;
};

const device = (app, device, data, error, intents) => {
    const div = mkel('div', {className: 'ttn-device'});
    if (error) {
        mkel('div', {className: 'ttn-device-error', innerHTML: error}, div);
    }

    mkel('div', {innerHTML: 'The Things Network'}, div);
    mkel('div', {}, div);
    const webif = mkel('div', {innerHTML: 'Web Interface'}, div);
    const butt = mkel('button', {innerHTML: 'Open Help'}, webif);
    butt.onclick = () => intents.openHelp();

    const divdl = mkel('div', {className: 'ttn-device-downlink'}, div);
    mkel('div', {innerHTML: 'Downlink:'}, divdl);
    mkel('div', {innerHTML: `<a href="https://console.thethingsnetwork.org/applications/${app}/devices/${device.dev_id}" target="_blank">Open TTN Console</a>`}, divdl);

    mkel('div', {className: 'ttn-device-db', innerHTML: LeftArrowIcon}, div);

    const wwwdl = mkel('div', {className: 'ttn-device-www'}, div);
    mkel('div', {innerHTML: '<a href="data" target="_blank">TTN Downlink POST URL</a>'}, wwwdl);

    const divul = mkel('div', {className: 'ttn-device-uplink'}, div);
    mkel('div', {innerHTML: 'Uplink:'}, divul);
    const list = mkel('div', {}, divul);
    if (data !== undefined) {
        for (const item of data) {
            const divitem = mkel('div', {className: 'ttn-device-uplink-item'}, list);
            mkel('span', {innerHTML: new Date(Date.parse(item.metadata.time)).toLocaleTimeString()}, divitem);
            mkel('span', {innerHTML: item.counter}, divitem);
            mkel('span', {innerHTML: item.port}, divitem);
            const payload = mkel('span', {}, divitem);
            mkel('span', {innerHTML: 'payload: '}, payload);
            mkel('span', {innerHTML: toHexString(base64js.toByteArray(item.payload_raw), ' ').toUpperCase()}, payload);
            if (item.payload_fields) {
                const fields = mkel('span', {}, payload);
                for (const field in item.payload_fields) {
                    mkel('span', {innerHTML: ' ' + field + ': '}, fields);
                    mkel('span', {innerHTML: item.payload_fields[field]}, fields);
                }
            }
        }
    }

    const db = mkel('div', {className: 'ttn-device-db'}, div);
    db.innerHTML = RightArrowIcon + DBIcon + RightArrowIcon;

    const wwwul = mkel('div', {className: 'ttn-device-www'}, div);
    mkel('div', {innerHTML: '<a href="data" target="_blank">DB Read URL</a>'}, wwwul);
    mkel('div', {innerHTML: '<a href="data" target="_blank">WebSocket Listen URL</a>'}, wwwul);

    // Scroll to bottom after DOM insertion
    setTimeout(() => list.scrollTop = list.scrollHeight, 0);
    return div;
};

class View {
    constructor() {
        this._els = {};
        this.root = mkel('div', {className: 'ttn-root'});
        const hdr = mkel('div', {className: 'ttn-header'}, this.root);
        const info = mkel('div', {className: 'ttn-info'}, hdr);
        this._els['info'] = info;
        const divlogo = mkel('div', {className: 'ttn-logo'}, hdr);
        const alogo = mkel('a', {href: "https://www.thethingsnetwork.org", target: "_blank"}, divlogo);
        mkel('img', {src: ttnlogo}, alogo);
        this._els['content'] = mkel('div', {className: 'ttn-content'}, this.root);
    }

    getRoot(visible) {
        this.root.style.display = visible ? 'flex' : 'none';
        return this.root;
    }

    static loading(message) {
        return {content: loading(message)};
    }

    static info(model, intents) {
        return {info: info(model.application, model.device, intents)};
    }

    static selectApp(model, intents) {
        return {content: selectApp(model.application, model.error, model.connecting, intents)};
    }

    static selectDevices(model, intents) {
        return {content: selectDevices(model.devices, model.limit, model.offset, model.error, intents)};
    }

    static device(model, intents) {
        return {content: device(model.application, model.device, model.devdata, model.error, intents)};
    }

    display(repr) {
        for (const r in repr) {
            if (!repr.hasOwnProperty(r)) {
                continue;
            }
            const el = this._els[r];
            el.innerHTML = '';
            if (repr[r] !== undefined) {
                el.appendChild(repr[r]);
            }
        }
    }
}


const state = {
    representation(model, view, intents) {
        let repr = view.constructor.info(model, intents);
        if (model.connecting === undefined) {
            repr = Object.assign({}, repr, view.constructor.loading('Connecting to backend ...'));
        }
        else if (!model.connectionOk) {
            repr = Object.assign({}, repr, view.constructor.selectApp(model, intents));
        } else if (model.device && model.device.dev_id) {
            repr = Object.assign({}, repr, view.constructor.device(model, intents));
        } else {
            repr = Object.assign({}, repr, view.constructor.selectDevices(model, intents));
        }
        view.display(repr);
    },

    nextAction(model, view, intents) {
        if (model.connectionOk && !(model.device && model.device.dev_id) && !model.devices) {
            intents.getDeviceList(10, 0);
        }
    },

    render(model, view, intents) {
        state.representation(model, view, intents);
        state.nextAction(model, view, intents);
    },

};


class Model {
    constructor() {
    }

    present(data, view, intents) {
        for (const id in data) {
            if (!data.hasOwnProperty(id)) {
                continue;
            }

            const payload = data[id];
            if (id === 'setApp') {
                this.application = payload;
            } else if (id === 'setError') {
                this.error = payload;
            } else if (id === 'setConnecting') {
                this.connecting = payload;
            } else if (id === 'setConnectionOk') {
                this.connectionOk = payload;
            } else if (id === 'setDevices') {
                this.devices = payload.devices;
                this.limit = payload.limit;
                this.offset = payload.offset;
            } else if (id === 'setDev') {
                this.device = payload;
            } else if (id === 'addDevData') {
                if (!this.devdata) {
                    this.devdata = [];
                }
                this.devdata.push(payload);
            } else if (id === 'clearDevData') {
                delete(this.devdata);
            } else {
                console.log('ttn: Unhandled model present id: ' + id);
            }
        }

        state.render(this, view, intents)
    }
}


class Actions {
    constructor(view, comms, services) {
        this.view = view;
        this.services = services;
        this.model = new Model();
        this.intents = {
            connectToApp: this.connectToApp.bind(this),
            selectDevice: this.selectDevice.bind(this),
            clearApp: this.clearApp.bind(this),
            clearDevice: this.clearDevice.bind(this),
            getDeviceList: this.getDeviceList.bind(this),
            registerDevice: this.registerDevice.bind(this),
            openHelp: this.openHelp.bind(this),
        };
        this.present({});
        this.rttn = comms.remoteTTN(
            this._onstatus.bind(this),
            this._onuplink.bind(this),
            this._ondata.bind(this),
            this._ondone.bind(this)
        );
    }

    cleanup() {
        this.rttn.stop();
        delete this.rttn;
    }

    async _onstatus(ok, appID, device, error) {
        console.log('ttn onstatus: ' + ok + ' appID=' + appID + ' device=' + JSON.stringify(device) + ' error=' + error);
        this.present({setConnecting: false, setConnectionOk: ok, setApp: appID, setDev: device, setError: error});
    }

    async getDeviceList(limit, offset) {
        const data = await this.rttn.getDevices(limit, offset);
        if (data.err_msg) {
            console.log("getDeviceList failed: " + data.err_msg);
            this.present({setError: data.err_msg});
            return;
        }
        console.log("devs:" + JSON.stringify(data.devices));
        this.present({setDevices: {devices: data.devices, limit: data.limit, offset: data.offset}});
    }

    _onuplink(msg) {
        console.log('ttn uplink: ' + JSON.stringify(msg));
        this.present({'addDevData': msg});
    }

    _ondata(type, data) {
        console.log('ttn ondata: ' + type + ': ' + JSON.stringify(data));
    }

    _ondone(error) {
        console.log('ttn ondone: ' + error);
    }

    present(data) {
        this.model.present(data, this.view, this.intents);
    }

    connectToApp(app, accesskey) {
        console.log("connectToApp: " + app + " " + accesskey);
        this.present({setConnecting: true, setApp: app});
        this.rttn.connect(app, accesskey);
    }

    clearApp() {
        this.clearDevData();
        this.connectToApp("", "");
    }

    selectDevice(devId) {
        console.log('selectDevice: ' + devId);
        this.rttn.selectDevice(devId);
    }

    clearDevice() {
        this.clearDevData();
        this.selectDevice("");
    }

    clearDevData() {
        this.present({'clearDevData': true});
    }

    registerDevice(devID) {
        console.log('register device: ' + devID);
    }

    openHelp() {
        this.services.openHelp('ttn:help', 'TTN Web Frontend Help', ttnhelp());
    }
}


export default class {
    constructor(comms, services) {
        this.view = new View();
        this.comms = comms;
        this.services = services;
    }

    getRoot(visible) {
        return this.view.getRoot(visible);
    }

    init() {
        if (!this.actions) {
            this.actions = new Actions(this.view, this.comms, this.services);
        }
    }

    cleanup() {
        this.actions.cleanup();
    }

    focus() {
    }

    resize() {
    }
}
