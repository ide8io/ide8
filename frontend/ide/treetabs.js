"use strict";

import './treetabs.css';

export default function(tabs, selected, intents) {
    const frag = document.createDocumentFragment();
    for (const t of tabs) {
        const tab = document.createElement('span');
        if (t === selected) {
            tab.classList.add('tree-tabs-tab-selected');
        } else {
            tab.classList.add('tree-tabs-tab');
        }
        tab.onclick = e => intents.select(t);
        tab.innerHTML = t;
        frag.appendChild(tab);
    }
    return frag;
}
