"use strict";

import './browser.css';
import { mkel } from '../utils';
import { reloadicon, ExternalIcon } from '../components/icons';

const browse = (url, intents) => {
    const frag = document.createDocumentFragment();
    const reload = mkel('span', {innerHTML: reloadicon}, frag);
    reload.onclick = () => intents.reload(url);
    mkel('span', {innerHTML: url}, frag);
    mkel('a', {href: url, innerHTML: ExternalIcon, target: '_blank'}, frag);
    return frag;
};

const iframe = (url, intents) => {
    const iframe = mkel('iframe', {src: url});
    iframe.onload = () => {
        intents.title(iframe.contentDocument.title);
        iframe.contentDocument.addEventListener('mousemove', e => {
            if (window.onmousemove) {
                const pos = iframe.getBoundingClientRect();
                const me = {pageX: e.pageX + pos.left, pageY: e.pageY + pos.top};
                window.onmousemove(me);
            }
        });
    };
    return iframe;
};

class View {
    constructor() {
        this._els = {};
        this.root = mkel('div', {className: 'browser'});
        this._els['hdr'] = mkel('div', {}, this.root);
        this._els['content'] = mkel('div', {}, this.root);
    }

    static browse(model, intents) {
        return {hdr: browse(model.url, intents), content: iframe(model.url, intents)};
    }

    display(repr) {
        for (const r in repr) {
            if (!repr.hasOwnProperty(r)) {
                continue;
            }
            const el = this._els[r];
            el.innerHTML = '';
            if (repr[r] !== undefined) {
                el.appendChild(repr[r]);
            }
        }
    }
}

const state = {
    representation(model, view, intents) {
        view.display(view.constructor.browse(model, intents));
    },

    nextAction(model, view, intents) {
    },

    render(model, view, intents) {
        state.representation(model, view, intents);
        state.nextAction(model, view, intents);
    },

};

class Model {
    constructor() {
    }

    present(data, view, intents) {
        for (const id in data) {
            if (!data.hasOwnProperty(id)) {
                continue;
            }

            const payload = data[id];
            if (id === 'url') {
                this.url = payload;
            } else {
                console.log('browser: Unhandled model present id: ' + id);
            }
        }

        state.render(this, view, intents)
    }
}

class Actions {
    constructor(url, view) {
        this.model = new Model();
        this.view = view;
        this.intents = {
            reload: this.reload.bind(this),
            title: this.title.bind(this),
        };
        this.present({url: url});
    }

    present(data) {
        this.model.present(data, this.view, this.intents);
    }

    reload(url) {
        this.present({url: url});
    }

    title(title) {
    }
}

export default class {
    constructor(url) {
        this.url = url;
        this.view = new View();
    }

    getRoot(visible) {
        if (visible && this.view.root.style.display !== 'flex') {
            this.view.root.style.display = 'flex';
        } else if (!visible && this.view.root.style.display !== 'none') {
            this.view.root.style.display = 'none';
        }
        return this.view.root;
    }

    async init() {
        if (this.actions === undefined) {
            this.actions = new Actions(this.url, this.view);
        }
    }

    cleanup() {
    }

    focus() {
    }

    resize() {
    }
}
