"use strict";

import './output.css';
import { Terminal } from 'xterm';
import * as fit from 'xterm/lib/addons/fit/fit';
import 'xterm/dist/xterm.css';

Terminal.applyAddon(fit);

export default class {
    constructor(name, comms) {
        this.name = name;
        this.root = document.createElement('div');
        this.root.id = 'output';
        const bar = document.createElement('div');
        this.root.appendChild(bar);
        bar.id = 'output-bar';
        const reset = document.createElement('button');
        bar.appendChild(reset);
        reset.innerHTML = "Clear";
        reset.onclick = () => this._clear();
        this.term = document.createElement('div');
        this.root.appendChild(this.term);
        this.term.id = "output-term";
        this.comms = comms;
    }

    _clear() {
        if (this.xterm) {
            this.xterm.clear();
        }
    }

    getRoot(visible) {
        this.root.style.display = visible ? 'block' : 'none';
        return this.root;
    }

    init() {
        if (this.xterm === undefined) {
            this.xterm = new Terminal();
            this.output = this.comms.listenOutput(data => this.xterm.write(data));
            this.xterm.open(this.term);
        }
        this.resize();
        this.focus();
    }

    cleanup() {
        if (this.ser) {
            this.ser.stop();
            delete this.ser;
        }
    }

    focus() {
        this.xterm.focus();
    }

    resize() {
        this.xterm.fit();
    }
}
