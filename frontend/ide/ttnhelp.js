"use strict";

import { mkel } from '../utils';

const help = `
<h2>Overview</h2>
Is sets up a subscription to TTN for selected device and stores these in a database. This database can be queried over
a REST interface or subscribed to for push over websocket.
<h2>Database</h2>
<h3>Read</h3>
<b>URL</b>: <code>db/read</code><br>
<br>
<b>Returns</b>: json object<br>
<br>
<b>URL Options</b>:
<ul>
<li>limit: How many items to read</li>
<li>offset: Offset</li>
</ul>
<h3>Listen (websocket)</h3>
<b>URL</b>: <code>db/listen</code><br>
<br>
<b>Returns</b>: json object<br>
<br>
<b>URL Options</b>:
<ul>
<li>count: Number of stored elements to return on connect</li>
</ul>
`;

export default function() {
    return mkel('div', {innerHTML: help});
}
