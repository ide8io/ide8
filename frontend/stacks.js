
import './stacks.css';

export default (stacks, intents) => {
    const div = document.createElement('div');
    div.classList.add('stacks');
    div.onclick = () => intents.close();
    div.oncontextmenu = () => intents.close();
    const closeShadow = document.createElement('div');
    closeShadow.classList.add('stacks-close');
    closeShadow.classList.add('stacks-close-shadow');
    div.appendChild(closeShadow);
    let count = 0;
    for (const stack of stacks) {
        const item = document.createElement('div');
        item.classList.add('stacks-item');
        item.style.top = 35 + Math.floor(count / 5) * 170 + 'px';
        item.style.left = 55 + (count % 5) * 205 + 'px';
        const logo = document.createElement('img');
        logo.classList.add('stacks-item-logo');
        logo.src = stack.logo;
        item.appendChild(logo);
        const title = document.createElement('div');
        title.classList.add('stacks-item-title');
        title.innerHTML = stack.name;
        item.appendChild(title);
        const ratings = document.createElement('div');
        ratings.classList.add('stacks-item-ratings');
        ratings.innerHTML = '&#9733;'.repeat(stack.rating.quality);
        if (stack.rating.difficulty > 8) {
            ratings.style.color = "black";
        } else if (stack.rating.difficulty > 5) {
            ratings.style.color = "red";
        } else if (stack.rating.difficulty > 3) {
            ratings.style.color = "blue";
        } else {
            ratings.style.color = "green";
        }
        item.appendChild(ratings);
        const info = document.createElement('div');
        info.classList.add('stacks-item-info');
        info.innerHTML = stack.info;
        item.appendChild(info);
        const skills = document.createElement('div');
        skills.classList.add('stacks-item-skills');
        for (const skill of stack.skills) {
            const skillsItem = document.createElement('div');
            const skillsDot = document.createElement('a');
            skillsDot.innerHTML = '●';
            skillsDot.style.color = skill.color;
            skillsItem.appendChild(skillsDot);
            skillsItem.appendChild(document.createTextNode(skill.name));
            skills.appendChild(skillsItem);
        }
        item.appendChild(skills);
        div.appendChild(item);
        item.onclick = () => intents.select(stack.id);
        count++;
    }
    const close = document.createElement('div');
    close.classList.add('stacks-close');
    close.innerHTML = 'X';
    close.onclick = () => intents.close();
    div.appendChild(close);
    return div;
};
