"use strict";

import nrf52Img from '../img/nrf52.png';
import arduinoImg from '../img/arduino.png';

const fixLogo = inst => {
    if (inst.type === 'nRF52 DK') {
        inst.logo = nrf52Img;
    } else if (inst.type === 'Arduino UNO') {
        inst.logo = arduinoImg;
    }
}

export function fetchDebuggerList(board) {
    let url = '/_boards/';
    if (board) {
        url += '?board=' + encodeURIComponent(board);
    }
    return fetch(url, {credentials: 'include', headers: {'cache-control': 'no-cache'}})
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('GET ' + url + ' failed with: ' + response.status + ' ' + response.statusText);
        })
        .then(ilist => {
            const retlist = [];
            for (const inst of ilist) {
                fixLogo(inst);
                retlist.push(inst);
            }
            return retlist;
        })
}

function getBoardsURL(id) {
    const idarr = id.split('/', 2);
    let url = '/_boards/' + encodeURIComponent(idarr[0]);
    if (idarr.length > 1) {
        url += '/' + encodeURIComponent(idarr[1]);
    }
    return url;
}

export function fetchMyBoards() {
    const url = '/_boards/?status=mine';
    return fetch(url, {credentials: 'include', headers: {'cache-control': 'no-cache'}})
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('GET ' + url + ' failed with: ' + response.status + ' ' + response.statusText);
        })
        .then(boards => {
            for (const board of boards) {
                fixLogo(board);
            }
            return boards;
        })
}

export function claimBoard(id, wsname) {
    return fetch(getBoardsURL(id) + '/workspace', {method: 'PUT', body: wsname, credentials: 'include'})
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('PUT ' + url + ' failed with: ' + response.status + ' ' + response.statusText);
        })
}

export function releaseBoard(id) {
    return fetch(getBoardsURL(id) + '/workspace', {method: 'DELETE', credentials: 'include'})
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('DELETE ' + url + ' failed with: ' + response.status + ' ' + response.statusText);
        })
}
