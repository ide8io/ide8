"use strict";

import { Websocket } from './websocket';
import { WebDAV } from '../comms/webdav';
import nrf52Img from '../img/nrf52.png';
import telenorImg from '../img/telenor.png';
import elsysImg from '../img/arduino.png';
import latticeImg from '../img/lattice.png';

const stacks = [
/*    {name: 'Telenor', logo: telenorImg, rating: {quality: 6, difficulty: 5},
        skills: [{name: 'C', color: 'red'}],
        info: 'Development environment for the EE02 board from Telenor.',
        boards: ['ee-02', 'nRF52DK']},*/
    {id: 'NordicSemi/nRF52/0.1', name: 'nRF52', logo: nrf52Img, rating: {quality: 2, difficulty: 6},
        skills: [{name: 'C', color: 'red'}, {name: 'Nordic SDK', color: 'red'}],
        info: 'Development environment for the nRF52832 chip from Nordic Semiconductors.',
        boards: ['nRF52 DK']},
    {id: 'Elsys/Elsys/0.1', name: 'Elsys', logo: elsysImg, rating: {quality: 6, difficulty: 1},
        skills: [{name: 'Arduino', color: 'green'}, {name: 'Wiring', color: 'green'}],
        info: 'NTNU Elsys development environment for Arduino UNO.',
        boards: ['Arduino UNO']},
    {id: 'Elsys/Elsys-TTN/0.1', name: 'Elsys TTN', logo: elsysImg, rating: {quality: 6, difficulty: 2},
        skills: [{name: 'Arduino', color: 'green'}, {name: 'Wiring', color: 'green'}, {name: 'LoRa', color: 'green'}],
        info: 'NTNU Elsys development environment for Arduino UNO with LoRa shield.',
        boards: ['Arduino UNO']},
    {id: 'IceStorm/IceStorm/0.1', name: 'IceStorm', logo: latticeImg, rating: {quality: 0, difficulty: 0},
        skills: [{name: 'Verilog', color: 'red'}],
        info: 'IceStorm development environment.',
        boards: ['Lattice']},
];

export class FileInfo {
    constructor(repo, path, writeable) {
        this.repo = repo;
        if (path === undefined) {
            path = "";
        }
        this.type = 'file';
        this.path = path;
        if (writeable === undefined) {
            if (repo === 'workspace') {
                writeable = true;
            }
        }
        this.writeable = writeable;
        this.commands = [];
        this.id = repo + ':' + path;
    }

    toString() {
        return this.id;
    }

    getDir() {
        let path = this.path.substring(0, this.path.lastIndexOf('/'));
        if (!path) {
            path = '';
        } else {
            path += '/';
        }
        return new FileInfo(this.repo, path);
    }

    static WorkspaceFileInfo(path) {
        return new FileInfo('workspace', path, true);
    }

    static ExamplesFileInfo(path) {
        return new FileInfo('examples', path, false);
    }
}

export function fetchStackList() {
    return new Promise(resolve => {
        resolve(stacks);
    })
}

export function getStack(name) {
    return new Promise(resolve => {
        const stack = stacks.find(s => s.id === name);
        if (stack !== undefined) {
            resolve(stack);
        } else {
            resolve();
        }
    })
}

export function fetchWorkspaceList() {
    return fetch('/_workspaces/', {credentials: 'include', headers: {'cache-control': 'no-cache'}})
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('GET /workspaces failed with: ' + response.status + ' ' + response.statusText);
        })
        .then(async wslist => {
            for (const ws of wslist) {
                if (ws.stack) {
                    const stack = await getStack(ws.stack);
                    if (stack) {
                        ws.logo = stack.logo;
                        ws.board = stack.boards[0];
                    }
                }
            }
            wslist.sort((a, b) => a.order - b.order);
            return wslist;
        })
}

export function createWorkspace(wsname, stack) {
    const data = JSON.stringify({name: wsname, stack: stack});
    return fetch('/_workspaces/', {method: 'POST', body: data, credentials: 'include'})
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Adding workspace failed with: ' + response.status + ' ' + response.statusText);
        })
}

export function deleteWorkspace(wsname) {
    return fetch('/_workspaces/' + wsname, {method: 'DELETE', credentials: 'include'})
        .then(response => {
            if (response.ok) {
                return true;
            }
            throw new Error('Adding workspace failed with: ' + response.status + ' ' + response.statusText);
        })
}

export function getWorkspace(wsname) {
    return fetch('/_workspaces/' + wsname, {credentials: 'include'})
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Connecting workspace failed with: ' + response.status + ' ' + response.statusText);
        })
}

export function startWorkspace(wsname, running) {
    const data = JSON.stringify(running);
    return fetch('/_workspaces/' + wsname + '/running', {method: 'PUT', body: data, credentials: 'include'})
        .then(response => {
            return response.text().then(text => ({text: text, ok: response.ok, status: response.status}));
        })
}

export class Workspace {
    constructor(wsname, stack) {
        this.wsname = wsname;
        this.stack = stack;
        this.env = [];
        this.resources = [];
        this.breakpointlisteners = {};
        this.pending = this._connect();
        this.fs = new WebDAV.Fs('');
    }

    destroy() {
        if (this.ws) {
            this.ws.destroy();
        }
    }

    on(event, callback) {
        switch (event) {
            case 'resource':
                this.onresourcecb = callback;
                break;
            case 'up':
                this.up = callback;
                break;
            case 'down':
                this.down = callback;
                break;
            case 'error':
                this.error = callback;
                break;
            default:
                throw new Error('Invalid Workspace event type: ' + event);
        }
    }

    _connect() {
        return getWorkspace(this.wsname)
            .then(async wsinfo => {
                console.log('connecting: ' + wsinfo.urls.websocket);
                this.wsinfo = wsinfo;
                this.ws = new Websocket(wsinfo.urls.websocket, this.env);
                await this.ws.isready();
                this.ws.listen(this._ondata.bind(this))
            })
            .catch(e => {
                if (this.error) {
                    this.error(e);
                }
            })
    }

    _ondata(type, payload) {
        if (type === 'resources:update') {
            payload.sort((a, b) => a.name.localeCompare(b.name));
            console.log('resources:update: ' + JSON.stringify(payload));
            this.resources = payload;
            if (this.onresourcecb) {
                this.onresourcecb(payload);
            }
        } else {
            console.log("Unhandled workspace ondata: " + type + ": " + JSON.stringify(payload));
        }
    }

    getResourcesByClass(cls) {
        const ress = [];
        for (const r of this.resources) {
            if (r.class === cls) {
                ress.push(r);
            }
        }
        return ress;
    }

    getResourcesByCapability(capability) {
        const ress = [];
        for (const r of this.resources) {
            for (const cap in r.capabilities) {
                if (cap === capability) {
                    ress.push(r.name);
                }
            }
        }
        return ress;
    }

    remoteCommand(cmd, fileinfo) {
        if (this.onOutput) {
            this.onOutput("\r\n=== Starting " + cmd + " ===\r\n");
        }
        return new Promise(resolve => {
            this.ws.remoteCommand(cmd, fileinfo.repo, fileinfo.path,
                data => {
                    if (this.onOutput) {
                        this.onOutput(data + "\r\n")
                    }
                },
                error => {
                    resolve(error);
                    if (this.onOutput) {
                        if (error) {
                            this.onOutput("\r\n\x1b[41m=== " + cmd + " failed with: " + error + " ===\x1b[0m\r\n");
                        } else {
                            this.onOutput("\r\n\x1b[42m=== " + cmd + " Completed Successfully ===\x1b[0m\r\n")
                        }
                    }
                });
        });
    }

    remotePty(ondata, ondone) {
        return this.ws.remotePty(ondata, ondone);
    }

    remoteSer(tunnelid, onstatus, ondata, ondone) {
        return this.ws.remoteSer(tunnelid, onstatus, ondata, ondone);
    }

    remoteTTN(onconnect, onuplink, ondata, ondone) {
        return this.ws.remoteTTN(onconnect, onuplink, ondata, ondone);
    }

    remotePlugin(boardid, tunnelid, onstatus, ondata, ondone) {
        return this.ws.remotePlugin(boardid, tunnelid, onstatus, ondata, ondone);
    }

    connectResource(name, meta, ondata, ondone) {
        return this.ws.connectResource(name, meta, ondata, ondone);
    }

    listenOutput(ondata) {
        this.onOutput = ondata;
    }

    getBaseURL() {
        return location + '/' + this.wsname + '/';
    }

    listDir(path) {
        return new Promise(async resolve => {
            if (this.pending) {
                // Make sure commands have been fetched before using them
                await this.pending;
                delete this.pending;
            }
            const url = this.wsinfo.urls.files + path;
            fetch(url, {credentials: 'include', headers: {'cache-control': 'no-cache'}})
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw new Error('GET ' + url + ' failed with: ' + response.status + ' ' + response.statusText);
                })
                .then(children => {
                    children.sort((a, b) => {
                        if (a.dir === b.dir) {
                            return a.name.localeCompare(b.name);
                        }
                        if (a.dir) {
                            return -1;
                        }
                        return 1;
                    });
                    const branch = [];
                    for (const child of children) {
                        let fullpath = path + child.name;
                        if (child.dir) {
                            fullpath += '/';
                        }
                        const abspath = this.getBaseURL() + fullpath;
                        const fileinfo = FileInfo.WorkspaceFileInfo(fullpath);
                        if (child.commands) {
                            fileinfo.commands = child.commands;
                        }
                        branch.push({name: child.name, type: child.type, id: fileinfo.id, info: fileinfo, abspath: abspath});
                    }
                    resolve(branch);
                });
        });
    }

    async listExamples(path) {
        if (this.pending) {
            // Make sure commands have been fetched before using them
            await this.pending;
            delete this.pending;
        }
        const url = this.wsinfo.urls.examples + path;
        return fetch(url, {credentials: 'include', headers: {'cache-control': 'no-cache'}})
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error('GET ' + url + ' failed with: ' + response.status + ' ' + response.statusText);
            })
            .then(async examples => {
                if (!examples) {
                    return [];
                }
                let prefix = "";
                if (path !== "") {
                    prefix = path + '/';
                }
                examples.sort((a, b) => {
                    if (a.type === b.type) {
                        return a.name.localeCompare(b.name);
                    }
                    if (a.type === 'dir' || a.type === 'collection') {
                        return -1;
                    }
                    return 1;
                });
                for (const ex of examples) {
                    const fileinfo = FileInfo.ExamplesFileInfo(prefix + ex.name);
                    if (ex.commands) {
                        fileinfo.commands = ex.commands;
                    }
                    ex.id = fileinfo.id;
                    ex.info = fileinfo;
                }
                return examples;
            })
    }

    mkdir(fileinfo) {
        return new Promise(resolve => {
            WebDAV.MKCOL(this.wsinfo.urls.dav + fileinfo.path, response => {
                resolve(response);
            })
        });
    }

    getURL(fileinfo) {
        if (fileinfo.repo === 'workspace') {
            return this.wsinfo.urls.files + fileinfo.path;
        } else if (fileinfo.repo === 'examples') {
            return this.wsinfo.urls.examples + fileinfo.path;
        } else if (fileinfo.repo === 'stack') {
            return this.wsinfo.urls.stack + fileinfo.path.replace(/^\//, '');
        } else {
            throw new Error('Unknown repository ' + fileinfo.repo);
        }
    }

    loadFile(fileinfo) {
        return fetch(this.getURL(fileinfo), {
            credentials: 'include',
            headers: {'cache-control': 'no-cache'}
        })
            .then(response => {
                if (response.ok) {
                    return response.text();
                }
                throw new Error('Failed to load file: ' + fileinfo);
            })
    }

    saveFile(fileinfo, data) {
        return fetch(this.wsinfo.urls.dav + fileinfo.path, {
            method: 'PUT',
            credentials: 'include',
            body: data,
        })
            .then(response => {
                if (response.ok) {
                    return response.text();
                }
                throw new Error('Failed to save file ' + fileinfo);
            })
    }

    deleteFile(fileinfo) {
        return new Promise(resolve => {
            WebDAV.DELETE(this.wsinfo.urls.dav + fileinfo.path, response => {
                resolve(response);
            })
        });
    }

    hasFile(fileinfo) {
        return fetch(this.getURL(fileinfo), {
            method: 'HEAD',
            credentials: 'include',
            headers: {'cache-control': 'no-cache'}
        })
            .then(response => response.status !== 404)
    }

    renameFile(fileinfo, dest) {
        return new Promise(resolve => {
            WebDAV.MOVE(this.wsinfo.urls.dav + fileinfo.path, this.wsinfo.urls.dav + dest, response => {
                resolve(response);
            })
        });
    }

    getUserShareURL(fullpath) {
        return this.wsinfo.urls.usershare + fullpath;
    }

    listProjects() {
        return fetch(this.wsinfo.urls.files + "../projects/", {
            credentials: 'include',
            headers: {'cache-control': 'no-cache'},
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error('Failed to fetch projects');
            })
    }

    addBreakpointListener(fileinfo, callback) {
        if (fileinfo in this.breakpointlisteners) {
            this.breakpointlisteners[fileinfo].push(callback);
        } else {
            this.breakpointlisteners[fileinfo] = [callback];
        }
    }

    removeBreakpointListener(fileinfo, callback) {
        const arr = this.breakpointlisteners[fileinfo];
        if (arr) {
            const cbi = arr.indexOf(callback);
            if (cbi > 0) {
                arr.splice(cbi, 1);
            }
        }
    }

    addBreakpoint(fileinfo, row) {
        if (this.breakpointhandler) {
            return this.breakpointhandler.add(fileinfo, row);
        }
        return false;
    }

    removeBreakpoint(fileinfo, row) {
        if (this.breakpointhandler) {
            return this.breakpointhandler.remove(fileinfo, row);
        }
        return false;
    }

    setBreakpointHandler(handler) {
        this.breakpointhandler = handler;
    }

    clearBreakpointHandler() {
        delete this.breakpointhandler;
    }

    publishBreakpoints(breakpoints) {
        for (const fileinfo in breakpoints) {
            if (breakpoints.hasOwnProperty(fileinfo)) {
                if (fileinfo in this.breakpointlisteners) {
                    const listeners = this.breakpointlisteners[fileinfo];
                    const rows = breakpoints[fileinfo];
                    for (const l of listeners) {
                        l(rows);
                    }
                }
            }
        }
        // Set for files with no breakpoints
        for (const fileinfo in this.breakpointlisteners) {
            if (!(fileinfo in breakpoints)) {
                const listeners = this.breakpointlisteners[fileinfo];
                for (const l of listeners) {
                    l([]);
                }
            }
        }
    }
}
