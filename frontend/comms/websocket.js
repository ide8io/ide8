
export class Websocket {
    constructor(url, env) {
        if (url.startsWith('ws://') || url.startsWith('wss://')) {
            this.url = url;
        } else {
            this.url = location.protocol.replace('http', 'ws') + '//' + location.host + url;
        }
        this.env = env;
        console.log("ws init");
        this.pending = [];
        this.channels = {};
        this.conn = null;
        this.ready = false;
        this.killswitch = false;
        this.openpromise = new Promise(this.connect.bind(this));
    }

    destroy() {
        this.killswitch = true;
        if (this.reconnecttimer) {
            clearTimeout(this.reconnecttimer);
        }
        if (this.conn) {
            this.conn.close();
        }
    }

    connect(resolve) {
        try {
            let envString = '';
            let firstEnv = true;
            for (const key in this.env) {
                envString += firstEnv ? '?' : '&';
                firstEnv = false;
                envString += key + '=' + this.env[key] + '&';
            }
            this.ready = false;
            this.conn = new WebSocket(this.url + envString);
            this.conn.onopen = () => {
                console.log("ws opened");
                this.ready = true;
                let p;
                while (p = this.pending.pop()) {
                    this.conn.send(p);
                }
                for (const id in this.channels) {
                    this.conn.send(JSON.stringify({type: "sync", id: id}))
                }
                if (resolve) {
                    resolve();
                }
            };
            this.conn.onclose = (evt) => {
                console.log("ws closed");
                delete this.conn;
                this.conn = null;
                this.ready = false;
                this._reconnect();
            };
            this.conn.onmessage = (evt) => {
                //console.log("ws: " + evt.data);
                const data = JSON.parse(evt.data);
                const ch = this.channels[data.id];
                if (ch) {
                    if (data.type === 'done') {
                        let error;
                        if (data.payload && data.payload.error) {
                            error = data.payload.error;
                        }
                        ch.ondone(error);
                        delete this.channels[data.id]
                    } else {
                        ch.ondata(data.type, data.payload);
                    }
                } else {
                    console.log('ws: Unhandled incoming data: ' + evt.data);
                }
            };
            this.conn.onerror = (evt) => {
                console.log("ws error: " + evt.data);
            };
        } catch (e) {
            this.conn = null;
            this.ready = false;
            console.log("ws connect exception: " + e);
            this._reconnect();
        }
    }

    _reconnect() {
        if (!this.killswitch) {
            this.reconnecttimer = setTimeout(() => {
                this.reconnecttimer = null;
                this.connect();
            }, 10000)
        }
    }

    isready() {
        return this.openpromise;
    }

    _send(data) {
        if (this.conn) {
            if (this.ready) {
                this.conn.send(data);
            } else {
                this.pending.push(data);
            }
        } else {
            this.pending.push(data);
            console.log("ws reconnect");
            this.connect();
        }
    }

    listen(ondata) {
        const id = Date.now().toString();
        this.channels[id] = {
            ondata: (type, data) => ondata(type, data),
            ondone: error => {
                if (error) {
                    console.log('workspace listen error: ', error);
                } else {
                    console.log('workspace listen done');
                }
                this.listen(ondata);
            }
        };
        const payload = JSON.stringify({type: 'start:resources', id: id});
        this._send(payload);
    }

    remoteCommand(cmd, repo, fullpath, ondata, ondone) {
        const id = Date.now().toString();
        this.channels[id] = {
            ondata: (type, data) => ondata(data),
            ondone: ondone
        };
        const payload = JSON.stringify({
            type: 'start:cmd',
            payload: {cmd: cmd, repo: repo, fullpath: fullpath},
            id: id
        });
        this._send(payload);
    }

    remotePty(ondata, ondone) {
        const id = Date.now().toString();
        this.channels[id] = {
            ondata: (type, data) => ondata(data),
            ondone: ondone
        };
        const payload = JSON.stringify({type: 'start:pty', payload: {}, id: id});
        this._send(payload);
        return {
            write: data => {
                const payload = JSON.stringify({type: 'pty:stdin', payload: data, id: id});
                this._send(payload)
            },
            resize: (rows, cols) => {
                const payload = JSON.stringify({type: 'pty:resize', payload: {rows: rows, cols: cols}, id: id});
                this._send(payload)
            },
            stop: () => {
                const payload = JSON.stringify({type: 'stop', id: id});
                this._send(payload)
            },
        };
    }

    static _quote(payload) {
        let result = '';
        for (let i = 0; i < payload.length; i++) {
            const c = payload[i];
            const code = c.charCodeAt(0);
            if ((code > 31) && (code < 127)) {
                result += c;
            } else if (c === '\a') {
                result += '\\a';
            } else if (c === '\b') {
                result += '\\b';
            } else if (c === '\f') {
                result += '\\f';
            } else if (c === '\t') {
                result += '\\t';
            } else if (c === '\r') {
                result += '\\r';
            } else if (c === '\n') {
                result += '\\n';
            } else if (c === '\v') {
                result += '\\v';
            } else if (c === '\\') {
                result += '\\\\';
            } else {
                if (code < 16) {
                    result += '\\x0' + code.toString(16)
                } else {
                    result += '\\x' + code.toString(16)
                }
            }

        }
        return result;
    }

    static _unquote(payload) {
        let result = '';
        let quote = false;
        let hex = '';
        for (let i = 0; i < payload.length; i++) {
            const c = payload[i];
            if (hex.length > 0) {
                hex += c;
                if (hex.length === 4) {
                    result += String.fromCharCode(parseInt(hex));
                    hex = '';
                }
            }
            else if (quote) {
                quote = false;
                if (c === 'r') {
                    result += '\r';
                } else if (c === 'n') {
                    result += '\n';
                } else if (c === 'a') {
                    result += '\a';
                } else if (c === 'b') {
                    result += '\b';
                } else if (c === 'f') {
                    result += '\f';
                } else if (c === 't') {
                    result += '\t';
                } else if (c === 'v') {
                    result += '\v';
                } else if (c === 'x') {
                    hex = '0x';
                } else if (c === '\\') {
                    result += c;
                } else {
                    result += '\\' + c;
                }
            } else if (c === '\\') {
                quote = true
            } else {
                result += c;
            }
        }
        return result;
    }

    remoteSer(tunnelid, onstatus, ondata, ondone) {
        const id = Date.now().toString();
        this.channels[id] = {
            ondata: (type, data) => {
                if (type === "ser:output") {
                    ondata(Websocket._unquote(data));
                } else if (type === "ser:status") {
                    onstatus(data);
                } else {
                    console.log('Unknown message type for serial: ' + type);
                }
            },
            ondone: ondone
        };
        const payload = JSON.stringify({type: 'start:ser', payload: {tunnelid: tunnelid}, id: id});
        this._send(payload);
        return {
            write: data => {
                const payload = JSON.stringify({type: 'ser:input', payload: Websocket._quote(data), id: id});
                this._send(payload)
            },
            stop: () => {
                const payload = JSON.stringify({type: 'stop', id: id});
                this._send(payload)
            },
            DTR: value => {
                if (Number.isInteger(value)) {
                    value = value.toString();
                }
                const payload = JSON.stringify({type: 'ser:dtr', id: id, payload: value});
                this._send(payload)
            },
            setBaudrate: baudrate => {
                const payload = JSON.stringify({type: 'ser:baud', id: id, payload: baudrate});
                this._send(payload)
            }
        };
    }

    remoteTTN(onstatus, onuplink, ondata, ondone) {
        const id = Date.now().toString();
        let getDevicesResponse;
        this.channels[id] = {
            ondata: (type, data) => {
                if (type === 'ttn:status') {
                    const device = {
                        dev_id: data.dev_id,
                        dev_eui: data.dev_eui,
                        app_eui: data.app_eui,
                        app_key: data.app_key
                    };
                    onstatus(data.connected, data.app_id, device, data.err_msg);
                } else if (type === 'ttn:devices') {
                    if (getDevicesResponse) {
                        getDevicesResponse(data);
                        getDevicesResponse = undefined;
                    }
                } else if (type === 'ttn:uplink') {
                    onuplink(data)
                } else {
                    ondata(type, data);
                }
            },
            ondone: ondone
        };
        const payload = JSON.stringify({type: 'start:ttn', payload: null, id: id});
        this._send(payload);
        return {
            connect: (app, key) => {
                const payload = JSON.stringify({type: 'ttn:connect', payload: {app_id: app, access_key: key}, id: id});
                this._send(payload);
            },
            getDevices: (limit, offset) => {
                const payload = JSON.stringify({type: 'ttn:devices', payload: {limit: limit, offset: offset}, id: id});
                this._send(payload);
                return new Promise(resolve => getDevicesResponse = resolve);
            },
            selectDevice: devID => {
                const payload = JSON.stringify({type: 'ttn:device', payload: devID, id: id});
                this._send(payload);
            },
            stop: () => {
                const payload = JSON.stringify({type: 'stop', id: id});
                this._send(payload);
            },
        };
    }

    remotePlugin(boardid, tunnelid, onstatus, ondata, ondone) {
        const id = Date.now().toString();
        this.channels[id] = {
            ondata: (type, data) => {
                if (type === "remote:status") {
                    onstatus(data);
                } else {
                    ondata(type, data);
                }
            },
            ondone: ondone
        };
        const payload = JSON.stringify({type: 'start:remote', payload: {boardid: boardid, tunnelid: tunnelid}, id: id});
        this._send(payload);
        return {
            start: () => {
                const payload = JSON.stringify({type: 'remote:start', payload: {}, id: id});
                this._send(payload);
            },
            stop: () => {
                const payload = JSON.stringify({type: 'remote:stop', payload: {}, id: id});
                this._send(payload);
            },
            close: () => {
                const payload = JSON.stringify({type: 'stop', id: id});
                this._send(payload);
            },
        }
    }

    connectResource(name, meta, ondata, ondone) {
        const id = Date.now().toString();
        const re = new RegExp('^' + name + ':');
        this.channels[id] = {
            ondata: (type, data) => ondata(type.replace(re, ''), data),
            ondone: ondone,
        };
        const payload = JSON.stringify({type: 'start:' + name, payload: meta, id: id});
        this._send(payload);
        return {
            send: (type, data) => {
                const payload = JSON.stringify({type: name + ":" + type, payload: data, id: id});
                this._send(payload);
            },
            close: () => {
                const payload = JSON.stringify({type: 'stop', id: id});
                this._send(payload);
            },
        }
    }
}
