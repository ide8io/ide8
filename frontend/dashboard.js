
import './dashboard.css';
import loading from './components/loading';

export default (selectedWorkspace, wslist, intents) => {
    if (wslist === undefined) {
        return loading("");
    }
    const frag = document.createDocumentFragment();
    for (const ws of wslist) {
        const item = document.createElement('div');
        item.classList.add('dashboard-item');
        if (ws === selectedWorkspace) {
            item.classList.add('selected');
        }
        item.onclick = () => intents.selectWorkspace(ws);
        item.oncontextmenu = e => {
            e.preventDefault();
            intents.contextmenu(ws.name, e);
        };
        if (ws.logo) {
            item.style.fontSize = '15px';
            item.style.lineHeight = '15px';
            const img = document.createElement('img');
            img.src = ws.logo;
            item.appendChild(img);
        }
        item.appendChild(document.createTextNode(ws.name));
        frag.appendChild(item);
    }
    return frag;
};
