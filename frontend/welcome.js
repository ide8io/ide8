"use strict";

import './welcome.css';

export default () => {
    return `<h1 class="welcome">Welcome to IDE8!</h1>`;
};
