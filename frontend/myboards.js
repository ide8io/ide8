
import './dashboard.css';

export default (myboards, wsname, intents) => {
    const frag = document.createDocumentFragment();
    for (const board of myboards) {
        if (board.workspace !== wsname) {
            continue;
        }
        const item = document.createElement('div');
        item.classList.add('dashboard-item');
        item.classList.add('selected');
        item.oncontextmenu = e => {
            e.preventDefault();
            intents.contextmenu(board.id, e);
        };
        if (board.logo) {
            item.style.fontSize = '15px';
            item.style.lineHeight = '15px';
            const img = document.createElement('img');
            img.src = board.logo;
            item.appendChild(img);
        }
        frag.appendChild(item);
    }
    return frag;
};
