"use strict";

import './pagenav.css';


export default function(count, limit, offset, setrange) {
    const root = document.createElement('span');
    root.classList.add('pagenav');
    const prev = document.createElement('span');
    root.appendChild(prev);
    prev.innerHTML = '<';
    if (offset > 0) {
        prev.classList.add('pagenav-active');
        prev.onclick = () => setrange(offset - 1, limit);
    } else {
        prev.classList.add('pagenav-inactive');
    }
    const range = document.createTextNode((offset + 1).toString() + '-' + (offset + count).toString());
    root.appendChild(range);
    const next = document.createElement('span');
    root.appendChild(next);
    next.innerHTML = '>';
    if (count >= limit) {
        next.classList.add('pagenav-active');
        next.onclick = () => setrange(offset + 1, limit);
    } else {
        next.classList.add('pagenav-inactive');
    }
    return root;
}
