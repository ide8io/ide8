"use strict";

import './loading.css';

export default function(message) {
    const root = document.createElement('div');
    root.className = 'loading';
    root.innerHTML = `
<div class="loading">
    <div>
        <svg width="100%" height="100%" viewBox="0 0 120 120" class="spinner">
            <circle cx="60" cy="60" r="50" />
            <circle cx="60" cy="60" r="50" class="bar" />
        </svg>
    </div>
    <div>${message}</div>
</div>`;
    return root;
}
