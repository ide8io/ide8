"use strict";

// Most of these are taken from: https://github.com/danklammer/bytesize-icons

export const LeftArrowIcon = `
<svg viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
    <path d="M10 6 L2 16 10 26 M2 16 L30 16" />
</svg>`;

export const RightArrowIcon = `
<svg viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
     <path d="M22 6 L30 16 22 26 M30 16 L2 16" />
</svg>`;

export const DBIcon = `
<svg viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
    <path d="M4 8 L4 23 C 4 31, 28 31, 28 23 L28 8 C 28 0, 4 0, 4 8 C 4 16, 28 16, 28 8 M28 13 C 28 21, 4 21, 4 13 M4 18 C 4 26, 28 26, 28 18" />
</svg>`;

export const ExternalIcon = `
<svg id="i-external" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
    <path d="M14 9 L3 9 3 29 23 29 23 18 M18 4 L28 4 28 14 M28 4 L14 18" />
</svg>`;

export const reloadicon = `
<svg id="i-reload" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
    <path d="M29 16 C29 22 24 29 16 29 8 29 3 22 3 16 3 10 8 3 16 3 21 3 25 6 27 9 M20 10 L27 9 28 2" />
</svg>`;
