"use strict";

export function StartPing(from) {
    (function ping(timeoutSeconds) {
        setTimeout(() => {
            fetch('/_ping', {credentials: 'include'})
                .then(response => {
                    if (!response.ok) {
                        console.log('ping not ok: ' + response.statusText);
                        if (response.status === 401) {
                            if (from) {
                                location.href = '/login?from=' + from;
                            } else {
                                location.href = '/login';
                            }
                        }
                    }
                    ping(60);
                })
                .catch(e => {
                    console.log('ping error: ' + e);
                    ping(60);
                })
        }, timeoutSeconds * 1000);
    })(10);
}

export function FormatBytes(a, b) {
    if (0 == a) {
        return "0 Bytes";
    }
    const c = 1024;
    const d = b || 2;
    const e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const f = Math.floor(Math.log(a) / Math.log(c));
    return parseFloat((a / Math.pow(c, f)).toFixed(d)) + " " + e[f];
}

export function mkel(tagname, attrs, parent) {
    const el = document.createElement(tagname);
    if (attrs) {
        for (const attr in attrs) {
            el[attr] = attrs[attr];
        }
    }
    if (parent) {
        parent.appendChild(el);
    }
    return el;
}

export function addel(parent, tagname, attrs) {
    const el = document.createElement(tagname);
    if (attrs) {
        for (const attr in attrs) {
            el[attr] = attrs[attr];
        }
    }
    if (parent) {
        parent.appendChild(el);
    }
    return el;
}

export function addtext(parent, text) {
    const node = document.createTextNode(text);
    if (parent) {
        parent.appendChild(node);
    }
    return node;
}

export function toHexString(byteArray, sep='') {
    return Array.from(byteArray, function(byte) {
        return ('0' + (byte & 0xFF).toString(16)).slice(-2);
    }).join(sep)
}
