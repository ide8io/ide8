"use strict";

import './starting.css';

export default (status, error) => {
    return `<div class="starting"><div>${status}</div><div>${error}</div></div>`;
};
