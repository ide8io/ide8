"use strict";

import './monitor.css';
import { StartPing, FormatBytes } from './utils';

const view = {};

view.stats = (model, intents) => {
    if (model.stats === undefined) {
        const text = document.createTextNode('loading...');
        return {content: text};
    }
    const frag = document.createDocumentFragment();
    (function iterProps(obj, parent) {
        for (const prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                const el = document.createElement('div');
                parent.appendChild(el);
                if (typeof(obj[prop]) === 'object') {
                    el.innerHTML = prop + ':';
                    iterProps(obj[prop], el);
                } else {
                    let val = obj[prop];
                    if ((prop === 'total_disk_usage') || (prop === 'alloc')) {
                        val = FormatBytes(val);
                    }
                    el.innerHTML = prop + ': ' + val;
                }
            }
        }
    })(model.stats, frag);
    return {content: frag};
};

view.display = repr => {
    for (const r in repr) {
        if (!repr.hasOwnProperty(r)) {
            continue;
        }
        document.getElementById(r).innerHTML = '';
        if (repr[r] !== undefined) {
            document.getElementById(r).appendChild(repr[r]);
        }
    }
};

const state = {};

state.representation = (model, intents) => {
    let repr = {};
    repr = Object.assign({}, repr, view.stats(model, intents));
    view.display(repr);
};

state.nextAction = (model, intents) => {
};

state.render = (model, intents) => {
    state.representation(model, intents);
    state.nextAction(model, intents);
};

const model = {};

model.present = (data, intents) => {
    console.log("present: " + JSON.stringify(data));
    for (const id in data) {
        if (!data.hasOwnProperty(id)) {
            continue;
        }

        const payload = data[id];
        if (id === "stats") {
            model.stats = payload;
        }
    }
    state.render(model, intents);
};


function get(url) {
    return fetch(url, {credentials: 'include'})
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('GET ' + url + ' failed with: ' + response.status + ' ' + response.statusText);
        })
}

const actions = {};

actions.updateStats = () => {
    get('stats').then(stats => {
        model.present({stats: stats}, actions);
    });
};

const margin = {top: 20, right: 150, bottom: 30, left: 30},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

// set the ranges
const x = d3.scaleTime().range([0, width]);
const y = d3.scaleLinear().range([height, 0]);

const valueline = d3.line().x(d => x(d.time)).y(d => y(d.users.total));
const valueline2 = d3.line().x(d => x(d.time)).y(d => y(d.users.active));

actions.updateUserslog = () => {
    get('stats/users').then(userslog => {
        console.log("userslog: " + JSON.stringify(userslog));
        const svg = d3.select(document.getElementById('graph')).append('svg')
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
            .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        userslog.forEach(d => {
            d.time = d3.isoParse(d.time);
            d.users.total = +d.users.total;
        });
        const lastitem = userslog[userslog.length - 1];
        userslog.push({time: new Date(), users: {total: lastitem.users.total, active: lastitem.users.active}});
        console.log("data: " + JSON.stringify(userslog));
        x.domain(d3.extent(userslog, d => d.time));
        y.domain([0, d3.max(userslog, d => d.users.total)]);
        svg.append("path")
            .data([userslog])
            .attr("class", "line")
            .attr("d", valueline);
        svg.append("path")
            .data([userslog])
            .attr("class", "line")
            .style("stroke", "red")
            .attr("d", valueline2);
        // Add the X Axis
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

        // Add the Y Axis
        svg.append("g")
            .call(d3.axisLeft(y));
        svg.append("text")
            .attr("transform", "translate("+(width+3)+","+y(userslog[userslog.length - 1].users.total)+")")
            .attr("dy", ".35em")
            .attr("text-anchor", "start")
            .text("Registered Users: " + userslog[userslog.length - 1].users.total);
        svg.append("text")
            .attr("transform", "translate("+(width+3)+","+y(userslog[userslog.length - 1].users.active)+")")
            .attr("dy", ".35em")
            .attr("text-anchor", "start")
            .text("Active Users: " + userslog[userslog.length - 1].users.active);
    });
};

class Monitor {
    constructor(actions) {
        this.root = document.createDocumentFragment();
        const content = document.createElement('div');
        this.root.appendChild(content);
        content.id = 'content';
        const graph = document.createElement('div');
        this.root.appendChild(graph);
        graph.id = 'graph';
        actions.updateStats();
        actions.updateUserslog();
    }
}

const admin = new Monitor(actions);

document.body.appendChild(admin.root);

StartPing('monitor');
