import './boards.css';

export default (boards, intents) => {
    const div = document.createElement('div');
    div.classList.add('boards');
    div.onclick = () => intents.close();
    div.oncontextmenu = () => intents.close();
    const closeShadow = document.createElement('div');
    closeShadow.classList.add('boards-close');
    closeShadow.classList.add('boards-close-shadow');
    div.appendChild(closeShadow);
    let count = 0;
    for (const board of boards) {
        const item = document.createElement('div');
        item.classList.add('boards-item');
        item.style.bottom = Math.floor(count / 5) * 170 + 'px';
        item.style.left = 55 + (count % 5) * 205 + 'px';
        const logo = document.createElement('img');
        logo.classList.add('boards-item-logo');
        logo.src = board.logo;
        item.appendChild(logo);
        const title = document.createElement('div');
        title.classList.add('boards-item-title');
        title.innerHTML = board.name;
        item.appendChild(title);
        const info = document.createElement('div');
        info.classList.add('boards-item-info');
        info.innerHTML = board.info;
        item.appendChild(info);
        if (board.status === 'free') {
            const claim = document.createElement('div');
            claim.classList.add('boards-item-select');
            claim.classList.add('boards-item-select-free');
            claim.innerHTML = 'Claim';
            claim.onclick = () => intents.claim(board.id, board.class);
            item.appendChild(claim);
        } else if (board.status === 'mine') {
            const release = document.createElement('div');
            release.classList.add('boards-item-select');
            release.classList.add('boards-item-select-mine');
            release.innerHTML = 'Release';
            release.onclick = () => intents.release(board.id);
            item.appendChild(release);
            const owner = document.createElement('div');
            item.appendChild(owner);
            owner.classList.add('boards-item-owner');
            owner.innerHTML = 'Claimed by workspace: ' + board.workspace;
        } else if (board.status === 'other') {
            const other = document.createElement('div');
            other.classList.add('boards-item-select');
            other.classList.add('boards-item-select-other');
            other.innerHTML = 'Occupied';
            item.appendChild(other);
            const owner = document.createElement('div');
            item.appendChild(owner);
            owner.classList.add('boards-item-owner');
            owner.innerHTML = 'Claimed by user: ' + board.user;
        }
        div.appendChild(item);
        count++;
    }
    const close = document.createElement('div');
    close.classList.add('boards-close');
    close.innerHTML = 'X';
    close.onclick = () => intents.close();
    div.appendChild(close);
    return div;
};
