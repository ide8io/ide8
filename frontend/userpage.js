"use strict";

import './userpage.css';
import './menu.css';
import contextmenu from './contextmenu';
import dashboard from './dashboard';
import stacks from './stacks';
import boards from './boards';
import myboards from './myboards';
import { Ide } from './ide/ide';
import welcome from './welcome';
import starting from './starting';
import * as workspace from './comms/workspace';
import * as instruments from './comms/instruments';
import ide8png from './ide8.png';
import { StartPing } from './utils';
import loading from './components/loading';

console.log('ide8 version: ' + VERSION);

const username = window.location.pathname.split('/').pop();

document.body.innerHTML =
    `<div id="menu" class="menu">
         <div class="menu-icon-frame"><img class="menu-icon" src="${ide8png}" /></div>
         <div id="ide8" class="menu-title">IDE8<span class="menu-dropdown">&#9660;</span></div>
         <div id="status" class="menu-status"></div>
         <div class="menu-right">
             <a id="download" target="_blank" href="/download/agent">
                 <svg id="i-download" viewBox="0 0 32 32" width="25" height="25" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                     <path d="M9 22 C0 23 1 12 9 13 6 2 23 2 22 10 32 7 32 23 23 22 M11 26 L16 30 21 26 M16 16 L16 30" />
                 </svg>
                 <span>Download Agent</span>
             </a>
             <span id="user" class="menu-user">
                 <span id="username" class="menu-username">${username}</span>
                 <svg id="i-user" viewBox="0 0 32 32" width="25" height="25" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                     <path d="M22 11 C22 16 19 20 16 20 13 20 10 16 10 11 10 6 12 3 16 3 20 3 22 6 22 11 Z M4 30 L28 30 C28 21 22 20 16 20 10 20 4 21 4 30 Z" />
                 </svg>
             </span>
         </div>
     </div>
     <div id="leftbar">
        <div id="add-workspace">+</div>
        <div id="dashboard" class="dashboard"></div>
        <div id="myboards" class="dashboard"></div>
        <div id="board">
            <svg viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                <path d="M13 2 L13 6 11 7 8 4 4 8 7 11 6 13 2 13 2 19 6 19 7 21 4 24 8 28 11 25 13 26 13 30 19 30 19 26 21 25 24 28 28 24 25 21 26 19 30 19 30 13 26 13 25 11 28 8 24 4 21 7 19 6 19 2 Z" />
                <circle cx="16" cy="16" r="4" />
            </svg>
        </div>
        <div id="stacks"></div>
        <div id="boards"></div>
     </div>
     <div id="content"></div>
     <div id="contextmenu"></div>`;


const view = {};

view.base = (model, intents) => {
    return {dashboard: dashboard(model.selectedWorkspace, model.wslist, intents)};
};

view.status = model => {
    return {status: model.status};
};

view.contextmenu = model => {
    return {contextmenu: contextmenu(model.contextmenu.posx, model.contextmenu.posy, model.contextmenu.items)};
};

view.stacks = model => {
    if (model.stacks) {
        return {stacks: stacks(model.stacks, intents.stacks)};
    } else {
        return {stacks: ''}
    }
};

view.boards = model => {
    if (model.boards) {
        return {boards: boards(model.boards, intents.boards)};
    } else {
        return {boards: ''}
    }
};

view.myboards = model => {
    if (model.selectedWorkspace) {
        return {myboards: myboards(model.myboards, model.selectedWorkspace.name, intents.myboards)};
    } else {
        return {myboards: ''}
    }
};

view.welcome = () => {
    return {content: welcome()};
};

view.starting = model => {
    console.log('render starting: ' + JSON.stringify(model.selectedWorkspace.starting));
    return {content: starting(model.selectedWorkspace.starting.status, model.selectedWorkspace.starting.error)};
};

view.plugin = model => {
    return {content: model.selectedWorkspace.plugin.root};
};

view.display = (repr) => {
    for (const r in repr) {
        if (!repr.hasOwnProperty(r)) {
            continue;
        }
        if (repr[r].nodeType) {
            document.getElementById(r).innerHTML = '';
            document.getElementById(r).appendChild(repr[r]);
        } else {
            document.getElementById(r).innerHTML = repr[r];
        }
    }
};


const state = {};

state.representation = (model) => {
    let repr = view.base(model, intents);
    if (state.isStatus(model)) {
        repr = Object.assign({}, repr, view.status(model));
    }

    if (model.updateStacks) {
        repr = Object.assign({}, repr, view.stacks(model));
    }

    if (model.updateBoards) {
        repr = Object.assign({}, repr, view.boards(model));
    }

    if (model.updateMyBoards) {
        repr = Object.assign({}, repr, view.myboards(model));
    }

    if (state.isContextmenu(model)) {
        repr = Object.assign({}, repr, view.contextmenu(model));
    } else {
        repr['contextmenu'] = '';
    }

    if (model.updateContent) {
        if (!model.selectedWorkspace) {
            repr = Object.assign({}, repr, view.welcome());
        } else if (model.selectedWorkspace.plugin) {
            repr = Object.assign({}, repr, view.plugin(model));
        } else if (model.selectedWorkspace.starting) {
            repr = Object.assign({}, repr, view.starting(model));
        } else {
            repr['content'] = loading('');
        }
    }

    view.display(repr);
};

state.isStatus = model => model.status !== undefined;
state.isStatusTimeout = model => model.timeoutSeconds !== undefined;
state.isContextmenu = model => model.contextmenu.items.length > 0;

state.nextAction = (model) => {
    if (model.selectedWorkspace && model.selectedWorkspace !== model.lastSelectedWorkspace) {
        if (model.selectedWorkspace.plugin) {
            model.selectedWorkspace.plugin.init();
        }
    }

    if (state.isStatusTimeout(model)) {
        setTimeout(() => {model.present({status: {message: ''}})}, model.timeoutSeconds * 1000);
    }
};

state.render = (model) => {
    state.representation(model);
    state.nextAction(model);
};

const model = {
    status: undefined,
    timeoutSeconds: undefined,
    contextmenu: {items: []},
    selectedWorkspace: undefined,
    lastSelectedWorkspace: undefined,
    myboards: [],
};

model.present = (data) => {
    // Reset these to use them as one-off events
    model.status = undefined;
    model.timeoutSeconds = undefined;
    model.updateStacks = false;
    model.updateBoards = false;
    model.updateMyBoards = false;
    model.updateContent = false;

    // These are used to detect state changes
    model.lastSelectedWorkspace = model.selectedWorkspace;

    for (const id in data) {
        if (!data.hasOwnProperty(id)) {
            continue;
        }

        const payload = data[id];

        if (id === 'status') {
            model.status = payload.message;
            if (payload.hasOwnProperty('timeoutSeconds')) {
                model.timeoutSeconds = payload.timeoutSeconds;
            } else {
                model.timeoutSeconds = undefined;
            }
        } else if (id === 'contextmenu') {
            if (payload !== undefined) {
                model.contextmenu = payload;
            } else {
                model.contextmenu.items = [];
            }
        } else if (id === 'stacks') {
            model.stacks = payload;
            model.updateStacks = true;
        } else if (id === 'wslist') {
            model.wslist = payload;
        } else if (id === 'createWorkspace') {
            model.selectedWorkspace = payload;
            model.wslist.push(model.selectedWorkspace);
            model.updateContent = true;
        } else if (id === 'deleteWorkspace') {
            for (const index in model.wslist) {
                if (model.wslist.hasOwnProperty(index)) {
                    const ws = model.wslist[index];
                    if (ws.name === payload) {
                        model.wslist.splice(index, 1);
                        if (model.selectedWorkspace && (model.selectedWorkspace.name === payload)) {
                            model.selectedWorkspace = undefined;
                            model.updateContent = true;
                        }
                        break;
                    }
                }
            }
        } else if (id === 'selectWorkspace') {
            for (const ws of model.wslist) {
                if (ws.name === payload.name) {
                    model.selectedWorkspace = ws;
                    model.updateContent = true;
                    model.updateMyBoards = true;
                    break;
                }
            }
        } else if (id === 'setStarting') {
            const found = model.wslist.find(ws => ws.name === payload.name);
            if (found) {
                found.starting = {status: payload.status, error: payload.error};
                model.updateContent = true;
            }
        } else if (id === 'setPlugin') {
            const found = model.wslist.find(ws => ws.name === payload.name);
            if (found) {
                found.plugin = payload.plugin;
                model.updateContent = true;
                model.lastSelectedWorkspace = undefined;
            }
        } else if (id === 'boards') {
            model.boards = payload;
            model.updateBoards = true;
        } else if (id === 'myboards') {
            model.myboards = payload;
            model.updateMyBoards = true;
        } else {
            console.log('userpage: Unknown model present id: ' + id);
        }
    }
    state.render(model);
};


const actions = {};

actions.services = {
    menu: {
        on: (posx, posy, menu) => model.present({contextmenu: {posx: posx, posy: posy, items: menu}}),
        off: () => model.present({contextmenu: undefined}),
    },
    status: (message, timeoutSeconds) => model.present({status: {message: message, timeoutSeconds: timeoutSeconds}}),
};

actions.selectStack = name => {
    actions.closeStacks();
    const wsname = prompt('Enter new workspace name:');
    if (wsname) {
        actions.createWorkspace({name: wsname, stack: name});
    }
};

actions.closeStacks = () => {
    model.present({stacks: undefined});
};

actions.updateBoards = () => {
    return instruments.fetchDebuggerList(model.selectedWorkspace ? model.selectedWorkspace.board : undefined)
        .then(debuggers => {
            const boards = [];
            for (const board of debuggers) {
                if (board.status === 'mine') {
                    boards.push(board);
                }
            }
            const pr = {myboards: boards};
            if (model.selectedWorkspace) {
                pr.boards = debuggers;
            }
            model.present(pr);
        });
};

actions.claimBoard = (id, cls) => {
    console.log('Claim board id: ' + id);
    if (model.selectedWorkspace) {
        instruments.claimBoard(id, model.selectedWorkspace.name)
            .then(() => {
                if (cls === 'Arduino' && model.selectedWorkspace.plugin) {
                    model.selectedWorkspace.plugin.assignBoard(id);
                }
                actions.updateBoards();
            })
            .catch (error => {
                console.log("Failed to claim board: " + error);
                model.present({status: {message: "Failed to claim board!", timeoutSeconds: 10}})
            });
    }
};

actions.releaseBoard = async id => {
    console.log('Release board id: ' + id);
    await instruments.releaseBoard(id);
    actions.updateBoards();
};

actions.closeBoards = () => {
    model.present({boards: undefined});
};

actions.createWorkspace = (data) => {
    workspace.createWorkspace(data.name, data.stack)
        .then(() => {
            workspace.getStack(data.stack)
                .then(stack => {
                    const ws = {name: data.name, stack: data.stack, logo: stack.logo, board: stack.boards[0]};
                    model.present({
                        createWorkspace: ws,
                        status: {message: "Workspace created successfully", timeoutSeconds: 3}
                    });
                    actions.selectWorkspace(ws)
                });
        })
        .catch (error => {
            console.log("Failed to create workspace: " + error);
            model.present({status: {message: "Failed to create workspace!", timeoutSeconds: 10}})
        });
    model.present({status: {message: "Creating workspace...", timeoutSeconds: 10}})
};

actions.deleteWorkspace = name => {
    if (!confirm('Are you sure you want to delete workspace ' + name + '?')) {
        return;
    }
    const found = model.wslist.find(ws => ws.name === name);
    workspace.deleteWorkspace(name)
        .then(() => {
            model.present({
                deleteWorkspace: name,
                status: {message: "Workspace deleted successfully", timeoutSeconds: 3}
            });
            if (found && found.plugin) {
                found.plugin.destroy();
                delete found.plugin;
            }
            actions.updateBoards();
        })
        .catch (error => {
            console.log("Failed to delete workspace: " + error);
            model.present({status: {message: "Failed to delete workspace!", timeoutSeconds: 10}})
        });
    model.present({status: {message: "Deleting workspace...", timeoutSeconds: 10}})
};

actions.startWorkspace = async (name, running) => {
    const response = await workspace.startWorkspace(name, running);
    console.log('start response: ' + JSON.stringify(response));
};

actions.selectWorkspace = ws => {
    if (!ws.plugin) {
        workspace.startWorkspace(ws.name, true)
            .then(response => {
                if (response.ok) {
                    const plugin = new Ide(ws.name, ws.stack, actions.services);
                    model.present({setPlugin: {name: ws.name, plugin: plugin}});
                } else {
                    console.log('Failed to start workspace: ' + response.text);
                    model.present({setStarting: {name: ws.name, status: 'error', error: response.text}});
                }
            });
        model.present({setStarting: {status: 'pending'}});
    }
    model.present({selectWorkspace: {name: ws.name}});
};

actions.contextmenu = (name, event) => {
    model.present({contextmenu: {posx: event.clientX, posy: event.clientY, items: [
        {name: 'Delete workspace', intent: () => actions.deleteWorkspace(name)},
        {name: 'Start workspace', intent: () => actions.startWorkspace(name, true)},
        {name: 'Stop workspace', intent: () => actions.startWorkspace(name, false)},
    ]}});
};

const intents = {
    stacks: {close: actions.closeStacks, select: actions.selectStack},
    boards: {close: actions.closeBoards, claim: actions.claimBoard, release: actions.releaseBoard},
    selectWorkspace: actions.selectWorkspace,
    contextmenu: actions.contextmenu,
};

window.addEventListener('click', () => {
    model.present({contextmenu: undefined});
});

window.addEventListener('resize', () => {
    if (model.selectedWorkspace && model.selectedWorkspace.plugin) {
        model.selectedWorkspace.plugin.resize();
    }
});

document.getElementById('add-workspace').onclick = e => {
    workspace.fetchStackList()
        .then(stacks => {
            model.present({stacks: stacks});
        });
};

document.getElementById('board').onclick = e => {
    if (model.selectedWorkspace) {
        actions.updateBoards();
    }
};

function ide8menu(e) {
    e.preventDefault();
    const pos = e.target.getBoundingClientRect();
    model.present({contextmenu: {posx: pos.left, posy: pos.top + pos.height, items: [
        {name: 'Welcome site', newtab: '/'},
        {name: 'Open forum site', newtab: 'http://forum.ide8.io'},
        {name: '--'},
        {name: 'Version: ' + VERSION},
    ]}});
}

const ide8el = document.getElementById('ide8');
ide8el.oncontextmenu = ide8menu;
ide8el.onclick = e => {e.stopPropagation(); ide8menu(e)};

const userel = document.getElementById('user');
userel.onclick = e => {
    e.preventDefault();
    e.stopPropagation();
    const pos = userel.getBoundingClientRect();
    model.present({contextmenu: {posx: pos.left, posy: pos.top + pos.height, items: [
        {name: 'Logout', intent: () => location.href = '/logout'},
    ]}});
};

// Display initial state
state.render(model);

workspace.fetchWorkspaceList()
    .then(result => {
        model.present({wslist: result});
        instruments.fetchMyBoards()
            .then(boards => {
                model.present({myboards: boards});
            })
    })
    .catch (error => {
        console.log('fetch failed: ', error);
    });

StartPing();
