
import './contextmenu.css';

export default (posx, posy, items) => {
    const div = document.createElement('div');
    div.classList.add('contextmenu');
    div.style.top = posy + 'px';
    div.style.left = posx + 'px';
    for (const item of items) {
        if (item.name === '--') {
            div.appendChild(document.createElement('hr'));
        } else {
            const el = document.createElement('div');
            div.appendChild(el);
            el.classList.add('contextmenu-item');
            if (item.newtab) {
                const a = document.createElement('a');
                el.appendChild(a);
                a.href = item.newtab;
                a.target = '_blank';
                a.innerHTML = item.name;
            } else {
                if (item.intent) {
                    el.onclick = item.intent;
                } else {
                    el.classList.add('contextmenu-item-disable');
                }
                el.innerHTML = item.name;
            }
        }
    }
    // Force menu to inside window once attached to DOM
    setTimeout(() => {
        const pos = div.getBoundingClientRect();
        if (pos.right > window.innerWidth) {
            div.style.left = null;
            div.style.right = '0';
        }
        if (pos.bottom > window.innerHeight) {
            div.style.top = null;
            div.style.bottom = '0';
        }
    }, 0);
    return div;
};
