import adapter from 'webrtc-adapter';
import Janus from '../external/janus.es';
import {mkel} from "./utils";

const hdr = document.getElementById('hdr');
const root = document.getElementById('root');

Janus.init({
    debug: true,
    dependencies: Janus.useDefaultDependencies({adapter: adapter}),
});

const ons = janus => {
    janus.attach({
        plugin: "janus.plugin.streaming",
        opaqueId: "streamingtest-" + Janus.randomString(12),
        success: onsuccess.bind(this),
        error: error => {
            console.log('attach error: ' + error);
        },
        onmessage: onmessage.bind(this),
        onremotestream: onremotestream.bind(this),
        oncleanup: () => {
        }
    });


};

const janus = new Janus({
    server: 'https://stream.ide8.io/janus',
    success: () => ons(janus),
    error: cause => {
        console.log('janus error: ', cause);
        throw cause;
    }
});

let streaming;

let count = 0;
let streams = [];

const start = () => {
    if (count === 0) {
        streaming.send({
            "message": {"request": "list"}, success: result => {
                if (result === null || result === undefined) {
                    console.log('janus stream list failed!');
                    return;
                }
                const list = result['list'];
                if (list !== null && list !== undefined) {
                    console.log('stream list: ' + JSON.stringify(list));
                    list.sort((a, b) => a.id - b.id);
                    streams = [];
                    for (const item of list) {
                        if (parseInt(item.video_age_ms) < 1000) {
                            streams.push(item);
                        }
                    }
                    watch();
                }
            }
        });
    }
    watch();
};

let watchtimer;
let first = true;
const watch = () => {
    if (count >= streams.length) {
        count = 0;
    }
    hdr.innerHTML = streams[count].description;
    console.log("watch stream: " + streams[count].description);
    if (first) {
        first = false;
        streaming.send({"message": {"request": "watch", id: streams[count].id}});
    } else {
        streaming.send({"message": {"request": "switch", id: streams[count].id}});
    }
    count++;
    watchtimer = setTimeout(() => {
        watchtimer = null;
        if (streaming) {
            //streaming.send({"message": {"request": "stop"}});
            //streaming.hangup();
            start();
        }
    }, 30000);
};

const onsuccess = pluginHandle => {
    console.log("onsuccess");
    streaming = pluginHandle;
    start();
};

const onmessage = (msg, jsep) => {
    console.log('onmessage: ' + JSON.stringify(msg));
    const result = msg["result"];
    if (result !== null && result !== undefined) {
        if (result["status"] !== undefined && result["status"] !== null) {
            const status = result["status"];
            if (status === 'stopped') {
                streaming.send({"message": {"request": "stop"}});
                streaming.hangup();
            }
        }
    } else if (msg["error"] !== undefined && msg["error"] !== null) {
        streaming.send({"message": {"request": "stop"}});
        streaming.hangup();
        streaming.detach();
        streaming = null;
        if (watchtimer) {
            clearTimeout(watchtimer);
            watchtimer = null;
        }
        ons(janus);
        return;
    }
    if (jsep !== undefined && jsep !== null) {
        streaming.createAnswer({
            jsep: jsep,
            media: {audioSend: false, videoSend: false},
            success: jsep => {
                streaming.send({"message": {"request": "start"}, "jsep": jsep});
            },
            error: error => {
                console.log('createAnswer error: ' + error);
            }
        });
    }
};

const onremotestream = stream => {
    console.log("onremotestream");
    root.innerHTML = '';
    const video = mkel('video', {width: 640, height: 480, autoplay: true, controls: true});
    root.appendChild(video);
    Janus.attachMediaStream(video, stream);
};
